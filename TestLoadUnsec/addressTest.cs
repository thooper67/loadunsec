﻿using System.Text.RegularExpressions;
using LoadUnsec;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestLoadUnsec
{
    
    
    /// <summary>
    ///This is a test class for addressTest and is intended
    ///to contain all addressTest Unit Tests
    ///</summary>
    [TestClass()]
    public class addressTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for address Constructor with a plain vanilla address
        ///</summary>
        [TestMethod()]
        public void addressCase1Test()
        {
            string inputLine1 = "123 MAIN ST";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^123$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^$"), "Direction should be empty.");
            StringAssert.Matches(target.street, new Regex("^MAIN$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^ST$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }

        /// <summary>
        ///A test for address Constructor with an address without a suffix
        ///</summary>
        [TestMethod()]
        public void addressCase2Test()
        {
            string inputLine1 = "123 MAIN";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^123$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^$"), "Direction should be empty.");
            StringAssert.Matches(target.street, new Regex("^MAIN$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }

        /// <summary>
        ///A test for address Constructor with an address with direction and suffix
        ///</summary>
        [TestMethod()]
        public void addressCase3Test()
        {
            string inputLine1 = "123 N MAIN ST";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^123$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^N$"), "Direction parsing failed.");
            StringAssert.Matches(target.street, new Regex("^MAIN$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^ST$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }

        /// <summary>
        ///A test for address Constructor with an address with direction and no suffix
        ///</summary>
        [TestMethod()]
        public void addressCase4Test()
        {
            string inputLine1 = "123 N MAIN";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^123$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^N$"), "Direction parsing failed.");
            StringAssert.Matches(target.street, new Regex("^MAIN$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }

        /// <summary>
        ///A test for address Constructor with an address with a two-word street name
        ///</summary>
        [TestMethod()]
        public void addressCase5Test()
        {
            string inputLine1 = "123 OAK AVENUE PKWY";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^123$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^$"), "Direction parsing failed.");
            StringAssert.Matches(target.street, new Regex("^OAK AVENUE$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^PKWY$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }

        /// <summary>
        ///A test for address Constructor with an address with a direction and two-word street name
        ///</summary>
        [TestMethod()]
        public void addressCase6Test()
        {
            string inputLine1 = "123 N OAK AVENUE PKWY";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^123$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^N$"), "Direction parsing failed.");
            StringAssert.Matches(target.street, new Regex("^OAK AVENUE$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^PKWY$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }

        /// <summary>
        ///A test for address Constructor with a plain vanilla address with suite
        ///</summary>
        [TestMethod()]
        public void addressCaseXTest()
        {
            string inputLine1 = "165 LENON LANE, SUITE 200";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^165$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^$"), "Direction should be empty.");
            StringAssert.Matches(target.street, new Regex("^LENON$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^LN$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.unit_no, new Regex("^STE 200$"), "Suite parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }
        
        /// <summary>
        ///A test for address Constructor with an address with a direction after the suffix and a suite number
        ///</summary>
        [TestMethod()]
        public void addressCaseX2Test()
        {
            string inputLine1 = "400 GALLERIA PKWY SE STE 1500";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^400$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^SE$"), "Direction should be empty.");
            StringAssert.Matches(target.street, new Regex("^GALLERIA$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^PKWY$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.unit_no, new Regex("^STE 1500$"), "Suite parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }

        /// <summary>
        ///A test for address Constructor with an address with # sign for unit number
        ///</summary>
        [TestMethod()]
        public void addressCaseX3Test()
        {
            string inputLine1 = "123 MAIN ST #101";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^123$"), "Street number parsing failed.");
            StringAssert.Matches(target.dir, new Regex("^$"), "Direction should be empty.");
            StringAssert.Matches(target.street, new Regex("^MAIN$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^ST$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.unit_no, new Regex("^101$"), "Unit parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }


        /// <summary>
        ///A test for address Constructor with a PO BOX address
        ///</summary>
        [TestMethod()]
        public void addressPOBoxCase1Test()
        {
            string inputLine1 = "P.O. BOX 123";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^$"), "Street number parsing failed.");
            StringAssert.Matches(target.street, new Regex("^PO BOX 123$"), "Street name parsing failed.");
            StringAssert.Matches(target.suff, new Regex("^$"), "Street suffix parsing failed.");
            StringAssert.Matches(target.city, new Regex("^ANYTOWN$"), "City parsing failed.");
            StringAssert.Matches(target.st, new Regex("^CA$"), "State parsing failed.");
            StringAssert.Matches(target.zip, new Regex("^12345$"), "Zip parsing failed.");
            StringAssert.Matches(target.zip4, new Regex("^6789$"), "Zip plus 4 parsing failed.");
        }

        /// <summary>
        ///
        ///</summary>
        [TestMethod()]
        [ExpectedException(typeof(Exception),
            "Unaccounted for str_sub: 123/456/456")]
        public void addressStr_Sub_Test1()
        {
            string inputLine1 = "123/456/456 MAIN ST";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
        }

        /// <summary>
        ///
        ///</summary>
        [TestMethod()]
        public void addressStr_Sub_Test2()
        {
            string inputLine1 = "123-125 MAIN ST";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^123-125$"), "Street number parsing failed.");
            StringAssert.Matches(target.str_sub, new Regex("^$"), "Street sub parsing failed.");
        }

        /// <summary>
        ///
        ///</summary>
        [TestMethod()]
        public void addressStr_Sub_Test3()
        {
            string inputLine1 = "434A MAIN ST";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^434$"), "Street number parsing failed.");
            StringAssert.Matches(target.str_sub, new Regex("^A$"), "Street sub parsing failed.");
        }

        /// <summary>
        ///
        ///</summary>
        [TestMethod()]
        public void addressStr_Sub_Test4()
        {
            string inputLine1 = "453/2 MAIN ST";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^453$"), "Street number parsing failed.");
            StringAssert.Matches(target.str_sub, new Regex("^2$"), "Street sub parsing failed.");
        }

        /// <summary>
        ///
        ///</summary>
        [TestMethod()]
        public void addressStr_Sub_Test5()
        {
            string inputLine1 = "453 1/2 MAIN ST";
            string inputLine2 = "ANYTOWN CA 12345-6789";
            address target = new address(inputLine1, inputLine2);
            StringAssert.Matches(target.strnum, new Regex("^453$"), "Street number parsing failed. '" + target.strnum + "'");
            StringAssert.Matches(target.str_sub, new Regex("^1/2$"), "Street sub parsing failed. '" + target.str_sub + "'");
        }

    }
}
