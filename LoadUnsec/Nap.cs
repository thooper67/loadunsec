﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Nap : MegabyteTax
    {
        public Nap()
        {
            co3 = "NAP";
            coNum = "28";
            populateStdTypes();      
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
            stdTypes.Add("980", "500");
            stdTypes.Add("985", "500");
            stdTypes.Add("990", "500");
            stdTypes.Add("991", "500");
            stdTypes.Add("995", "500");
            stdTypes.Add("996", "500");
        }
    }
}
