﻿using System;
using System.Linq;

namespace LoadUnsec
{
    public class fixfield
    {
        public string Value = "";
        public string Name = "";
        public string Format = "";
        public int Length;
        public int Offset;
        public byte Index;

        public fixfield(byte index, string name)
        {
            Name = name;
            Index = 0;
        }
        public fixfield(int length, int offset)
        {
            Length = length;
            Offset = offset;
        }
        public fixfield(int length, int offset, string name)
        {
            Name = name;
            Length = length;
            Offset = offset;
        }
        public fixfield(int length, int offset, string name, string format)
        {
            Name = name;
            Length = length;
            Offset = offset;
            Format = format;
        }
    }
}
