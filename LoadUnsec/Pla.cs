﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Pla : County
    {
        public Pla()
        {
            co3 = "PLA";
            coNum = "31";
            populateStdTypes();
            populateStdCity();
        }

        #region Propertes
        // Field names and positions from AsmtMaster header
        //  0 Asmt
        //  1 FeeParcel
        //  2 TRA
        //  3 AsmtDescription
        //  4 Zoning1
        //  5 LandUse1
        //  6 NeighborhoodCode
        //  7 Acres
        //  8 CurrentDocNum
        //  9 CurrentDocDate
        // 10 TaxabilityFull
        // 11 AssesseeName
        // 12 InCareOf
        // 13 DBA
        // 14 AddressStreet
        // 15 AddressCity
        // 16 AddressState
        // 17 AddressZip
        // 18 Street
        // 19 StreetNum
        // 20 StreetType
        // 21 StreetDirection
        // 22 SpaceApt
        // 23 Community
        // 24 Zip
        // 25 AsmtStatus
        // 26 KilledDate
        // 27 Inactive

        // Field names and positions from CertifiedValues header
        // 0 Asmt
        // 1 Land
        // 2 Structure
        // 3 Growing
        // 4 Fixtures
        // 5 PP
        // 6 NetValue
        // 7 Hox
        // 8 OtherExemption

        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            int iValueCount = 0;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;
            CsvReader csv2 = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true, '|');
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }
            string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
            if (File.Exists(srcFile2))
            {
                try
                {
                    csv2 = new CsvReader(new StreamReader(srcFile2), true, '|');
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile2 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile2 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Get both files in their own DataTables
                DataTable dtRoll = getTable(csv1);
                DataTable dtValues = getTable(csv2);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        // Keep reading value records until we find a match, or realize there isn't a match.
                        bool bValMatch = false;
                        string sRollAsmt = drRoll["Asmt"].ToString();
                        string sValueAsmt = dtValues.Rows[iValueCount]["Asmt"].ToString();
                        while ((iValueCount < dtValues.Rows.Count - 1) && (sRollAsmt.CompareTo(sValueAsmt) > 0))
                        {
                            iValueCount++;
                            sValueAsmt = dtValues.Rows[iValueCount]["Asmt"].ToString();
                        }
                        if (sRollAsmt.CompareTo(sValueAsmt) == 0) bValMatch = true;

                        thisRec.APN_S = drRoll["Asmt"].ToString();
                        thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                        thisRec.FEE_PARCEL_S = drRoll["FeeParcel"].ToString();
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = drRoll["TRA"].ToString();
                        thisRec.OWNER1 = cleanLine(drRoll["AssesseeName"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;
                        thisRec.EXE_CD = "";
                        thisRec.LEGAL = cleanLine(drRoll["AsmtDescription"].ToString());
                        thisRec.TYPE = drRoll["Asmt"].ToString().Substring(0, 3);
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                        if (bValMatch)
                        {
                            values myVals = new values();
                            myVals.LAND = dtValues.Rows[iValueCount]["Land"].ToString();
                            myVals.IMPR = dtValues.Rows[iValueCount]["Structure"].ToString();
                            myVals.FIXTR = dtValues.Rows[iValueCount]["Fixtures"].ToString();
                            myVals.PERSPROP = dtValues.Rows[iValueCount]["PP"].ToString();
                            myVals.EXE_AMT = dtValues.Rows[iValueCount]["OtherExemption"].ToString();
                            thisRec.LAND = myVals.LAND;
                            thisRec.IMPR = myVals.IMPR;
                            thisRec.FIXTR = myVals.FIXTR;
                            thisRec.PERSPROP = myVals.PERSPROP;
                            thisRec.GROSS = myVals.GROSS;
                            thisRec.EXE_AMT = myVals.EXE_AMT;
                        }

                        string myZip = drRoll["AddressZip"].ToString();
                        Regex reZip = new Regex(@"^\d{9}$");
                        if (reZip.IsMatch(myZip))
                        {
                            myZip = myZip.Substring(0, 5) + "-" + myZip.Substring(5, 4);
                        }
                        mailing myMail = new mailing(drRoll["AddressStreet"].ToString(), cleanLine(drRoll["AddressCity"].ToString()) + " " + drRoll["AddressState"].ToString() + " " + myZip, "", "");
                        Regex reDBA = new Regex(@"^(" + County.RE_dba + ")", RegexOptions.IgnoreCase);
                        thisRec.DBA = cleanLine(drRoll["DBA"].ToString());
                        Match m1 = reDBA.Match(thisRec.DBA);
                        if (m1.Success)
                        {
                            Group g = m1.Groups[1];
                            thisRec.DBA = thisRec.DBA.Replace(g.ToString(), "").Trim();
                        }

                        Regex reCO = new Regex(@"^(" + County.RE_care_of + ")", RegexOptions.IgnoreCase);
                        thisRec.CARE_OF = cleanLine(drRoll["InCareOf"].ToString());
                        Match m2 = reCO.Match(thisRec.CARE_OF);
                        if (m2.Success)
                        {
                            Group g = m2.Groups[1];
                            thisRec.CARE_OF = thisRec.CARE_OF.Replace(g.ToString(), "").Trim();
                        }
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = cleanLine(drRoll["AddressCity"].ToString());
                        thisRec.M_ST = drRoll["AddressState"].ToString();
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string tmpUnit = "";
                        if (drRoll["SpaceApt"].ToString().Trim().Length > 0) tmpUnit = " #" + drRoll["SpaceApt"].ToString();
                        string situs1 = drRoll["StreetNum"].ToString() + " " + drRoll["StreetDirection"].ToString() + " " + drRoll["Street"].ToString() + " " + drRoll["StreetType"].ToString() + tmpUnit;
                        string situs2 = xlatCommunity(drRoll["Community"].ToString()) + " CA " + drRoll["Zip"].ToString();
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("810", "101");
            stdTypes.Add("811", "106");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("899", "106");
        }
        private void populateStdCity()
        {
            // Community Translation Table
            stdCity.Add("ALP", "ALPINE MEADOWS");
            stdCity.Add("ALT", "ALTA");
            stdCity.Add("ALTA", "ALTA");
            stdCity.Add("APP", "APPLEGATE");
            stdCity.Add("AUB", "AUBURN");
            stdCity.Add("BAX", "BAXTER");
            stdCity.Add("BIC", "LINCOLN");
            stdCity.Add("CAR", "CARNELIAN BAY");
            stdCity.Add("COL", "COLFAX");
            stdCity.Add("DUT", "DUTCH FLAT");
            stdCity.Add("ELV", "ELVERTA");
            stdCity.Add("EMI", "EMIGRANT GAP");
            stdCity.Add("FOR", "FORESTHILL");
            stdCity.Add("GOL", "GOLD RUN");
            stdCity.Add("GRA", "GRANITE BAY");
            stdCity.Add("HOM", "HOMEWOOD");
            stdCity.Add("IOW", "IOWA HILL");
            stdCity.Add("KIN", "KINGS BEACH");
            stdCity.Add("KING", "KINGS BEACH");
            stdCity.Add("LIN", "LINCOLN");
            stdCity.Add("LOO", "LOOMIS");
            stdCity.Add("MEA", "MEADOW VISTA");
            stdCity.Add("NEW", "NEWCASTLE");
            stdCity.Add("NOR", "NORDEN");
            stdCity.Add("OLY", "OLYMPIC VALLEY");
            stdCity.Add("PEN", "PENRYN");
            stdCity.Add("PLE", "PLEASANT GROVE");
            stdCity.Add("ROC", "ROCKLIN");
            stdCity.Add("ROS", "ROSEVILLE");
            stdCity.Add("SHE", "SHERIDAN");
            stdCity.Add("SOD", "SODA SPRINGS");
            stdCity.Add("TAC", "TAHOE CITY");
            stdCity.Add("TACX", "TAHOE CITY");
            stdCity.Add("TAV", "TAHOE VISTA");
            stdCity.Add("TAH", "TAHOMA");
            stdCity.Add("TRU", "TRUCKEE");
            stdCity.Add("WEI", "WEIMAR");
        }

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                string myBook = csv[0].Substring(0, 3);
                if (stdTypes.ContainsKey(myBook))
                {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
                }
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
    }
}
