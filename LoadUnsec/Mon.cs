﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Mon : Megabyte
    {
        public Mon()
        {
            co3 = "MON";
            coNum = "27";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("810", "101");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("835", "401");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("861", "503");
            stdTypes.Add("870", "503");
            stdTypes.Add("980", "500");
            stdTypes.Add("981", "500");
            stdTypes.Add("990", "500");
            stdTypes.Add("991", "500");
            stdTypes.Add("995", "500");
            stdTypes.Add("996", "500");
        }
    }
}
