﻿using System;
using System.Configuration;
using System.IO;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Ker_new : County
    {
        public Ker_new()
        {
            co3 = "KER";
            coNum = "15";
            populateStdTypes();
        }

        #region Properties
        // Field names and positions from roll header
        //  0 ROLL
        //  1 TRA   
        //  2 ATN        
        //  3 FILE-NO      
        //  4 APN      
        //  5 USE 
        //  6 EX
        //  7 ASSE NAME                                   
        //  8 ACRES   
        //  9 CITRUS  
        // 10 AG-PRESERVE    
        // 11 MINERAL       
        // 12 LAND          
        // 13 IMPROVEMENTS  
        // 14 OTHER IMP     
        // 15 PERS PROP     
        // 16 EXEMPTIONS    
        // 17 NET-VALUE     
        // 18 P8
        // 19 P8 BYV-AG BYV 
        // 20 DIST
        // 21 BPS
        // 22 PERS-PROP-TYPE             
        // 23 SITUS                                                       
        // 24 LEGAL TYPE
        // 25 LEGAL                                                       
        // 26 IN-CARE-OF                              
        // 27 DBA                                               
        // 28 BILLING ADDR1                           
        // 29 BILLING ADDR2                           
        // 30 BILLING ADDR3                 
        // 31 ZIPCODE  
        // 32 BILL-KEY       
        // 33 BILL-AMT     
        // 34 TE-NO     
        // 35 ASSESSE
        // 36 AE INFO1       
        // 37 AE INFO2                      
        // 38 AE INFO3                                    
        // 39 PRIOR LD VALUE
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [Sheet1$] WHERE [ROLL] = '4' ORDER BY [FILE-NO]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = fields["FILE-NO"].ToString();
                        thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                        thisRec.FEE_PARCEL_S = fields["APN"].ToString();
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = fields["TRA"].ToString();
                        thisRec.OWNER1 = cleanLine(fields["ASSE NAME"].ToString());
                        thisRec.CARE_OF = cleanLine(fields["IN-CARE-OF"].ToString());
                        thisRec.DBA = cleanLine(fields["DBA"].ToString());
                        thisRec.EXE_CD = fields["EX"].ToString();
                        thisRec.LEGAL = fields["LEGAL"].ToString();
                        thisRec.TYPE = fields["USE"].ToString();
                        thisRec.TYPE_STD = xlatType(fields["USE"].ToString());

                        values myVals = new values();
                        myVals.LAND = fields["LAND"].ToString();
                        myVals.IMPR = fields["IMPROVEMENTS"].ToString();
                        myVals.FIXTR = fields["OTHER IMP"].ToString();
                        myVals.PERSPROP = fields["PERS PROP"].ToString();
                        myVals.EXE_AMT = fields["EXEMPTIONS"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        mailing myMail = new mailing(fields["BILLING ADDR1"].ToString(), fields["BILLING ADDR2"].ToString(), fields["BILLING ADDR3"].ToString(), "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        situs mySitus = new situs(fields["SITUS"].ToString(), "");
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("0000", "500");
            stdTypes.Add("0101", "500");
            stdTypes.Add("0106", "500");
            stdTypes.Add("0186", "500");
            stdTypes.Add("0500", "500");
            stdTypes.Add("0503", "500");
            stdTypes.Add("0700", "500");
            stdTypes.Add("1020", "500");
            stdTypes.Add("1100", "100");
            stdTypes.Add("1111", "100");
            stdTypes.Add("1116", "100");
            stdTypes.Add("1119", "100");
            stdTypes.Add("1120", "100");
            stdTypes.Add("1123", "100");
            stdTypes.Add("1124", "100");
            stdTypes.Add("1186", "100");
            stdTypes.Add("1190", "100");
            stdTypes.Add("1200", "100");
            stdTypes.Add("1201", "100");
            stdTypes.Add("1202", "100");
            stdTypes.Add("1203", "100");
            stdTypes.Add("1300", "100");
            stdTypes.Add("1301", "100");
            stdTypes.Add("1308", "100");
            stdTypes.Add("1309", "100");
            stdTypes.Add("1310", "100");
            stdTypes.Add("1312", "100");
            stdTypes.Add("1316", "100");
            stdTypes.Add("1386", "100");
            stdTypes.Add("1390", "100");
            stdTypes.Add("1400", "100");
            stdTypes.Add("1401", "100");
            stdTypes.Add("1402", "100");
            stdTypes.Add("1403", "100");
            stdTypes.Add("1404", "100");
            stdTypes.Add("1405", "100");
            stdTypes.Add("1407", "100");
            stdTypes.Add("1490", "100");
            stdTypes.Add("1500", "100");
            stdTypes.Add("1501", "100");
            stdTypes.Add("1502", "100");
            stdTypes.Add("1600", "100");
            stdTypes.Add("1601", "100");
            stdTypes.Add("1602", "100");
            stdTypes.Add("1603", "100");
            stdTypes.Add("1604", "108");
            stdTypes.Add("1605", "108");
            stdTypes.Add("1606", "108");
            stdTypes.Add("1607", "108");
            stdTypes.Add("1613", "108");
            stdTypes.Add("1614", "100");
            stdTypes.Add("1690", "100");
            stdTypes.Add("1700", "108");
            stdTypes.Add("1701", "108");
            stdTypes.Add("1702", "108");
            stdTypes.Add("1703", "108");
            stdTypes.Add("1704", "108");
            stdTypes.Add("1705", "108");
            stdTypes.Add("1706", "108");
            stdTypes.Add("1707", "108");
            stdTypes.Add("1708", "108");
            stdTypes.Add("1710", "108");
            stdTypes.Add("1712", "108");
            stdTypes.Add("1713", "108");
            stdTypes.Add("1714", "108");
            stdTypes.Add("1716", "500");
            stdTypes.Add("1717", "500");
            stdTypes.Add("1718", "500");
            stdTypes.Add("1720", "108");
            stdTypes.Add("1790", "501");
            stdTypes.Add("1800", "100");
            stdTypes.Add("1801", "100");
            stdTypes.Add("1802", "100");
            stdTypes.Add("1803", "100");
            stdTypes.Add("1804", "100");
            stdTypes.Add("1805", "100");
            stdTypes.Add("1806", "100");
            stdTypes.Add("1807", "100");
            stdTypes.Add("1808", "100");
            stdTypes.Add("1890", "100");
            stdTypes.Add("1900", "100");
            stdTypes.Add("1901", "100");
            stdTypes.Add("1902", "100");
            stdTypes.Add("1903", "100");
            stdTypes.Add("1904", "100");
            stdTypes.Add("1905", "100");
            stdTypes.Add("1906", "100");
            stdTypes.Add("1907", "100");
            stdTypes.Add("1908", "500");
            stdTypes.Add("1909", "100");
            stdTypes.Add("1910", "100");
            stdTypes.Add("1911", "100");
            stdTypes.Add("1912", "100");
            stdTypes.Add("1913", "100");
            stdTypes.Add("1914", "100");
            stdTypes.Add("1917", "100");
            stdTypes.Add("1919", "100");
            stdTypes.Add("1990", "501");
            stdTypes.Add("2100", "100");
            stdTypes.Add("2101", "100");
            stdTypes.Add("2102", "100");
            stdTypes.Add("2103", "100");
            stdTypes.Add("2190", "501");
            stdTypes.Add("2200", "100");
            stdTypes.Add("2201", "100");
            stdTypes.Add("2202", "100");
            stdTypes.Add("2203", "100");
            stdTypes.Add("2204", "100");
            stdTypes.Add("2205", "100");
            stdTypes.Add("2206", "100");
            stdTypes.Add("2207", "100");
            stdTypes.Add("2208", "100");
            stdTypes.Add("2290", "100");
            stdTypes.Add("2300", "100");
            stdTypes.Add("2301", "100");
            stdTypes.Add("2302", "100");
            stdTypes.Add("2303", "100");
            stdTypes.Add("2304", "100");
            stdTypes.Add("2305", "100");
            stdTypes.Add("2390", "100");
            stdTypes.Add("2400", "100");
            stdTypes.Add("2401", "100");
            stdTypes.Add("2402", "100");
            stdTypes.Add("2403", "100");
            stdTypes.Add("2404", "100");
            stdTypes.Add("2405", "100");
            stdTypes.Add("2406", "100");
            stdTypes.Add("2490", "100");
            stdTypes.Add("2501", "100");
            stdTypes.Add("2600", "100");
            stdTypes.Add("2601", "100");
            stdTypes.Add("2602", "100");
            stdTypes.Add("2603", "100");
            stdTypes.Add("2604", "100");
            stdTypes.Add("2605", "100");
            stdTypes.Add("2606", "100");
            stdTypes.Add("2607", "100");
            stdTypes.Add("2608", "100");
            stdTypes.Add("2609", "100");
            stdTypes.Add("2690", "100");
            stdTypes.Add("2700", "100");
            stdTypes.Add("2701", "100");
            stdTypes.Add("2790", "100");
            stdTypes.Add("2800", "100");
            stdTypes.Add("2801", "100");
            stdTypes.Add("2890", "100");
            stdTypes.Add("2900", "100");
            stdTypes.Add("2901", "100");
            stdTypes.Add("2990", "501");
            stdTypes.Add("3019", "500");
            stdTypes.Add("3020", "501");
            stdTypes.Add("3050", "500");
            stdTypes.Add("3080", "500");
            stdTypes.Add("3100", "100");
            stdTypes.Add("3101", "100");
            stdTypes.Add("3102", "100");
            stdTypes.Add("3103", "100");
            stdTypes.Add("3104", "100");
            stdTypes.Add("3105", "100");
            stdTypes.Add("3106", "100");
            stdTypes.Add("3107", "100");
            stdTypes.Add("3108", "100");
            stdTypes.Add("3120", "100");
            stdTypes.Add("3180", "100");
            stdTypes.Add("3186", "100");
            stdTypes.Add("3190", "100");
            stdTypes.Add("3200", "100");
            stdTypes.Add("3201", "100");
            stdTypes.Add("3202", "100");
            stdTypes.Add("3203", "100");
            stdTypes.Add("3205", "100");
            stdTypes.Add("3206", "100");
            stdTypes.Add("3290", "100");
            stdTypes.Add("3300", "100");
            stdTypes.Add("3301", "100");
            stdTypes.Add("3400", "100");
            stdTypes.Add("3401", "100");
            stdTypes.Add("3402", "100");
            stdTypes.Add("3490", "100");
            stdTypes.Add("3500", "100");
            stdTypes.Add("3501", "100");
            stdTypes.Add("3502", "100");
            stdTypes.Add("3503", "100");
            stdTypes.Add("3504", "100");
            stdTypes.Add("3505", "100");
            stdTypes.Add("3506", "100");
            stdTypes.Add("3507", "100");
            stdTypes.Add("3508", "100");
            stdTypes.Add("3509", "100");
            stdTypes.Add("3590", "100");
            stdTypes.Add("3600", "100");
            stdTypes.Add("3601", "100");
            stdTypes.Add("3602", "100");
            stdTypes.Add("3603", "100");
            stdTypes.Add("3604", "100");
            stdTypes.Add("3605", "100");
            stdTypes.Add("3700", "100");
            stdTypes.Add("3701", "100");
            stdTypes.Add("3702", "100");
            stdTypes.Add("3703", "100");
            stdTypes.Add("3704", "105");
            stdTypes.Add("3705", "100");
            stdTypes.Add("3706", "100");
            stdTypes.Add("3707", "100");
            stdTypes.Add("3708", "100");
            stdTypes.Add("3709", "100");
            stdTypes.Add("3710", "100");
            stdTypes.Add("3720", "100");
            stdTypes.Add("3780", "100");
            stdTypes.Add("3790", "100");
            stdTypes.Add("3800", "100");
            stdTypes.Add("3801", "100");
            stdTypes.Add("3880", "100");
            stdTypes.Add("3890", "100");
            stdTypes.Add("3900", "402");
            stdTypes.Add("3901", "500");
            stdTypes.Add("3902", "503");
            stdTypes.Add("3960", "503");
            stdTypes.Add("3961", "503");
            stdTypes.Add("3962", "503");
            stdTypes.Add("3964", "403");
            stdTypes.Add("3965", "503");
            stdTypes.Add("3968", "503");
            stdTypes.Add("3969", "503");
            stdTypes.Add("3984", "503");
            stdTypes.Add("4100", "502");
            stdTypes.Add("4101", "502");
            stdTypes.Add("4110", "502");
            stdTypes.Add("4120", "502");
            stdTypes.Add("4121", "502");
            stdTypes.Add("4150", "502");
            stdTypes.Add("4160", "502");
            stdTypes.Add("4200", "502");
            stdTypes.Add("4210", "502");
            stdTypes.Add("4300", "502");
            stdTypes.Add("4301", "502");
            stdTypes.Add("4390", "502");
            stdTypes.Add("4400", "502");
            stdTypes.Add("4500", "502");
            stdTypes.Add("4600", "502");
            stdTypes.Add("4700", "502");
            stdTypes.Add("4800", "502");
            stdTypes.Add("4801", "502");
            stdTypes.Add("4802", "502");
            stdTypes.Add("4806", "500");
            stdTypes.Add("4900", "502");
            stdTypes.Add("4908", "502");
            stdTypes.Add("6030", "504");
            stdTypes.Add("6040", "504");
            stdTypes.Add("6050", "504");
            stdTypes.Add("6100", "503");
            stdTypes.Add("6400", "503");
            stdTypes.Add("6500", "503");
            stdTypes.Add("8100", "100");
            stdTypes.Add("8101", "100");
            stdTypes.Add("8102", "100");
            stdTypes.Add("8103", "100");
            stdTypes.Add("8104", "100");
            stdTypes.Add("8105", "100");
            stdTypes.Add("8201", "500");
            stdTypes.Add("8203", "403");
            stdTypes.Add("8204", "403");
            stdTypes.Add("8209", "100");
            stdTypes.Add("8300", "503");
            stdTypes.Add("8301", "503");
            stdTypes.Add("8302", "503");
            stdTypes.Add("8304", "503");
            stdTypes.Add("8400", "401");
            stdTypes.Add("8700", "503");
            stdTypes.Add("8701", "503");
            stdTypes.Add("8704", "503");
            stdTypes.Add("9000", "400");
            stdTypes.Add("9001", "400");
            stdTypes.Add("9002", "400");
            stdTypes.Add("9003", "400");
            stdTypes.Add("9004", "400");
            stdTypes.Add("9010", "400");
            stdTypes.Add("9011", "400");
            stdTypes.Add("9012", "400");
            stdTypes.Add("9020", "400");
            stdTypes.Add("9021", "400");
            stdTypes.Add("9022", "400");
            stdTypes.Add("9023", "400");
            stdTypes.Add("9031", "400");
            stdTypes.Add("9032", "400");
            stdTypes.Add("9040", "400");
            stdTypes.Add("9100", "103");
            stdTypes.Add("9101", "103");
            stdTypes.Add("9103", "103");
            stdTypes.Add("9104", "103");
            stdTypes.Add("9200", "501");
            stdTypes.Add("9201", "501");
            stdTypes.Add("9202", "501");
            stdTypes.Add("9203", "403");
            stdTypes.Add("9204", "503");
            stdTypes.Add("9500", "501");
            stdTypes.Add("9700", "200");
            stdTypes.Add("9701", "104");
            stdTypes.Add("9702", "301");
            stdTypes.Add("9703", "300");
            stdTypes.Add("9704", "200");
        }
    }
}
