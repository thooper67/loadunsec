﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Cal : Megabyte
    {
        public Cal()
        {
            co3 = "CAL";
            coNum = "5";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "104"); // BUSINESS ASSESSMENTS
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("890", "500");
        }
    }
}
