﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class situs
    {
        public situs() 
        {
        }
        public situs(string inputLine1, string inputLine2)
        {
            line1 = inputLine1;
            line2 = inputLine2;
            parse();
        }

        #region properties
        public string line1 = "";
        public string line2 = "";
        public string s_unit_no = "";
        public string s_hseno = "";
        public string s_strnum = "";
        public string s_str_sub = "";
        public string s_dir = "";
        public string s_street = "";
        public string s_suff = "";
        public string s_city = ""; 
        public string s_st = "CA";
        public string s_zip = "";
        public string s_zip4 = "";
        public string s_addr_d = "";
        public string s_cty_st_d = "";
        public string msgs = "";
        #endregion

        #region methods
        public void parse()
        {
            address myAddr = new address(line1, line2);
            s_unit_no = myAddr.unit_no;
            s_hseno = myAddr.hseno;
            s_strnum = myAddr.strnum;
            s_str_sub = myAddr.str_sub;
            s_dir = myAddr.dir;
            s_street = myAddr.street;
            s_suff = myAddr.suff;
            s_city = myAddr.city;
            s_zip = myAddr.zip;
            s_zip4 = myAddr.zip4;
            s_addr_d = myAddr.addr_d;
            s_cty_st_d = myAddr.cty_st_d;
            msgs = myAddr.msgs;
        }
        #endregion
    }
}
