﻿using System;
using System.Configuration;
using System.IO;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Sol : County
    {
        public Sol()
        {
            co3 = "SOL";
            coNum = "48";
            populateStdTypes();
            populateStdCity();
            populateStdUnit();
        }

        #region Properties
        // Field names and positions from unsecured roll header
        //  0 AsmtNumber
        //  1 TaxYear
        //  2 PersonalPropertyCategory
        //  3 CraftNumber
        //  4 ReferenceAPN
        //  5 AsmtType
        //  6 Assessee
        //  7 MailingAddressLine1
        //  8 MailingAddressLine2
        //  9 MailingAddressCityStateZip
        // 10 DBAName
        // 11 SiteCity
        // 12 SitusStreetName
        // 13 SitusStreetNumber
        // 14 SiteBuilding
        // 15 SiteUnit
        // 16 UnitCode
        // 17 TaxAreaCode
        // 18 LandValue
        // 19 ImprovementValue
        // 20 PersonalPropertyValue
        // 21 FixedMachineryEquipValue
        // 22 FishingVessel
        // 23 PenaltyNonFiling
        // 24 ExemptionType
        // 25 ExemptionAmount
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [Sheet1$] ORDER BY [AsmtNumber]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = fields["AsmtNumber"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        string myFeePcl = "";
                        if (fields["ReferenceAPN"].ToString() != "") myFeePcl = fields["ReferenceAPN"].ToString();
                        thisRec.FEE_PARCEL_S = myFeePcl;
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = fields["TaxAreaCode"].ToString().PadLeft(6, '0');
                        thisRec.TYPE = fields["AsmtType"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.OWNER1 = cleanLine(fields["Assessee"].ToString());
                        thisRec.ASSESSEE = cleanLine(fields["Assessee"].ToString());
                        thisRec.DBA = cleanLine(fields["DBAName"].ToString());
                        thisRec.CARE_OF = cleanLine(fields["MailingAddressLine2"].ToString());
                        thisRec.EXE_CD = fields["ExemptionType"].ToString();
                        thisRec.LEGAL = cleanLine(fields["PersonalPropertyCategory"].ToString());
                        thisRec.ALT_APN = fields["CraftNumber"].ToString();

                        values myVals = new values();
                        myVals.LAND = fields["LandValue"].ToString();
                        myVals.IMPR = fields["ImprovementValue"].ToString();
                        myVals.FIXTR = fields["FixedMachineryEquipValue"].ToString();
                        myVals.PERSPROP = addStrings(fields["PersonalPropertyValue"].ToString(), fields["FishingVessel"].ToString());
                        myVals.EXE_AMT = fields["ExemptionAmount"].ToString();
                        myVals.PENALTY = fields["PenaltyNonFiling"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;
                        thisRec.PENALTY = myVals.PENALTY;

                        mailing myMail = new mailing(fields["MailingAddressLine1"].ToString(), fields["MailingAddressCityStateZip"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = fields["SitusStreetNumber"].ToString() + " " + fields["SitusStreetName"].ToString();
                        string situs2 = xlatCommunity(fields["SiteCity"].ToString()) + " CA"; ;
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = xlatUnit(fields["UnitCode"].ToString()) + " " + fields["SiteUnit"].ToString();
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = cleanLine(thisRec.S_STRNUM + " " + thisRec.S_DIR + " " + thisRec.S_STREET + " " + thisRec.S_SUFF + " " + thisRec.S_UNITNO);
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput().ToUpper());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("AC", "300");
            stdTypes.Add("BT", "200");
            stdTypes.Add("IL", "501");
            stdTypes.Add("PI", "400");
            stdTypes.Add("UP", "101");
        }

        private void populateStdCity()
        {
            stdCity.Add("BN", "BENICIA");
            stdCity.Add("DX", "DIXON");
            stdCity.Add("FF", "FAIRFIELD");
            stdCity.Add("RV", "RIO VISTA");
            stdCity.Add("SU", "SUISUN CITY");
            stdCity.Add("UN", "UNINCORPORATED");
            stdCity.Add("VV", "VACAVILLE");
            stdCity.Add("VJ", "VALLEJO");
        }

        private void populateStdUnit()
        {
            stdUnit.Add("#", "#");
            stdUnit.Add("BE", "BERTH");
            stdUnit.Add("BERTH", "BERTH");
            stdUnit.Add("BL", "BLDG");
            stdUnit.Add("HANGAR", "HANGAR");
            stdUnit.Add("HG", "HANGAR");
            stdUnit.Add("QR", "QTRS");
            stdUnit.Add("QUARTERS", "QTRS");
            stdUnit.Add("SPACE", "SPACE");
            stdUnit.Add("STUDIO", "STUDIO");
            stdUnit.Add("ST", "STE");
            stdUnit.Add("SU", "STE");
            stdUnit.Add("SUITE", "STE");
            stdUnit.Add("UN", "UNIT");
            stdUnit.Add("UNIT", "UNIT");
        }
    }
}
