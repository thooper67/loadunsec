﻿using System;
using System.Configuration;
using System.IO;


namespace LoadUnsec
{
    class Teh : MegabyteTax
    {
        public Teh()
        {
            co3 = "TEH";
            coNum = "52";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("810", "101");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
            stdTypes.Add("880", "500");
        }
    }
}