﻿
namespace LoadUnsec
{
    class Tul : Megabyte
    {
        public Tul()
        {
            co3 = "TUL";
            coNum = "54";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "102");
            stdTypes.Add("805", "502");
            stdTypes.Add("807", "401");
            stdTypes.Add("810", "103");
            stdTypes.Add("811", "106");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("855", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
        }
    }

    // Routine to process TUL on old, mainframe system
    //using System;
    //using System.Collections.Generic;
    //using System.Configuration;
    //using System.Data;
    //using System.IO;
    //using System.IO.MemoryMappedFiles;
    //using System.Linq;

    //class Tul : County
    //{
    //    public Tul()
    //    {
    //        co3 = "TUL";
    //        coNum = "54";
    //        populateStdTypes();
    //    }

    //    #region Properties
    //    fixfield fldAPN;
    //    List<fixfield> fields1 = new List<fixfield>();
    //    DataTable dtRoll = new DataTable();
    //    #endregion

    //    #region Methods
    //    public override string process()
    //    {
    //        string result = "";
    //        string version = ConfigurationManager.AppSettings[co3 + "version"];
    //        if (version == "2014")
    //        {
    //            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
    //            int srcFile1RecLen = 2180;
    //            long srcFileLength = 0;
    //            initialize();

    //            StreamWriter swOutFile = null;
    //            int count = 0;

    //            #region srcFile1
    //            if (File.Exists(srcFile1))
    //            {
    //                try
    //                {
    //                    FileInfo f = new FileInfo(srcFile1);
    //                    srcFileLength = f.Length;
    //                    using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(srcFile1, FileMode.Open))
    //                    {
    //                        using (MemoryMappedViewAccessor va = mmf.CreateViewAccessor(0, srcFileLength))
    //                        {
    //                            long i = 0;
    //                            while (i * srcFile1RecLen < srcFileLength)
    //                            {
    //                                long offset = i * srcFile1RecLen;
    //                                byte[] buffer = new byte[srcFile1RecLen];
    //                                va.ReadArray(offset, buffer, 0, srcFile1RecLen);
    //                                string myRec = ebcdic.ConvertE2A(buffer);

    //                                DataRow dr = dtRoll.NewRow();
    //                                foreach (fixfield thisField in fields1)
    //                                {
    //                                    switch (thisField.Format)
    //                                    {
    //                                        case "PD":
    //                                            dr[thisField.Name] = ebcdic.ConvertPd2Str(getBufferSlice(buffer, thisField));
    //                                            break;
    //                                        default:
    //                                            dr[thisField.Name] = getStr(myRec, thisField);
    //                                            break;
    //                                    }
    //                                }
    //                                dtRoll.Rows.Add(dr);
    //                                i++;
    //                            }
    //                        }
    //                    }
    //                }
    //                catch (Exception e)
    //                {
    //                    result = "Error opening source file1: '" + srcFile1 + "'. " + e.Message;
    //                    return result;
    //                }

    //            }
    //            else
    //            {
    //                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
    //                log(result);
    //                return result;
    //            }
    //            #endregion

    //            // open output file
    //            try
    //            {
    //                swOutFile = new StreamWriter(outFile, false);
    //                log("Opened output file " + outFile);
    //            }
    //            catch (Exception e)
    //            {
    //                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
    //                log(result);
    //            }

    //            // Write out header
    //            unsecrec hdr = new unsecrec();
    //            swOutFile.WriteLine(hdr.writeHeader());

    //            // Loop through input
    //            try
    //            {
    //                dtRoll.DefaultView.Sort = "APN";
    //                DataView view = dtRoll.DefaultView;
    //                DataTable dtRollSort = view.ToTable();
    //                count = 0;
    //                foreach (DataRow drRoll in dtRollSort.Rows)
    //                {
    //                    count++;
    //                    unsecrec thisRec = new unsecrec();
    //                    thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
    //                    thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

    //                    thisRec.APN_S = drRoll["APN"].ToString();
    //                    thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
    //                    thisRec.FEE_PARCEL_S = drRoll["FEE_PCL"].ToString();
    //                    thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
    //                    thisRec.CO_NUM = coNum;
    //                    thisRec.CO_ID = co3;
    //                    thisRec.YRASSD = drRoll["ROLL_YR"].ToString();
    //                    thisRec.TRA = drRoll["TRA"].ToString();
    //                    thisRec.LEGAL = cleanLine(drRoll["DESC"].ToString());
    //                    thisRec.TYPE = drRoll["APN"].ToString().Substring(0, 3);
    //                    thisRec.TYPE_STD = xlatType(thisRec.TYPE);
    //                    thisRec.EXE_CD = drRoll["EXE_CD"].ToString();

    //                    thisRec.OWNER1 = cleanLine(drRoll["OWNER"].ToString());
    //                    thisRec.ASSESSEE = cleanLine(drRoll["ASSESSEE"].ToString());

    //                    values myVals = new values();
    //                    myVals.LAND = drRoll["LAND"].ToString();
    //                    myVals.IMPR = drRoll["IMPR"].ToString();
    //                    myVals.FIXTR = drRoll["FIXTR"].ToString();
    //                    myVals.PERSPROP = drRoll["PERSPROP"].ToString();
    //                    myVals.EXE_AMT = drRoll["EXE_AMT"].ToString();
    //                    thisRec.LAND = myVals.LAND;
    //                    thisRec.IMPR = myVals.IMPR;
    //                    thisRec.FIXTR = myVals.FIXTR;
    //                    thisRec.PERSPROP = myVals.PERSPROP;
    //                    thisRec.GROSS = myVals.GROSS;
    //                    thisRec.EXE_AMT = myVals.EXE_AMT;

    //                    string line1, line2, line3, line4, lName;
    //                    lessorLessee(drRoll["M_ADDR1"].ToString(), drRoll["M_ADDR2"].ToString(), drRoll["M_ADDR3"].ToString(), drRoll["M_ADDR4"].ToString(), out line1, out line2, out line3, out line4, out lName);
    //                    mailing myMail = new mailing(line1, line2, line3, line4);
    //                    thisRec.OWNER2 = cleanLine(lName);
    //                    thisRec.DBA = myMail.dba;
    //                    thisRec.CARE_OF = myMail.care_of;
    //                    thisRec.M_STRNUM = myMail.m_strnum;
    //                    thisRec.M_STR_SUB = myMail.m_str_sub;
    //                    thisRec.M_DIR = myMail.m_dir;
    //                    thisRec.M_STREET = myMail.m_street;
    //                    thisRec.M_SUFF = myMail.m_suff;
    //                    thisRec.M_UNITNO = myMail.m_unit_no;
    //                    thisRec.M_CITY = myMail.m_city;
    //                    thisRec.M_ST = myMail.m_st;
    //                    thisRec.M_ZIP = myMail.m_zip;
    //                    thisRec.M_ZIP4 = myMail.m_zip4;
    //                    thisRec.M_ADDR_D = myMail.m_addr_d;
    //                    thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

    //                    swOutFile.WriteLine(thisRec.writeOutput());
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log("Error in record: " + count + ". " + e.Message);
    //            }

    //            // close output file
    //            if (swOutFile != null)
    //            {
    //                swOutFile.Flush();
    //                swOutFile.Close();
    //            }
    //        }
    //        else
    //        {
    //            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
    //            int srcFile1RecLen = 2740;
    //            long srcFileLength = 0;
    //            initialize2();

    //            StreamWriter swOutFile = null;
    //            int count = 0;

    //            #region srcFile1
    //            if (File.Exists(srcFile1))
    //            {
    //                try
    //                {
    //                    FileInfo f = new FileInfo(srcFile1);
    //                    srcFileLength = f.Length;
    //                    using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(srcFile1, FileMode.Open))
    //                    {
    //                        using (MemoryMappedViewAccessor va = mmf.CreateViewAccessor(0, srcFileLength))
    //                        {
    //                            long i = 0;
    //                            while (i * srcFile1RecLen < srcFileLength)
    //                            {
    //                                long offset = i * srcFile1RecLen;
    //                                byte[] buffer = new byte[srcFile1RecLen];
    //                                va.ReadArray(offset, buffer, 0, srcFile1RecLen);
    //                                string myRec = System.Text.Encoding.ASCII.GetString(buffer);
    //                                // Only process records in appropriate range
    //                                string myAPN = getStr(myRec, fldAPN);
    //                                string myBook = myAPN.Substring(0, 3);

    //                                if (stdTypes.ContainsKey(myBook))
    //                                {
    //                                    DataRow dr = dtRoll.NewRow();
    //                                    foreach (fixfield thisField in fields1)
    //                                    {
    //                                        dr[thisField.Name] = getStr(myRec, thisField);
    //                                    }
    //                                    dtRoll.Rows.Add(dr);
    //                                }
    //                                else
    //                                {
    //                                    i++;
    //                                }
    //                                i++;
    //                            }
    //                        }
    //                    }
    //                }
    //                catch (Exception e)
    //                {
    //                    result = "Error opening source file1: '" + srcFile1 + "'. " + e.Message;
    //                    return result;
    //                }

    //            }
    //            else
    //            {
    //                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
    //                log(result);
    //                return result;
    //            }
    //            #endregion

    //            // open output file
    //            try
    //            {
    //                swOutFile = new StreamWriter(outFile, false);
    //                log("Opened output file " + outFile);
    //            }
    //            catch (Exception e)
    //            {
    //                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
    //                log(result);
    //            }

    //            // Write out header
    //            unsecrec hdr = new unsecrec();
    //            swOutFile.WriteLine(hdr.writeHeader());

    //            // Loop through input
    //            try
    //            {
    //                dtRoll.DefaultView.Sort = "APN";
    //                DataView view = dtRoll.DefaultView;
    //                DataTable dtRollSort = view.ToTable();
    //                count = 0;
    //                foreach (DataRow drRoll in dtRollSort.Rows)
    //                {
    //                    count++;
    //                    unsecrec thisRec = new unsecrec();
    //                    thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
    //                    thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

    //                    thisRec.APN_S = drRoll["APN"].ToString();
    //                    thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
    //                    thisRec.FEE_PARCEL_S = drRoll["FEE_PCL"].ToString();
    //                    thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
    //                    thisRec.CO_NUM = coNum;
    //                    thisRec.CO_ID = co3;
    //                    thisRec.YRASSD = drRoll["ROLL_YR"].ToString();
    //                    thisRec.TRA = drRoll["TRA"].ToString();
    //                    thisRec.LEGAL = cleanLine(drRoll["DESC"].ToString());
    //                    thisRec.TYPE = drRoll["APN"].ToString().Substring(0, 3);
    //                    thisRec.TYPE_STD = xlatType(thisRec.TYPE);
    //                    thisRec.EXE_CD = drRoll["EXE_CD"].ToString();

    //                    thisRec.OWNER1 = cleanLine(drRoll["OWNER"].ToString());
    //                    thisRec.ASSESSEE = cleanLine(drRoll["ASSESSEE"].ToString());

    //                    values myVals = new values();
    //                    myVals.LAND = drRoll["LAND"].ToString();
    //                    myVals.IMPR = drRoll["IMPR"].ToString();
    //                    myVals.FIXTR = drRoll["FIXTR"].ToString();
    //                    myVals.PERSPROP = drRoll["PERSPROP"].ToString();
    //                    myVals.GROWING = drRoll["GROWING"].ToString();
    //                    myVals.FIXTR_RP = drRoll["FIXTR_RP"].ToString();
    //                    myVals.EXE_AMT = drRoll["EXE_AMT"].ToString();
    //                    thisRec.LAND = myVals.LAND;
    //                    thisRec.IMPR = myVals.IMPR;
    //                    thisRec.FIXTR = myVals.FIXTR;
    //                    thisRec.PERSPROP = myVals.PERSPROP;
    //                    thisRec.GROWING = myVals.GROWING;
    //                    thisRec.FIXTR_RP = myVals.FIXTR_RP;
    //                    thisRec.GROSS = myVals.GROSS;
    //                    thisRec.EXE_AMT = myVals.EXE_AMT;

    //                    string line1, line2, line3, line4, lName;
    //                    lessorLessee(drRoll["M_ADDR1"].ToString(), drRoll["M_ADDR2"].ToString(), drRoll["M_ADDR3"].ToString(), drRoll["M_ADDR4"].ToString(), out line1, out line2, out line3, out line4, out lName);
    //                    mailing myMail = new mailing(line1, line2, line3, line4);
    //                    thisRec.OWNER2 = cleanLine(lName);
    //                    thisRec.DBA = myMail.dba;
    //                    thisRec.CARE_OF = myMail.care_of;
    //                    thisRec.M_STRNUM = myMail.m_strnum;
    //                    thisRec.M_STR_SUB = myMail.m_str_sub;
    //                    thisRec.M_DIR = myMail.m_dir;
    //                    thisRec.M_STREET = myMail.m_street;
    //                    thisRec.M_SUFF = myMail.m_suff;
    //                    thisRec.M_UNITNO = myMail.m_unit_no;
    //                    thisRec.M_CITY = myMail.m_city;
    //                    thisRec.M_ST = myMail.m_st;
    //                    thisRec.M_ZIP = myMail.m_zip;
    //                    thisRec.M_ZIP4 = myMail.m_zip4;
    //                    thisRec.M_ADDR_D = myMail.m_addr_d;
    //                    thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

    //                    swOutFile.WriteLine(thisRec.writeOutput());
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                log("Error in record: " + count + ". " + e.Message);
    //            }

    //            // close output file
    //            if (swOutFile != null)
    //            {
    //                swOutFile.Flush();
    //                swOutFile.Close();
    //            }
    //        }
    //        return result;
    //    }
    //    private void initialize()
    //    {
    //        //                     Length, Position, Name
    //        fields1.Add(new fixfield(12, 0, "APN"));
    //        fields1.Add(new fixfield(6, 26, "TRA"));
    //        fields1.Add(new fixfield(41, 66, "OWNER"));
    //        fields1.Add(new fixfield(41, 139, "ASSESSEE"));
    //        fields1.Add(new fixfield(41, 180, "M_ADDR1"));
    //        fields1.Add(new fixfield(30, 221, "M_ADDR2"));
    //        fields1.Add(new fixfield(30, 251, "M_ADDR3"));
    //        fields1.Add(new fixfield(30, 281, "M_ADDR4"));
    //        fields1.Add(new fixfield(64, 312, "DESC"));
    //        fields1.Add(new fixfield(5, 408, "LAND", "PD"));
    //        fields1.Add(new fixfield(5, 413, "FIXTR", "PD"));
    //        fields1.Add(new fixfield(5, 423, "IMPR", "PD"));
    //        fields1.Add(new fixfield(5, 428, "PERSPROP", "PD"));
    //        fields1.Add(new fixfield(1, 469, "EXE_CD"));
    //        fields1.Add(new fixfield(5, 471, "EXE_AMT", "PD"));
    //        fields1.Add(new fixfield(4, 1095, "ROLL_YR"));
    //        fields1.Add(new fixfield(12, 1867, "FEE_PCL"));
    //        fields1.Add(new fixfield(8, 2052, "DOC_DT"));
    //        fields1.Add(new fixfield(12, 2060, "DOC_NUM"));
    //        foreach (fixfield field in fields1)
    //        {
    //            dtRoll.Columns.Add(field.Name, typeof(string));
    //        }
    //    }
    //    private void initialize2()
    //    {
    //        //                     Length, Position, Name
    //        fldAPN = new fixfield(12, 0, "APN");
    //        fields1.Add(fldAPN);
    //        fields1.Add(new fixfield(6, 26, "TRA"));
    //        fields1.Add(new fixfield(41, 66, "OWNER"));
    //        fields1.Add(new fixfield(41, 144, "ASSESSEE"));
    //        fields1.Add(new fixfield(41, 185, "M_ADDR1"));
    //        fields1.Add(new fixfield(30, 226, "M_ADDR2"));
    //        fields1.Add(new fixfield(30, 256, "M_ADDR3"));
    //        fields1.Add(new fixfield(30, 286, "M_ADDR4"));
    //        fields1.Add(new fixfield(64, 317, "DESC"));
    //        fields1.Add(new fixfield(9, 417, "LAND"));
    //        fields1.Add(new fixfield(9, 427, "FIXTR"));
    //        fields1.Add(new fixfield(9, 437, "GROWING"));
    //        fields1.Add(new fixfield(9, 447, "IMPR"));
    //        fields1.Add(new fixfield(9, 457, "PERSPROP"));
    //        fields1.Add(new fixfield(9, 467, "FIXTR_RP"));
    //        fields1.Add(new fixfield(1, 513, "EXE_CD"));
    //        fields1.Add(new fixfield(9, 515, "EXE_AMT"));
    //        fields1.Add(new fixfield(4, 1625, "ROLL_YR"));
    //        fields1.Add(new fixfield(12, 2425, "FEE_PCL"));
    //        fields1.Add(new fixfield(8, 2610, "DOC_DT"));
    //        fields1.Add(new fixfield(12, 2618, "DOC_NUM"));
    //        foreach (fixfield field in fields1)
    //        {
    //            dtRoll.Columns.Add(field.Name, typeof(string));
    //        }
    //    }
    //    private void lessorLessee(string s1, string s2, string s3, string s4, out string o1, out string o2, out string o3, out string o4, out string lName)
    //    {
    //        string[] ins = new string[5] { s1 + "        ", s2 + "        ", s3 + "        ", s4 + "        ", "        " };
    //        lName = "";
    //        int i;
    //        int j;
    //        for (i = 0; i < 4; i++)
    //        {
    //            if ((ins[i].Substring(0, 7) == "LESSOR:") || (ins[i].Substring(0, 7) == "LESSEE:") || (ins[i].Substring(0, 6) == "LESSEE"))
    //            {
    //                lName = ins[i].Trim();
    //                for (j = i; j < 4; j++)
    //                {
    //                    ins[j] = ins[j + 1];
    //                }
    //            }
    //        }
    //        o1 = ins[0].Trim();
    //        o2 = ins[1].Trim();
    //        o3 = ins[2].Trim();
    //        o4 = ins[3].Trim();

    //        // Check for blanks in line 1 and 2 and if true, move address3 and address4 down two positions. If only address line 2 is blank, move address3 and address4 down one position. 
    //        if (o1 == "" && o2 == "")
    //        {
    //            o1 = o3;
    //            o2 = o4;
    //            o3 = "";
    //            o4 = "";
    //        }
    //        else if (o1 == "")
    //        {
    //            o1 = o2;
    //            o2 = o3;
    //            o3 = "";
    //            o4 = "";
    //        }
    //        else if (o2 == "")
    //        {
    //            o2 = o3;
    //            o3 = o4;
    //            o4 = "";
    //        }
    //    }
    //    #endregion

    //    private void populateStdTypes()
    //    {
    //        stdTypes.Add("800", "102");
    //        stdTypes.Add("810", "103");
    //        stdTypes.Add("820", "300");
    //        stdTypes.Add("830", "200");
    //        stdTypes.Add("850", "501");
    //        stdTypes.Add("860", "400");
    //    }
    //}
}
