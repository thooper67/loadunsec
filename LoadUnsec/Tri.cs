﻿using System;
using System.Linq;


namespace LoadUnsec
{
    class Tri : Megabyte
    {
        public Tri()
        {
            co3 = "TRI";
            coNum = "53";

            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("807", "401");
            stdTypes.Add("810", "101");
            stdTypes.Add("811", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
        }

        // Old Crest processing

        //private void populateStdTypes()
        //{
        //    stdTypes.Add("01", "300");
        //    stdTypes.Add("02", "101");
        //    stdTypes.Add("04", "400");
        //    stdTypes.Add("05", "400");
        //    stdTypes.Add("06", "200");
        //    stdTypes.Add("08", "400");
        //    stdTypes.Add("13", "400");
        //    stdTypes.Add("24", "402");
        //    stdTypes.Add("26", "400");
        //    stdTypes.Add("27", "401");
        //    stdTypes.Add("B0", "101");
        //    stdTypes.Add("B1", "101");
        //    stdTypes.Add("B2", "101");
        //}
    }
}