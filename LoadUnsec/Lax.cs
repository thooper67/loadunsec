﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Lax : County
    {
        public Lax()
        {
            co3 = "LAX";
            coNum = "19";
            populateStdTypes();
        }

        #region Properties
        // Field name, length, offset
        fixfield fldREC_CD = new fixfield(2, 0);
        fixfield fldOWNER1 = new fixfield(32, 2);
        fixfield fldAPN = new fixfield(10, 34);
        fixfield fldBILL_TYPE = new fixfield(1, 44);
        fixfield fldASSR_ID_NUM = new fixfield(10, 45);
        fixfield fldOWNER1_OVRFLW = new fixfield(32, 55);
        fixfield fldOWNER2 = new fixfield(32, 87);
        fixfield fldM_ADDR = new fixfield(32, 119);
        fixfield fldS_ADDR = new fixfield(32, 151);
        fixfield fldM_CITY_ST = new fixfield(22, 183);
        fixfield fldM_ZIP = new fixfield(5, 205);
        fixfield fldS_CITY_ST = new fixfield(22, 219);
        fixfield fldS_ZIP = new fixfield(14, 241);
        fixfield fldCARE_OF = new fixfield(32, 255);
        fixfield fldDESC1 = new fixfield(40, 287);
        fixfield fldDESC2 = new fixfield(40, 327);
        fixfield fldDESC3 = new fixfield(40, 367);
        fixfield fldDESC4 = new fixfield(40, 407);
        fixfield fldDESC5 = new fixfield(40, 447);
        fixfield fldDESC6 = new fixfield(40, 487);
        fixfield fldDESC7 = new fixfield(40, 527);
        fixfield fldDESC8 = new fixfield(40, 567);
        fixfield fldDESC9 = new fixfield(40, 607);
        fixfield fldDESC10 = new fixfield(40, 647);
        fixfield fldLAND = new fixfield(9, 687);
        fixfield fldIMPR = new fixfield(9, 696);
        fixfield fldAIR_VAL = new fixfield(9, 705);
        fixfield fldFIXTR = new fixfield(9, 714);
        fixfield fldGROUP_KEY = new fixfield(1, 723);
        fixfield fldPERSPROP = new fixfield(9, 724);
        fixfield fldEXE_CD = new fixfield(1, 733);
        fixfield fldEXE_AMT = new fixfield(9, 734);
        fixfield fldNET_VAL = new fixfield(9, 743);
        fixfield fldTRA = new fixfield(5, 752);
        fixfield fldBILL_NUM = new fixfield(7, 757);
        fixfield fldTOTAL_PEN = new fixfield(11, 764);
        fixfield fldPENALTY = new fixfield(9, 775);
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            string line = "";
            int count = 0;
            StreamReader srInputFile = null;
            StreamWriter swOutFile = null;

            // open input file
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    srInputFile = new StreamReader(srcFile1);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    DataTable table = new DataTable();
                    table.Columns.Add("apn", typeof(string));
                    table.Columns.Add("line", typeof(string));
                    // Read the first line
                    line = srInputFile.ReadLine();
                    while (!string.IsNullOrEmpty(line))
                    {
                        count++;
                        //if (count > 8000) break;
                        line = line.Trim();
                        if (line.Length == 784)
                        {
                            DataRow dr = table.NewRow();
                            dr["apn"] = line.Substring(fldAPN.Offset, fldAPN.Length);
                            dr["line"] = line;
                            table.Rows.Add(dr);
                        }
                        else
                        {
                            log("Bad record(line: " + count + "): " + line);
                        }
                        // Read the next line
                        line = srInputFile.ReadLine();
                    }

                    // close input file
                    if (srInputFile != null) srInputFile.Close();
                    table.DefaultView.Sort = "apn";
                    DataView view = table.DefaultView;
                    DataTable dtRoll = view.ToTable();

                    count = 0;
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        line = drRoll["line"].ToString();
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = getStr(line, fldTRA);
                        thisRec.APN_S = getStr(line, fldAPN);
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.FEE_PARCEL_S = getStr(line, fldASSR_ID_NUM);
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.BILL_NUM = getStr(line, fldBILL_NUM);

                        // Concat O1 w/ O1_ovrfl; Clean up "dba dba" in concat.
                        string myOwner = getStr(line, fldOWNER1) + " " + getStr(line, fldOWNER1_OVRFLW);
                        myOwner = cleanLine(Regex.Replace(myOwner, @"dba\s*dba", "dba ", RegexOptions.IgnoreCase));
                        string myOwner2 = getStr(line, fldOWNER2);

                        // If owner2 starts with "&" then concat with myOwner and blank out myowner2
                        Regex re = new Regex(@"^\s*&", RegexOptions.IgnoreCase);
                        Match m1 = re.Match(myOwner2);
                        if (m1.Success)
                        {
                            myOwner += " " + myOwner2;
                            myOwner = cleanLine(myOwner);
                            myOwner2 = "";
                        }
                        string RE_dba = @"[\s\\/\.\)\,\-\(]+DBA\s*[:;,]*\s*";

                        while (true)
                        {
                            // Find a dba NOT at the end
                            // If concat contains dba split on dba, 1 -> o1, 2-> dba, O2 -> O2  (3,5) *** Doesn't catch case of O2 as dba ovfl (8)
                            re = new Regex(@"(.+)" + RE_dba + "(.+)$", RegexOptions.IgnoreCase);
                            m1 = re.Match(myOwner);
                            if (m1.Success)
                            {
                                Group g1 = m1.Groups[1];
                                Group g2 = m1.Groups[2];
                                thisRec.OWNER1 = g1.ToString();
                                thisRec.DBA = cleanLine(g2.ToString()); // Regex.Replace(g2.ToString(), @"dba\s*$", "", RegexOptions.IgnoreCase);
                                thisRec.OWNER2 = cleanLine(Regex.Replace(myOwner2, RE_dba, "", RegexOptions.IgnoreCase));
                                break;
                            }

                            // If concat ends w/ dba O2 -> DBA; Rem dba from concat and -> O1 (6,7)
                            re = new Regex(@"(.+)" + RE_dba + "$", RegexOptions.IgnoreCase);
                            m1 = re.Match(myOwner);
                            if (m1.Success)
                            {
                                Group g1 = m1.Groups[1];
                                thisRec.OWNER1 = g1.ToString();
                                thisRec.DBA = cleanLine(Regex.Replace(myOwner2, @"^" + County.RE_dba, "", RegexOptions.IgnoreCase));
                                break;
                            }

                            // If O2 starts w/ dba: concat -> O1, Remove dba from O2 and result -> dba
                            re = new Regex(@"^" + County.RE_dba + "(.+)", RegexOptions.IgnoreCase);
                            m1 = re.Match(myOwner2.Trim());
                            if (m1.Success)
                            {
                                Group g1 = m1.Groups[1];
                                thisRec.OWNER1 = myOwner;
                                thisRec.DBA = cleanLine(g1.ToString());
                                break;
                            }

                            // Concat -> O1, O2 -> O2 (1,2,4)
                            thisRec.OWNER1 = myOwner;
                            thisRec.OWNER2 = cleanLine(myOwner2.Trim());
                            break;
                        }

                        string myCareOf = getStr(line, fldCARE_OF);
                        re = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        m1 = re.Match(myCareOf);
                        if (m1.Success)
                        {
                            Group g1 = m1.Groups[2];
                            thisRec.CARE_OF = cleanLine(g1.ToString());
                        }
                        else
                        {
                            thisRec.CARE_OF = cleanLine(myCareOf);
                        }

                        string exeCd = getStr(line, fldEXE_CD);
                        if (exeCd != "0") thisRec.EXE_CD = exeCd;
                        thisRec.LEGAL = cleanLine(line.Substring(fldDESC1.Offset, fldDESC1.Length) + " " + line.Substring(fldDESC2.Offset, fldDESC2.Length));
                        thisRec.TYPE = line.Substring(fldGROUP_KEY.Offset, fldGROUP_KEY.Length);
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                        values myVals = new values();
                        myVals.LAND = getStr(line, fldLAND);
                        myVals.IMPR = getStr(line, fldIMPR);
                        myVals.FIXTR = getStr(line, fldFIXTR);
                        myVals.PERSPROP = getStr(line, fldPERSPROP);
                        myVals.EXE_AMT = getStr(line, fldEXE_AMT);
                        myVals.PENALTY = getStr(line, fldPENALTY);
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;
                        thisRec.PENALTY = myVals.PENALTY;

                        mailing myMail = new mailing(getStr(line, fldM_ADDR), getStr(line, fldM_CITY_ST) + " " + getStr(line, fldM_ZIP), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        situs mySitus = new situs(getStr(line, fldS_ADDR), getStr(line, fldS_CITY_ST));
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d + " " + "CA";

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }
            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }
            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("0", "101");
            stdTypes.Add("1", "300");
            stdTypes.Add("2", "200");
            stdTypes.Add("3", "200");
            stdTypes.Add("4", "200");
            stdTypes.Add("7", "201");
        }
    }
}
