﻿using System;
using System.Configuration;
using System.Data;
using System.IO;

using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Iny : County
    {
        public Iny()
        {
            co3 = "INY";
            coNum = "14";
        }

        #region Properties
        // Record layout
        //  0 PIN
        //  1 TaxYear
        //  2 NextYear
        //  3 AsmtType
        //  4 RollType
        //  5 Tag
        //  6 ClassCd
        //  7 CountyName
        //  8 PrimaryOwner
        //  9 Recipient
        // 10 DeliveryAddr
        // 11 LastLine
        // 12 SitusAddr
        // 13 SitusCity
        // 14 SitusPostalCd
        // 15 SitusComplete
        // 16 TotalMarket
        // 17 TotalFBYV
        // 18 HOX
        // 19 DVX
        // 20 LDVX
        // 21 OtherExmpt
        // 22 AssdLand
        // 23 AssdImp
        // 24 AssdPersonal
        // 25 AssdFixtures
        // 26 AssdLivImp
        // 27 AssessedFull
        // 28 TotalExmpt
        // 29 NetTaxable
        // 30 AsmtTranId
        // 31 Id
        // 32 RevObjId
        // 33 PrintDate
        // 34 AssessorMailWithCountyOf
        // 35 AssessorMailWithCounty
        // 36 AssessorMailComplete
        // 37 AttentionLine
        // 38 MailComplete
        // 39 MailCompleteWithAttention
        // 40 ValueDiffPct
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Get files in their own DataTables
                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_S = drRoll["PIN"].ToString();
                        thisRec.APN_D = drRoll["PIN"].ToString();
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = drRoll["Tag"].ToString().PadLeft(6, '0');
                        thisRec.OWNER1 = cleanLine(drRoll["Recipient"].ToString());
                        thisRec.LEGAL = cleanLine(drRoll["ClassCd"].ToString());

                        values myVals = new values();
                        myVals.LAND = drRoll["AssdLand"].ToString();
                        myVals.IMPR = drRoll["AssdImp"].ToString();
                        myVals.PERSPROP = drRoll["AssdPersonal"].ToString();
                        myVals.FIXTR = drRoll["AssdFixtures"].ToString();
                        myVals.GROWING = drRoll["AssdLivImp"].ToString();
                        myVals.EXE_AMT = drRoll["TotalExmpt"].ToString();
                        myVals.PENALTY = drRoll["Penalty"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;
                        if (drRoll["HOX"].ToString() != "0")
                        {
                            thisRec.EXE_CD = "HO";
                        }
                        else if (drRoll["DVX"].ToString() != "0" || drRoll["LDVX"].ToString() != "0")
                        {
                            thisRec.EXE_CD = "DV";
                        }
                        else if (drRoll["OtherExmpt"].ToString() != "0")
                        {
                            thisRec.EXE_CD = "OE";
                        }
                        thisRec.PENALTY = myVals.PENALTY;

                        mailing myMail = new mailing(drRoll["DeliveryAddr"].ToString(), drRoll["LastLine"].ToString(), "", "");
                        thisRec.CARE_OF = drRoll["AttentionLine"].ToString();
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        situs mySitus = new situs(drRoll["SitusAddr"].ToString(), drRoll["SitusCity"].ToString() + " CA " + drRoll["SitusPostalCd"].ToString());
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                string asmtType = csv[3];
                if (asmtType == "Unsecured")
                {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
                }
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
    }
}
