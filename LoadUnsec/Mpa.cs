﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Mpa : Megabyte
    {
        public Mpa()
        {
            co3 = "MPA";
            coNum = "22";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("807", "401");
            stdTypes.Add("810", "101");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
        }
    }
}
