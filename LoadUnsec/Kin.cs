﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Kin : Megabyte
    {
        public Kin()
        {
            co3 = "KIN";
            coNum = "16";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "104");
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
        }
    }
}
