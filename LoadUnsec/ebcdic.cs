﻿using System;
using System.Linq;
using System.Text;

namespace LoadUnsec
{
    public static class ebcdic
    {
        public static string ConvertE2A(string inStr)
        {
            return ConvertE2A(GetBytes(inStr));
        }
        public static string ConvertE2A(byte[] fileData)
        {
            Encoding ascii = Encoding.ASCII;
            Encoding ebcdic = Encoding.GetEncoding("IBM037");
            //Encoding ebcdic = Encoding.UTF32;
            byte[] convertedByte = Encoding.Convert(ebcdic, ascii, fileData);
            return Encoding.ASCII.GetString(convertedByte);
        }
        public static string ConvertPd2Str(byte[] myBytes)
        {
            // We can ignore the sign byte.
            int i;
            string result = "";

            for (i = 0; i < myBytes.GetUpperBound(0); i++)
            {
                result += (myBytes[i] >> 4); // High nibble
                result += (myBytes[i] & 0x0F); // Low nibble
            }
            result += (myBytes[i] >> 4); // High nibble
            return result;
        }
        static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }


    }
}
