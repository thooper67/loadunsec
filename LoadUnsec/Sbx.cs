﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Sbx : County
    {
        public Sbx()
        {
            co3 = "SBX";
            coNum = "42";
            populateStdTypes();
        }

        #region Properties
        // Field names and positions from unsecured roll header
        //  0 PropertyNbr
        //  1 SecAPN
        //  2 OwnerName
        //  3 RollYear
        //  4 FiscalYear
        //  5 FileType
        //  6 PersonalProperty
        //  7 TradeFixture
        //  8 Improvements
        //  9 Land
        // 10 MineralRights
        // 11 Aircraft
        // 12 Boat
        // 13 Penalty
        // 14 PenaltyPercent
        // 15 Inventory
        // 16 InventoryExempt
        // 17 HomeOwnerExempt
        // 18 OtherExempt
        // 19 VeteranExempt
        // 20 GrossValue
        // 21 TotalExemptions
        // 22 TaxValue
        // 23 BillNumber
        // 24 TRA
        // 25 SegmentType
        // 26 Book
        // 27 AddressLine1
        // 28 AddressLine2
        // 29 StreetNbr
        // 30 StreetFract
        // 31 Direction
        // 32 Street
        // 33 Suffix
        // 34 UnitType
        // 35 Unit
        // 36 POBox
        // 37 MailToPOBox
        // 38 City
        // 39 State
        // 40 ProvinceState
        // 41 PostalCode
        // 42 Country
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [Unsecured$] ORDER BY [PropertyNbr]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = fields["PropertyNbr"].ToString().Replace("-", "").Replace(" ", "");
                        thisRec.APN_D = fields["PropertyNbr"].ToString();
                        string myFeePcl = "";
                        if (fields["SecAPN"].ToString() != "") myFeePcl = fields["SecAPN"].ToString().PadLeft(9, '0');
                        thisRec.FEE_PARCEL_S = myFeePcl;
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = fields["RollYear"].ToString();
                        thisRec.TRA = fields["TRA"].ToString().PadLeft(6, '0');
                        thisRec.TYPE = fields["FileType"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                        if (fields["HomeOwnerExempt"].ToString() != "0" && fields["HomeOwnerExempt"].ToString() != "") thisRec.EXE_CD = "HO";
                        if (fields["VeteranExempt"].ToString() != "0" && fields["VeteranExempt"].ToString() != "") thisRec.EXE_CD = "VE";
                        if (fields["OtherExempt"].ToString() != "0" && fields["OtherExempt"].ToString() != "") thisRec.EXE_CD = fields["OtherExemptCode"].ToString();

                        int ptrTop = 1;
                        int ptrBot = 0;
                        string addr1 = fields["MailAddressLine1"].ToString();
                        string addr2 = fields["MailAddressLine2"].ToString();
                        string[] ins = new string[2] { addr1, addr2 };

                        string myPOBox = "";
                        if (fields["MailPOBox"].ToString() != "") myPOBox = "PO BOX " + fields["MailPOBox"].ToString();
                        string addrLine1 = myPOBox + " " + fields["MailStreetNbr"].ToString() + " " + fields["MailStreetFract"].ToString() + " " + fields["MailDirection"].ToString() + " " + fields["MailStreet"].ToString() + " " + fields["MailSuffix"].ToString() + " " + fields["MailUnitType"].ToString() + " " + fields["MailUnit"].ToString();

                        if (fields["PrimaryOwner"].ToString() != "")
                        {
                            thisRec.OWNER1 = cleanLine(fields["PrimaryOwner"].ToString());
                        }
                        else
                        {
                            thisRec.OWNER1 = cleanLine(fields["DBA"].ToString());
                        }
                        thisRec.OWNER2 = cleanLine(fields["SecondaryOwners"].ToString());
                        thisRec.DBA = cleanLine(fields["DBA"].ToString());

                        address tmpAddr = new address();

                        while (ptrBot <= ptrTop)
                        {
                            Regex re1 = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                            Regex re2 = new Regex(@"^(#\s*|MS\s*|SUITE\s*|STE\s*|PMB\s*|DEPT\s*)", RegexOptions.IgnoreCase);
                            //Regex re3 = new Regex(@"(FLOOR\s*)$", RegexOptions.IgnoreCase); Using array 'myItems' instead.
                            Regex re4 = new Regex(@"^(P\.?\s?O\.?\s*B.?X|P\.?\s?O\.?\s+DRAWER)(.+)$", RegexOptions.IgnoreCase);
                            Regex re5 = new Regex(@"^(\d+.+)$", RegexOptions.IgnoreCase);
                            Regex re6 = new Regex(@"^([^\d]+)$", RegexOptions.IgnoreCase); 
                            Match m1 = re1.Match(ins[ptrBot]);
                            Match m6 = re6.Match(ins[ptrBot]);
                            string[] myItems = ins[ptrBot].ToUpper().Split(' ');
                            string myLastItem = "";

                            if (myItems.GetUpperBound(0) > 0)
                            {
                                myLastItem = myItems[myItems.GetUpperBound(0)];
                            }

                            if (re1.IsMatch(ins[ptrBot]))
                            {
                                Group g2 = m1.Groups[2];
                                thisRec.CARE_OF += ' ' + cleanLine(g2.ToString());
                                ptrBot++;
                            }
                            else if (re2.IsMatch(ins[ptrBot]))
                            {
                                addrLine1 += ' ' + ins[ptrBot];
                                ptrBot++;
                            }
                            else if ((!string.IsNullOrEmpty(myLastItem)) && (tmpAddr.floors.ContainsKey(myLastItem)))
                            {
                                addrLine1 += ' ' + ins[ptrBot];
                                ptrBot++;
                            }
                            else if (re4.IsMatch(ins[ptrBot]))
                            {
                                addrLine1 = ins[ptrBot] + ' ' + addrLine1;
                                ptrBot++;
                            }
                            else if (re5.IsMatch(ins[ptrBot]))
                            {
                                addrLine1 += ' ' + ins[ptrBot];
                                ptrBot++;
                            }
                            else if (re6.IsMatch(ins[ptrBot]))
                            {
                                Group g1 = m6.Groups[1];
                                thisRec.CARE_OF += ' ' + cleanLine(g1.ToString());
                                ptrBot++;
                            }
                            else
                            {
                                ptrBot++;
                            }
                        }

                        values myVals = new values();
                        myVals.LAND = addStrings(fields["Land"].ToString(), fields["MineralRights"].ToString());
                        myVals.IMPR = fields["Improvements"].ToString();
                        myVals.FIXTR = fields["TradeFixture"].ToString();
                        myVals.PERSPROP = addStrings(fields["PersonalProperty"].ToString(), fields["Aircraft"].ToString(), fields["Boat"].ToString());
                        myVals.EXE_AMT = fields["TotalExemptions"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string mail1 = addrLine1;
                        string mail2 = fields["MailCity"].ToString() + " " + fields["MailState"].ToString() + " " + fields["MailPostalCode"].ToString().PadLeft(5, '0');
                        mailing myMail = new mailing(mail1, mail2, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = fields["SitusStreetNbr"].ToString() + " " + fields["SitusStreetFract"].ToString() + " " + fields["SitusDirection"].ToString() + " " + fields["SitusStreet"].ToString() + " " + fields["SitusSuffix"].ToString() + " " + fields["SitusUnitType"].ToString() + " " + fields["SitusUnit"].ToString();
                        string situs2 = fields["SitusCity"].ToString() + " " + fields["SitusState"].ToString() + " " + fields["SitusPostalCode"].ToString();
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput().ToUpper());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("A", "300");
            stdTypes.Add("B", "200");
            stdTypes.Add("C", "203");
            stdTypes.Add("D", "100");
            stdTypes.Add("E", "502");
            stdTypes.Add("F", "201");
            stdTypes.Add("G", "202");
            stdTypes.Add("H", "400");
            stdTypes.Add("J", "501");
            stdTypes.Add("L", "403");
            stdTypes.Add("M", "401");
            stdTypes.Add("T", "500");
            stdTypes.Add("U", "103");
            stdTypes.Add("W", "504");
            stdTypes.Add("Y", "502");
        }
    }
}
