﻿
namespace LoadUnsec
{
    class Edx : Megabyte
    {
        public Edx()
        {
            co3 = "EDX";
            coNum = "9";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("807", "500");
            stdTypes.Add("810", "101");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
        }
    }

    // Routine to process EDX on old, mainframe system 
    //class Edx : County
    //{
    //    public Edx()
    //    {
    //        co3 = "EDX";
    //        coNum = "9";
    //        populateStdTypes();
    //    }

    //    #region Properties
    //    fixfield fldREC_CD;
    //    List<fixfield> fields = new List<fixfield>();
    //    DataTable dtRoll = new DataTable();
    //    DataTable dtUpdate = new DataTable();
    //    #endregion

    //    #region Methods
    //    public override string process()
    //    {
    //        string result = "";
    //        string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
    //        string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
    //        int srcFile1RecLen = 547;
    //        int srcFile2RecLen = 547;
    //        long srcFileLength = 0;
    //        int iUpdateCount = 0;
    //        initialize();

    //        StreamWriter swOutFile = null;
    //        int count = 0;

    //        #region srcFile1
    //        if (File.Exists(srcFile1))
    //        {
    //            try
    //            {
    //                FileInfo f = new FileInfo(srcFile1);
    //                srcFileLength = f.Length;
    //                using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(srcFile1, FileMode.Open))
    //                {
    //                    using (MemoryMappedViewAccessor va = mmf.CreateViewAccessor(0, srcFileLength))
    //                    {
    //                        long i = 0;
    //                        while (i * srcFile1RecLen < srcFileLength)
    //                        {
    //                            long offset = i * srcFile1RecLen;
    //                            byte[] buffer = new byte[srcFile1RecLen];
    //                            va.ReadArray(offset, buffer, 0, srcFile1RecLen);
    //                            string myRec = ebcdic.ConvertE2A(buffer);
    //                            // Only process records in appropriate range
    //                            string recCd = getStr(myRec, fldREC_CD);
    //                            if (recCd == "1")
    //                            {
    //                                DataRow dr = dtRoll.NewRow();
    //                                foreach (fixfield thisField in fields)
    //                                {
    //                                    dr[thisField.Name] = getStr(myRec, thisField);
    //                                }
    //                                dtRoll.Rows.Add(dr);
    //                            }
    //                            else
    //                            {
    //                                break;
    //                            }
    //                            i++;
    //                        }
    //                    }
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                result = "Error opening source file1: '" + srcFile1 + "'. " + e.Message;
    //                return result;
    //            }

    //        }
    //        else
    //        {
    //            result = co3 + " Source file '" + srcFile1 + "' does not exist.";
    //            log(result);
    //            return result;
    //        }
    //        #endregion

    //        #region srcFile2
    //        if (File.Exists(srcFile2))
    //        {
    //            try
    //            {
    //                FileInfo f = new FileInfo(srcFile2);
    //                srcFileLength = f.Length;
    //                bool inUnsecRecs = true;
    //                using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(srcFile2, FileMode.Open))
    //                {
    //                    long chunk = 0;
    //                    long viewStart;
    //                    long viewLength;
    //                    long chunkRecCnt = 5000;
    //                    while (inUnsecRecs)
    //                    {
    //                        viewStart = chunk * chunkRecCnt * srcFile2RecLen;
    //                        viewLength = chunkRecCnt * srcFile2RecLen;
    //                        if (viewStart + viewLength > srcFileLength) viewLength = srcFileLength - viewStart;
    //                        using (MemoryMappedViewAccessor va = mmf.CreateViewAccessor(viewStart, viewLength))
    //                        {
    //                            bool inChunk = true;
    //                            long i = 0;
    //                            while (inChunk)
    //                            {
    //                                long offset = i * srcFile2RecLen;
    //                                byte[] buffer = new byte[srcFile2RecLen];
    //                                va.ReadArray(offset, buffer, 0, srcFile2RecLen);
    //                                string myRec = ebcdic.ConvertE2A(buffer);
    //                                // Only process records in appropriate range and type
    //                                string recCd = getStr(myRec, fldREC_CD);
    //                                if (recCd == "1")
    //                                {
    //                                    DataRow dr = dtUpdate.NewRow();
    //                                    foreach (fixfield thisField in fields)
    //                                    {
    //                                        dr[thisField.Name] = getStr(myRec, thisField);
    //                                    }
    //                                    dtUpdate.Rows.Add(dr);
    //                                }
    //                                else
    //                                {
    //                                    inUnsecRecs = false;
    //                                    inChunk = false;
    //                                }
    //                                i++;
    //                                if ((i >= chunkRecCnt) || (viewStart + (i * srcFile2RecLen) >= srcFileLength))
    //                                {
    //                                    inChunk = false;
    //                                }
    //                            }
    //                        }
    //                        chunk++;
    //                    }

    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                result = "Error in source file2: '" + srcFile2 + "'. " + e.Message;
    //                return result;
    //            }

    //        }
    //        else
    //        {
    //            result = co3 + " Source file '" + srcFile2 + "' does not exist.";
    //            log(result);
    //            return result;
    //        }
    //        #endregion

    //        // open output file
    //        try
    //        {
    //            swOutFile = new StreamWriter(outFile, false);
    //            log("Opened output file " + outFile);
    //        }
    //        catch (Exception e)
    //        {
    //            result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
    //            log(result);
    //        }

    //        // Write out header
    //        unsecrec hdr = new unsecrec();
    //        swOutFile.WriteLine(hdr.writeHeader());

    //        // Loop through input
    //        try
    //        {
    //            count = 0;
    //            foreach (DataRow drRoll in dtRoll.Rows)
    //            {
    //                count++;
    //                unsecrec thisRec = new unsecrec();
    //                thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
    //                thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

    //                thisRec.APN_S = drRoll["APN"].ToString();
    //                thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
    //                thisRec.FEE_PARCEL_S = drRoll["FEE_PCL"].ToString();
    //                thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
    //                thisRec.CO_NUM = coNum;
    //                thisRec.CO_ID = co3;
    //                thisRec.YRASSD = drRoll["ROLL_YR"].ToString();
    //                thisRec.TRA = drRoll["TRA"].ToString();
    //                thisRec.TYPE = drRoll["APN"].ToString().Substring(0, 1);
    //                thisRec.TYPE_STD = xlatType(thisRec.TYPE);
    //                thisRec.LEGAL = drRoll["DESC"].ToString();
    //                string myExeCd = drRoll["EXE_CD"].ToString();
    //                if (myExeCd != "00") thisRec.EXE_CD = myExeCd;

    //                // Keep reading value records until we find a match, or realize there isn't a match.
    //                bool bValMatch = false;
    //                string name1, name2, myAddr, myZip, myZip4;
    //                string sValueAsmt = dtUpdate.Rows[iUpdateCount]["APN"].ToString();
    //                while ((iUpdateCount < dtUpdate.Rows.Count - 1) && (thisRec.APN_S.CompareTo(sValueAsmt) > 0))
    //                {
    //                    iUpdateCount++;
    //                    sValueAsmt = dtUpdate.Rows[iUpdateCount]["APN"].ToString();
    //                }
    //                if (thisRec.APN_S.CompareTo(sValueAsmt) == 0) bValMatch = true;

    //                if (bValMatch)
    //                {
    //                    name1 = cleanLine(dtUpdate.Rows[iUpdateCount]["NAME1"].ToString());
    //                    name2 = cleanLine(dtUpdate.Rows[iUpdateCount]["NAME2"].ToString());
    //                    myAddr = dtUpdate.Rows[iUpdateCount]["M_ADDR"].ToString();
    //                    myZip = dtUpdate.Rows[iUpdateCount]["M_ZIP"].ToString();
    //                    myZip4 = dtUpdate.Rows[iUpdateCount]["M_ZIP4"].ToString();
    //                    if ((name1 != cleanLine(drRoll["NAME1"].ToString())) ||
    //                        (name2 != cleanLine(drRoll["NAME2"].ToString())) ||
    //                        (myAddr != drRoll["M_ADDR"].ToString()) ||
    //                        (myZip != drRoll["M_ZIP"].ToString()) ||
    //                        (myZip4 != drRoll["M_ZIP4"].ToString()))
    //                    {
    //                        log("APN: " + thisRec.APN_S + " - look for change.");
    //                    }
    //                }
    //                else
    //                {
    //                    name1 = cleanLine(drRoll["NAME1"].ToString());
    //                    name2 = cleanLine(drRoll["NAME2"].ToString());
    //                    myAddr = drRoll["M_ADDR"].ToString();
    //                    myZip = drRoll["M_ZIP"].ToString();
    //                    myZip4 = drRoll["M_ZIP4"].ToString();
    //                }

    //                thisRec.OWNER1 = name1;
    //                while (true)
    //                {
    //                    Regex reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
    //                    Match mCO = reCO.Match(name2);
    //                    if (mCO.Success)
    //                    {
    //                        Group gCO = mCO.Groups[2];
    //                        thisRec.CARE_OF = cleanLine(gCO.ToString());
    //                        break;
    //                    }

    //                    Regex reDBA = new Regex(@"^(" + County.RE_dba + ")(.+)$", RegexOptions.IgnoreCase);
    //                    Match mDBA = reDBA.Match(name2);
    //                    if (mDBA.Success)
    //                    {
    //                        Group gDBA = mDBA.Groups[2];
    //                        thisRec.DBA = gDBA.ToString();
    //                        break;
    //                    }

    //                    thisRec.OWNER2 = name2;
    //                    break;
    //                }

    //                values myVals = new values();
    //                myVals.LAND = drRoll["LAND"].ToString();
    //                myVals.IMPR = drRoll["IMPR"].ToString();
    //                myVals.FIXTR = drRoll["FIXTR"].ToString();
    //                myVals.PERSPROP = drRoll["PERSPROP"].ToString();
    //                myVals.EXE_AMT = drRoll["EXE_AMT"].ToString();
    //                thisRec.LAND = myVals.LAND;
    //                thisRec.IMPR = myVals.IMPR;
    //                thisRec.FIXTR = myVals.FIXTR;
    //                thisRec.PERSPROP = myVals.PERSPROP;
    //                thisRec.GROSS = myVals.GROSS;
    //                thisRec.EXE_AMT = myVals.EXE_AMT;

    //                Regex re1 = new Regex(@"^(%)(.+)(P\.?\s?O\.?\s*B.?X)(.+)$", RegexOptions.IgnoreCase);
    //                Regex re2 = new Regex(@"^(%)([^\d]+)(\d+.+)$", RegexOptions.IgnoreCase);
    //                Match m1 = re1.Match(myAddr);
    //                Match m2 = re2.Match(myAddr);
    //                if (m1.Success)
    //                {
    //                    Group g2 = m1.Groups[2];
    //                    Group g3 = m1.Groups[3];
    //                    Group g4 = m1.Groups[4];
    //                    thisRec.CARE_OF += ' ' + cleanLine(g2.ToString());
    //                    myAddr = g3.ToString() + g4.ToString();
    //                }
    //                else if (m2.Success)
    //                {
    //                    Group g2 = m2.Groups[2];
    //                    Group g3 = m2.Groups[3];
    //                    Group g4 = m2.Groups[4];
    //                    thisRec.CARE_OF += ' ' + cleanLine(g2.ToString());
    //                    myAddr = g3.ToString() + g4.ToString();
    //                }

    //                if (myZip4 != "")
    //                {
    //                    myZip = myZip + "-" + myZip4;
    //                }
    //                mailing myMail = new mailing(myAddr, myZip, "", "");
    //                thisRec.M_STRNUM = myMail.m_strnum;
    //                thisRec.M_STR_SUB = myMail.m_str_sub;
    //                thisRec.M_DIR = myMail.m_dir;
    //                thisRec.M_STREET = myMail.m_street;
    //                thisRec.M_SUFF = myMail.m_suff;
    //                thisRec.M_UNITNO = myMail.m_unit_no;
    //                thisRec.M_CITY = myMail.m_city;
    //                thisRec.M_ST = myMail.m_st;
    //                thisRec.M_ZIP = myMail.m_zip;
    //                thisRec.M_ZIP4 = myMail.m_zip4;
    //                thisRec.M_ADDR_D = myMail.m_addr_d;
    //                thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

    //                string myUnit = "";
    //                if (drRoll["S_UNITNO"].ToString() != "0000") myUnit = drRoll["S_UNITNO"].ToString();
    //                Regex re = new Regex(@"^0+(.+)", RegexOptions.IgnoreCase);
    //                Match m = re.Match(myUnit);
    //                if (m.Success)
    //                {
    //                    Group g1 = m.Groups[1];
    //                    myUnit = g1.ToString();
    //                    myUnit = cleanLine(drRoll["S_UNIT_TYPE"].ToString() + " " + myUnit);
    //                }
    //                string situs1 = drRoll["S_NUM"].ToString() + " " + drRoll["S_DIR"].ToString() + " " + drRoll["S_STR"].ToString() + " " + drRoll["S_SFX"].ToString();
    //                situs mySitus = new situs(situs1, "");
    //                thisRec.S_HSENO = mySitus.s_hseno;
    //                thisRec.S_STRNUM = mySitus.s_strnum;
    //                thisRec.S_STR_SUB = mySitus.s_str_sub;
    //                thisRec.S_DIR = mySitus.s_dir;
    //                thisRec.S_STREET = mySitus.s_street;
    //                thisRec.S_SUFF = mySitus.s_suff;
    //                thisRec.S_UNITNO = myUnit;
    //                thisRec.S_CITY = mySitus.s_city;
    //                thisRec.S_ST = mySitus.s_st;
    //                thisRec.S_ZIP = mySitus.s_zip;
    //                thisRec.S_ZIP4 = mySitus.s_zip4;
    //                thisRec.S_ADDR_D = mySitus.s_addr_d + " " + myUnit;
    //                thisRec.S_CTY_ST_D = "CA";

    //                swOutFile.WriteLine(thisRec.writeOutput());
    //            }
    //        }
    //        catch (Exception e)
    //        {
    //            log("Error in record: " + count + ". " + e.Message);
    //        }

    //        // close output file
    //        if (swOutFile != null)
    //        {
    //            swOutFile.Flush();
    //            swOutFile.Close();
    //        }
    //        return result;
    //    }
    //    private void initialize()
    //    {
    //        //                     Length, Position, Name
    //        fldREC_CD = new fixfield(1, 0, "REC_CD");
    //        fields.Add(fldREC_CD);
    //        fields.Add(new fixfield(11, 1, "APN"));
    //        fields.Add(new fixfield(6, 12, "TRA"));
    //        fields.Add(new fixfield(30, 92, "NAME1"));
    //        fields.Add(new fixfield(29, 122, "NAME2"));
    //        fields.Add(new fixfield(30, 152, "M_ADDR"));
    //        fields.Add(new fixfield(5, 182, "M_ZIP"));
    //        fields.Add(new fixfield(4, 187, "M_ZIP4"));
    //        fields.Add(new fixfield(6, 192, "S_NUM"));
    //        fields.Add(new fixfield(25, 198, "S_STR"));
    //        fields.Add(new fixfield(5, 223, "S_UNIT_TYPE"));
    //        fields.Add(new fixfield(4, 228, "S_UNITNO"));
    //        fields.Add(new fixfield(2, 232, "S_DIR"));
    //        fields.Add(new fixfield(2, 234, "S_SFX"));
    //        fields.Add(new fixfield(16, 241, "DESC"));
    //        fields.Add(new fixfield(10, 263, "FEE_PCL"));
    //        fields.Add(new fixfield(9, 311, "LAND"));
    //        fields.Add(new fixfield(9, 338, "IMPR"));
    //        fields.Add(new fixfield(9, 347, "FIXTR"));
    //        fields.Add(new fixfield(9, 374, "PERSPROP"));
    //        fields.Add(new fixfield(4, 450, "ROLL_YR"));
    //        fields.Add(new fixfield(2, 454, "EXE_CD"));
    //        fields.Add(new fixfield(9, 456, "EXE_AMT"));
    //        foreach (fixfield field in fields)
    //        {
    //            dtRoll.Columns.Add(field.Name, typeof(string));
    //            dtUpdate.Columns.Add(field.Name, typeof(string));
    //        }
    //    }
    //    #endregion

    //    private void populateStdTypes()
    //    {
    //        stdTypes.Add("1", "100");
    //        stdTypes.Add("2", "200");
    //        stdTypes.Add("3", "300");
    //        stdTypes.Add("4", "100");
    //        stdTypes.Add("5", "502");
    //        stdTypes.Add("6", "501");
    //        stdTypes.Add("7", "401");
    //        stdTypes.Add("8", "500");
    //        stdTypes.Add("9", "400");
    //    }
    //}
}
