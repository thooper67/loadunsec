﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class owner
    {
        public owner()
        {
        }
        public owner(string inputLine1, string inputLine2)
        {
            line1 = inputLine1;
            line2 = inputLine2;
            parse();
        }

        #region properties
        public string line1 = "";
        public string line2 = "";
        public string owner1 = "";
        public string owner2 = "";
        public string dba = "";
        public string care_of = "";
        #endregion

        #region methods
        public void parse()
        {

            // Remove apostrophe and multiple spaces and trim.
            line1 = line1.Replace("'", "").Replace("  ", " ").Trim();
            line2 = line2.Replace("'", "").Replace("  ", " ").Trim();

            // Plug in Owner1
            owner1 = line1;

            // Process line 2
            bool foundMatch = false;
            // Check for DBA or C/O
            Regex rgx = new Regex(@"^(#\s*|" + County.RE_dba + ")", RegexOptions.IgnoreCase);
            Match m = rgx.Match(line2);
            if (rgx.IsMatch(line2))
            {
                Group g = m.Groups[1];
                dba = line2.Replace(g.ToString(), "").Trim();
                foundMatch = true;
            }

            string care_of_re = @"^(&\s*|!\s*|\$\s*|" + County.RE_care_of + ")";
            rgx = new Regex(care_of_re, RegexOptions.IgnoreCase);
            m = rgx.Match(line2);
            if (rgx.IsMatch(line2))
            {
                Group g = m.Groups[1];
                care_of = line2.Replace(g.ToString(), "").Trim();
                foundMatch = true;
            }

            if (!foundMatch)
            {
                owner2 = line2;
            }
        }
        #endregion
    }
    
}
