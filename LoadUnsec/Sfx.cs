﻿using System;
using System.Configuration;
using System.IO;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Sfx : County
    {
        public Sfx()
        {
            co3 = "SFX";
            coNum = "38";
            populateStdTypes();
        }

        #region Properties
        // Field names and positions from unsecured roll header
        //  0 Property: Property ID
        //  1 Assessment Year
        //  2 Sequence Number
        //  3 Property: Record Type
        //  4 Property: Type
        //  5 Account Name
        //  6 Property: Doing Business As
        //  7 Care Of
        //  8 Mailing Address 1
        //  9 Mailing Address 2
        // 10 Mailing Address 3
        // 11 Situs Address 1
        // 12 Situs Address 2
        // 13 Property: Habitual Location of Vessel
        // 14 Property: Other Location
        // 15 Total Land Value
        // 16 Total Improvement Value
        // 17 Total Personal Property Value
        // 18 Total Fixtures Value
        // 29 Exemption Type
        // 20 Exemption Subtype
        // 21 Calculated Amount Exempt
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [Sheet1$] ORDER BY [Property: Property ID]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();

                        thisRec.APN_S = fields["Property: Property ID"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TYPE = fields["Property: Type"].ToString().ToUpper();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.OWNER1 = cleanLine(fields["Account Name"].ToString());
                        thisRec.DBA = cleanLine(fields["Property: Doing Business As"].ToString());
                        thisRec.CARE_OF = cleanLine(fields["Care Of"].ToString());
                        thisRec.EXE_CD = fields["Exemption Subtype"].ToString().ToUpper();
                        if (fields["Property: Habitual Location of Vessel"].ToString() == "Other")
                        {
                            thisRec.LEGAL = fields["Property: Other Location"].ToString().ToUpper();
                        }
                        else
                        {
                            thisRec.LEGAL = fields["Property: Habitual Location of Vessel"].ToString().ToUpper();
                        }

                        values myVals = new values();
                        myVals.LAND = fields["Total Land Value"].ToString();
                        myVals.IMPR = fields["Total Improvement Value"].ToString();
                        myVals.FIXTR = fields["Total Fixtures Value"].ToString();
                        myVals.PERSPROP = fields["Total Personal Property Value"].ToString();
                        myVals.EXE_AMT = fields["Calculated Amount Exempt"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        mailing myMail = new mailing(fields["Mailing Address 1"].ToString(), fields["Mailing Address 2"].ToString(), fields["Mailing Address 3"].ToString(), "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        situs mySitus = new situs(fields["Situs Address 1"].ToString(), fields["Situs Address 2"].ToString());
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("APARTMENT UNIT", "101");
            stdTypes.Add("BARGE", "200");
            stdTypes.Add("BUSINESS", "100");
            stdTypes.Add("LESSEE", "103");
            stdTypes.Add("LESSOR", "103");
            stdTypes.Add("POSSESSORY INTEREST", "400");
            stdTypes.Add("POWERBOAT", "200");
            stdTypes.Add("SAILBOAT", "200");
        }
    }
}
