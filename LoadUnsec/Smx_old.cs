﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Smx_old : County
    {
        public Smx_old()
        {
            co3 = "SMX";
            coNum = "41";
            populateStdTypes();
        }

        #region Properties
        List<string> fields = new List<string>();
        List<fixfield> fields1 = new List<fixfield>();
        List<fixfield> fields2 = new List<fixfield>();
        List<fixfield> fields3 = new List<fixfield>();
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            initialize();

            StreamReader srInputFile1 = null;
            StreamReader srInputFile2 = null;
            StreamReader srInputFile3 = null;
            StreamWriter swOutFile = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
            string srcFile3 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file3"]);
            srInputFile1 = openStreamReader(srcFile1);
            srInputFile2 = openStreamReader(srcFile2);
            srInputFile3 = openStreamReader(srcFile3);
            if ((srcFile1 == null) || (srcFile2 == null) || (srcFile3 == null)) result = "Problem with source file.";

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    // Get input data
                    DataTable table = new DataTable();
                    foreach (string fieldName in fields)
                    {
                        table.Columns.Add(fieldName, typeof(string));
                    }

                    table = fillDataTable(srInputFile1, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file1Length"]), fields1, false, true);
                    table = fillDataTable(srInputFile2, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file2Length"]), fields2, false, false);
                    table = fillDataTable(srInputFile3, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file3Length"]), fields3, false, false);

                    // close input file
                    if (srInputFile1 != null) srInputFile1.Close();
                    if (srInputFile2 != null) srInputFile2.Close();
                    if (srInputFile3 != null) srInputFile3.Close();
                    table.DefaultView.Sort = "ACCOUNT";
                    DataView view = table.DefaultView;
                    DataTable dtRoll = view.ToTable();

                    count = 0;
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];
                        thisRec.APN_S = drRoll["ACCOUNT"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        string myFeePcl = drRoll["FEE_PCL"].ToString();
                        if (myFeePcl != "000000000") thisRec.FEE_PARCEL_S = myFeePcl;
                        thisRec.FEE_PARCEL_D = formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = drRoll["ROLL_YR"].ToString();
                        thisRec.TRA = drRoll["TRA"].ToString().PadLeft(6, '0');
                        thisRec.TYPE = drRoll["TYPE"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.EXE_CD = drRoll["EXE_CD"].ToString();

                        thisRec.OWNER1 = cleanLine(drRoll["OWNNAM1"].ToString());
                        thisRec.OWNER2 = cleanLine(drRoll["OWNNAM2"].ToString());
                        thisRec.DBA = cleanLine(drRoll["DBA"].ToString());

                        while (true)
                        {
                            Regex reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                            Match mCO = reCO.Match(drRoll["CARE_OF"].ToString());
                            if (mCO.Success)
                            {
                                Group gCO = mCO.Groups[2];
                                thisRec.CARE_OF = cleanLine(gCO.ToString());
                                break;
                            }

                            reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                            mCO = reCO.Match(drRoll["DBA"].ToString());
                            if (mCO.Success)
                            {
                                Group gCO = mCO.Groups[2];
                                thisRec.CARE_OF = cleanLine(gCO.ToString());
                                thisRec.DBA = "";
                                break;
                            }
                            
                            thisRec.CARE_OF = cleanLine(drRoll["CARE_OF"].ToString());
                            break;
                        }

                        values myVals = new values();
                        myVals.LAND = drRoll["LAND"].ToString();
                        myVals.IMPR = drRoll["IMPR"].ToString();
                        myVals.FIXTR = drRoll["FIXTR"].ToString();
                        myVals.PERSPROP = drRoll["PERSPROP"].ToString();
                        myVals.EXE_AMT = drRoll["EXE_AMT"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string myZip = "";
                        if (drRoll["ZIPCD1"].ToString().Substring(5, 4) != "0000")
                        {
                            myZip = drRoll["ZIPCD1"].ToString().Substring(0, 5) + "-" + drRoll["ZIPCD1"].ToString().Substring(5, 4);
                        }
                        else
                        {
                            myZip = drRoll["ZIPCD1"].ToString().Substring(0, 5);
                        }
                        string mail1 = drRoll["STRNUM"].ToString() + drRoll["STRFRA"].ToString() + " " + drRoll["STRDIR"].ToString() + " " + drRoll["STRNAM"].ToString() + " " + drRoll["STRTYP"].ToString() + " " + drRoll["STRSRM"].ToString();
                        string mail2 = drRoll["CITY"].ToString() + " " + drRoll["STATE"].ToString() + " " + myZip;
                        mailing myMail = new mailing(mail1, mail2, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = drRoll["LOCNUM"].ToString() + drRoll["LOCFRA"].ToString() + " " + drRoll["LOCDIR"].ToString() + " " + drRoll["LOCNAM"].ToString() + " " + drRoll["LOCTYP"].ToString() + " " + drRoll["LOCSRM"].ToString();
                        string situs2 = drRoll["LOCCIT"].ToString() + " CA";
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }
            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }
            return result;
        }
        private void initialize()
        {
            fields.Add("ACCOUNT");
            fields.Add("TYPE");
            fields.Add("ROLL_YR");
            fields.Add("OWNNAM1");
            fields.Add("OWNNAM2");
            fields.Add("CARE_OF");
            fields.Add("DBA");
            fields.Add("STRNUM");
            fields.Add("STRFRA");
            fields.Add("STRNAM");
            fields.Add("STRDIR");
            fields.Add("STRTYP");
            fields.Add("STRSRM");
            fields.Add("CITY");
            fields.Add("STATE");
            fields.Add("ZIPCD1");
            fields.Add("LOCNUM");
            fields.Add("LOCFRA");
            fields.Add("LOCNAM");
            fields.Add("LOCDIR");
            fields.Add("LOCTYP");
            fields.Add("LOCSRM");
            fields.Add("LOCCIT");
            fields.Add("LAND");
            fields.Add("IMPR");
            fields.Add("FIXTR");
            fields.Add("PERSPROP");
            fields.Add("TRA");
            fields.Add("FEE_PCL");
            fields.Add("EXE_CD");
            fields.Add("EXE_AMT");

            //                     Length, Position, Name
            fields1.Add(new fixfield(20, 0, "ACCOUNT"));
            fields1.Add(new fixfield(3, 20, "TYPE"));
            fields1.Add(new fixfield(4, 27, "ROLL_YR"));
            fields1.Add(new fixfield(40, 31, "OWNNAM1"));
            fields1.Add(new fixfield(40, 71, "OWNNAM2"));
            fields1.Add(new fixfield(40, 111, "CARE_OF"));
            fields1.Add(new fixfield(40, 151, "DBA"));
            fields1.Add(new fixfield(12, 191, "STRNUM"));
            fields1.Add(new fixfield(3, 203, "STRFRA"));
            fields1.Add(new fixfield(32, 206, "STRNAM"));
            fields1.Add(new fixfield(3, 238, "STRDIR"));
            fields1.Add(new fixfield(4, 241, "STRTYP"));
            fields1.Add(new fixfield(8, 245, "STRSRM"));
            fields1.Add(new fixfield(21, 253, "CITY"));
            fields1.Add(new fixfield(2, 274, "STATE"));
            fields1.Add(new fixfield(9, 276, "ZIPCD1"));
            fields1.Add(new fixfield(12, 299, "LOCNUM"));
            fields1.Add(new fixfield(3, 311, "LOCFRA"));
            fields1.Add(new fixfield(32, 314, "LOCNAM"));
            fields1.Add(new fixfield(3, 346, "LOCDIR"));
            fields1.Add(new fixfield(4, 349, "LOCTYP"));
            fields1.Add(new fixfield(8, 353, "LOCSRM"));
            fields1.Add(new fixfield(21, 361, "LOCCIT"));
            fields1.Add(new fixfield(12, 382, "LAND"));
            fields1.Add(new fixfield(12, 394, "IMPR"));
            fields1.Add(new fixfield(12, 406, "FIXTR"));
            fields1.Add(new fixfield(12, 418, "PERSPROP"));
            fields1.Add(new fixfield(10, 460, "TRA"));
            fields1.Add(new fixfield(11, 470, "FEE_PCL"));
            fields1.Add(new fixfield(3, 488, "EXE_CD"));
            fields1.Add(new fixfield(12, 499, "EXE_AMT"));

            fields2.Add(new fixfield(20, 0, "ACCOUNT"));
            fields2.Add(new fixfield(3, 20, "TYPE"));
            fields2.Add(new fixfield(4, 27, "ROLL_YR"));
            fields2.Add(new fixfield(32, 31, "OWNNAM1"));
            fields2.Add(new fixfield(32, 63, "OWNNAM2"));
            fields2.Add(new fixfield(32, 95, "CARE_OF"));
            fields2.Add(new fixfield(12, 127, "STRNUM"));
            fields2.Add(new fixfield(3, 139, "STRFRA"));
            fields2.Add(new fixfield(32, 142, "STRNAM"));
            fields2.Add(new fixfield(3, 174, "STRDIR"));
            fields2.Add(new fixfield(3, 177, "STRTYP"));
            fields2.Add(new fixfield(8, 180, "STRSRM"));
            fields2.Add(new fixfield(21, 188, "CITY"));
            fields2.Add(new fixfield(2, 209, "STATE"));
            fields2.Add(new fixfield(9, 211, "ZIPCD1"));
            fields2.Add(new fixfield(12, 254, "LOCNUM"));
            fields2.Add(new fixfield(3, 266, "LOCFRA"));
            fields2.Add(new fixfield(32, 269, "LOCNAM"));
            fields2.Add(new fixfield(3, 301, "LOCDIR"));
            fields2.Add(new fixfield(3, 304, "LOCTYP"));
            fields2.Add(new fixfield(8, 307, "LOCSRM"));
            fields2.Add(new fixfield(21, 315, "LOCCIT"));
            fields2.Add(new fixfield(10, 354, "TRA"));
            fields2.Add(new fixfield(10, 364, "FEE_PCL"));
            fields2.Add(new fixfield(9, 377, "LAND"));
            fields2.Add(new fixfield(9, 386, "IMPR"));
            fields2.Add(new fixfield(9, 395, "PERSPROP"));
            fields2.Add(new fixfield(3, 404, "EXE_CD"));
            fields2.Add(new fixfield(12, 415, "EXE_AMT"));

            fields3.Add(new fixfield(20, 0, "ACCOUNT"));
            fields3.Add(new fixfield(3, 20, "TYPE"));
            fields3.Add(new fixfield(4, 27, "ROLL_YR"));
            fields3.Add(new fixfield(32, 31, "OWNNAM1"));
            fields3.Add(new fixfield(32, 63, "OWNNAM2"));
            fields3.Add(new fixfield(32, 95, "CARE_OF"));
            fields3.Add(new fixfield(12, 127, "STRNUM"));
            fields3.Add(new fixfield(3, 139, "STRFRA"));
            fields3.Add(new fixfield(32, 142, "STRNAM"));
            fields3.Add(new fixfield(3, 174, "STRDIR"));
            fields3.Add(new fixfield(3, 177, "STRTYP"));
            fields3.Add(new fixfield(8, 180, "STRSRM"));
            fields3.Add(new fixfield(21, 188, "CITY"));
            fields3.Add(new fixfield(2, 209, "STATE"));
            fields3.Add(new fixfield(9, 211, "ZIPCD1"));
            fields3.Add(new fixfield(12, 254, "LOCNUM"));
            fields3.Add(new fixfield(3, 266, "LOCFRA"));
            fields3.Add(new fixfield(32, 269, "LOCNAM"));
            fields3.Add(new fixfield(3, 301, "LOCDIR"));
            fields3.Add(new fixfield(3, 304, "LOCTYP"));
            fields3.Add(new fixfield(8, 307, "LOCSRM"));
            fields3.Add(new fixfield(21, 315, "LOCCIT"));
            fields3.Add(new fixfield(10, 354, "TRA"));
            fields3.Add(new fixfield(10, 364, "FEE_PCL"));
            fields3.Add(new fixfield(9, 374, "LAND"));
            fields3.Add(new fixfield(9, 383, "IMPR"));
            fields3.Add(new fixfield(9, 392, "PERSPROP"));
            fields3.Add(new fixfield(3, 401, "EXE_CD"));
            fields3.Add(new fixfield(12, 412, "EXE_AMT"));
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("AGR", "502");
            stdTypes.Add("AHS", "502");
            stdTypes.Add("ALF", "108");
            stdTypes.Add("APT", "100");
            stdTypes.Add("BHO", "200");
            stdTypes.Add("BIA", "100");
            stdTypes.Add("BIL", "100");
            stdTypes.Add("BIN", "100");
            stdTypes.Add("BIO", "100");
            stdTypes.Add("BNA", "504");
            stdTypes.Add("BUS", "100");
            stdTypes.Add("CA", "301");
            stdTypes.Add("CAB", "107");
            stdTypes.Add("CEL", "503");
            stdTypes.Add("CHU", "100");
            stdTypes.Add("CSC", "103");
            stdTypes.Add("CSD", "104");
            stdTypes.Add("CTV", "100");
            stdTypes.Add("DA", "500");
            stdTypes.Add("DAB", "100");
            stdTypes.Add("DAC", "100");
            stdTypes.Add("DAW", "504");
            stdTypes.Add("DRG", "100");
            stdTypes.Add("FAC", "301");
            stdTypes.Add("FC", "106");
            stdTypes.Add("FCL", "106");
            stdTypes.Add("FH", "200");
            stdTypes.Add("FOA", "300");
            stdTypes.Add("GA", "300");
            stdTypes.Add("GAH", "300");
            stdTypes.Add("GLF", "100");
            stdTypes.Add("GRO", "100");
            stdTypes.Add("HB", "500");
            stdTypes.Add("HTL", "100");
            stdTypes.Add("LEB", "103");
            stdTypes.Add("LES", "103");
            stdTypes.Add("LHI", "501");
            stdTypes.Add("LHO", "501");
            stdTypes.Add("MBT", "200");
            stdTypes.Add("ORL", "100");
            stdTypes.Add("PBE", "504");
            stdTypes.Add("PBT", "202");
            stdTypes.Add("PI", "400");
            stdTypes.Add("PIA", "402");
            stdTypes.Add("PIB", "400");
            stdTypes.Add("PIE", "400");
            stdTypes.Add("PIO", "400");
            stdTypes.Add("PTV", "400");
            stdTypes.Add("QBT", "201");
            stdTypes.Add("REF", "500");
            stdTypes.Add("RPV", "501");
            stdTypes.Add("SHH", "100");
            stdTypes.Add("SHS", "100");
            stdTypes.Add("SHT", "100");
            stdTypes.Add("SST", "100");
            stdTypes.Add("TYC", "100");
            stdTypes.Add("VEB", "100");
            stdTypes.Add("VEN", "100");
            stdTypes.Add("VIS", "100");
            stdTypes.Add("WEL", "504");
        }
    }
}
