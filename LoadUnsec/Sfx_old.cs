﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Sfx_old : County
    {
        public Sfx_old()
        {
            co3 = "SFX";
            coNum = "38";
            populateStdTypes();
        }

        #region Properties
        List<string> fields = new List<string>();
        List<fixfield> fields1 = new List<fixfield>();
        List<fixfield> fields2 = new List<fixfield>();
        List<fixfield> fields3 = new List<fixfield>();
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            initialize();

            StreamReader srInputFile1 = null;
            StreamReader srInputFile2 = null;
            StreamReader srInputFile3 = null;
            StreamWriter swOutFile = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
            string srcFile3 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file3"]);
            srInputFile1 = openStreamReader(srcFile1);
            srInputFile2 = openStreamReader(srcFile2);
            srInputFile3 = openStreamReader(srcFile3);
            if ((srcFile1 == null) || (srcFile2 == null) || (srcFile3 == null)) result = "Problem with source file.";

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    // Get input data
                    DataTable table = new DataTable();
                    foreach (string fieldName in fields)
                    {
                        table.Columns.Add(fieldName, typeof(string));
                    }

                    table = fillDataTable(srInputFile1, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file1Length"]), fields1, false, false);
                    table = fillDataTable(srInputFile2, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file2Length"]), fields2, false, false);
                    table = fillDataTable(srInputFile3, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file3Length"]), fields3, false, false);

                    // close input file
                    if (srInputFile1 != null) srInputFile1.Close();
                    if (srInputFile2 != null) srInputFile2.Close();
                    if (srInputFile3 != null) srInputFile3.Close();
                    table.DefaultView.Sort = "ACCOUNT";
                    DataView view = table.DefaultView;
                    DataTable dtRoll = view.ToTable();

                    count = 0;
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_S = drRoll["ACCOUNT"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = drRoll["ROLL_YR"].ToString();
                        thisRec.TYPE = drRoll["TYPE"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.EXE_CD = drRoll["EXE_CD"].ToString();
                        thisRec.LEGAL = cleanLine(drRoll["DESC"].ToString());

                        thisRec.OWNER1 = cleanLine(drRoll["OWNER"].ToString());
                        thisRec.DBA = cleanLine(drRoll["DBA"].ToString());
                        Regex reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mCO = reCO.Match(drRoll["CARE_OF"].ToString());
                        if (mCO.Success)
                        {
                            Group gCO = mCO.Groups[2];
                            thisRec.CARE_OF = cleanLine(gCO.ToString());
                        }
                        else
                        {
                            thisRec.CARE_OF = cleanLine(drRoll["CARE_OF"].ToString());
                        }

                        values myVals = new values();
                        myVals.LAND = drRoll["LAND"].ToString();
                        myVals.IMPR = drRoll["IMPR"].ToString();
                        myVals.FIXTR = drRoll["FIXTR"].ToString();
                        myVals.PERSPROP = drRoll["PERSPROP"].ToString();
                        myVals.EXE_AMT = drRoll["EXE_AMT"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string myZip = drRoll["M_ZIP"].ToString().Replace("  ", "");
                        if (myZip.Length > 8 && myZip.Substring(5, 4) != "0000")
                        {
                            myZip = myZip.Substring(0, 5) + "-" + myZip.Substring(5, 4);
                        }
                        if (myZip.Length > 8 && myZip.Substring(5, 4) == "0000")
                        {
                            myZip = myZip.Substring(0, 5);
                        }
                        string mail1 = drRoll["M_NUM"].ToString() + " " + drRoll["M_STR"].ToString() + " " + drRoll["M_SFX"].ToString();
                        string mail2 = drRoll["M_CITY"].ToString() + " " + drRoll["M_ST"].ToString() + " " + myZip;
                        mailing myMail = new mailing(mail1, mail2, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = drRoll["S_NUM"].ToString() + " " + drRoll["S_STR"].ToString() + " " + drRoll["S_SFX"].ToString();
                        string situs2 = drRoll["S_CITY"].ToString() + " CA";
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }
            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }
            return result;
        }
        private void initialize()
        {
            fields.Add("ACCOUNT");
            fields.Add("TYPE");
            fields.Add("ROLL_YR");
            fields.Add("OWNER");
            fields.Add("DBA");
            fields.Add("CARE_OF");
            fields.Add("M_NUM");
            fields.Add("M_STR");
            fields.Add("M_SFX");
            fields.Add("M_CITY");
            fields.Add("M_ST");
            fields.Add("M_ZIP");
            fields.Add("S_NUM");
            fields.Add("S_STR");
            fields.Add("S_SFX");
            fields.Add("S_CITY");
            fields.Add("DESC");
            fields.Add("LAND");
            fields.Add("IMPR");
            fields.Add("FIXTR");
            fields.Add("PERSPROP");
            fields.Add("EXE_CD");
            fields.Add("EXE_AMT");

            //                     Length, Position, Name
            fields1.Add(new fixfield(20, 0, "ACCOUNT"));
            fields1.Add(new fixfield(4, 20, "ROLL_YR"));
            fields1.Add(new fixfield(40, 27, "OWNER"));
            fields1.Add(new fixfield(40, 67, "CARE_OF"));
            fields1.Add(new fixfield(12, 107, "M_NUM"));
            fields1.Add(new fixfield(32, 119, "M_STR"));
            fields1.Add(new fixfield(4, 151, "M_SFX"));
            fields1.Add(new fixfield(21, 155, "M_CITY"));
            fields1.Add(new fixfield(2, 176, "M_ST"));
            fields1.Add(new fixfield(9, 178, "M_ZIP"));
            fields1.Add(new fixfield(20, 207, "DESC"));
            fields1.Add(new fixfield(13, 227, "PERSPROP"));
            fields1.Add(new fixfield(3, 282, "EXE_CD"));
            fields1.Add(new fixfield(12, 285, "EXE_AMT"));

            fields2.Add(new fixfield(20, 0, "ACCOUNT"));
            fields2.Add(new fixfield(4, 20, "ROLL_YR"));
            fields2.Add(new fixfield(40, 27, "OWNER"));
            fields2.Add(new fixfield(40, 67, "DBA"));
            fields2.Add(new fixfield(40, 107, "CARE_OF"));
            fields2.Add(new fixfield(21, 147, "M_CITY"));
            fields2.Add(new fixfield(2, 168, "M_ST"));
            fields2.Add(new fixfield(9, 170, "M_ZIP"));
            fields2.Add(new fixfield(12, 179, "M_NUM"));
            fields2.Add(new fixfield(32, 191, "M_STR"));
            fields2.Add(new fixfield(3, 223, "M_SFX"));
            fields2.Add(new fixfield(30, 226, "DESC"));
            fields2.Add(new fixfield(9, 281, "LAND"));
            fields2.Add(new fixfield(9, 290, "IMPR"));
            fields2.Add(new fixfield(4, 308, "EXE_CD"));
            fields2.Add(new fixfield(9, 312, "EXE_AMT"));

            // Changed layout for 2017 and later processing
            fields3.Add(new fixfield(20, 0, "ACCOUNT"));
            fields3.Add(new fixfield(4, 20, "ROLL_YR"));
            fields3.Add(new fixfield(3, 27, "TYPE"));
            fields3.Add(new fixfield(40, 30, "OWNER"));
            fields3.Add(new fixfield(40, 70, "DBA"));
            fields3.Add(new fixfield(40, 110, "CARE_OF"));
            fields3.Add(new fixfield(12, 150, "M_NUM"));
            fields3.Add(new fixfield(32, 162, "M_STR"));
            fields3.Add(new fixfield(4, 194, "M_SFX"));
            fields3.Add(new fixfield(21, 198, "M_CITY"));
            fields3.Add(new fixfield(2, 219, "M_ST"));
            fields3.Add(new fixfield(5, 221, "M_ZIP"));
            fields3.Add(new fixfield(12, 226, "S_NUM"));
            fields3.Add(new fixfield(32, 238, "S_STR"));
            fields3.Add(new fixfield(4, 270, "S_SFX"));
            fields3.Add(new fixfield(21, 274, "S_CITY"));
            fields3.Add(new fixfield(12, 295, "PERSPROP"));
            fields3.Add(new fixfield(12, 307, "FIXTR"));
            fields3.Add(new fixfield(3, 319, "EXE_CD"));
            fields3.Add(new fixfield(12, 322, "EXE_AMT"));
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("APT", "101");
            stdTypes.Add("BIO", "101");
            stdTypes.Add("BLB", "101");
            stdTypes.Add("BUS", "101");
            stdTypes.Add("CHR", "101");
            stdTypes.Add("EXM", "504");
            stdTypes.Add("FIN", "101");
            stdTypes.Add("GOV", "500");
            stdTypes.Add("HST", "500");
            stdTypes.Add("INA", "101");
            stdTypes.Add("INS", "101");
            stdTypes.Add("LES", "101");
            stdTypes.Add("LSG", "103");
            stdTypes.Add("PTR", "101");
            stdTypes.Add("REF", "101");
        }
    }
}
