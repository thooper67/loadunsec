﻿using System;
using System.Configuration;
using System.IO;
using System.Data.Odbc;

namespace LoadUnsec
{
    class Ven : County
    {
        public Ven()
        {
            co3 = "VEN";
            coNum = "56";
            populateStdTypes();
        }

        #region Properties
        // Field names and positions from roll header
        //  0 PIN
        //  1 TaxYear
        //  2 AsmtType
        //  3 RollType
        //  4 Tag
        //  5 ClassCd
        //  6 CountyName
        //  7 PrimaryOwner
        //  8 Recipient
        //  9 DeliveryAddr
        // 10 LastLine
        // 11 SitusAddr
        // 12 SitusCity
        // 13 SitusPostalCd
        // 14 SitusComplete
        // 15 TotalFBYV
        // 16 HOX
        // 17 DVX
        // 18 LDVX
        // 19 OtherExmpt
        // 20 AssdLand
        // 21 AssdImp
        // 22 AssdPersonal
        // 23 AssdFixtures
        // 24 AssdLivImp
        // 25 AssessedFull
        // 26 TotalExmpt
        // 27 NetTaxable
        // 28 AttentionLine
        // 29 MailComplete
        // 30 MailCompleteWithAttention
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [Sheet1$] ORDER BY [PropertyNumber]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = fields["PropertyNumber"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.FEE_PARCEL_S = fields["ParcelNumber"].ToString();
                        thisRec.FEE_PARCEL_D = County.formattedNumber(fields["ParcelNumber"].ToString(), thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = fields["TaxRateArea"].ToString().PadLeft(6, '0');
                        thisRec.OWNER1 = cleanLine(fields["AssesseeName"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;
                        thisRec.OWNER2 = cleanLine(fields["SecondaryAssesseeName"].ToString());
                        thisRec.CARE_OF = cleanLine(fields["CareOf"].ToString());
                        if (fields["BusinessAccountNumber"].ToString() != "")
                        {
                            thisRec.ALT_APN = fields["BusinessAccountNumber"].ToString();
                        }
                        else if (fields["RegistrationNumber"].ToString() != "")
                        {
                            thisRec.ALT_APN = fields["RegistrationNumber"].ToString();
                        }

                        values myVals = new values();
                        myVals.LAND = fields["LandValue"].ToString();
                        myVals.IMPR = fields["ImprovementValue"].ToString();
                        myVals.FIXTR = fields["TradeFixtureValue"].ToString();
                        myVals.PERSPROP = fields["PersonalPropertyValue"].ToString();
                        myVals.GROWING = fields["TreesVinesValue"].ToString();
                        myVals.EXE_AMT = fields["ExemptionAmount"].ToString();
                        myVals.PENALTY = fields["PenaltyAmount"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROWING = myVals.GROWING;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;
                        thisRec.PENALTY = myVals.PENALTY;
                        thisRec.EXE_CD = fields["ExemptionTypeName"].ToString();

                        mailing myMail = new mailing(fields["Mail1"].ToString(), fields["Mail2"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        //situs mySitus = new situs(fields["SitusAddr"].ToString(), fields["SitusCity"].ToString() + " CA " + fields["SitusPostalCd"].ToString());
                        //thisRec.S_HSENO = mySitus.s_hseno;
                        //thisRec.S_STRNUM = mySitus.s_strnum;
                        //thisRec.S_STR_SUB = mySitus.s_str_sub;
                        //thisRec.S_DIR = mySitus.s_dir;
                        //thisRec.S_STREET = mySitus.s_street;
                        //thisRec.S_SUFF = mySitus.s_suff;
                        //thisRec.S_UNITNO = mySitus.s_unit_no;
                        //thisRec.S_CITY = mySitus.s_city;
                        //thisRec.S_ST = mySitus.s_st;
                        //thisRec.S_ZIP = mySitus.s_zip;
                        //thisRec.S_ZIP4 = mySitus.s_zip4;
                        //thisRec.S_ADDR_D = mySitus.s_addr_d;
                        //thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("001", "108");
            stdTypes.Add("002", "108");
            stdTypes.Add("003", "402");
            stdTypes.Add("004", "108");
            stdTypes.Add("005", "108");
            stdTypes.Add("006", "100");
            stdTypes.Add("007", "100");
            stdTypes.Add("008", "100");
            stdTypes.Add("009", "100");
            stdTypes.Add("010", "100");
            stdTypes.Add("011", "100");
            stdTypes.Add("012", "100");
            stdTypes.Add("013", "100");
            stdTypes.Add("014", "100");
            stdTypes.Add("015", "100");
            stdTypes.Add("016", "100");
            stdTypes.Add("017", "100");
            stdTypes.Add("018", "100");
            stdTypes.Add("019", "108");
            stdTypes.Add("020", "100");
            stdTypes.Add("021", "100");
            stdTypes.Add("022", "100");
            stdTypes.Add("023", "200");
            stdTypes.Add("024", "100");
            stdTypes.Add("025", "100");
            stdTypes.Add("026", "100");
            stdTypes.Add("027", "100");
            stdTypes.Add("028", "100");
            stdTypes.Add("029", "100");
            stdTypes.Add("030", "100");
            stdTypes.Add("031", "108");
            stdTypes.Add("032", "100");
            stdTypes.Add("033", "100");
            stdTypes.Add("034", "100");
            stdTypes.Add("035", "100");
            stdTypes.Add("036", "108");
            stdTypes.Add("037", "100");
            stdTypes.Add("038", "100");
            stdTypes.Add("039", "100");
            stdTypes.Add("040", "100");
            stdTypes.Add("041", "100");
            stdTypes.Add("042", "100");
            stdTypes.Add("043", "108");
            stdTypes.Add("044", "100");
            stdTypes.Add("045", "100");
            stdTypes.Add("046", "100");
            stdTypes.Add("047", "100");
            stdTypes.Add("048", "100");
            stdTypes.Add("049", "100");
            stdTypes.Add("050", "100");
            stdTypes.Add("051", "105");
            stdTypes.Add("052", "100");
            stdTypes.Add("053", "100");
            stdTypes.Add("054", "100");
            stdTypes.Add("055", "100");
            stdTypes.Add("056", "100");
            stdTypes.Add("057", "100");
            stdTypes.Add("058", "100");
            stdTypes.Add("059", "100");
            stdTypes.Add("060", "100");
            stdTypes.Add("061", "100");
            stdTypes.Add("062", "100");
            stdTypes.Add("063", "100");
            stdTypes.Add("064", "100");
            stdTypes.Add("065", "100");
            stdTypes.Add("066", "100");
            stdTypes.Add("067", "100");
            stdTypes.Add("068", "100");
            stdTypes.Add("069", "100");
            stdTypes.Add("070", "100");
            stdTypes.Add("071", "100");
            stdTypes.Add("072", "100");
            stdTypes.Add("073", "100");
            stdTypes.Add("074", "100");
            stdTypes.Add("075", "108");
            stdTypes.Add("076", "100");
            stdTypes.Add("077", "100");
            stdTypes.Add("078", "100");
            stdTypes.Add("079", "105");
            stdTypes.Add("080", "100");
            stdTypes.Add("081", "100");
            stdTypes.Add("082", "100");
            stdTypes.Add("083", "100");
            stdTypes.Add("084", "100");
            stdTypes.Add("085", "203");
            stdTypes.Add("086", "108");
            stdTypes.Add("087", "100");
            stdTypes.Add("088", "100");
            stdTypes.Add("089", "100");
            stdTypes.Add("090", "100");
            stdTypes.Add("091", "108");
            stdTypes.Add("092", "100");
            stdTypes.Add("093", "100");
            stdTypes.Add("094", "100");
            stdTypes.Add("095", "108");
            stdTypes.Add("096", "100");
            stdTypes.Add("097", "100");
            stdTypes.Add("098", "100");
            stdTypes.Add("099", "100");
            stdTypes.Add("100", "108");
            stdTypes.Add("101", "108");
            stdTypes.Add("102", "100");
            stdTypes.Add("103", "100");
            stdTypes.Add("104", "100");
            stdTypes.Add("105", "100");
            stdTypes.Add("106", "108");
            stdTypes.Add("107", "100");
            stdTypes.Add("108", "100");
            stdTypes.Add("109", "100");
            stdTypes.Add("110", "100");
            stdTypes.Add("111", "100");
            stdTypes.Add("112", "100");
            stdTypes.Add("113", "100");
            stdTypes.Add("114", "100");
            stdTypes.Add("115", "100");
            stdTypes.Add("116", "100");
            stdTypes.Add("117", "100");
            stdTypes.Add("118", "100");
            stdTypes.Add("119", "100");
            stdTypes.Add("120", "100");
            stdTypes.Add("121", "100");
            stdTypes.Add("122", "100");
            stdTypes.Add("123", "100");
            stdTypes.Add("124", "100");
            stdTypes.Add("125", "100");
            stdTypes.Add("126", "100");
            stdTypes.Add("127", "100");
            stdTypes.Add("128", "100");
            stdTypes.Add("129", "100");
            stdTypes.Add("130", "100");
            stdTypes.Add("131", "100");
            stdTypes.Add("132", "100");
            stdTypes.Add("133", "100");
            stdTypes.Add("134", "100");
            stdTypes.Add("135", "503");
            stdTypes.Add("136", "100");
            stdTypes.Add("137", "100");
            stdTypes.Add("138", "100");
            stdTypes.Add("139", "100");
            stdTypes.Add("140", "100");
            stdTypes.Add("141", "100");
            stdTypes.Add("142", "100");
            stdTypes.Add("143", "100");
            stdTypes.Add("144", "100");
            stdTypes.Add("145", "108");
            stdTypes.Add("146", "100");
            stdTypes.Add("147", "501");
            stdTypes.Add("148", "100");
            stdTypes.Add("149", "100");
            stdTypes.Add("150", "300");
            stdTypes.Add("151", "108");
            stdTypes.Add("157", "400");
            stdTypes.Add("160", "100");
            stdTypes.Add("161", "100");
            stdTypes.Add("162", "100");
            stdTypes.Add("163", "100");
            stdTypes.Add("164", "403");
            stdTypes.Add("165", "100");
            stdTypes.Add("166", "100");
            stdTypes.Add("167", "100");
            stdTypes.Add("200", "100");
            stdTypes.Add("800", "500");
            stdTypes.Add("888", "500");
            stdTypes.Add("890", "103");
            stdTypes.Add("897", "500");
            stdTypes.Add("898", "500");
            stdTypes.Add("899", "500");
            stdTypes.Add("901", "100");
            stdTypes.Add("902", "500");
            stdTypes.Add("940", "500");
            stdTypes.Add("941", "500");
            stdTypes.Add("942", "300");
            stdTypes.Add("943", "500");
            stdTypes.Add("944", "200");
            stdTypes.Add("945", "500");
            stdTypes.Add("946", "500");
            stdTypes.Add("960", "500");
            stdTypes.Add("961", "500");
            stdTypes.Add("962", "102");
            stdTypes.Add("965", "500");
            stdTypes.Add("AIR", "300");
            stdTypes.Add("BPP", "100");
            stdTypes.Add("MAR", "200");
        }
    }
}
