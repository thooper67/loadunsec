﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Security.Policy;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Smx : County
    {
        public Smx()
        {
            co3 = "SMX";
            coNum = "41";
            populateStdTypes();
        }

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Get file in a DataTable
                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_S = drRoll["Account Number"].ToString().PadLeft(10, '0');
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];
                        if (drRoll["APN: Parcel Number Legacy"].ToString() != "")
                        {
                            thisRec.FEE_PARCEL_S = drRoll["APN: Parcel Number Legacy"].ToString().PadLeft(9, '0');
                            thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        }
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = drRoll["Assessment Year"].ToString();
                        thisRec.TRA = drRoll["TRA"].ToString().PadLeft(6, '0');
                        thisRec.OWNER1 = cleanLine(drRoll["Owner Name 1"].ToString());
                        thisRec.OWNER2 = cleanLine(drRoll["Owner Name 2"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;
                        thisRec.DBA = cleanLine(drRoll["Property: Property Name / Doing Business As"].ToString());
                        thisRec.CARE_OF = cleanLine(drRoll["Care Of"].ToString());
                        thisRec.TYPE = drRoll["Property: Business Type Code"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.BILL_NUM = drRoll["Bill Number"].ToString();
                        thisRec.ALT_APN = drRoll["Tail Number/Vessel ID"].ToString();

                        values myVals = new values();
                        myVals.LAND = drRoll["Diff Land Assessed Value"].ToString();
                        myVals.IMPR = drRoll["Diff Improvement Assessed Value"].ToString();
                        myVals.FIXTR = drRoll["Diff Fixtures"].ToString();
                        myVals.PERSPROP = drRoll["Diff Personal Property Assessed Value"].ToString();
                        myVals.EXE_AMT = drRoll["Exemptions"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;
                        thisRec.EXE_CD = drRoll["Exemption Code Formatted"].ToString();

                        mailing myMail = new mailing(drRoll["Mailing Street"].ToString(), drRoll["Mailing City"].ToString() + " " + drRoll["Mailing State"].ToString() + " " + drRoll["Mailing Zip"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        situs mySitus = new situs(drRoll["Situs Street Num1"].ToString() + " " + drRoll["Situs Street Name"].ToString() + " " + drRoll["Situs Type"].ToString() + " " + drRoll["Situs Unit Number"].ToString(), drRoll["Situs City"].ToString() + " CA");
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("AGR", "502");
            stdTypes.Add("AHS", "502");
            stdTypes.Add("ALF", "108");
            stdTypes.Add("APT", "100");
            stdTypes.Add("BHO", "200");
            stdTypes.Add("BIA", "100");
            stdTypes.Add("BIL", "100");
            stdTypes.Add("BIN", "100");
            stdTypes.Add("BIO", "100");
            stdTypes.Add("BNA", "504");
            stdTypes.Add("BUS", "100");
            stdTypes.Add("CA", "301");
            stdTypes.Add("CAB", "107");
            stdTypes.Add("CEL", "503");
            stdTypes.Add("CHU", "100");
            stdTypes.Add("CSC", "103");
            stdTypes.Add("CSD", "104");
            stdTypes.Add("CTV", "100");
            stdTypes.Add("DA", "500");
            stdTypes.Add("DAB", "100");
            stdTypes.Add("DAC", "100");
            stdTypes.Add("DAW", "504");
            stdTypes.Add("DRG", "100");
            stdTypes.Add("FAC", "301");
            stdTypes.Add("FC", "106");
            stdTypes.Add("FCL", "106");
            stdTypes.Add("FH", "200");
            stdTypes.Add("FOA", "300");
            stdTypes.Add("GA", "300");
            stdTypes.Add("GAH", "300");
            stdTypes.Add("GLF", "100");
            stdTypes.Add("GRO", "100");
            stdTypes.Add("HB", "500");
            stdTypes.Add("HTL", "100");
            stdTypes.Add("LEB", "103");
            stdTypes.Add("LES", "103");
            stdTypes.Add("LHI", "501");
            stdTypes.Add("LHO", "501");
            stdTypes.Add("MBT", "200");
            stdTypes.Add("ORL", "100");
            stdTypes.Add("PBE", "504");
            stdTypes.Add("PBT", "202");
            stdTypes.Add("PI", "400");
            stdTypes.Add("PIA", "402");
            stdTypes.Add("PIB", "400");
            stdTypes.Add("PIE", "400");
            stdTypes.Add("PIO", "400");
            stdTypes.Add("PTV", "400");
            stdTypes.Add("QBT", "201");
            stdTypes.Add("REF", "500");
            stdTypes.Add("RPV", "501");
            stdTypes.Add("SHH", "100");
            stdTypes.Add("SHS", "100");
            stdTypes.Add("SHT", "100");
            stdTypes.Add("SST", "100");
            stdTypes.Add("TYC", "100");
            stdTypes.Add("VEB", "100");
            stdTypes.Add("VEN", "100");
            stdTypes.Add("VIS", "100");
            stdTypes.Add("WEL", "504");
        }

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                DataRow dr = table.NewRow();
                for (int i = 0; i < csv.FieldCount; i++)
                {
                    dr[i] = csv[i];
                }
                table.Rows.Add(dr);
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
    }
}
