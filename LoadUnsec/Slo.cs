﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Slo : County
    {
        public Slo()
        {
            co3 = "SLO";
            coNum = "40";
            populateStdTypes();
            populateValues();
        }

        #region Properties
        Dictionary<string, int> valueCodeDict = new Dictionary<string, int>();

        // Field names and positions from unsecured roll header
        //  0 ASSMT
        //  1 ROLL_YR
        //  2 VALUE_DATE
        //  3 TRA
        //  4 OSC
        //  5 ASSESSEE
        //  6 IN_CARE_OF
        //  7 ADDRESS_1
        //  8 ADDRESS_2
        //  9 ADDRESS_3
        // 10 CITY
        // 11 STATE
        // 12 ZIP
        // 13 LEGAL_DESC
        // 14 BASE_DATE
        // 15 SENIOR_FLAG
        // 16 ENROLL_FLAG
        // 17 LAND
        // 18 IMPR
        // 19 VALUE_CODE1
        // 20 VALUE_AMT1
        // 21 VALUE_FLAGS1
        // 22 VALUE_CODE2
        // 23 VALUE_AMT2
        // 24 VALUE_FLAGS2
        // 25 VALUE_CODE3
        // 26 VALUE_AMT3
        // 27 VALUE_FLAGS3
        // 28 EXEMP_CODE1
        // 29 EXEMP_AMT1
        // 31 EXEMP_CODE2
        // 32 EXEMP_AMT2
        // 34 EXEMP_CODE3
        // 35 EXEMP_AMT3
        // 36 SITUS_NUM
        // 37 SITUS_DIRECTION
        // 38 SITUS_STREET
        // 39 SITUS_TYPE
        // 40 SITUS_UNIT
        // 41 FEE_ASSMT
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [PTS Query$] ORDER BY [ASSMT]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = fields["ASSMT"].ToString();
                        thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                        thisRec.FEE_PARCEL_S = fields["FEE_ASSMT"].ToString();
                        thisRec.FEE_PARCEL_D = formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = fields["ROLL_YR"].ToString().Substring(0, 4);
                        thisRec.TRA = fields["TRA"].ToString();
                        thisRec.LEGAL = cleanLine(fields["LEGAL_DESC"].ToString());
                        thisRec.TYPE = fields["ASSMT"].ToString().Substring(0, 3);
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                        values myVals = new values();
                        for (int i = 1; i < 16; i++)
                        {
                            myVals = includeValue(myVals, fields["VALUE_CODE" + i.ToString()].ToString(), fields["VALUE_AMT" + i.ToString()].ToString());
                        }
                        myVals.EXE_AMT = addStrings(fields["EXEMP_AMT1"].ToString(), fields["EXEMP_AMT2"].ToString(), fields["EXEMP_AMT3"].ToString());
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string addrLine1 = "";
                        int ptrTop = 2;
                        int ptrBot = 0;
                        bool addr1Asgn = false;
                        string addr1 = fields["ADDRESS_1"].ToString();
                        string addr2 = fields["ADDRESS_2"].ToString();
                        string addr3 = fields["ADDRESS_3"].ToString();
                        string[] ins = new string[3] { addr1, addr2, addr3 };

                        // Assigns C/O & Addr1
                        thisRec.OWNER1 = cleanLine(fields["ASSESSEE"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;

                        Regex re = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Match m = re.Match(fields["IN_CARE_OF"].ToString());
                        if (m.Success)
                        {
                            Group g2 = m.Groups[2];
                            thisRec.CARE_OF = cleanLine(g2.ToString());
                        }
                        else
                        {
                            thisRec.CARE_OF = cleanLine(fields["IN_CARE_OF"].ToString());
                        }

                        while (ptrBot <= ptrTop)
                        {
                            Regex re1 = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                            Regex re2 = new Regex(@"^([^\d]+)$", RegexOptions.IgnoreCase);
                            Regex re3 = new Regex(@"^(P\.?\s?O\.?\s*B.?X|P\.?\s?O\.?\s+DRAWER)(.+)$", RegexOptions.IgnoreCase);
                            Regex re4 = new Regex(@"^(\d+.+)$", RegexOptions.IgnoreCase);
                            Match m1 = re1.Match(ins[ptrBot]);
                            Match m2 = re2.Match(ins[ptrBot]);
                            Match m3 = re3.Match(ins[ptrBot]);
                            Match m4 = re4.Match(ins[ptrBot]);

                            if (m1.Success)
                            {
                                Group g2 = m1.Groups[2];
                                thisRec.CARE_OF += ' ' + cleanLine(g2.ToString());
                                ptrBot++;
                            }
                            else
                            {
                                if (m2.Success)
                                {
                                    if (addr1Asgn == true)
                                    {
                                        Group g1 = m2.Groups[1];
                                        addrLine1 += ' ' + g1.ToString();
                                    }
                                    else
                                    {
                                        Group g1 = m2.Groups[1];
                                        thisRec.CARE_OF += ' ' + cleanLine(g1.ToString());
                                    }
                                }

                                if (m3.Success)
                                {
                                    addrLine1 = ins[ptrBot];
                                    ptrBot++;
                                }

                                if (m4.Success)
                                {
                                    addrLine1 += ' ' + ins[ptrBot];
                                    addr1Asgn = true;
                                    ptrBot++;
                                }

                                else
                                {
                                    ptrBot++;
                                }
                            }
                        }

                        string myState = "";
                        if (fields["STATE"].ToString() != "FR") myState = fields["STATE"].ToString();
                        string myZip = "";
                        if (fields["ZIP"].ToString().Trim() != "FR")
                        {
                            if (fields["ZIP"].ToString().Trim().Length > 8)
                            {
                                myZip = fields["ZIP"].ToString().Substring(0, 5) + "-" + fields["ZIP"].ToString().Substring(5, 4);
                            }
                            else
                            {
                                myZip = fields["ZIP"].ToString();
                            }
                        }
                        string mail1 = addrLine1;
                        string mail2 = fields["CITY"].ToString() + " " + myState + " " + myZip;
                        mailing myMail = new mailing(mail1, mail2, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string myHouse = "";
                        if (fields["SITUS_NUM"].ToString() != "99999") myHouse = fields["SITUS_NUM"].ToString();

                        string myUnit = fields["SITUS_UNIT"].ToString();
                        re = new Regex(@"^0*(.+)", RegexOptions.IgnoreCase);
                        m = re.Match(myUnit);
                        if (m.Success)
                        {
                            Group g1 = m.Groups[1];
                            myUnit = cleanLine(g1.ToString());
                        }
                        string situs1 = myHouse + " " + fields["SITUS_DIRECTION"].ToString().Trim() + " " + fields["SITUS_STREET"].ToString().Trim() + " " + fields["SITUS_TYPE"].ToString();
                        string situs2 = "CA";
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = myUnit;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d + " " + myUnit;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }

        private values includeValue(values myRec, string code, string value)
        {
            if ((string.IsNullOrEmpty(code.Trim())) && (value == "0"))
            {
                // Ignore code if null or empty & value if equal to zero.
            }
            else if (valueCodeDict.ContainsKey(code))
            {
                switch (valueCodeDict[code])
                {
                    case 1:
                        myRec.IMPR = addStrings(myRec.IMPR, value);
                        break;
                    case 2:
                        myRec.FIXTR = addStrings(myRec.FIXTR, value);
                        break;
                    case 3:
                        myRec.PERSPROP = addStrings(myRec.PERSPROP, value);
                        break;
                }
            }
            else
            {
                log("Unknown value code: '" + code + "'");
            }
            return myRec;
        }
#endregion

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("806", "103");
            stdTypes.Add("807", "100");
            stdTypes.Add("808", "106");
            stdTypes.Add("809", "100");
            stdTypes.Add("810", "502");
            stdTypes.Add("815", "501");
            stdTypes.Add("817", "502");
            stdTypes.Add("818", "502");
            stdTypes.Add("820", "300");
            stdTypes.Add("821", "402");
            stdTypes.Add("830", "200");
            stdTypes.Add("831", "203");
        }

        private void populateValues()
        {
            valueCodeDict.Add("61", 3);
            valueCodeDict.Add("63", 3);
            valueCodeDict.Add("65", 3);
            valueCodeDict.Add("67", 3);
            valueCodeDict.Add("69", 3);
            valueCodeDict.Add("71", 2);
            valueCodeDict.Add("73", 1);
        }

    }
}
