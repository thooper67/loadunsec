﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Sbt : Megabyte
    {
        public Sbt()
        {
            co3 = "SBT";
            coNum = "35";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
        }
    }
}
