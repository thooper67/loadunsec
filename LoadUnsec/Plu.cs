﻿using System;
using System.Configuration;
using System.Data;
using System.IO;

using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Plu : County
    {
        public Plu()
        {
            co3 = "PLU";
            coNum = "32";
            populateStdTypes();
        }

        #region Propertes
        // Layouts for three files used: main unsecured roll, unsecured situs and daily roll for legal description.
        // Field names and positions from Plumas County_Unsecured header
        //  0 FeeParcel
        //  1 Asmt
        //  2 TRA
        //  3 TaxabilityFull
        //  4 CreatingDocNum
        //  5 CurrentDocNum
        //  6 LandUse1
        //  7 LandUse2
        //  8 AssesseeName
        //  9 Address1
        // 10 Address2
        // 11 Address3
        // 12 Address4
        // 13 ZipForSortOnly
        // 14 eLand
        // 15 eImpr
        // 16 ePprop
        // 17 eHoe
        // 18 eNonHoe

        // Field names and positions from Plumas County_UnsecuredSitus header
        //  0 Asmt
        //  1 Sequence
        //  2 FormattedSitus1
        //  3 FormattedSitus2

        // Field names and positions from plu_Roll header
        // 0 Asmt
        // 1 FeeParcel
        // 2 TRA
        // 3 AsmtDescription

        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            int iSitusCount = 0;
            int iLegalCount = 0;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;
            CsvReader csv2 = null;
            CsvReader csv3 = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }
            string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
            if (File.Exists(srcFile2))
            {
                try
                {
                    csv2 = new CsvReader(new StreamReader(srcFile2), true);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile2 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile2 + "' does not exist.";
                log(result);
                return result;
            }
            string srcFile3 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file3"]);
            if (File.Exists(srcFile3))
            {
                try
                {
                    csv3 = new CsvReader(new StreamReader(srcFile3), true, '|');
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile3 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile3 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Get files in their own DataTables
                DataTable dtRoll = getTable2(csv1);
                DataTable dtSitus = getTable(csv2);
                DataTable dtLegal = getTable(csv3);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        // Keep reading situs records until we find a match, or realize there isn't a match.
                        bool bSitusMatch = false;
                        string sRollAsmt = drRoll["Asmt"].ToString();
                        string sSitusAsmt = dtSitus.Rows[iSitusCount]["Asmt"].ToString();
                        while ((iSitusCount < dtSitus.Rows.Count - 1) && (sRollAsmt.CompareTo(sSitusAsmt) > 0))
                        {
                            iSitusCount++;
                            sSitusAsmt = dtSitus.Rows[iSitusCount]["Asmt"].ToString();
                        }
                        if (sRollAsmt.CompareTo(sSitusAsmt) == 0) bSitusMatch = true;

                        thisRec.APN_S = drRoll["Asmt"].ToString();
                        thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                        thisRec.FEE_PARCEL_S = drRoll["FeeParcel"].ToString();
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = drRoll["TRA"].ToString();
                        thisRec.OWNER1 = cleanLine(drRoll["AssesseeName"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;
                        thisRec.EXE_CD = "";
                        thisRec.TYPE = drRoll["Asmt"].ToString().Substring(0, 3);
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                        // Keep reading legal records until we find a match, or realize there isn't a match.
                        bool bLegalMatch = false;
                        string sLegalAsmt = dtLegal.Rows[iLegalCount]["Asmt"].ToString();
                        while ((iLegalCount < dtLegal.Rows.Count - 1) && (sRollAsmt.CompareTo(sLegalAsmt) > 0))
                        {
                            iLegalCount++;
                            sLegalAsmt = dtLegal.Rows[iLegalCount]["Asmt"].ToString();
                        }
                        if (sRollAsmt.CompareTo(sLegalAsmt) == 0) bLegalMatch = true;
                        if (bLegalMatch)
                        {
                            thisRec.LEGAL = cleanLine(dtLegal.Rows[iLegalCount]["AsmtDescription"].ToString());
                        }

                        values myVals = new values();
                        myVals.LAND = drRoll["eLand"].ToString();
                        myVals.IMPR = drRoll["eImpr"].ToString();
                        myVals.PERSPROP = drRoll["ePprop"].ToString();
                        myVals.HO_EXE = drRoll["eHoe"].ToString();
                        myVals.OTHER_EXE = drRoll["eNonHoe"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        mailing myMail = new mailing(drRoll["Address1"].ToString(), drRoll["Address2"].ToString(), drRoll["Address3"].ToString(), drRoll["Address4"].ToString());
                        thisRec.DBA = myMail.dba;
                        thisRec.CARE_OF = myMail.care_of;
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        if (bSitusMatch)
                        {
                            string situs1 = "";
                            if ((dtSitus.Rows[iSitusCount]["FormattedSitus1"].ToString().Length > 0) && (dtSitus.Rows[iSitusCount]["FormattedSitus1"].ToString().Substring(0, 1) != "0")) situs1 = dtSitus.Rows[iSitusCount]["FormattedSitus1"].ToString();
                            situs mySitus = new situs(situs1, dtSitus.Rows[iSitusCount]["FormattedSitus2"].ToString());
                            thisRec.S_HSENO = mySitus.s_hseno;
                            thisRec.S_STRNUM = mySitus.s_strnum;
                            thisRec.S_STR_SUB = mySitus.s_str_sub;
                            thisRec.S_DIR = mySitus.s_dir;
                            thisRec.S_STREET = mySitus.s_street;
                            thisRec.S_SUFF = mySitus.s_suff;
                            thisRec.S_UNITNO = mySitus.s_unit_no;
                            thisRec.S_CITY = mySitus.s_city;
                            thisRec.S_ST = mySitus.s_st;
                            thisRec.S_ZIP = mySitus.s_zip;
                            thisRec.S_ZIP4 = mySitus.s_zip4;
                            thisRec.S_ADDR_D = mySitus.s_addr_d;
                            thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;
                        }

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("800", "104"); // BUSINESS ASSESSMENTS
            stdTypes.Add("820", "300");
            stdTypes.Add("821", "402");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("855", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("862", "401");
            stdTypes.Add("864", "502");
            stdTypes.Add("868", "402");
            stdTypes.Add("870", "503");
            stdTypes.Add("880", "503");
        }

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                string myBook = csv[0].Substring(0, 3);
                if (stdTypes.ContainsKey(myBook))
                {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
                }
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }

        private DataTable getTable2(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                string myBook = csv[1].Substring(0, 3);
                if (stdTypes.ContainsKey(myBook))
                {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
                }
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[1];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
    }
}
