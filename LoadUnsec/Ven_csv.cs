﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Ven_csv : County
    {
        public Ven_csv()
        {
            co3 = "VEN";
            coNum = "56";
            populateStdTypes();
        }

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Get file in a DataTable
                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        if (drRoll["BusinessAccountNumber"].ToString() != "")
                        {
                            thisRec.APN_S = drRoll["BusinessAccountNumber"].ToString();
                        }
                        else if (drRoll["MarineAirAccountNumber"].ToString() != "")
                        {
                            thisRec.APN_S = drRoll["MarineAirAccountNumber"].ToString();
                        }
                        else 
                        {
                            thisRec.APN_S = drRoll["ParcelNumber"].ToString();
                        }
                        thisRec.APN_D = thisRec.APN_S;
                        if (drRoll["BusinessAccountNumber"].ToString() != "" || drRoll["MarineAirAccountNumber"].ToString() != "")
                        {
                            thisRec.FEE_PARCEL_S = drRoll["ParcelNumber"].ToString();
                            thisRec.FEE_PARCEL_D = County.formattedNumber(drRoll["ParcelNumber"].ToString(), thisRec.FEE_PARCEL_D_format);
                        }
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = drRoll["TaxRateArea"].ToString().PadLeft(6, '0');
                        thisRec.OWNER1 = cleanLine(drRoll["PrimaryOwnerName"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;
                        thisRec.DBA = cleanLine(drRoll["DBA"].ToString());
                        thisRec.CARE_OF = cleanLine(drRoll["CareOf"].ToString());
                        thisRec.LEGAL = cleanLine(drRoll["Location"].ToString());
                        if (drRoll["StatementTypeName"].ToString() != "")
                        {
                            thisRec.TYPE = drRoll["StatementTypeName"].ToString();
                        }
                        else if (drRoll["MarineAirStatementTypeName"].ToString() != "")
                        {
                            thisRec.TYPE = drRoll["MarineAirStatementTypeName"].ToString();
                        }
                        else
                        {
                            thisRec.TYPE = drRoll["ParcelTypeName"].ToString();
                        }
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.ALT_APN = drRoll["AssessmentNumber"].ToString();

                        values myVals = new values();
                        myVals.LAND = drRoll["Land Value"].ToString();
                        myVals.IMPR = drRoll["Imp Value"].ToString();
                        if (drRoll["TF Value"].ToString() == "" && drRoll["Tree Value"].ToString() == "" && drRoll["PP Value"].ToString() == "")
                        {
                            myVals.PERSPROP = drRoll["FullCashValue"].ToString();
                        }
                        else
                        {
                            myVals.FIXTR = drRoll["TF Value"].ToString();
                            myVals.GROWING = drRoll["Tree Value"].ToString();
                            myVals.PERSPROP = drRoll["PP Value"].ToString();
                        }
                        myVals.PENALTY = drRoll["PenaltyAmount"].ToString();
                        myVals.EXE_AMT = drRoll["ExemptionAmount"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.GROWING = myVals.GROWING;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.PENALTY = myVals.PENALTY;
                        thisRec.EXE_AMT = myVals.EXE_AMT;
                        thisRec.EXE_CD = drRoll["ExemptionTypeName"].ToString();

                        mailing myMail = new mailing(drRoll["Mail1"].ToString(), drRoll["Mail2"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        situs mySitus = new situs(drRoll["Situs1"].ToString(), drRoll["Situs2"].ToString());
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("AGRICULTURE", "502");
            stdTypes.Add("AGRICULTURE-BEES", "502");
            stdTypes.Add("AGRICULTURE-HORSES", "502");
            stdTypes.Add("APARTMENT HOUSE", "100");
            stdTypes.Add("BOAT OVER 100,000", "200");
            stdTypes.Add("BOAT UNDER 100,000", "200");
            stdTypes.Add("BUSINESS IN-HOME (RESIDENCE)", "100");
            stdTypes.Add("BUSINESS PROPERTY STATEMENT", "100");
            stdTypes.Add("BUSINESS WELFARE", "100");
            stdTypes.Add("CABLE TV", "503");
            stdTypes.Add("CELL TOWERS/CELL TENANTS", "503");
            stdTypes.Add("CHARTER/TAXI", "201");
            stdTypes.Add("CHARTER/TAXI > 50%", "201");
            stdTypes.Add("CO-GENERATION", "500");
            stdTypes.Add("COMMERCIAL FISHERS", "201");
            stdTypes.Add("COMMERCIAL TRANSPORT", "201");
            stdTypes.Add("FICTITIOUS SEPARATE", "500");
            stdTypes.Add("FINANCIAL INSTITUTIONS", "106");
            stdTypes.Add("FRACTIONAL AIRCRAFT", "300");
            stdTypes.Add("GENERAL AIRCRAFT", "300");
            stdTypes.Add("GENERAL BOAT", "200");
            stdTypes.Add("GOVERNMENT LEASE/PI", "400");
            stdTypes.Add("HISTORICAL AIRCRAFT", "300");
            stdTypes.Add("JETSKI", "202");
            stdTypes.Add("LEASED EQUIP-LESSEE NO STMT", "103");
            stdTypes.Add("LEASED EQUIPMENT, REGULAR", "103");
            stdTypes.Add("MINERAL RIGHTS EQUIPMENT", "104");
            stdTypes.Add("MOBILE EQUIPMENT", "104");
            stdTypes.Add("MULTIPLES", "500");
            stdTypes.Add("RESEARCH VESSELS", "201");
            stdTypes.Add("RUBBISH", "500");
            stdTypes.Add("SERVICE MEMBER BOAT", "200");
            stdTypes.Add("SERVICE STATION", "100");
        }

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                DataRow dr = table.NewRow();
                for (int i = 0; i < csv.FieldCount; i++)
                {
                    dr[i] = csv[i];
                }
                table.Rows.Add(dr);
            }
            DataView view = table.DefaultView;

            return view.ToTable();
        }
    }
}
