﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Ker : County
    {
        public Ker()
        {
            co3 = "KER";
            coNum = "15";
            populateStdTypes();
            populateCities();
        }

        #region Properties
        Dictionary<string, string> cities = new Dictionary<string, string>();
        // Field names and positions from roll header
        //  0 ID
        //  1 TE NO
        //  2 ASSESSEE NO      
        //  3 TRA NO      
        //  4 ATN      
        //  5 FILE NO 
        //  6 USE CODE
        //  7 EXEMPT TYPE CODE                                   
        //  8 OWNER NAME
        //  9 OWNER ADDR L1  
        // 10 OWNER ADDR L2  
        // 11 ZIP CODE 
        // 12 LAND ACRES         
        // 13 CITRUS ACRES  
        // 14 MINERAL VALUE     
        // 15 LAND VALUE     
        // 16 IM VALUE    
        // 17 OI VALUE     
        // 18 PP VALUE
        // 19 EXEMPT VALUE
        // 20 NET VALUE
        // 21 ROLL TYPE CODE
        // 22 SUPERV DIST NO             
        // 23 SITUS ADDR                                                       
        // 24 LEGAL DESCRIPTION
        // 25 IN CARE OF NAME                                                 
        // 26 DBA NAME                              
        // 27 AG PRESERVE                                               
        // 28 OWNER ADDR L3                         
        // 29 Field36
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [HDL 2024$] WHERE [ROLL TYPE CODE] = '4' ORDER BY [FILE NO]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = fields["FILE NO"].ToString();
                        thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                        thisRec.FEE_PARCEL_S = fields["ATN"].ToString();
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = fields["TRA NO"].ToString();
                        thisRec.OWNER1 = cleanLine(fields["OWNER NAME"].ToString());
                        thisRec.CARE_OF = cleanLine(fields["IN CARE OF  NAME"].ToString());
                        thisRec.DBA = cleanLine(fields["DBA NAME"].ToString());
                        thisRec.EXE_CD = fields["EXMPT TYPE CODE"].ToString();
                        thisRec.LEGAL = fields["LEGAL DESCRIPTION"].ToString();
                        thisRec.TYPE = fields["USE CODE"].ToString();
                        thisRec.TYPE_STD = xlatType(fields["USE CODE"].ToString());
                        thisRec.ALT_APN = fields["TE NO"].ToString();

                        values myVals = new values();
                        myVals.LAND = fields["LAND VALUE"].ToString();
                        myVals.IMPR = fields["IM VALUE"].ToString();
                        myVals.FIXTR = fields["OI VALUE"].ToString();
                        myVals.PERSPROP = fields["PP VALUE"].ToString();
                        myVals.EXE_AMT = fields["EXMPT VALUE"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        mailing myMail = new mailing(fields["OWNER ADDR L1"].ToString(), fields["OWNER ADDR L2"].ToString(), fields["OWNER ADDR L3"].ToString(), "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string addr1 = fields["SITUS ADDR"].ToString();
                        string addr2 = "";
                        foreach (KeyValuePair<string, string> city in cities)
                        {
                            Regex reCity = new Regex("(.*)(" + city.Key + @")\s*$", RegexOptions.IgnoreCase);
                            Match mCity = reCity.Match(fields["SITUS ADDR"].ToString());
                            if (mCity.Success)
                            {
                                Group gSitus = mCity.Groups[1];
                                addr1 = gSitus.ToString();
                                addr2 = city.Value + ", CA";
                                break;
                            }
                        }
                        situs mySitus = new situs(addr1, addr2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("0000", "500");
            stdTypes.Add("0010", "500");
            stdTypes.Add("0101", "500");
            stdTypes.Add("0106", "500");
            stdTypes.Add("0186", "500");
            stdTypes.Add("0500", "500");
            stdTypes.Add("0503", "500");
            stdTypes.Add("0700", "500");
            stdTypes.Add("1020", "500");
            stdTypes.Add("1100", "100");
            stdTypes.Add("1111", "100");
            stdTypes.Add("1116", "100");
            stdTypes.Add("1119", "100");
            stdTypes.Add("1120", "100");
            stdTypes.Add("1123", "100");
            stdTypes.Add("1124", "100");
            stdTypes.Add("1186", "100");
            stdTypes.Add("1190", "100");
            stdTypes.Add("1200", "100");
            stdTypes.Add("1201", "100");
            stdTypes.Add("1202", "100");
            stdTypes.Add("1203", "100");
            stdTypes.Add("1300", "100");
            stdTypes.Add("1301", "100");
            stdTypes.Add("1308", "100");
            stdTypes.Add("1309", "100");
            stdTypes.Add("1310", "100");
            stdTypes.Add("1312", "100");
            stdTypes.Add("1316", "100");
            stdTypes.Add("1386", "100");
            stdTypes.Add("1390", "100");
            stdTypes.Add("1400", "100");
            stdTypes.Add("1401", "100");
            stdTypes.Add("1402", "100");
            stdTypes.Add("1403", "100");
            stdTypes.Add("1404", "100");
            stdTypes.Add("1405", "100");
            stdTypes.Add("1407", "100");
            stdTypes.Add("1490", "100");
            stdTypes.Add("1500", "100");
            stdTypes.Add("1501", "100");
            stdTypes.Add("1502", "100");
            stdTypes.Add("1600", "100");
            stdTypes.Add("1601", "100");
            stdTypes.Add("1602", "100");
            stdTypes.Add("1603", "100");
            stdTypes.Add("1604", "108");
            stdTypes.Add("1605", "108");
            stdTypes.Add("1606", "108");
            stdTypes.Add("1607", "108");
            stdTypes.Add("1613", "108");
            stdTypes.Add("1614", "100");
            stdTypes.Add("1690", "100");
            stdTypes.Add("1700", "108");
            stdTypes.Add("1701", "108");
            stdTypes.Add("1702", "108");
            stdTypes.Add("1703", "108");
            stdTypes.Add("1704", "108");
            stdTypes.Add("1705", "108");
            stdTypes.Add("1706", "108");
            stdTypes.Add("1707", "108");
            stdTypes.Add("1708", "108");
            stdTypes.Add("1710", "108");
            stdTypes.Add("1712", "108");
            stdTypes.Add("1713", "108");
            stdTypes.Add("1714", "108");
            stdTypes.Add("1716", "500");
            stdTypes.Add("1717", "500");
            stdTypes.Add("1718", "500");
            stdTypes.Add("1720", "108");
            stdTypes.Add("1790", "501");
            stdTypes.Add("1800", "100");
            stdTypes.Add("1801", "100");
            stdTypes.Add("1802", "100");
            stdTypes.Add("1803", "100");
            stdTypes.Add("1804", "100");
            stdTypes.Add("1805", "100");
            stdTypes.Add("1806", "100");
            stdTypes.Add("1807", "100");
            stdTypes.Add("1808", "100");
            stdTypes.Add("1890", "100");
            stdTypes.Add("1899", "100");
            stdTypes.Add("1900", "100");
            stdTypes.Add("1901", "100");
            stdTypes.Add("1902", "100");
            stdTypes.Add("1903", "100");
            stdTypes.Add("1904", "100");
            stdTypes.Add("1905", "100");
            stdTypes.Add("1906", "100");
            stdTypes.Add("1907", "100");
            stdTypes.Add("1908", "500");
            stdTypes.Add("1909", "100");
            stdTypes.Add("1910", "100");
            stdTypes.Add("1911", "100");
            stdTypes.Add("1912", "100");
            stdTypes.Add("1913", "100");
            stdTypes.Add("1914", "100");
            stdTypes.Add("1917", "100");
            stdTypes.Add("1919", "100");
            stdTypes.Add("1990", "501");
            stdTypes.Add("2100", "100");
            stdTypes.Add("2101", "100");
            stdTypes.Add("2102", "100");
            stdTypes.Add("2103", "100");
            stdTypes.Add("2190", "501");
            stdTypes.Add("2200", "100");
            stdTypes.Add("2201", "100");
            stdTypes.Add("2202", "100");
            stdTypes.Add("2203", "100");
            stdTypes.Add("2204", "100");
            stdTypes.Add("2205", "100");
            stdTypes.Add("2206", "100");
            stdTypes.Add("2207", "100");
            stdTypes.Add("2208", "100");
            stdTypes.Add("2290", "100");
            stdTypes.Add("2300", "100");
            stdTypes.Add("2301", "100");
            stdTypes.Add("2302", "100");
            stdTypes.Add("2303", "100");
            stdTypes.Add("2304", "100");
            stdTypes.Add("2305", "100");
            stdTypes.Add("2390", "100");
            stdTypes.Add("2400", "100");
            stdTypes.Add("2401", "100");
            stdTypes.Add("2402", "100");
            stdTypes.Add("2403", "100");
            stdTypes.Add("2404", "100");
            stdTypes.Add("2405", "100");
            stdTypes.Add("2406", "100");
            stdTypes.Add("2490", "100");
            stdTypes.Add("2501", "100");
            stdTypes.Add("2590", "100");
            stdTypes.Add("2600", "100");
            stdTypes.Add("2601", "100");
            stdTypes.Add("2602", "100");
            stdTypes.Add("2603", "100");
            stdTypes.Add("2604", "100");
            stdTypes.Add("2605", "100");
            stdTypes.Add("2606", "100");
            stdTypes.Add("2607", "100");
            stdTypes.Add("2608", "100");
            stdTypes.Add("2609", "100");
            stdTypes.Add("2690", "100");
            stdTypes.Add("2700", "100");
            stdTypes.Add("2701", "100");
            stdTypes.Add("2705", "100");
            stdTypes.Add("2790", "100");
            stdTypes.Add("2800", "100");
            stdTypes.Add("2801", "100");
            stdTypes.Add("2890", "100");
            stdTypes.Add("2900", "100");
            stdTypes.Add("2901", "100");
            stdTypes.Add("2990", "501");
            stdTypes.Add("3000", "500");
            stdTypes.Add("3019", "500");
            stdTypes.Add("3020", "501");
            stdTypes.Add("3050", "500");
            stdTypes.Add("3080", "500");
            stdTypes.Add("3086", "500");
            stdTypes.Add("3100", "100");
            stdTypes.Add("3101", "100");
            stdTypes.Add("3102", "100");
            stdTypes.Add("3103", "100");
            stdTypes.Add("3104", "100");
            stdTypes.Add("3105", "100");
            stdTypes.Add("3106", "100");
            stdTypes.Add("3107", "100");
            stdTypes.Add("3108", "100");
            stdTypes.Add("3120", "100");
            stdTypes.Add("3180", "100");
            stdTypes.Add("3186", "100");
            stdTypes.Add("3190", "100");
            stdTypes.Add("3200", "100");
            stdTypes.Add("3201", "100");
            stdTypes.Add("3202", "100");
            stdTypes.Add("3203", "100");
            stdTypes.Add("3205", "100");
            stdTypes.Add("3206", "100");
            stdTypes.Add("3290", "100");
            stdTypes.Add("3300", "100");
            stdTypes.Add("3301", "100");
            stdTypes.Add("3400", "100");
            stdTypes.Add("3401", "100");
            stdTypes.Add("3402", "100");
            stdTypes.Add("3490", "100");
            stdTypes.Add("3500", "100");
            stdTypes.Add("3501", "100");
            stdTypes.Add("3502", "100");
            stdTypes.Add("3503", "100");
            stdTypes.Add("3504", "100");
            stdTypes.Add("3505", "100");
            stdTypes.Add("3506", "100");
            stdTypes.Add("3507", "100");
            stdTypes.Add("3508", "100");
            stdTypes.Add("3509", "100");
            stdTypes.Add("3590", "100");
            stdTypes.Add("3600", "100");
            stdTypes.Add("3601", "100");
            stdTypes.Add("3602", "100");
            stdTypes.Add("3603", "100");
            stdTypes.Add("3604", "100");
            stdTypes.Add("3605", "100");
            stdTypes.Add("3700", "100");
            stdTypes.Add("3701", "100");
            stdTypes.Add("3702", "100");
            stdTypes.Add("3703", "100");
            stdTypes.Add("3704", "105");
            stdTypes.Add("3705", "100");
            stdTypes.Add("3706", "100");
            stdTypes.Add("3707", "100");
            stdTypes.Add("3708", "100");
            stdTypes.Add("3709", "100");
            stdTypes.Add("3710", "100");
            stdTypes.Add("3720", "100");
            stdTypes.Add("3780", "100");
            stdTypes.Add("3790", "100");
            stdTypes.Add("3800", "100");
            stdTypes.Add("3801", "100");
            stdTypes.Add("3880", "100");
            stdTypes.Add("3890", "100");
            stdTypes.Add("3900", "402");
            stdTypes.Add("3901", "500");
            stdTypes.Add("3902", "503");
            stdTypes.Add("3960", "503");
            stdTypes.Add("3961", "503");
            stdTypes.Add("3962", "503");
            stdTypes.Add("3964", "403");
            stdTypes.Add("3965", "503");
            stdTypes.Add("3966", "503");
            stdTypes.Add("3968", "503");
            stdTypes.Add("3969", "503");
            stdTypes.Add("3984", "503");
            stdTypes.Add("4100", "502");
            stdTypes.Add("4101", "502");
            stdTypes.Add("4103", "502");
            stdTypes.Add("4110", "502");
            stdTypes.Add("4120", "502");
            stdTypes.Add("4121", "502");
            stdTypes.Add("4136", "502");
            stdTypes.Add("4150", "502");
            stdTypes.Add("4160", "502");
            stdTypes.Add("4200", "502");
            stdTypes.Add("4210", "502");
            stdTypes.Add("4300", "502");
            stdTypes.Add("4301", "502");
            stdTypes.Add("4390", "502");
            stdTypes.Add("4400", "502");
            stdTypes.Add("4500", "502");
            stdTypes.Add("4600", "502");
            stdTypes.Add("4700", "502");
            stdTypes.Add("4800", "502");
            stdTypes.Add("4801", "502");
            stdTypes.Add("4802", "502");
            stdTypes.Add("4806", "500");
            stdTypes.Add("4900", "502");
            stdTypes.Add("4908", "502");
            stdTypes.Add("6030", "504");
            stdTypes.Add("6040", "504");
            stdTypes.Add("6050", "504");
            stdTypes.Add("6100", "503");
            stdTypes.Add("6400", "503");
            stdTypes.Add("6500", "503");
            stdTypes.Add("8100", "100");
            stdTypes.Add("8101", "100");
            stdTypes.Add("8102", "100");
            stdTypes.Add("8103", "100");
            stdTypes.Add("8104", "100");
            stdTypes.Add("8105", "100");
            stdTypes.Add("8201", "500");
            stdTypes.Add("8203", "403");
            stdTypes.Add("8204", "403");
            stdTypes.Add("8209", "100");
            stdTypes.Add("8300", "503");
            stdTypes.Add("8301", "503");
            stdTypes.Add("8302", "503");
            stdTypes.Add("8304", "503");
            stdTypes.Add("8400", "401");
            stdTypes.Add("8700", "503");
            stdTypes.Add("8701", "503");
            stdTypes.Add("8704", "503");
            stdTypes.Add("9000", "400");
            stdTypes.Add("9001", "400");
            stdTypes.Add("9002", "400");
            stdTypes.Add("9003", "400");
            stdTypes.Add("9004", "400");
            stdTypes.Add("9010", "400");
            stdTypes.Add("9011", "400");
            stdTypes.Add("9012", "400");
            stdTypes.Add("9020", "400");
            stdTypes.Add("9021", "400");
            stdTypes.Add("9022", "400");
            stdTypes.Add("9023", "400");
            stdTypes.Add("9031", "400");
            stdTypes.Add("9032", "400");
            stdTypes.Add("9040", "400");
            stdTypes.Add("9100", "103");
            stdTypes.Add("9101", "103");
            stdTypes.Add("9102", "103");
            stdTypes.Add("9103", "103");
            stdTypes.Add("9104", "103");
            stdTypes.Add("9200", "501");
            stdTypes.Add("9201", "501");
            stdTypes.Add("9202", "501");
            stdTypes.Add("9203", "403");
            stdTypes.Add("9204", "503");
            stdTypes.Add("9500", "501");
            stdTypes.Add("9501", "501");
            stdTypes.Add("9700", "200");
            stdTypes.Add("9701", "104");
            stdTypes.Add("9702", "301");
            stdTypes.Add("9703", "300");
            stdTypes.Add("9704", "200");
        }
        private void populateCities()
        {
            cities.Add("ACTIS", "ACTIS");
            cities.Add("AERIAL ACRES", "AERIAL ACRES");
            cities.Add("ALPINE FOREST", "ALPINE FOREST");
            cities.Add("ALTA SIERRA", "ALTA SIERRA");
            cities.Add("ARVIN", "ARVIN");
            cities.Add("BAKERSFIELD", "BAKERSFIELD");
            cities.Add("BEAR VALLEY", "BEAR VALLEY");
            cities.Add("BELLA VISTA", "BELLA VISTA");
            cities.Add("BELRIDGE", "BELRIDGE");
            cities.Add("BLACKWELLS CORNER", "BLACKWELLS CORNER");
            cities.Add("BLACKWELLS", "BLACKWELLS CORNER");
            cities.Add("BODFISH", "BODFISH");
            cities.Add("BORON", "BORON");
            cities.Add("BUTTONWILLOW", "BUTTONWILLOW");
            cities.Add("BUTTONWLLOW", "BUTTONWILLOW");
            cities.Add("CALIENTE", "CALIENTE");
            cities.Add("CALIFORNIA CITY", "CALIFORNIA CITY");
            cities.Add("CALIF CITY", "CALIFORNIA CITY");
            cities.Add("CAL CITY AIR PK", "CALIFORNIA CITY");
            cities.Add("CANE BRAKE", "CANE BRAKE");
            cities.Add("CANBRAKE", "CANE BRAKE");
            cities.Add("CANEBRAKE", "CANE BRAKE");
            cities.Add("CANTIL", "CANTIL");
            cities.Add("CHOCTAW VALLEY", "CHOCTAW VALLEY");
            cities.Add("CHOLAME", "CHOLAME");
            cities.Add("CLARAVILLE", "CLARAVILLE");
            cities.Add("CUMMINGS VALLEY", "CUMMINGS VALLEY");
            cities.Add("DELANO", "DELANO");
            cities.Add("DERBY ACRES", "DERBY ACRES");
            cities.Add("DESERT LAKE", "DESERT LAKE");
            cities.Add("DEVILS DEN", "DEVILS DEN");
            cities.Add("DI GIORGIO", "DI GIORGIO");
            cities.Add("DUSTIN ACRES", "DUSTIN ACRES");
            cities.Add("EAST BAKERSFIELD", "EAST BAKERSFIELD");
            cities.Add("EDISON", "EDISON");
            cities.Add("EDWARDS AFB", "EDWARDS AFB");
            cities.Add("EDWARDS", "EDWARDS");
            cities.Add("ELK HILLS", "ELK HILLS");
            cities.Add("FAMOSO", "FAMOSO");
            cities.Add("FELLOWS", "FELLOWS");
            cities.Add("FORD CITY", "FORD CITY");
            cities.Add("FRAZIER PARK", "FRAZIER PARK");
            cities.Add("FREEMAN", "FREEMAN");
            cities.Add("FREMONT VALLEY", "FREMONT VALLEY");
            cities.Add("FREMONT", "FREMONT VALLEY");
            cities.Add("FRUITVALE", "FRUITVALE");
            cities.Add("FULLER ACRES", "FULLER ACRES");
            cities.Add("GARFIELD", "GARFIELD");
            cities.Add("GARLOCK", "GARLOCK");
            cities.Add("GLENNVILLE", "GLENNVILLE");
            cities.Add("GLENVILLE", "GLENNVILLE");
            cities.Add("GOLDEN HILLS", "GOLDEN HILLS");
            cities.Add("GRANITE STATION", "GRANITE STATION");
            cities.Add("GRAPEVINE", "GRAPEVINE");
            cities.Add("GREENACRES", "GREENACRES");
            cities.Add("GREENFIELD", "GREENFIELD");
            cities.Add("GREENHORN", "GREENHORN");
            cities.Add("HART PARK", "HART PARK");
            cities.Add("HAVILAH", "HAVILAH");
            cities.Add("HIGHTS CORNER", "HIGHTS CORNER");
            cities.Add("INYOKERN", "INYOKERN");
            cities.Add("JOHANNESBURG", "JOHANNESBURG");
            cities.Add("KECKS CORNER", "KECKS CORNER");
            cities.Add("KEENE", "KEENE");
            cities.Add("KERN CITY", "KERN CITY");
            cities.Add("KERN COUNTY", "KERN COUNTY");
            cities.Add("KERNVALE", "KERNVILLE");
            cities.Add("KERNVILLE", "KERNVILLE");
            cities.Add("LAKE ISABELLA", "LAKE ISABELLA");
            cities.Add("LAKE OF THE WOODS", "LAKE OF THE WOODS");
            cities.Add("LAKE OF THE WDS", "LAKE OF THE WOODS");
            cities.Add("LAKE OF WOODS", "LAKE OF THE WOODS");
            cities.Add("LK OF THE WDS", "LAKE OF THE WOODS");
            cities.Add("LK OF WOODS", "LAKE OF THE WOODS");
            cities.Add("LAMONT", "LAMONT");
            cities.Add("LEBEC", "LEBEC");
            cities.Add("LELITER", "LELITER");
            cities.Add("LOMA PARK", "LOMA PARK");
            cities.Add("LORAINE", "LORAINE");
            cities.Add("PARIS-LORAINE", "LORAINE");
            cities.Add("LOST HILLS", "LOST HILLS");
            cities.Add("MAGUNDEN", "MAGUNDEN");
            cities.Add("MARICOPA", "MARICOPA");
            cities.Add("MAYFAIR", "MAYFAIR");
            cities.Add("MCFARLAND", "MCFARLAND");
            cities.Add("MC FARLAND", "MCFARLAND");
            cities.Add("MCKITTRICK", "MCKITTRICK");
            cities.Add("MC KITTRICK", "MCKITTRICK");
            cities.Add("METTLER", "METTLER");
            cities.Add("MINTER VILLAGE", "MINTER VILLAGE");
            cities.Add("MIRACLE HOT SPRINGS", "MIRACLE HOT SPRINGS");
            cities.Add("MOJAVE", "MOJAVE");
            cities.Add("MONMOUTH", "MONMOUTH");
            cities.Add("MONOLITH", "MONOLITH");
            cities.Add("MT MESA", "MOUNTAIN MESA");
            cities.Add("MT. MESA", "MOUNTAIN MESA");
            cities.Add("MTN MESA", "MOUNTAIN MESA");
            cities.Add("MOUNTAIN MESA", "MOUNTAIN MESA");
            cities.Add("NORTH EDWARDS", "NORTH EDWARDS");
            cities.Add("NO EDWARDS", "NORTH EDWARDS");
            cities.Add("OILDALE", "OILDALE");
            cities.Add("OLD RIVER", "OLD RIVER");
            cities.Add("ONYX", "ONYX");
            cities.Add("PANAMA", "PANAMA");
            cities.Add("PINE MOUNTAIN", "PINE MOUNTAIN");
            cities.Add("PINE MTN", "PINE MOUNTAIN");
            cities.Add("PINION PINES", "PINION PINES");
            cities.Add("POND", "POND");
            cities.Add("PUMPKIN CENTER", "PUMPKIN CENTER");
            cities.Add("PUMPKIN", "PUMPKIN CENTER");
            cities.Add("RANDSBURG", "RANDSBURG");
            cities.Add("RICARDO", "RICARDO");
            cities.Add("RIDGECREST", "RIDGECREST");
            cities.Add("RDGCRST", "RIDGECREST");
            cities.Add("RIO BRAVO", "RIO BRAVO");
            cities.Add("RIVER KERN", "RIVER KERN");
            cities.Add("RIVERKERN", "RIVER KERN");
            cities.Add("ROSAMOND", "ROSAMOND");
            cities.Add("ROSEDALE", "ROSEDALE");
            cities.Add("SALTDALE", "SALTDALE");
            cities.Add("SANBORN", "SANBORN");
            cities.Add("SAND CANYON", "SAND CANYON");
            cities.Add("SHAFTER", "SHAFTER");
            cities.Add("SOUTH BELRIDGE", "SOUTH BELRIDGE");
            cities.Add("SOUTH LAKE", "SOUTH LAKE");
            cities.Add("SOUTHLAKE", "SOUTH LAKE");
            cities.Add("SOUTH TAFT", "SOUTH TAFT");
            cities.Add("SQUIRREL", "SQUIRREL");
            cities.Add("STALLION SPRING", "STALLION SPRING");
            cities.Add("TAFT", "TAFT");
            cities.Add("TEHACHAPI", "TEHACHAPI");
            cities.Add("TROPICO", "TROPICO");
            cities.Add("TUPMAN", "TUPMAN");
            cities.Add("TWIN OAKS", "TWIN OAKS");
            cities.Add("VALLEY ACRES", "VALLEY ACRES");
            cities.Add("WALKER BASIN", "WALKER BASIN");
            cities.Add("WALKER PASS", "WALKER PASS");
            cities.Add("WASCO", "WASCO");
            cities.Add("WEED PATCH", "WEED PATCH");
            cities.Add("WEEDPATCH", "WEED PATCH");
            cities.Add("WELDON", "WELDON");
            cities.Add("WHEELER RIDGE", "WHEELER RIDGE");
            cities.Add("WILLOW SPRINGS", "WILLOW SPRINGS");
            cities.Add("WILLOW", "WILLOW SPRINGS");
            cities.Add("WOFFORD HEIGHTS", "WOFFORD HEIGHTS");
            cities.Add("WOFFORD", "WOFFORD HEIGHTS");
            cities.Add("WOODFORD", "WOODFORD");
            cities.Add("WOODY", "WOODY");
        }
    }
}
