﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class But : Megabyte
    {
        public But() 
        {
            co3 = "BUT";
            coNum = "4";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("810", "101");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("865", "401");
            stdTypes.Add("880", "501");
            stdTypes.Add("890", "400");
        }
    }
}
