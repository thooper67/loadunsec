﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Mer : Megabyte
    {
        public Mer()
        {
            co3 = "MER";
            coNum = "24";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("810", "101");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("880", "500");
        }
    }
}
