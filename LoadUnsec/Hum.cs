﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Hum : Megabyte
    {
        public Hum()
        {
            co3 = "HUM";
            coNum = "12";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "104"); // BUSINESS ASSESSMENTS
            stdTypes.Add("805", "100");
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("880", "401");
        }
    }
}
