﻿using System;
using System.Configuration;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Collections.Generic;
using System.Data;

namespace LoadUnsec
{
    class Crest : County
    {
        public Crest()
        {
        }

        #region Properties
        List<fixfield> fields1 = new List<fixfield>();
        DataTable dtRoll = new DataTable();
        
        //                                length, offset, name
        public fixfield fldTYPE = new fixfield(2, 0, "TYPE"); // Different for each Crest county
        fixfield fldYEAR = new fixfield(4, 2, "YEAR");
        fixfield fldTRA = new fixfield(6, 6, "TRA");
        fixfield fldAPN = new fixfield(10, 12, "APN");
        fixfield fldPP_STMT_CD = new fixfield(1, 22, "PP_STMT_CD");
        fixfield fldPP_PEN_CD = new fixfield(1, 23, "PP_PEN_CD");
        fixfield fldCPI_CD = new fixfield(3, 24, "CPI_CD");
        fixfield fldNOTIFY_FLG = new fixfield(1, 27, "NOTIFY_FLG");
        fixfield fldEXE_CLAIM_CD = new fixfield(1, 28, "EXE_CLAIM_CD");
        fixfield fldUSE_CD = new fixfield(6, 29, "USE_CD");
        fixfield fldBASE_CD = new fixfield(2, 35, "BASE_CD");
        public fixfield fldDOC_NUM = new fixfield(8, 37, "DOC_NUM");
        public fixfield fldDOC_DATE = new fixfield(8, 45, "DOC_DATE");
        fixfield fldNAME1 = new fixfield(29, 53, "NAME1");
        fixfield fldNAME2 = new fixfield(29, 82, "NAME2");
        fixfield fldADDR1 = new fixfield(29, 111, "ADDR1");
        fixfield fldADDR2 = new fixfield(29, 140, "ADDR2");
        fixfield fldDESC = new fixfield(31, 169, "DESC");
        fixfield fldZIP = new fixfield(6, 200, "ZIP");
        fixfield fldZIP4 = new fixfield(4, 206, "ZIP4");
        fixfield fldZIPBOTH = new fixfield(10, 200, "ZIPBOTH");
        fixfield fldID_CD = new fixfield(7, 210, "ID_CD");
        fixfield fldLAND = new fixfield(10, 217, "LAND");
        fixfield fldIMPR = new fixfield(10, 227, "IMPR");
        fixfield fldPERSPROP = new fixfield(10, 237, "PERSPROP");
        fixfield fldTOTAL_EXE = new fixfield(10, 247, "TOTAL_EXE");
        fixfield fldFIXTR = new fixfield(10, 257, "FIXTR");
        fixfield fldACRES = new fixfield(10, 267, "ACRES");
        fixfield fldEXE_CD1 = new fixfield(2, 277, "EXE_CD1");
        fixfield fldEXE_CD2 = new fixfield(2, 279, "EXE_CD2");
        fixfield fldEXE_CD3 = new fixfield(2, 281, "EXE_CD3");
        fixfield fldEXE_CD4 = new fixfield(2, 283, "EXE_CD4");
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;

            // open input file
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            long srcFileLength = 0;
            int srcFile1RecLen;
            int.TryParse(ConfigurationManager.AppSettings[co3 + "file1RecLen"], out srcFile1RecLen);
            initialize();

            if (File.Exists(srcFile1))
            {
                try
                {
                    FileInfo f = new FileInfo(srcFile1);
                    srcFileLength = f.Length;
                    using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(srcFile1, FileMode.Open))
                    {
                        using (MemoryMappedViewAccessor va = mmf.CreateViewAccessor(0, srcFileLength))
                        {
                            long i = 0;
                            while (srcFileLength - i * srcFile1RecLen >= srcFile1RecLen)
                            {
                                long offset = i * srcFile1RecLen;
                                byte[] buffer = new byte[srcFile1RecLen];
                                va.ReadArray(offset, buffer, 0, srcFile1RecLen);
                                string myRec = System.Text.Encoding.ASCII.GetString(buffer);
                                // if (myRec.Substring(0,2) == "\r\n") continue;
                                DataRow dr = dtRoll.NewRow();
                                foreach (fixfield thisField in fields1)
                                {
                                    dr[thisField.Name] = getStr(myRec, thisField);
                                }
                                dtRoll.Rows.Add(dr);
                                i++;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Error opening source file1: '" + srcFile1 + "'. " + e.Message;
                    return result;
                }

            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            // Write out header
            unsecrec hdr = new unsecrec();
            swOutFile.WriteLine(hdr.writeHeader());

            try
            {
                dtRoll.DefaultView.Sort = "APN";
                DataView view = dtRoll.DefaultView;
                DataTable dtRollSort = view.ToTable();
                count = 0;
                foreach (DataRow drRoll in dtRollSort.Rows)
                {
                    count++;
                    unsecrec thisRec = new unsecrec();

                    thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                    thisRec.TRA = drRoll["TRA"].ToString();
                    thisRec.APN_S = drRoll["APN"].ToString();
                    thisRec.APN_D = thisRec.APN_S;
                    thisRec.CO_NUM = coNum;
                    thisRec.CO_ID = co3;
                    thisRec.TYPE = drRoll["TYPE"].ToString();
                    thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                    thisRec.LEGAL = cleanLine(drRoll["DESC"].ToString());
                    thisRec.EXE_CD = drRoll["EXE_CD1"].ToString();

                    DateTime? myDocDate = County.fixDate(drRoll["DOC_DATE"].ToString());
                    thisRec.DOC_DATE = (myDocDate == null) ? "" : ((DateTime)myDocDate).ToString("yyyyMMdd");
                    string myDocNum = drRoll["DOC_NUM"].ToString();
                    thisRec.DOC_NUM = (myDocNum == "0") ? "" : myDocNum;

                    string name1 = drRoll["NAME1"].ToString();
                    string name2 = drRoll["NAME2"].ToString();
                    owner myOwner = new owner(name1, name2);
                    thisRec.OWNER1 = myOwner.owner1;
                    thisRec.OWNER2 = myOwner.owner2;
                    thisRec.DBA = myOwner.dba;
                    thisRec.CARE_OF = myOwner.care_of;

                    values myVals = new values();
                    myVals.LAND = drRoll["LAND"].ToString();
                    myVals.IMPR = drRoll["IMPR"].ToString();
                    myVals.FIXTR = drRoll["FIXTR"].ToString();
                    myVals.PERSPROP = drRoll["PERSPROP"].ToString();
                    myVals.EXE_AMT = drRoll["TOTAL_EXE"].ToString();
                    thisRec.LAND = myVals.LAND;
                    thisRec.IMPR = myVals.IMPR;
                    thisRec.FIXTR = myVals.FIXTR;
                    thisRec.PERSPROP = myVals.PERSPROP;
                    thisRec.GROSS = myVals.GROSS;
                    thisRec.EXE_AMT = myVals.EXE_AMT;

                    string addr1 = drRoll["ADDR1"].ToString();
                    string addr2 = drRoll["ADDR2"].ToString() + " " + drRoll["ZIPBOTH"].ToString();
                    mailing myMail = new mailing(addr1, addr2, "", "");
                    thisRec.M_STRNUM = myMail.m_strnum;
                    thisRec.M_STR_SUB = myMail.m_str_sub;
                    thisRec.M_DIR = myMail.m_dir;
                    thisRec.M_STREET = myMail.m_street;
                    thisRec.M_SUFF = myMail.m_suff;
                    thisRec.M_UNITNO = myMail.m_unit_no;
                    thisRec.M_CITY = myMail.m_city;
                    thisRec.M_ST = myMail.m_st;
                    thisRec.M_ZIP = myMail.m_zip;
                    thisRec.M_ZIP4 = myMail.m_zip4;
                    thisRec.M_ADDR_D = myMail.m_addr_d;
                    thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                    swOutFile.WriteLine(thisRec.writeOutput());
                }
            }
            catch (Exception e)
            {
                log("Error in record: " + count + ". " + e.Message);
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }

        private void initialize()
        {
            fields1.Add(fldTYPE);
            fields1.Add(fldYEAR);
            fields1.Add(fldTRA);
            fields1.Add(fldAPN);
            fields1.Add(fldPP_STMT_CD);
            fields1.Add(fldPP_PEN_CD);
            fields1.Add(fldCPI_CD);
            fields1.Add(fldNOTIFY_FLG);
            fields1.Add(fldEXE_CLAIM_CD);
            fields1.Add(fldUSE_CD);
            fields1.Add(fldBASE_CD);
            fields1.Add(fldDOC_NUM);
            fields1.Add(fldDOC_DATE);
            fields1.Add(fldNAME1);
            fields1.Add(fldNAME2);
            fields1.Add(fldADDR1);
            fields1.Add(fldADDR2);
            fields1.Add(fldDESC);
            fields1.Add(fldZIP);
            fields1.Add(fldZIP4);
            fields1.Add(fldZIPBOTH);
            fields1.Add(fldID_CD);
            fields1.Add(fldLAND);
            fields1.Add(fldIMPR);
            fields1.Add(fldPERSPROP);
            fields1.Add(fldTOTAL_EXE);
            fields1.Add(fldFIXTR);
            fields1.Add(fldACRES);
            fields1.Add(fldEXE_CD1);
            fields1.Add(fldEXE_CD2);
            fields1.Add(fldEXE_CD3);
            fields1.Add(fldEXE_CD4);

            foreach (fixfield field in fields1)
            {
                dtRoll.Columns.Add(field.Name, typeof(string));
            }
        }
        #endregion
    }
}
