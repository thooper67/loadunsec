﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Sha : Megabyte
    {
        public Sha()
        {
            co3 = "SHA";
            coNum = "45";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "104"); // BUSINESS ASSESSMENTS
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("861", "400");
        }
    }
}
