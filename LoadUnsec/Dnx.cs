﻿using System;
using System.Linq;


namespace LoadUnsec
{
    class Dnx : Megabyte
    {
        public Dnx()
        {
            co3 = "DNX";
            coNum = "8";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
        }
    }
}
