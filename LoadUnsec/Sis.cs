﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Sis : Megabyte
    {
        public Sis()
        {
            co3 = "SIS";
            coNum = "47";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "104"); // BUSINESS ASSESSMENTS
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("881", "401");
            stdTypes.Add("882", "100");
            stdTypes.Add("883", "500");
        }
    }
}
