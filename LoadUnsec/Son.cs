﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Son : Megabyte
    {
        public Son()
        {
            co3 = "SON";
            coNum = "49";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("802", "502");
            stdTypes.Add("803", "503");
            stdTypes.Add("804", "100");
            stdTypes.Add("805", "106");
            stdTypes.Add("810", "103");
            stdTypes.Add("811", "103");
            stdTypes.Add("815", "106");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("840", "502");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
        }
    }
}
