﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Fre : County
    {
        enum fieldName { owner1, dba, co };

        public Fre()
        {
            co3 = "FRE";
            coNum = "10";
            populateStdTypes();
        }

        #region Properties
        List<fixfield> fields1 = new List<fixfield>();
        DataTable dtRoll = new DataTable();
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            int srcFile1RecLen = 937;
            long srcFileLength = 0;
            initialize();

            StreamWriter swOutFile = null;
            int count = 0;

            #region srcFile1
            if (File.Exists(srcFile1))
            {
                try
                {
                    FileInfo f = new FileInfo(srcFile1);
                    srcFileLength = f.Length;
                    using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(srcFile1, FileMode.Open))
                    {
                        using (MemoryMappedViewAccessor va = mmf.CreateViewAccessor(0, srcFileLength))
                        {
                            long i = 0;
                            while (i * srcFile1RecLen < srcFileLength)
                            {
                                long offset = i * srcFile1RecLen;
                                byte[] buffer = new byte[srcFile1RecLen];
                                va.ReadArray(offset, buffer, 0, srcFile1RecLen);
                                string myRec = System.Text.Encoding.ASCII.GetString(buffer);
                                DataRow dr = dtRoll.NewRow();
                                foreach (fixfield thisField in fields1)
                                {
                                    dr[thisField.Name] = getStr(myRec, thisField);
                                }
                                dtRoll.Rows.Add(dr);
                                i++;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Error opening source file1: '" + srcFile1 + "'. " + e.Message;
                    return result;
                }

            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }
            #endregion

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            // Write out header
            unsecrec hdr = new unsecrec();
            swOutFile.WriteLine(hdr.writeHeader());

            // Loop through input
            try
            {
                count = 0;
                foreach (DataRow drRoll in dtRoll.Rows)
                {
                    count++;
                    unsecrec thisRec = new unsecrec();

                    string myAPN = drRoll["APN"].ToString() + drRoll["SUB_NO"].ToString();
                    Regex rgx = new Regex(@" +");
                    myAPN = rgx.Replace(myAPN, "");
                    thisRec.APN_S = myAPN;
                    thisRec.APN_D = thisRec.APN_S;
                    thisRec.CO_NUM = coNum;
                    thisRec.CO_ID = co3;
                    thisRec.YRASSD = thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                    thisRec.TRA = drRoll["TRA"].ToString();
                    thisRec.LEGAL = cleanLine(drRoll["DESC1"].ToString() + " " + drRoll["DESC2"].ToString() + " " + drRoll["DESC3"].ToString());
                    thisRec.TYPE = drRoll["STMT_CODE"].ToString();
                    thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                    values myVals = new values();
                    myVals.LAND = drRoll["LAND"].ToString();
                    myVals.IMPR = drRoll["IMPR"].ToString();
                    myVals.FIXTR = drRoll["FIXTR"].ToString();
                    myVals.PERSPROP = addStrings(drRoll["PERSPROP"].ToString(), drRoll["OTHER_VAL"].ToString());
                    myVals.MHPP = drRoll["MHPP"].ToString();
                    myVals.PENALTY = drRoll["PENALTY"].ToString();
                    myVals.EXE_AMT = addStrings(drRoll["EXE_AMT1"].ToString(), drRoll["EXE_AMT2"].ToString(), drRoll["EXE_AMT3"].ToString(), drRoll["EXE_AMT4"].ToString());
                    thisRec.LAND = myVals.LAND;
                    thisRec.IMPR = myVals.IMPR;
                    thisRec.FIXTR = myVals.FIXTR;
                    thisRec.PERSPROP = myVals.PERSPROP;
                    thisRec.MHPP = myVals.MHPP;
                    thisRec.GROSS = myVals.GROSS;
                    thisRec.PENALTY = myVals.PENALTY;
                    thisRec.EXE_AMT = myVals.EXE_AMT;

                    bool al2done = false;
                    bool o1Asgn = false;
                    bool coAsgn = false;
                    bool dbaAsgn = false;
                    string addrLine1 = "";
                    string addrLine2 = "";
                    int ptrTop = 5;
                    int ptrBot = 0;
                    string name1 = drRoll["NAME1"].ToString();
                    string name2 = drRoll["NAME2"].ToString();
                    string name3 = drRoll["NAME3"].ToString();
                    string addr1 = drRoll["M_ADDR1"].ToString();
                    string addr2 = drRoll["M_ADDR2"].ToString();
                    string addr3 = drRoll["M_ADDR3"].ToString();
                    string[] ins = new string[6] {name1, name2, name3, addr1, addr2, addr3};

                    // Assigns addrLine2
                    while(ptrTop > 0 && !al2done)
                    {
	                    if (!string.IsNullOrEmpty(ins[ptrTop]))
	                    {
                            Regex re = new Regex(@"^(\s*CANADA\s*)|^(\s*\d{5}-?\d*\s*)$", RegexOptions.IgnoreCase);
                            bool bZipXCanada = re.IsMatch(ins[ptrTop]);

		                    if ((ptrTop > 0) && bZipXCanada)
		                    {
			                    addrLine2 = ins[ptrTop - 1] + ' ' + ins[ptrTop];
			                    ptrTop--;
		                    }
                            else
                            {
                                addrLine2 = ins[ptrTop];
                            }
                            al2done = true;
	                    }
	                    ptrTop--;
                    }

                    // Assigns addrLine1
                    while(ptrTop > 0)
                    {
                        if (!string.IsNullOrEmpty(ins[ptrTop]))
                        {
                            bool bUnit = false; // ins[ptrTop] starts w/ a unit
                            bool bSfx = false; // ins[ptrTop] starts w/ a  suffix
                            bool bHyphen = false; // last non space char in ins[ptrTop-1] is a '-'

                            Regex re = new Regex(@"^(#\s*|MS\s*|SUITE\s*|PMB\s*)", RegexOptions.IgnoreCase);
                            if (re.IsMatch(ins[ptrTop])) bUnit = true;

                            string[] items = ins[ptrTop].Split(' ');
                            address sfxChk = new address();
                            if (sfxChk.suffixes.ContainsKey(items[0])) bSfx = true;
                            
                            re = new Regex(@"-$", RegexOptions.IgnoreCase);
                            if (re.IsMatch(ins[ptrTop-1].Trim())) bUnit = true;

                            if ((ptrTop > 0) && ((bUnit || bSfx) || (bHyphen)))
                            {
                                addrLine1 = ins[ptrTop - 1] + ' ' + ins[ptrTop];
                                ptrTop = ptrTop - 2;
                                break;
                            }
                            else
                            {
                                addrLine1 = ins[ptrTop];
                                ptrTop--;
                                break;
                            }
                        }
                        ptrTop--;
                    }

                    // Assigns Owner, DBA, C/O
                    fieldName lastAssigned = fieldName.owner1;

                    while(ptrBot <= ptrTop)
                    {
                        if (!o1Asgn) // Lowest entry is always Owner1
                        {
                            thisRec.OWNER1 = cleanLine(ins[ptrBot]);
                            o1Asgn = true;
                            lastAssigned = fieldName.owner1;
                        }
                        else if (string.IsNullOrEmpty(ins[ptrBot])) // Empty, so just look at the next one.
                        {
                            // Don't do anything
                        }
                        else
                        {
                            Regex re = new Regex(@"^(" + County.RE_dba + ")(.+)$", RegexOptions.IgnoreCase);
                            Match m1 = re.Match(ins[ptrBot]);
                            re = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                            Match m2 = re.Match(ins[ptrBot]);

                            if (!dbaAsgn && m1.Success)
                            {
                                Group g2 = m1.Groups[2];
                                thisRec.DBA = cleanLine(g2.ToString());
                                dbaAsgn = true;
                                lastAssigned = fieldName.dba;
                            }
                            else if ((!coAsgn) && (m2.Success))
                            {
                                Group g2 = m2.Groups[2];
                                thisRec.CARE_OF = cleanLine(g2.ToString());
                                coAsgn = true;
                                lastAssigned = fieldName.co;
                            }
                            else
                            {
                                switch (lastAssigned)
                                {
                                    case fieldName.owner1:
                                        thisRec.OWNER1 += ' ' + cleanLine(ins[ptrBot]);
                                        break;
                                    case fieldName.dba:
                                        thisRec.DBA += ' ' + cleanLine(ins[ptrBot]);
                                        break;
                                    case fieldName.co:
                                        thisRec.CARE_OF += ' ' + cleanLine(ins[ptrBot]);
                                        break;
                                    default:
                                        thisRec.OWNER2 += ' ' + cleanLine(ins[ptrBot]); 
                                        break;
                                }
                            }
                        }
                        ptrBot++;
                    }
                    mailing myMail = new mailing(addrLine1, addrLine2, "", "");
                    thisRec.M_STRNUM = myMail.m_strnum;
                    thisRec.M_STR_SUB = myMail.m_str_sub;
                    thisRec.M_DIR = myMail.m_dir;
                    thisRec.M_STREET = myMail.m_street;
                    thisRec.M_SUFF = myMail.m_suff;
                    thisRec.M_UNITNO = myMail.m_unit_no;
                    thisRec.M_CITY = myMail.m_city;
                    thisRec.M_ST = myMail.m_st;
                    thisRec.M_ZIP = myMail.m_zip;
                    thisRec.M_ZIP4 = myMail.m_zip4;
                    thisRec.M_ADDR_D = myMail.m_addr_d;
                    thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                    swOutFile.WriteLine(thisRec.writeOutput());
                }
            }
            catch (Exception e)
            {
                log("Error in record: " + count + ". " + e.Message);
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }
            return result;
        }
        private void initialize()
        {
            //                     Length, Position, Name
            fields1.Add(new fixfield(10, 0, "APN"));
            fields1.Add(new fixfield(5, 10, "SUB_NO"));
            fields1.Add(new fixfield(6, 15, "TRA"));
            fields1.Add(new fixfield(25, 40, "NAME1"));
            fields1.Add(new fixfield(25, 65, "NAME2"));
            fields1.Add(new fixfield(25, 90, "NAME3"));
            fields1.Add(new fixfield(20, 115, "M_ADDR1"));
            fields1.Add(new fixfield(20, 135, "M_ADDR2"));
            fields1.Add(new fixfield(20, 155, "M_ADDR3"));
            fields1.Add(new fixfield(20, 175, "DESC1"));
            fields1.Add(new fixfield(20, 195, "DESC2"));
            fields1.Add(new fixfield(20, 215, "DESC3"));
            fields1.Add(new fixfield(8, 267, "LAND"));
            fields1.Add(new fixfield(8, 275, "IMPR"));
            fields1.Add(new fixfield(8, 283, "FIXTR"));
            fields1.Add(new fixfield(8, 291, "PERSPROP"));
            fields1.Add(new fixfield(8, 307, "OTHER_VAL"));
            fields1.Add(new fixfield(8, 325, "EXE_AMT1"));
            fields1.Add(new fixfield(8, 334, "EXE_AMT2"));
            fields1.Add(new fixfield(8, 343, "EXE_AMT3"));
            fields1.Add(new fixfield(8, 352, "EXE_AMT4"));
            fields1.Add(new fixfield(7, 647, "PENALTY"));
            fields1.Add(new fixfield(1, 693, "STMT_CODE"));
            fields1.Add(new fixfield(8, 703, "MHPP"));
            foreach (fixfield field in fields1)
            {
                dtRoll.Columns.Add(field.Name, typeof(string));
            }
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("A", "502");
            stdTypes.Add("B", "100");
            stdTypes.Add("C", "108");
            stdTypes.Add("D", "403");
            stdTypes.Add("E", "100");
            stdTypes.Add("F", "500");
            stdTypes.Add("G", "200");
            stdTypes.Add("H", "100");
            stdTypes.Add("I", "103");
            stdTypes.Add("J", "502");
            stdTypes.Add("K", "100");
            stdTypes.Add("L", "103");
            stdTypes.Add("M", "300");
            stdTypes.Add("N", "100");
            stdTypes.Add("O", "500");
            stdTypes.Add("P", "400");
            stdTypes.Add("Q", "402");
            stdTypes.Add("R", "100");
            stdTypes.Add("S", "100");
            stdTypes.Add("T", "106");
            stdTypes.Add("U", "500");
            stdTypes.Add("V", "500");
            stdTypes.Add("W", "100");
            stdTypes.Add("X", "500");
            stdTypes.Add("Y", "500");
            stdTypes.Add("Z", "500");
        }
    }
}
