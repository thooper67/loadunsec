﻿using System;
using System.Configuration;
using System.IO;
using System.Data.Odbc;

namespace LoadUnsec
{
    class Men : County
    {
        public Men()
        {
            co3 = "MEN";
            coNum = "23";
            populateStdTypes();
        }

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [Sheet1$] ORDER BY [PIN]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();

                        thisRec.APN_S = fields["PIN"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = fields["TAG"].ToString().PadLeft(6, '0');
                        thisRec.OWNER1 = cleanLine(fields["Recipient"].ToString());
                        thisRec.CARE_OF = thisRec.OWNER1;
                        thisRec.TYPE = fields["ClassCd"].ToString();
                        thisRec.TYPE_STD = xlatType(fields["ClassCd"].ToString());

                        values myVals = new values();
                        myVals.LAND = fields["AssessedLand"].ToString();
                        myVals.IMPR = fields["AssessedImp"].ToString();
                        myVals.FIXTR = fields["AssessedFixtures"].ToString();
                        myVals.PERSPROP = fields["AssessedPersonal"].ToString();
                        myVals.GROWING = fields["AssessedLivImp"].ToString();
                        myVals.PENALTY = fields["Penalty"].ToString();
                        myVals.EXE_AMT = fields["TotalExemptions"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.PENALTY = myVals.PENALTY;
                        thisRec.EXE_AMT = myVals.EXE_AMT;
                        if (fields["HOX"].ToString() != "0")
                        {
                            thisRec.EXE_CD = "HO";
                        }
                        else if (fields["DVX"].ToString() != "0")
                        {
                            thisRec.EXE_CD = "DV";
                        }
                        else if (fields["LDVX"].ToString() != "0")
                        {
                            thisRec.EXE_CD = "LDV";
                        }
                        else if (fields["OtherExmpt"].ToString() != "0")
                        {
                            thisRec.EXE_CD = "OE";
                        }

                        mailing myMail = new mailing(fields["DeliveryAddr"].ToString(), fields["LastLine"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        situs mySitus = new situs(fields["SitusAddr"].ToString(), fields["SitusCity"].ToString() + " " + fields["SitusState"].ToString() + " " + fields["SitusPostalCd"].ToString());
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("PPA - AGRICULTURAL", "502");
            stdTypes.Add("PPA - AIRCRAFT", "300");
            stdTypes.Add("PPA - BUSINESS AND EQUIPMENT", "100");
            stdTypes.Add("PPA - FINANCIAL", "106");
            stdTypes.Add("PPA - POSSESSORY INTEREST", "400");
            stdTypes.Add("PPA - TRUE LEASE", "107");
            stdTypes.Add("PPA - VESSEL", "200");
        }
    }
}
