﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LoadUnsec
{
    class unsecrec
    {
        private string _delim = "|";
        private List<urfield> _fields = new List<urfield>();

        public unsecrec()
        {
            _fields.Add(_APN_S);
            _fields.Add(_APN_D);
            _fields.Add(_CO_NUM);
            _fields.Add(_CO_ID);
            _fields.Add(_TRA);
            _fields.Add(_OWNER1);
            _fields.Add(_OWNER2);
            _fields.Add(_ASSESSEE);
            _fields.Add(_DBA);
            _fields.Add(_CARE_OF);
            _fields.Add(_S_HSENO);
            _fields.Add(_S_STRNUM);
            _fields.Add(_S_STR_SUB);
            _fields.Add(_S_DIR);
            _fields.Add(_S_STREET);
            _fields.Add(_S_SUFF);
            _fields.Add(_S_UNITNO);
            _fields.Add(_S_CITY);
            _fields.Add(_S_ST);
            _fields.Add(_S_ZIP);
            _fields.Add(_S_ZIP4);
            _fields.Add(_M_STRNUM);
            _fields.Add(_M_STR_SUB);
            _fields.Add(_M_DIR);
            _fields.Add(_M_STREET);
            _fields.Add(_M_SUFF);
            _fields.Add(_M_UNITNO);
            _fields.Add(_M_CITY);
            _fields.Add(_M_ST);
            _fields.Add(_M_ZIP);
            _fields.Add(_M_ZIP4);
            _fields.Add(_FEE_PARCEL_S);
            _fields.Add(_FEE_PARCEL_D);
            _fields.Add(_LAND);
            _fields.Add(_IMPR);
            _fields.Add(_FIXTR);
            _fields.Add(_PERSPROP);
            _fields.Add(_GROWING);
            _fields.Add(_FIXTR_RP);
            _fields.Add(_MHPP);
            _fields.Add(_GROSS);
            _fields.Add(_PENALTY);
            _fields.Add(_EXE_CD);
            _fields.Add(_EXE_AMT);
            _fields.Add(_YRASSD);
            _fields.Add(_LEGAL);
            _fields.Add(_TYPE);
            _fields.Add(_TYPE_STD);
            _fields.Add(_DOC_DATE);
            _fields.Add(_DOC_NUM);
            _fields.Add(_S_ADDR_D);
            _fields.Add(_S_CTY_ST_D);
            _fields.Add(_M_ADDR_D);
            _fields.Add(_M_CTY_ST_D);
            _fields.Add(_BILL_NUM);
            _fields.Add(_ALT_APN);
            _fields.Add(_ALT_TYPE);
        }

        #region properties
        public string delim
        {
            set { _delim = value.Replace("\0", ""); }
            get { return _delim; }
        }
        private urfield _APN_S = new urfield("APN_S", "");
        public string APN_S
        {
            set
            {
                _APN_S.Value = value.Replace("\0", "");
            }
            get
            {
                return _APN_S.Value;
            }
        }
        private urfield _APN_D = new urfield("APN_D", "");
        public string APN_D
        {
            set
            {
                _APN_D.Value = value.Replace("\0", "");
            }
            get
            {
                return _APN_D.Value;
            }
        }
        public string APN_D_format
        {
            set
            {
                _APN_D.Format = value.Replace("\0", "");
            }
            get
            {
                return _APN_D.Format;
            }
        }
        private urfield _CO_NUM = new urfield("CO_NUM", "");
        public string CO_NUM
        {
            set
            {
                _CO_NUM.Value = value.Replace("\0", "");
            }
            get
            {
                return _CO_NUM.Value;
            }
        }
        private urfield _CO_ID = new urfield("CO_ID", "");
        public string CO_ID
        {
            set
            {
                _CO_ID.Value = value.Replace("\0", "");
            }
            get
            {
                return _CO_ID.Value;
            }
        }
        private urfield _TRA = new urfield("TRA", "");
        public string TRA
        {
            set
            {
                _TRA.Value = value.Replace("\0", "");
            }
            get
            {
                return _TRA.Value;
            }
        }
        private urfield _OWNER1 = new urfield("OWNER1", "");
        public string OWNER1
        {
            set
            {
                _OWNER1.Value = value.Replace("\0", "");
            }
            get
            {
                return _OWNER1.Value;
            }
        }
        private urfield _OWNER2 = new urfield("OWNER2", "");
        public string OWNER2
        {
            set
            {
                _OWNER2.Value = value.Replace("\0", "");
            }
            get
            {
                return _OWNER2.Value;
            }
        }
        private urfield _ASSESSEE = new urfield("ASSESSEE", "");
        public string ASSESSEE
        {
            set
            {
                _ASSESSEE.Value = value.Replace("\0", "");
            }
            get
            {
                return _ASSESSEE.Value;
            }
        }
        private urfield _DBA = new urfield("DBA", "");
        public string DBA
        {
            set
            {
                _DBA.Value = value.Replace("\0", "");
            }
            get
            {
                return _DBA.Value;
            }
        }
        private urfield _CARE_OF = new urfield("CARE_OF", "");
        public string CARE_OF
        {
            set
            {
                _CARE_OF.Value = value.Replace("\0", "");
            }
            get
            {
                return _CARE_OF.Value;
            }
        }
        private urfield _S_HSENO = new urfield("S_HSENO", "");
        public string S_HSENO
        {
            set
            {
                _S_HSENO.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_HSENO.Value;
            }
        }
        private urfield _S_STRNUM = new urfield("S_STRNUM", "");
        public string S_STRNUM
        {
            set
            {
                _S_STRNUM.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_STRNUM.Value;
            }
        }
        private urfield _S_STR_SUB = new urfield("S_STR_SUB", "");
        public string S_STR_SUB
        {
            set
            {
                _S_STR_SUB.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_STR_SUB.Value;
            }
        }
        private urfield _S_DIR = new urfield("S_DIR", "");
        public string S_DIR
        {
            set
            {
                _S_DIR.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_DIR.Value;
            }
        }
        private urfield _S_STREET = new urfield("S_STREET", "");
        public string S_STREET
        {
            set
            {
                _S_STREET.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_STREET.Value;
            }
        }
        private urfield _S_SUFF = new urfield("S_SUFF", "");
        public string S_SUFF
        {
            set
            {
                _S_SUFF.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_SUFF.Value;
            }
        }
        private urfield _S_UNITNO = new urfield("S_UNITNO", "");
        public string S_UNITNO
        {
            set
            {
                _S_UNITNO.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_UNITNO.Value;
            }
        }
        private urfield _S_CITY = new urfield("S_CITY", "");
        public string S_CITY
        {
            set
            {
                _S_CITY.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_CITY.Value;
            }
        }
        private urfield _S_ST = new urfield("S_ST", "");
        public string S_ST
        {
            set
            {
                _S_ST.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_ST.Value;
            }
        }
        private urfield _S_ZIP = new urfield("S_ZIP", "");
        public string S_ZIP
        {
            set
            {
                _S_ZIP.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_ZIP.Value;
            }
        }
        private urfield _S_ZIP4 = new urfield("S_ZIP4", "");
        public string S_ZIP4
        {
            set
            {
                _S_ZIP4.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_ZIP4.Value;
            }
        }
        private urfield _M_STRNUM = new urfield("M_STRNUM", "");
        public string M_STRNUM
        {
            set
            {
                _M_STRNUM.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_STRNUM.Value;
            }
        }
        private urfield _M_STR_SUB = new urfield("M_STR_SUB", "");
        public string M_STR_SUB
        {
            set
            {
                _M_STR_SUB.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_STR_SUB.Value;
            }
        }
        private urfield _M_DIR = new urfield("M_DIR", "");
        public string M_DIR
        {
            set
            {
                _M_DIR.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_DIR.Value;
            }
        }
        private urfield _M_STREET = new urfield("M_STREET", "");
        public string M_STREET
        {
            set
            {
                _M_STREET.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_STREET.Value;
            }
        }
        private urfield _M_SUFF = new urfield("M_SUFF", "");
        public string M_SUFF
        {
            set
            {
                _M_SUFF.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_SUFF.Value;
            }
        }
        private urfield _M_UNITNO = new urfield("M_UNITNO", "");
        public string M_UNITNO
        {
            set
            {
                _M_UNITNO.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_UNITNO.Value;
            }
        }
        private urfield _M_CITY = new urfield("M_CITY", "");
        public string M_CITY
        {
            set
            {
                _M_CITY.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_CITY.Value;
            }
        }
        private urfield _M_ST = new urfield("M_ST", "");
        public string M_ST
        {
            set
            {
                _M_ST.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_ST.Value;
            }
        }
        private urfield _M_ZIP = new urfield("M_ZIP", "");
        public string M_ZIP
        {
            set
            {
                _M_ZIP.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_ZIP.Value;
            }
        }
        private urfield _M_ZIP4 = new urfield("M_ZIP4", "");
        public string M_ZIP4
        {
            set
            {
                _M_ZIP4.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_ZIP4.Value;
            }
        }
        private urfield _FEE_PARCEL_S = new urfield("FEE_PARCEL_S", "");
        public string FEE_PARCEL_S
        {
            set
            {
                _FEE_PARCEL_S.Value = value.Replace("\0", "");
            }
            get
            {
                return _FEE_PARCEL_S.Value;
            }
        }
        private urfield _FEE_PARCEL_D = new urfield("FEE_PARCEL_D", "");
        public string FEE_PARCEL_D
        {
            set
            {
                _FEE_PARCEL_D.Value = value.Replace("\0", "");
            }
            get
            {
                return _FEE_PARCEL_D.Value;
            }
        }
        public string FEE_PARCEL_D_format
        {
            set
            {
                _FEE_PARCEL_D.Format = value.Replace("\0", "");
            }
            get
            {
                return _FEE_PARCEL_D.Format;
            }
        }
        private urfield _LAND = new urfield("LAND", "");
        public string LAND
        {
            set
            {
                _LAND.Value = value.Replace("\0", "");
            }
            get
            {
                return _LAND.Value;
            }
        }
        private urfield _IMPR = new urfield("IMPR", "");
        public string IMPR
        {
            set
            {
                _IMPR.Value = value.Replace("\0", "");
            }
            get
            {
                return _IMPR.Value;
            }
        }
        private urfield _FIXTR = new urfield("FIXTR", "");
        public string FIXTR
        {
            set
            {
                _FIXTR.Value = value.Replace("\0", "");
            }
            get
            {
                return _FIXTR.Value;
            }
        }
        private urfield _PERSPROP = new urfield("PERSPROP", "");
        public string PERSPROP
        {
            set
            {
                _PERSPROP.Value = value.Replace("\0", "");
            }
            get
            {
                return _PERSPROP.Value;
            }
        }
        private urfield _GROWING = new urfield("GROWING", "");
        public string GROWING
        {
            set
            {
                _GROWING.Value = value.Replace("\0", "");
            }
            get
            {
                return _GROWING.Value;
            }
        }
        private urfield _FIXTR_RP = new urfield("FIXTR_RP", "");
        public string FIXTR_RP
        {
            set
            {
                _FIXTR_RP.Value = value.Replace("\0", "");
            }
            get
            {
                return _FIXTR_RP.Value;
            }
        }
        private urfield _MHPP = new urfield("MHPP", "");
        public string MHPP
        {
            set
            {
                _MHPP.Value = value.Replace("\0", "");
            }
            get
            {
                return _MHPP.Value;
            }
        }
        private urfield _GROSS = new urfield("GROSS", "");
        public string GROSS
        {
            set
            {
                _GROSS.Value = value.Replace("\0", "");
            }
            get
            {
                return _GROSS.Value;
            }
        }
        private urfield _PENALTY = new urfield("PENALTY", "");
        public string PENALTY
        {
            set
            {
                _PENALTY.Value = value.Replace("\0", "");
            }
            get
            {
                return _PENALTY.Value;
            }
        }
        private urfield _EXE_CD = new urfield("EXE_CD", "");
        public string EXE_CD
        {
            set
            {
                _EXE_CD.Value = value.Replace("\0", "");
            }
            get
            {
                return _EXE_CD.Value;
            }
        }
        private urfield _EXE_AMT = new urfield("EXE_AMT", "");
        public string EXE_AMT
        {
            set
            {
                _EXE_AMT.Value = value.Replace("\0", "");
            }
            get
            {
                return _EXE_AMT.Value;
            }
        }
        private urfield _YRASSD = new urfield("YR_ASSD", "");
        public string YRASSD
        {
            set
            {
                _YRASSD.Value = value.Replace("\0", "");
            }
            get
            {
                return _YRASSD.Value;
            }
        }
        private urfield _LEGAL = new urfield("LEGAL", "");
        public string LEGAL
        {
            set
            {
                _LEGAL.Value = value.Replace("\0", "");
            }
            get
            {
                return _LEGAL.Value;
            }
        }
        private urfield _TYPE = new urfield("TYPE", "");
        public string TYPE
        {
            set
            {
                _TYPE.Value = value.Replace("\0", "");
            }
            get
            {
                return _TYPE.Value;
            }
        }
        private urfield _TYPE_STD = new urfield("TYPE_STD", "");
        public string TYPE_STD
        {
            set
            {
                _TYPE_STD.Value = value.Replace("\0", "");
            }
            get
            {
                return _TYPE_STD.Value;
            }
        }
        private urfield _DOC_DATE = new urfield("DOC_DATE", "");
        public string DOC_DATE
        {
            set
            {
                _DOC_DATE.Value = value.Replace("\0", "");
            }
            get
            {
                return _DOC_DATE.Value;
            }
        }
        private urfield _DOC_NUM = new urfield("DOC_NUM", "");
        public string DOC_NUM
        {
            set
            {
                _DOC_NUM.Value = value.Replace("\0", "");
            }
            get
            {
                return _DOC_NUM.Value;
            }
        }
        private urfield _S_ADDR_D = new urfield("S_ADDR_D", "");
        public string S_ADDR_D
        {
            set
            {
                _S_ADDR_D.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_ADDR_D.Value;
            }
        }
        private urfield _S_CTY_ST_D = new urfield("S_CTY_ST_D", "");
        public string S_CTY_ST_D
        {
            set
            {
                _S_CTY_ST_D.Value = value.Replace("\0", "");
            }
            get
            {
                return _S_CTY_ST_D.Value;
            }
        }
        private urfield _M_ADDR_D = new urfield("M_ADDR_D", "");
        public string M_ADDR_D
        {
            set
            {
                _M_ADDR_D.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_ADDR_D.Value;
            }
        }
        private urfield _M_CTY_ST_D = new urfield("M_CTY_ST_D", "");
        public string M_CTY_ST_D
        {
            set
            {
                _M_CTY_ST_D.Value = value.Replace("\0", "");
            }
            get
            {
                return _M_CTY_ST_D.Value;
            }
        }
        private urfield _BILL_NUM = new urfield("BILL_NUM", "");
        public string BILL_NUM
        {
            set
            {
                _BILL_NUM.Value = value.Replace("\0", "");
            }
            get
            {
                return _BILL_NUM.Value;
            }
        }
        private urfield _ALT_APN = new urfield("ALT_APN", "");
        public string ALT_APN
        {
            set
            {
                _ALT_APN.Value = value.Replace("\0", "");
            }
            get
            {
                return _ALT_APN.Value;
            }
        }
        private urfield _ALT_TYPE = new urfield("ALT_TYPE", "");
        public string ALT_TYPE
        {
            set
            {
                _ALT_TYPE.Value = value.Replace("\0", "");
            }
            get
            {
                return _ALT_TYPE.Value;
            }
        }
        #endregion

        #region methods
        public string writeHeader()
        {
            string result = "";
            foreach (urfield fld in _fields)
            {
                if (result != "") result += _delim;
                result += fld.Name;
            }
            return result;
        }
        public string writeOutput()
        {
            string result = "";
            foreach (urfield fld in _fields)
            {
                if (result != "") result += _delim;
                result += fld.Value.Trim();
            }
            return result;
        }
        #endregion
    }
}
