﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Program
    {
        static void Main(string[] args)
        {
            string allOK = "empty";
            County myCounty = null;

            string co3 = GetCo3(args);
            switch (co3)
            {
                case "ALA":
                    myCounty = new Ala();
                    allOK = myCounty.process();
                    break;
                case "ALP":
                    myCounty = new Alp();
                    allOK = myCounty.process();
                    break;
                case "AMA":
                    myCounty = new Ama();
                    allOK = myCounty.process();
                    break;
                case "BUT":
                    myCounty = new But();
                    allOK = myCounty.process();
                    break;
                case "CAL":
                    myCounty = new Cal();
                    allOK = myCounty.process();
                    break;
                case "CCX":
                    myCounty = new Ccx();
                    allOK = myCounty.process();
                    break;
                case "COL":
                    myCounty = new Col();
                    allOK = myCounty.process();
                    break;
                case "DNX":
                    myCounty = new Dnx();
                    allOK = myCounty.process();
                    break;
                case "EDX":
                    myCounty = new Edx();
                    allOK = myCounty.process();
                    break;
                case "FRE":
                    myCounty = new Fre();
                    allOK = myCounty.process();
                    break;
                case "GLE":
                    myCounty = new Gle();
                    allOK = myCounty.process();
                    break;
                case "HUM":
                    myCounty = new Hum();
                    allOK = myCounty.process();
                    break;
                case "IMP":
                    myCounty = new Imp();
                    allOK = myCounty.process();
                    break;
                case "INY":
                    myCounty = new Iny();
                    allOK = myCounty.process();
                    break;
                case "KER":
                    myCounty = new Ker();
                    allOK = myCounty.process();
                    break;
                case "KIN":
                    myCounty = new Kin();
                    allOK = myCounty.process();
                    break;
                case "LAK":
                    myCounty = new Lak();
                    allOK = myCounty.process();
                    break;
                case "LAS":
                    myCounty = new Las();
                    allOK = myCounty.process();
                    break;
                case "LAX":
                    myCounty = new Lax();
                    allOK = myCounty.process();
                    break;
                case "MAD":
                    myCounty = new Mad();
                    allOK = myCounty.process();
                    break;
                case "MEN":
                    myCounty = new Men();
                    allOK = myCounty.process();
                    break;
                case "MER":
                    myCounty = new Mer();
                    allOK = myCounty.process();
                    break;
                case "MNO":
                    myCounty = new Mno();
                    allOK = myCounty.process();
                    break;
                case "MOD":
                    myCounty = new Mod();
                    allOK = myCounty.process();
                    break;
                case "MON":
                    myCounty = new Mon();
                    allOK = myCounty.process();
                    break;
                case "MPA":
                    myCounty = new Mpa();
                    allOK = myCounty.process();
                    break;
                case "MRN":
                    myCounty = new Mrn();
                    allOK = myCounty.process();
                    break;
                case "NAP":
                    myCounty = new Nap();
                    allOK = myCounty.process();
                    break;
                case "NEV":
                    myCounty = new Nev();
                    allOK = myCounty.process();
                    break;
                case "ORG":
                    myCounty = new Org();
                    allOK = myCounty.process();
                    break;
                case "PLA":
                    myCounty = new Pla();
                    allOK = myCounty.process();
                    break;
                case "PLU":
                    myCounty = new Plu();
                    allOK = myCounty.process();
                    break;
                case "RIV":
                    myCounty = new Riv();
                    allOK = myCounty.process();
                    break;
                case "SAC":
                    myCounty = new Sac();
                    allOK = myCounty.process();
                    break;
                case "SBD":
                    myCounty = new Sbd();
                    allOK = myCounty.process();
                    break;
                case "SBT":
                    myCounty = new Sbt();
                    allOK = myCounty.process();
                    break;
                case "SBX":
                    myCounty = new Sbx();
                    allOK = myCounty.process();
                    break;
                case "SCL":
                    myCounty = new Scl();
                    allOK = myCounty.process();
                    break;
                case "SCR":
                    myCounty = new Scr();
                    allOK = myCounty.process();
                    break;
                case "SDX":
                    myCounty = new Sdx();
                    allOK = myCounty.process();
                    break;
                case "SFX":
                    myCounty = new Sfx();
                    allOK = myCounty.process();
                    break;
                case "SHA":
                    myCounty = new Sha();
                    allOK = myCounty.process();
                    break;
                case "SIE":
                    myCounty = new Sie();
                    allOK = myCounty.process();
                    break;
                case "SIS":
                    myCounty = new Sis();
                    allOK = myCounty.process();
                    break;
                case "SJX":
                    myCounty = new Sjx();
                    allOK = myCounty.process();
                    break;
                case "SLO":
                    myCounty = new Slo();
                    allOK = myCounty.process();
                    break;
                case "SMX":
                    myCounty = new Smx();
                    allOK = myCounty.process();
                    break;
                case "SOL":
                    myCounty = new Sol();
                    allOK = myCounty.process();
                    break;
                case "SON":
                    myCounty = new Son();
                    allOK = myCounty.process();
                    break;
                case "STA":
                    myCounty = new Sta();
                    allOK = myCounty.process();
                    break;
                case "SUT":
                    myCounty = new Sut();
                    allOK = myCounty.process();
                    break;
                case "TEH":
                    myCounty = new Teh();
                    allOK = myCounty.process();
                    break;
                case "TRI":
                    myCounty = new Tri();
                    allOK = myCounty.process();
                    break;
                case "TUL":
                    myCounty = new Tul();
                    allOK = myCounty.process();
                    break;
                case "TUO":
                    myCounty = new Tuo();
                    allOK = myCounty.process();
                    break;
                case "VEN":
                    myCounty = new Ven();
                    allOK = myCounty.process();
                    break;
                case "YOL":
                    myCounty = new Yol();
                    allOK = myCounty.process();
                    break;
                case "YUB":
                    myCounty = new Yub();
                    allOK = myCounty.process();
                    break;
                default:
                    System.Console.WriteLine("Example: LoadUnsec -C AMA");
                    if (co3 == "") 
                    {
                        allOK = "It appears that you didn't correctly specify a county.";
                    } 
                    else 
                    {
                        allOK = "ERROR: '" + co3 + "' is not a valid co3 Code.";
                    }
                    break;
            }

            if (allOK == "")
            {
                myCounty.log(myCounty.co3 + " apparently finished without any errors.");
            }
            else
            {
                
                System.Console.WriteLine("There was a problem: " + allOK);
            }
        }
        static string GetCo3(string[] args)
        {
            string result = "";
            int i;
            for (i = 0; i < args.Length - 1; i++)
            {
                if (args[i] == "-C")
                    result = args[i + 1];
            }
            return result;
        }
    }
}
