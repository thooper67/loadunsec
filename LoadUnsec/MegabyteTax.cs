﻿// This routine only applies to KIN, NAP and TEH where we receive the tax Agency CD files.
using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class MegabyteTax : County
    {
        public MegabyteTax()
        {
        }

        #region Properties
        public int iFldAPN = 0;
        public int iFldYEAR = 1;
        public int iFldFEE_PARCEL = 8;
        public int iFldTRA = 12;
        public int iFldLAND = 21;
        public int iFldFIXTR = 22;
        public int iFldGROWING = 23;
        public int iFldIMPR = 24;
        public int iFldPERSPROP = 25;
        public int iFldMHPP = 26;
        public int iFldOWNER = 35;
        public int iFldASSESSEE = 36;
        public int iFldMAILADDR1 = 37;
        public int iFldMAILADDR2 = 38;
        public int iFldMAILADDR3 = 39;
        public int iFldMAILADDR4 = 40;
        public int iFldEXE_CD = 43;
        public int iFldTOTAL_EXE = 44;
        public int iFldSITUS1 = 82;
        public int iFldSITUS2 = 83;
        public int iFldDESC = 85;
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            string line = "";
            int count = 0;
            StreamReader srInputFile = null;
            StreamWriter swOutFile = null;

            // open input file
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    srInputFile = new StreamReader(srcFile1);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while ((line = srInputFile.ReadLine()) != null)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];
                        line = cleanLine(line);

                        string[] fields = line.Split('\t');
                        if (fields[iFldYEAR] == ConfigurationManager.AppSettings["year"])
                        {
                            thisRec.APN_S = fields[iFldAPN];
                            thisRec.APN_D = County.formattedNumber(fields[iFldAPN], thisRec.APN_D_format);
                            thisRec.FEE_PARCEL_S = fields[iFldFEE_PARCEL];
                            thisRec.FEE_PARCEL_D = County.formattedNumber(fields[iFldFEE_PARCEL], thisRec.FEE_PARCEL_D_format);
                            thisRec.CO_NUM = coNum;
                            thisRec.CO_ID = co3;
                            thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                            thisRec.TRA = fields[iFldTRA];
                            thisRec.OWNER1 = fields[iFldOWNER];
                            thisRec.ASSESSEE = fields[iFldASSESSEE];
                            thisRec.EXE_CD = fields[iFldEXE_CD].Trim();
                            thisRec.LEGAL = cleanLine(fields[iFldDESC]);
                            thisRec.TYPE = thisRec.APN_S.Substring(0, 3);
                            thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                            values myVals = new values();
                            myVals.LAND = fields[iFldLAND];
                            myVals.IMPR = fields[iFldIMPR];
                            myVals.FIXTR = fields[iFldFIXTR];
                            myVals.PERSPROP = fields[iFldPERSPROP]; 
                            myVals.GROWING = fields[iFldGROWING];
                            myVals.MHPP = fields[iFldMHPP];
                            myVals.EXE_AMT = fields[iFldTOTAL_EXE];
                            thisRec.LAND = myVals.LAND;
                            thisRec.IMPR = myVals.IMPR;
                            thisRec.FIXTR = myVals.FIXTR;
                            thisRec.PERSPROP = myVals.PERSPROP;
                            thisRec.GROWING = myVals.GROWING;
                            thisRec.FIXTR_RP = myVals.FIXTR_RP;
                            thisRec.MHPP = myVals.MHPP;
                            thisRec.GROSS = myVals.GROSS;
                            thisRec.EXE_AMT = myVals.EXE_AMT;
                            if (thisRec.EXE_AMT == "7000") thisRec.EXE_CD = "E01";

                            mailing myMail = new mailing(fields[iFldMAILADDR1], fields[iFldMAILADDR2], fields[iFldMAILADDR3], fields[iFldMAILADDR4]);
                            if (co3=="ALP")
                            {
                                if (fields[iFldMAILADDR1].ToString().Contains("DBA ") == true)
                                {
                                    thisRec.DBA = myMail.dba;
                                }
                                else
                                {
                                    thisRec.DBA = "";
                                }
                            }
                            else
                            {
                                thisRec.DBA = myMail.dba;
                            }
                            thisRec.CARE_OF = myMail.care_of;
                            thisRec.M_STRNUM = myMail.m_strnum;
                            thisRec.M_STR_SUB = myMail.m_str_sub;
                            thisRec.M_DIR = myMail.m_dir;
                            thisRec.M_STREET = myMail.m_street;
                            thisRec.M_SUFF = myMail.m_suff;
                            thisRec.M_UNITNO = myMail.m_unit_no;
                            thisRec.M_CITY = myMail.m_city;
                            thisRec.M_ST = myMail.m_st;
                            thisRec.M_ZIP = myMail.m_zip;
                            thisRec.M_ZIP4 = myMail.m_zip4;
                            thisRec.M_ADDR_D = myMail.m_addr_d;
                            thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                            string situs1 = "";
                            if ((fields[iFldSITUS1].Length > 0) && (fields[iFldSITUS1].Substring(0, 1) != "0")) situs1 = fields[iFldSITUS1];
                            situs mySitus = new situs(situs1, fields[iFldSITUS2]);
                            if (co3 == "NAP")
                            {
                                if (fields[iFldDESC] == " ")
                                {
                                    mySitus = new situs(fields[iFldSITUS1], fields[iFldSITUS2]);
                                }
                                else
                                {
                                    mySitus = new situs(fields[iFldDESC], fields[iFldSITUS2]);
                                }
                            }
                            thisRec.S_HSENO = mySitus.s_hseno;
                            thisRec.S_STRNUM = mySitus.s_strnum;
                            thisRec.S_STR_SUB = mySitus.s_str_sub;
                            thisRec.S_DIR = mySitus.s_dir;
                            thisRec.S_STREET = mySitus.s_street;
                            thisRec.S_SUFF = mySitus.s_suff;
                            thisRec.S_UNITNO = mySitus.s_unit_no;
                            thisRec.S_CITY = mySitus.s_city;
                            thisRec.S_ST = mySitus.s_st;
                            thisRec.S_ZIP = mySitus.s_zip;
                            thisRec.S_ZIP4 = mySitus.s_zip4;
                            thisRec.S_ADDR_D = mySitus.s_addr_d;
                            thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                            swOutFile.WriteLine(thisRec.writeOutput());
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (srInputFile != null) srInputFile.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

    }
}
