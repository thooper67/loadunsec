﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Riv : County
    {
        public Riv()
        {
            co3 = "RIV";
            coNum = "33";
            populateStdTypes();
        }

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Get file in a DataTable
                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = drRoll["PIN"].ToString();
                        thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                        thisRec.FEE_PARCEL_S = drRoll["GEO"].ToString();
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = drRoll["TRA"].ToString();
                        thisRec.OWNER1 = cleanLine(drRoll["LegalParty1"].ToString());
                        thisRec.OWNER2 = cleanLine(drRoll["LegalParty2"].ToString() + " " + drRoll["LegalParty3"].ToString() + " " + drRoll["LegalParty4"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;
                        thisRec.LEGAL = cleanLine(drRoll["ClassDescription"].ToString());
                        thisRec.TYPE = drRoll["PIN"].ToString().Substring(0, 4);
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        if (drRoll["BusinessUseCodeDescription"].ToString() != "")
                        thisRec.ALT_TYPE = drRoll["BusinessUseCodeDescription"].ToString().Substring(0, 3);

                        Regex reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mCO = reCO.Match(drRoll["MailName"].ToString());
                        if (mCO.Success)
                        {
                            Group gCO = mCO.Groups[2];
                            thisRec.CARE_OF = cleanLine(gCO.ToString());
                        }
                        else
                        {
                            thisRec.CARE_OF = cleanLine(drRoll["MailName"].ToString());
                        }

                        values myVals = new values();
                        long.TryParse(removeDecimal(drRoll["ChurchExemption"].ToString()), out long lChurchExe);
                        long.TryParse(removeDecimal(drRoll["ReligiousExemption"].ToString()), out long lReligiousExe);
                        long.TryParse(removeDecimal(drRoll["PublicSchoolExemption"].ToString()), out long lSchoolExe);
                        long.TryParse(removeDecimal(drRoll["WelfareCollegeExemption"].ToString()), out long lCollegeExe);
                        long.TryParse(removeDecimal(drRoll["WelfareHospitalExemption"].ToString()), out long lHospitalExe);
                        long.TryParse(removeDecimal(drRoll["WelfareCharityReligiousExemption"].ToString()), out long lWelfareExe);
                        myVals.LAND = drRoll["Land"].ToString();
                        myVals.IMPR = drRoll["Improvements"].ToString();
                        myVals.FIXTR = drRoll["TradeFixturesAmount"].ToString();
                        myVals.GROWING = drRoll["LivingImprovements"].ToString();
                        myVals.PERSPROP = drRoll["PersonalValue"].ToString();
                        myVals.FIXTR_RP = drRoll["PersonalPropertyAppraised"].ToString();
                        myVals.PENALTY = drRoll["TenPercentAssessedPenalty"].ToString();
                        myVals.HO_EXE = (lChurchExe + lReligiousExe + lSchoolExe + lCollegeExe + lHospitalExe + lWelfareExe).ToString();
                        myVals.OTHER_EXE = drRoll["OtherExemption"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.GROWING = myVals.GROWING;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.FIXTR_RP = myVals.FIXTR_RP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.PENALTY = myVals.PENALTY;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        mailing myMail = new mailing(drRoll["MailAddress"].ToString(), cleanLine(drRoll["MailCity"].ToString()) + " " + drRoll["MailState"].ToString() + " " + drRoll["MailZipCode"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = drRoll["SitusStreetNumber"].ToString() + " " + drRoll["SitusStreetNumberSuffix"].ToString() + " " + drRoll["SitusStreetPredirectional"].ToString() + " " + drRoll["SitusStreetName"].ToString() + " " + drRoll["SitusStreetType"].ToString() + " " + drRoll["SitusUnitNumber"].ToString();
                        string situs2 = drRoll["SitusCityName"].ToString() + " CA " + drRoll["SitusZipCode"].ToString();
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("0000", "100");
            stdTypes.Add("0001", "100");
            stdTypes.Add("0002", "100");
            stdTypes.Add("0003", "100");
            stdTypes.Add("0004", "100");
            stdTypes.Add("0005", "100");
            stdTypes.Add("0006", "100");
            stdTypes.Add("0007", "401");
            stdTypes.Add("0011", "500");
            stdTypes.Add("0013", "300");
            stdTypes.Add("0014", "500");
            stdTypes.Add("0015", "200");
            stdTypes.Add("0016", "500");
            stdTypes.Add("0017", "500");
        }

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                string myBook = csv[0].Substring(0, 4);
                if (stdTypes.ContainsKey(myBook))
                {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
                }
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
        public static string removeDecimal(string input)
        {
            string result = input;
            Regex re = new Regex(@"(\.\d+)$");
            Match m1 = re.Match(input.Trim());
            if (re.IsMatch(input))
            {
                Group g = m1.Groups[1];
                result = input.Replace(g.ToString(), "").Trim();
            }
            return result;
        }
    }
}
