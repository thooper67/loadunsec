﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class mailing
    {
        public mailing() 
        {
        }
        public mailing(string inputLine1, string inputLine2, string inputLine3, string inputLine4)
        {
            line1 = inputLine1;
            line2 = inputLine2;
            line3 = inputLine3;
            line4 = inputLine4;
            parse();
        }

        #region properties
        public string line1 = "";
        public string line2 = "";
        public string line3 = "";
        public string line4 = "";
        public string dba = "";
        public string care_of = "";
        public string m_unit_no = "";
        public string m_strnum = "";
        public string m_str_sub = "";
        public string m_dir = "";
        public string m_street = "";
        public string m_suff = "";
        public string m_city = ""; 
        public string m_st = "";
        public string m_zip = "";
        public string m_zip4 = "";
        public string m_addr_d = "";
        public string m_cty_st_d = "";
        public string msgs = "";
        #endregion

        #region methods
        public void parse()
        {
            string addr1 = "";
            string addr2 = "";

            // Remove apostrophe and multiple spaces and trim.
            line1 = County.cleanLine(line1.Replace("'", ""));
            line2 = County.cleanLine(line2.Replace("'", ""));
            line3 = County.cleanLine(line3.Replace("'", ""));
            line4 = County.cleanLine(line4.Replace("'", ""));

            fillAddr1and2(line1, line2, line3, line4, out addr1, out addr2);
            if ((string.IsNullOrEmpty(addr1)) && (string.IsNullOrEmpty(addr2)) && !(string.IsNullOrEmpty(line2)) && !(string.IsNullOrEmpty(line3)))
            {
                fillAddr1and2(line2, line3, line4, "", out addr1, out addr2);
            }

            address myAddr = new address(addr1, addr2);
            m_unit_no = myAddr.unit_no;
            m_strnum = myAddr.strnum;
            m_str_sub = myAddr.str_sub;
            m_dir = myAddr.dir;
            m_street = myAddr.street;
            m_suff = myAddr.suff;
            m_city = myAddr.city;
            m_st = myAddr.st;
            m_zip = myAddr.zip;
            m_zip4 = myAddr.zip4;
            m_addr_d = myAddr.addr_d;
            m_cty_st_d = myAddr.cty_st_d;
            msgs = myAddr.msgs;
        }
        #endregion

        private void fillAddr1and2(string line1, string line2, string line3, string line4, out string addr1, out string addr2)
        {
            Regex reDBA = new Regex("^(" + County.RE_dba + ")", RegexOptions.IgnoreCase);
            Regex reCO = new Regex("^(" + County.RE_care_of + ")", RegexOptions.IgnoreCase);

            addr1 = line1;
            addr2 = line2;
            string name1 = "";
            string name2 = "";

            bool bLine1Blank = ((string.IsNullOrEmpty(line1)) || (line1 == "\0"));
            bool bLine2Blank = ((string.IsNullOrEmpty(line2)) || (line2 == "\0"));
            bool bLine3Blank = ((string.IsNullOrEmpty(line3)) || (line3 == "\0"));
            bool bLine4Blank = ((string.IsNullOrEmpty(line4)) || (line4 == "\0"));

            // Figure out addr1 and addr2
            while (!bLine1Blank)
            {
                if ((bLine3Blank) && (!bLine4Blank))
                {
                    line3 = line4;
                    line4 = "";
                    bLine3Blank = false;
                    bLine4Blank = true;
                }
                if ((bLine2Blank) && (!bLine3Blank))
                {
                    line2 = line3;
                    line3 = "";
                    bLine2Blank = false;
                    bLine3Blank = true;
                }
                addr1 = line1;
                addr2 = line2;
                if ((!bLine3Blank) && (!bLine4Blank))
                {
                    addr1 = line3;
                    addr2 = line4;
                    name1 = line1;
                    name2 = line2;
                    break;
                }
                if ((!bLine2Blank) && (!bLine3Blank))
                {
                    addr1 = line2;
                    addr2 = line3;
                    name1 = line1;
                    break;
                }
                Match m2 = reCO.Match(line1); // Make sure line1 doesn't have a C/O
                if (m2.Success)
                {
                    addr1 = "";
                    name1 = line1;
                }
                break;
            }

            // Figure out dba and c/o
            while (true)
            {
                int iFoundDBA = 0;
                int iFoundCO = 0;

                Match m1 = reDBA.Match(name1);
                if (m1.Success)
                {
                    Group g = m1.Groups[1];
                    dba = name1.Replace(g.ToString(), "").Trim();
                    iFoundDBA = 1;
                }
                Match m2 = reCO.Match(name1);
                if (m2.Success)
                {
                    Group g = m2.Groups[1];
                    care_of = name1.Replace(g.ToString(), "").Trim();
                    iFoundCO = 1;
                }

                if (iFoundDBA == 0)
                {
                    m1 = reDBA.Match(name2);
                    if (m1.Success)
                    {
                        Group g = m1.Groups[1];
                        dba = name2.Replace(g.ToString(), "").Trim();
                        iFoundDBA = 2;
                    }
                }
                if (iFoundCO == 0)
                {
                    m2 = reCO.Match(name2);
                    if (m2.Success)
                    {
                        Group g = m2.Groups[1];
                        care_of = name2.Replace(g.ToString(), "").Trim();
                        iFoundCO = 2;
                    }
                }

                if ((iFoundDBA == 1) && (iFoundCO == 0))
                {
                    care_of = name2;
                    iFoundCO = 2;
                }
                if ((iFoundDBA == 2) && (iFoundCO == 0))
                {
                    care_of = name1;
                    iFoundCO = 1;
                }
                if ((iFoundCO == 1) && (iFoundDBA == 0))
                {
                    dba = name2;
                    iFoundDBA = 2;
                }
                if ((iFoundCO == 2) && (iFoundDBA == 0))
                {
                    dba = name1;
                    iFoundDBA = 1;
                }
                if ((iFoundDBA == 0) && (iFoundCO == 0))
                {
                    // This test is to fix what we perceive as a county error.
                    Regex regx = new Regex(@"\WTAX\W", RegexOptions.IgnoreCase);
                    Match m = regx.Match(name1);
                    if ((m.Success) && (! string.IsNullOrEmpty(name2)))
                    {
                        // Console.WriteLine("Switched " + name2 + " to dba and " + name1 + " to care of");
                        dba = name2;
                        care_of = name1;
                    }
                    else
                    {
                        dba = name1;
                        care_of = name2;
                    }
                }
                break;
            }
        }
    }
}
