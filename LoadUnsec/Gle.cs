﻿using System;
using System.Linq;


namespace LoadUnsec
{
    class Gle : Megabyte
    {
        public Gle()
        {
            co3 = "GLE";
            coNum = "11";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("810", "101");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
            stdTypes.Add("880", "500");
        }

        // Old Crest processing

        // Field name, length, offset
        //fldTYPE = new fixfield(2, 12, "TYPE");

        //private void populateStdTypes()
        //{
        //    stdTypes.Add("01", "300");
        //    stdTypes.Add("02", "100");
        //    stdTypes.Add("03", "100");
        //    stdTypes.Add("04", "100");
        //    stdTypes.Add("05", "503");
        //    stdTypes.Add("06", "200");
        //    stdTypes.Add("07", "100");
        //    stdTypes.Add("08", "100");
        //    stdTypes.Add("09", "105");
        //    stdTypes.Add("10", "108");
        //    stdTypes.Add("11", "100");
        //    stdTypes.Add("12", "104");
        //    stdTypes.Add("13", "501");
        //    stdTypes.Add("14", "100");
        //    stdTypes.Add("15", "100");
        //    stdTypes.Add("16", "108");
        //    stdTypes.Add("17", "100");
        //    stdTypes.Add("18", "403");
        //    stdTypes.Add("19", "108");
        //    stdTypes.Add("20", "100");
        //    stdTypes.Add("21", "501");
        //    stdTypes.Add("22", "100");
        //    stdTypes.Add("23", "100");
        //    stdTypes.Add("24", "103");
        //    stdTypes.Add("25", "105");
        //    stdTypes.Add("26", "100");
        //    stdTypes.Add("27", "400");
        //}
    }
}