﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Alp : Megabyte
    {
        public Alp()
        {
            co3 = "ALP";
            coNum = "2";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("807", "401");
            stdTypes.Add("830", "200");
            stdTypes.Add("860", "400");
        }
    }
}
