﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Lak : Megabyte
    {
        public Lak()
        {
            co3 = "LAK";
            coNum = "17";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "104"); // BUSINESS ASSESSMENTS
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
        }
    }
}
