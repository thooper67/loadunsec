﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class address
    {
        public readonly Dictionary<string, string> directions = new Dictionary<string, string>();
        public readonly Dictionary<string, string> suffixes = new Dictionary<string, string>();
        public readonly Dictionary<string, string> suites = new Dictionary<string, string>();
        public readonly Dictionary<string, string> floors = new Dictionary<string, string>();

        public address() 
        {
            populateDirections();
            populateSuffixes();
            populateSuites();
            populateFloors();
        }
        public address(string inputLine1, string inputLine2)
        {
            populateDirections();
            populateSuffixes();
            populateSuites();
            populateFloors();
            line1 = inputLine1;
            line2 = inputLine2;
            parse();
        }

        #region properties
        public string line1 = "";
        public string line2 = "";
        public string unit_no = "";
        public string hseno = "";
        public string strnum = "";
        public string str_sub = "";
        public string _dir = "";
        public string dir
        {
            set 
            {
                if (directions.ContainsKey(value))
                {
                    _dir = directions[value];
                }
                else
                {
                    _dir = value;
                }
            }
            get { return _dir; }
        }
        public string street = "";
        public string suff = "";
        public string city = ""; 
        public string st = "";
        public string zip = "";
        public string zip4 = "";
        public string addr_d = "";
        public string cty_st_d = "";
        public string msgs = "";
        #endregion

        #region methods
        public void parse()
        {
            int aryTop = -1;
            int aryBottom = 0;

            // Remove commas, apostrophes and multiple spaces and trim.
            line1 = County.cleanLine(line1.Replace(",", "").Replace("'", ""));
            line2 = County.cleanLine(line2.Replace(",", "").Replace("'", ""));

            // Display fields
            addr_d = line1;
            cty_st_d = line2;

            // Process line 1
            // Unit Number
            if (line1.IndexOf("#") >= 1)
            {
                unit_no = line1.Substring(line1.IndexOf("#") + 1).Trim();
                Regex re = new Regex(@"(\s+ms\s*#.*)", RegexOptions.IgnoreCase);
                Match mUN = re.Match(line1);
                if (mUN.Success)
                {
                    Group gUN = mUN.Groups[1];
                    line1 = line1.Replace(gUN.ToString(), "").Trim();
                    unit_no = "MS " + unit_no;
                }
                else
                {
                    line1 = line1.Substring(0, line1.IndexOf("#")).Trim();  // Removes the unit number from the end of line 1.
                }
            }
            else if (line1.IndexOf("#") == 0)
            {
                line1 = line1.Substring(1).Trim(); // Throw away the first character since it is a # and more than likely it is the street number.
            }

            string[] aL1items = line1.Split(' ');
            aryTop = aL1items.GetUpperBound(0);
            while (aL1items.Length > 1)
            {
                // If the first element is a number, then that is the house number
                Regex rgx = new Regex(@"^[A-Z]*\d+");
                if (rgx.IsMatch(aL1items[aryBottom]))
                {
                    strnum = aL1items[aryBottom];
                    aryBottom++;

                    // This covers the 123/2 Main St case
                    string[] tmp1 = strnum.Split('/');
                    if (tmp1.GetUpperBound(0) > 0)
                    {
                        if (tmp1.GetUpperBound(0) > 1)
                        {
                            msgs += " Unaccounted for str_sub: " + strnum;
                        }
                        str_sub = tmp1[1];
                        strnum = tmp1[0];
                    }

                    // Is the next item a fraction?
                    rgx = new Regex(@"1/\d");
                    if (rgx.IsMatch(aL1items[aryBottom]))
                    {
                        str_sub = aL1items[aryBottom];
                        aryBottom++;
                    }

                    // If we haven't already found a sub, look at strnum for characters at the end and put them in sub
                    if (string.IsNullOrEmpty(str_sub))
                    {
                        rgx = new Regex(@"\d([a-zA-Z]+)$");
                        Match m1 = rgx.Match(strnum);
                        if (m1.Success)
                        {
                            Group g = m1.Groups[1];
                            str_sub = g.ToString();
                            strnum = strnum.Replace(g.ToString(), "").Trim();
                        }
                    }
                }
                rgx = new Regex(@"^0+$");
                if ((strnum == "") || (rgx.IsMatch(strnum)))
                {
                    rgx = new Regex(@"^([0\s]*)(P\.?\s?O\.?\s*B.?X|B.?X|P\.?\s?O\.?\s+DRAWER)\s*(\w.*)");
                    Match m = rgx.Match(line1);
                    if (rgx.IsMatch(line1))
                    {
                        //Group g = m.Groups[1];
                        //if ((g.ToString().Length >= 2) && (m.Groups[1].Length > 3))
                        //    street = line1.Replace(g.ToString(), "PO BOX").Trim();
                        street = "PO BOX " + m.Groups[3].ToString();
                        break;
                    }
                }                

                // Handle apt, ste, unit
                if ((aryTop > 1) && (suites.ContainsKey(aL1items[aryTop])))
                {
                    // The last item is a suite with nothing after it, so just throw the last item away if it isn't a direction.
                    if (directions.ContainsKey(aL1items[aryTop]))
                    {
                        dir = aL1items[aryTop];
                    }
                    aryTop--;
                }

                //rgx = new Regex(@"^\d+$");
                if ((aryTop > 2) && (suites.ContainsKey(aL1items[aryTop - 1])) && (!suffixes.ContainsKey(aL1items[aryTop])))
                {
                    unit_no = suites[aL1items[aryTop - 1]] + " " + aL1items[aryTop];
                    aryTop -= 2;
                }
                // Impossible because of  5 N HWY 88; 5 STATE HWY 88 etc.
                //else if ((aryTop > 2) && (aryTop - aryBottom > 1) && (!directions.ContainsKey(aL1items[aryTop - 2])) && (suffixes.ContainsKey(aL1items[aryTop - 1])) && (rgx.IsMatch(aL1items[aryTop])))
                //{
                //    unit_no = aL1items[aryTop];
                //    aryTop--;
                //}

                // Case where the last element is a direction
                if (!(suites.ContainsKey(aL1items[aryTop - 1])) && (directions.ContainsKey(aL1items[aryTop])))
                {
                    dir = aL1items[aryTop];
                    aryTop--;
                }

                // Handle floor
                if ((aryTop > 1) && (floors.ContainsKey(aL1items[aryTop])))
                {
                    unit_no = aL1items[aryTop - 1] + " FLOOR " + unit_no;
                    unit_no = unit_no.Trim();
                    aryTop -= 2;
                }
                else if ((aryTop > 1) && (floors.ContainsKey(aL1items[aryTop - 1])))
                {
                    Regex rgx1 = new Regex(@"^(\d+)");
                    Match mOrd = rgx1.Match(aL1items[aryTop]);
                    if (rgx1.IsMatch(aL1items[aryTop]))
                    {
                        Group g = mOrd.Groups[1];
                        int tmpOut;
                        int.TryParse(g.ToString(), out tmpOut);
                        unit_no = AddOrdinal(tmpOut) + " FLOOR " + unit_no;
                        unit_no = unit_no.Trim();
                        aryTop -= 2;
                    }
                }

                // Suffix
                if ((suffixes.ContainsKey(aL1items[aryTop])) && (aryTop > 1))
                {
                    suff = suffixes[aL1items[aryTop]];
                    aryTop--;
                }

                if (aryTop == 1)
                {
                    // Only a house number and one more item, so that item must be the street name.
                    if (strnum == "")
                    {
                        street = aL1items[aryTop - 1] + ' ' + aL1items[aryTop];
                    }
                    else
                    {
                        street = aL1items[aryTop];
                    }
                    break;
                }

                // Direction
                if (directions.ContainsKey(aL1items[aryBottom]))
                {
                    dir = aL1items[aryBottom];
                    aryBottom++;
                }

                // What remains is the street name
                // Below logic is commented out because it breaks a lot of stuff such as "HWY 049" in AMA.
                //if ((string.IsNullOrEmpty(suff)) && (string.IsNullOrEmpty(unit_no)))
                //{
                //    string tmpUnit = "";
                //    for (int i = aryTop; i >= aryBottom; i--)
                //    {
                //        if (suffixes.ContainsKey(aL1items[i]))
                //        {
                //            suff = suffixes[aL1items[i]];
                //            unit_no = tmpUnit.Trim();
                //            aryTop = i - 1;
                //            i = aryBottom - 1;
                //        }
                //        tmpUnit = aL1items[i] + " " + tmpUnit;
                //    }
                //}
                for (int i = aryBottom; i <= aryTop; i++)
                {
                    street += aL1items[i] + " ";
                }
                street = street.Trim();
                break;
            }

            // Process line 2
            // split line2 with space delimiter
            string[] aL2items = line2.Split(' ');
            while (aL2items.Length >= 1)
            {
                aryTop = aL2items.GetUpperBound(0);

                // Zip
                // does the last element have a "-"?
                if (aL2items[aryTop].IndexOf("-") >= 4)
                {
                    string[] myZip = aL2items[aryTop].Split('-');
                    if (myZip.Length == 2)
                    {
                        zip = myZip[0];
                        zip4 = myZip[1];
                    }
                    else // Something is funky, so just put the first item into zip
                    {
                        zip = myZip[0];
                    }
                    aryTop--;
                }
                else
                {
                    // if the last element is a number, then it is the zip
                    int iZip;
                    int.TryParse(aL2items[aryTop], out iZip);
                    if (iZip > 0)
                    {
                        zip = aL2items[aryTop];
                        aryTop--;
                    }
                }

                // The remaining last element should be the state
                if ((aryTop >= 0) && (aL2items[aryTop].Length == 2))
                {
                    st = aL2items[aryTop].Trim();
                    aryTop--;
                }

                // Whatever is left is the city
                for (int i = 0; i <= aryTop; i++)
                {
                    city += aL2items[i] + " ";
                }
                city = city.Trim();
                break;
            }
            Regex re0 = new Regex(@"^\s*0+(.+)", RegexOptions.IgnoreCase);
            Match m1a = re0.Match(strnum);
            if (re0.IsMatch(strnum))
            {
                Group g1 = m1a.Groups[1];
                strnum = g1.ToString();
            }
            m1a = re0.Match(hseno);
            if (re0.IsMatch(hseno))
            {
                Group g1 = m1a.Groups[1];
                hseno = g1.ToString();
            }
            m1a = re0.Match(addr_d);
            if (re0.IsMatch(addr_d))
            {
                Group g1 = m1a.Groups[1];
                addr_d = g1.ToString();
            }
            re0 = new Regex(@"^\s*0+\s*$", RegexOptions.IgnoreCase);
            if (re0.IsMatch(addr_d))
            {
                addr_d = "";
            }
            re0 = new Regex(@"^\s*0+", RegexOptions.IgnoreCase);
            if ((re0.IsMatch(strnum)) || (re0.IsMatch(hseno)))
            {
                strnum = "";
                hseno = "";
            }
        }
        #endregion

        private void populateDirections()
        {
            directions.Add("N", "N");
            directions.Add("NORTH", "N");
            directions.Add("NO", "N");
            directions.Add("NO.", "N");
            directions.Add("N.", "N");
            directions.Add("S", "S");
            directions.Add("SOUTH", "S");
            directions.Add("SO", "S");
            directions.Add("SO.", "S");
            directions.Add("S.", "S");
            directions.Add("E", "E");
            directions.Add("EAST", "E");
            directions.Add("EA", "E");
            directions.Add("E.", "E");
            directions.Add("W", "W");
            directions.Add("WEST", "W");
            directions.Add("WE", "W");
            directions.Add("W.", "W");
            directions.Add("NE", "NE");
            directions.Add("N.E.", "NE");
            directions.Add("N. E. ", "NE");
            directions.Add("NW", "NW");
            directions.Add("N.W.", "NW");
            directions.Add("N. W. ", "NW");
            directions.Add("SE", "SE");
            directions.Add("S.E.", "SE");
            directions.Add("S. E. ", "SE");
            directions.Add("SW", "SW");
            directions.Add("S.W.", "SW");
            directions.Add("S. W. ", "SW");
        }
        private void populateSuffixes()
        {
            suffixes.Add("AL", "ALY");
            suffixes.Add("ALY", "ALY");
            suffixes.Add("ANX", "ANX");
            suffixes.Add("ARC", "ARC");
            suffixes.Add("AV", "AVE");
            suffixes.Add("AV.", "AVE");
            suffixes.Add("AVE", "AVE");
            suffixes.Add("AVE.", "AVE");
            suffixes.Add("AVE,", "AVE");
            suffixes.Add("AVE.,", "AVE");
            suffixes.Add("AVENUE", "AVE");
            suffixes.Add("AVENUE,", "AVE");
            suffixes.Add("BCH", "BCH");
            suffixes.Add("BG", "BG");
            suffixes.Add("BGS", "BGS");
            suffixes.Add("BL", "BLVD");
            suffixes.Add("BLF", "BLF");
            suffixes.Add("BLFS", "BLFS");
            suffixes.Add("BLV", "BLVD");
            suffixes.Add("BLV.", "BLVD");
            suffixes.Add("BLV,", "BLVD");
            suffixes.Add("BLVD", "BLVD");
            suffixes.Add("BLVD.", "BLVD");
            suffixes.Add("BLVD,", "BLVD");
            suffixes.Add("BLVD.,", "BLVD");
            suffixes.Add("BND", "BND");
            suffixes.Add("BOULEVARD", "BLVD");
            suffixes.Add("BR", "BR");
            suffixes.Add("BRG", "BRG");
            suffixes.Add("BRK", "BRK");
            suffixes.Add("BRKS", "BRKS");
            suffixes.Add("BRNCH", "BRNCH");
            suffixes.Add("BTM", "BTM");
            suffixes.Add("BYP", "BYP");
            suffixes.Add("BYU", "BYU");
            suffixes.Add("CENTER", "CTR");
            suffixes.Add("CI", "CIR");
            suffixes.Add("CIR", "CIR");
            suffixes.Add("CIRCLE", "CIR");
            suffixes.Add("CIRS", "CIRS");
            suffixes.Add("CL", "CIR");
            suffixes.Add("CLB", "CLB");
            suffixes.Add("CLF", "CLF");
            suffixes.Add("CLFS", "CLFS");
            suffixes.Add("CM", "CMN");
            suffixes.Add("CMN", "CMN");
            suffixes.Add("CO", "CT");
            suffixes.Add("COR", "COR");
            suffixes.Add("CORS", "CORS");
            suffixes.Add("COURT", "CT");
            suffixes.Add("CP", "CP");
            suffixes.Add("CPE", "CPE");
            suffixes.Add("CR", "CIR");
            suffixes.Add("CRES", "CRES");
            suffixes.Add("CRK", "CRK");
            suffixes.Add("CRSE", "CRSE");
            suffixes.Add("CRST", "CRST");
            suffixes.Add("CSWY", "CSWY");
            suffixes.Add("CT", "CT");
            suffixes.Add("CT.", "CT");
            suffixes.Add("CT,", "CT");
            suffixes.Add("CT.,", "CT");
            suffixes.Add("CTR", "CTR");
            suffixes.Add("CTRS", "CTRS");
            suffixes.Add("CTS", "CTS");
            suffixes.Add("CURV", "CURV");
            suffixes.Add("CURVE", "CURV");
            suffixes.Add("CV", "CV");
            suffixes.Add("CVS", "CVS");
            suffixes.Add("CY", "CYN");
            suffixes.Add("CYN", "CYN");
            suffixes.Add("DL", "DL");
            suffixes.Add("DM", "DM");
            suffixes.Add("DR", "DR");
            suffixes.Add("DR.", "DR");
            suffixes.Add("DR,", "DR");
            suffixes.Add("DR.,", "DR");
            suffixes.Add("DRIVE", "DR");
            suffixes.Add("DRIVE,", "DR");
            suffixes.Add("DRS", "DRS");
            suffixes.Add("DV", "DV");
            suffixes.Add("EST", "EST");
            suffixes.Add("ESTS", "ESTS");
            suffixes.Add("EX", "EXT");
            suffixes.Add("EXPRESSWAY", "EXPY");
            suffixes.Add("EXPRESSWAY,", "EXPY");
            suffixes.Add("EXPRS", "EXPY");
            suffixes.Add("EXPY", "EXPY");
            suffixes.Add("EXPWY", "EXPY");
            suffixes.Add("EXT", "EXT");
            suffixes.Add("EXTS", "EXTS");
            suffixes.Add("FALL_X", "FALL");
            suffixes.Add("FLD", "FLD");
            suffixes.Add("FLDS", "FLDS");
            suffixes.Add("FLS", "FLS");
            suffixes.Add("FLT", "FLT");
            suffixes.Add("FLTS", "FLTS");
            suffixes.Add("FRD", "FRD");
            suffixes.Add("FRDS", "FRDS");
            suffixes.Add("FREEWAY", "FWY");
            suffixes.Add("FRG", "FRG");
            suffixes.Add("FRGS", "FRGS");
            suffixes.Add("FRK", "FRK");
            suffixes.Add("FRKS", "FRKS");
            suffixes.Add("FRST", "FRST");
            suffixes.Add("FRY", "FRY");
            suffixes.Add("FT", "FT");
            suffixes.Add("FWY", "FWY");
            suffixes.Add("GDN", "GDN");
            suffixes.Add("GDNS", "GDNS");
            suffixes.Add("GLN", "GLN");
            suffixes.Add("GLNS", "GLNS");
            suffixes.Add("GRD", "GRD");
            suffixes.Add("GRN", "GRN");
            suffixes.Add("GRNS", "GRNS");
            suffixes.Add("GRV", "GRV");
            suffixes.Add("GRVS", "GRVS");
            suffixes.Add("GTWY", "GTWY");
            suffixes.Add("HBR", "HBR");
            suffixes.Add("HBRS", "HBRS");
            suffixes.Add("HGHT", "HGHT");
            suffixes.Add("HIGHWAY", "HWY");
            suffixes.Add("HL", "HL");
            suffixes.Add("HLS", "HLS");
            suffixes.Add("HOLW", "HOLW");
            suffixes.Add("HTS", "HTS");
            suffixes.Add("HVN", "HVN");
            suffixes.Add("HW", "HWY");
            suffixes.Add("HWY", "HWY");
            suffixes.Add("HY", "HWY");
            suffixes.Add("INLT", "INLT");
            suffixes.Add("INTG", "INTG");
            suffixes.Add("IS", "IS");
            suffixes.Add("ISLE", "ISLE");
            suffixes.Add("ISS", "ISS");
            suffixes.Add("JCT", "JCT");
            suffixes.Add("JCTS", "JCTS");
            suffixes.Add("KNL", "KNL");
            suffixes.Add("KNLS", "KNLS");
            suffixes.Add("KY", "KY");
            suffixes.Add("KYS", "KYS");
            suffixes.Add("LA", "LN");
            suffixes.Add("LAND", "LAND");
            suffixes.Add("LANDING", "LNDG");
            suffixes.Add("LANE", "LN");
            suffixes.Add("LANE,", "LN");
            suffixes.Add("LCK", "LCK");
            suffixes.Add("LCKS", "LCKS");
            suffixes.Add("LDG", "LDG");
            suffixes.Add("LEVEE", "LEVEE");
            suffixes.Add("LF", "LF");
            suffixes.Add("LGT", "LGT");
            suffixes.Add("LGTS", "LGTS");
            suffixes.Add("LK", "LK");
            suffixes.Add("LKS", "LKS");
            suffixes.Add("LN", "LN");
            suffixes.Add("LN.", "LN");
            suffixes.Add("LN,", "LN");
            suffixes.Add("LNDG", "LNDG");
            suffixes.Add("LO", "LOOP");
            suffixes.Add("LOOP", "LOOP");
            suffixes.Add("LP", "LOOP");
            suffixes.Add("MALL", "MALL");
            suffixes.Add("MDW", "MDW");
            suffixes.Add("MDWS", "MDWS");
            suffixes.Add("MEWS", "MEWS");
            suffixes.Add("ML", "ML");
            suffixes.Add("MLS", "MLS");
            suffixes.Add("MNR", "MNR");
            suffixes.Add("MNRS", "MNRS");
            suffixes.Add("MSN", "MSN");
            suffixes.Add("MT", "MT");
            suffixes.Add("MTN", "MTN");
            suffixes.Add("MTNS", "MTNS");
            suffixes.Add("MTWY", "MTWY");
            suffixes.Add("NCK", "NCK");
            suffixes.Add("OPAS", "OPAS");
            suffixes.Add("ORCH", "ORCH");
            suffixes.Add("OVAL", "OVAL");
            suffixes.Add("PARK", "PARK");
            suffixes.Add("PARKWAY", "PKWY");
            suffixes.Add("PARKWAY,", "PKWY");
            suffixes.Add("PASS", "PASS");
            suffixes.Add("PATH", "PATH");
            suffixes.Add("PIKE", "PIKE");
            suffixes.Add("PK", "PARK");
            suffixes.Add("PKWY", "PKWY");
            suffixes.Add("PKWY.", "PKWY");
            suffixes.Add("PKWY,", "PKWY");
            suffixes.Add("PKWY.,", "PKWY");
            suffixes.Add("PLWY", "PKWY");
            suffixes.Add("PKY", "PKWY");
            suffixes.Add("PL", "PL");
            suffixes.Add("PL.", "PL");
            suffixes.Add("PL,", "PL");
            suffixes.Add("PLA", "PLZ");
            suffixes.Add("PLACE", "PL");
            suffixes.Add("PLAZA", "PLZ");
            suffixes.Add("PLN", "PLN");
            suffixes.Add("PLNS", "PLNS");
            suffixes.Add("PLZ", "PLZ");
            suffixes.Add("PNE", "PNE");
            suffixes.Add("PNES", "PNES");
            suffixes.Add("PR", "PR");
            suffixes.Add("PRT", "PRT");
            suffixes.Add("PRTS", "PRTS");
            suffixes.Add("PSGE", "PSGE");
            suffixes.Add("PT", "PT");
            suffixes.Add("PTS", "PTS");
            suffixes.Add("PW", "PKWY");
            suffixes.Add("PY", "PKWY");
            suffixes.Add("PZ", "PLZ");
            suffixes.Add("RADL", "RADL");
            suffixes.Add("RAMP", "RAMP");
            suffixes.Add("RAOD", "RD");
            suffixes.Add("RD", "RD");
            suffixes.Add("RD.", "RD");
            suffixes.Add("RD,", "RD");
            suffixes.Add("RD.,", "RD");
            suffixes.Add("RDG", "RDG");
            suffixes.Add("RDGS", "RDGS");
            suffixes.Add("RDS", "RDS");
            suffixes.Add("RDWY", "RDWY");
            suffixes.Add("RIV", "RIV");
            suffixes.Add("RN", "RUN");
            suffixes.Add("RNCH", "RNCH");
            suffixes.Add("ROAD", "RD");
            suffixes.Add("ROAD,", "RD");
            suffixes.Add("ROW", "ROW");
            suffixes.Add("RPD", "RPD");
            suffixes.Add("RPDS", "RPDS");
            suffixes.Add("RST", "RST");
            suffixes.Add("RT", "RT");
            suffixes.Add("RTE", "RTE");
            suffixes.Add("RUE", "RUE");
            suffixes.Add("RUN", "RUN");
            suffixes.Add("RW", "RW");
            suffixes.Add("SHL", "SHL");
            suffixes.Add("SHLS", "SHLS");
            suffixes.Add("SHR", "SHR");
            suffixes.Add("SHRS", "SHRS");
            suffixes.Add("SKWY", "SKWY");
            suffixes.Add("SMT", "SMT");
            suffixes.Add("SPDWY", "SPDWY");
            suffixes.Add("SPG", "SPG");
            suffixes.Add("SPGS", "SPGS");
            suffixes.Add("SPUR", "SPUR");
            suffixes.Add("SQ", "SQ");
            suffixes.Add("SQUARE", "SQ");
            suffixes.Add("SQS", "SQS");
            suffixes.Add("ST", "ST");
            suffixes.Add("ST.", "ST");
            suffixes.Add("ST,", "ST");
            suffixes.Add("ST.,", "ST");
            suffixes.Add("STA", "STA");
            suffixes.Add("STRA", "STRA");
            suffixes.Add("STREET", "ST");
            suffixes.Add("STREET,", "ST");
            suffixes.Add("STRM", "STRM");
            suffixes.Add("STS", "ST");
            suffixes.Add("TE", "TER");
            suffixes.Add("TER", "TER");
            suffixes.Add("TERR", "TER");
            suffixes.Add("TERRACE", "TER");
            suffixes.Add("THRUW", "THRUW");
            suffixes.Add("TL", "TRL");
            suffixes.Add("TPKE", "TPKE");
            suffixes.Add("TR", "TRL");
            suffixes.Add("TRAIL", "TRL");
            suffixes.Add("TRAK", "TRAK");
            suffixes.Add("TRCE", "TRCE");
            suffixes.Add("TRFY", "TRFY");
            suffixes.Add("TRL", "TRL");
            suffixes.Add("TRWY", "TRWY");
            suffixes.Add("TUNL", "TUNL");
            suffixes.Add("UN", "UN");
            suffixes.Add("UNS", "UNS");
            suffixes.Add("UPAS", "UPAS");
            suffixes.Add("VDCT", "VDCT");
            suffixes.Add("VIADUCT", "VIA");
            suffixes.Add("VIS", "VIS");
            suffixes.Add("VL", "VL");
            suffixes.Add("VLG", "VLG");
            suffixes.Add("VLGS", "VLGS");
            suffixes.Add("VLY", "VLY");
            suffixes.Add("VLYS", "VLYS");
            suffixes.Add("VW", "VW");
            suffixes.Add("VWS", "VWS");
            suffixes.Add("WA", "WAY");
            suffixes.Add("WALK", "WALK");
            suffixes.Add("WALL", "WALL");
            suffixes.Add("WAY", "WAY");
            suffixes.Add("WAYS", "WAYS");
            suffixes.Add("WK", "WK");
            suffixes.Add("WL", "WL");
            suffixes.Add("WLS", "WLS");
            suffixes.Add("WY", "WAY");
            suffixes.Add("XING", "XING");
            suffixes.Add("XRD", "XRD");
        }
        private void populateSuites()
        {
            suites.Add("APT", "APT");
            suites.Add("APT.", "APT");
            suites.Add("ATE", "STE");
            suites.Add("BLD", "BLDG");
            suites.Add("BLD.", "BLDG");
            suites.Add("BLDG", "BLDG");
            suites.Add("BLDG.", "BLDG");
            suites.Add("NO", "NO");
            suites.Add("NO.", "NO");
            suites.Add("ROOM", "RM");
            suites.Add("RM", "RM");
            suites.Add("SECT", "SECT");
            suites.Add("SP", "SP");
            suites.Add("SP.", "SP");
            suites.Add("SPC", "SP");
            suites.Add("SPACE", "SP");
            suites.Add("STE", "STE");
            suites.Add("STE.", "STE");
            suites.Add("STE:", "STE");
            suites.Add("SU", "STE");
            suites.Add("SUITE", "STE");
            suites.Add("UNIT", "UNIT");          
        }
        private void populateFloors()
        {
            floors.Add("FLOOR", "FL");
            floors.Add("FLR", "FL");
            floors.Add("FL", "FL");
            floors.Add("FL.", "FL");
        }
        public string AddOrdinal(int num)
        {
            switch (num % 100)
            {
                case 11:
                case 12:
                case 13:
                    return num.ToString() + "TH";
            }

            switch (num % 10)
            {
                case 1:
                    return num.ToString() + "ST";
                case 2:
                    return num.ToString() + "ND";
                case 3:
                    return num.ToString() + "RD";
                default:
                    return num.ToString() + "TH";
            }
        }
    }
}
