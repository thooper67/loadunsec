﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Sac : County
    {
        public Sac()
        {
            co3 = "SAC";
            coNum = "34";
            populateStdTypes();
        }

        #region Properties
        public int iFldMAPB = 0;
        public int iFldPG = 1;
        public int iFldPCL = 2;
        public int iFldPSUB = 3;
        public int iFldPARSU = 4;
        public int iFldCLASS_CODE = 5;
        public int iFldTRA = 6;
        public int iFldSITUS_ST_NUMB = 7;
        public int iFldSITUS_PRE_DIR = 8;
        public int iFldSITUS_STREET = 9;
        public int iFldSITUS_SUFFIX = 10;
        public int iFldSITUS_POST_DIR = 11;
        public int iFldSITUS_UNIT_IND = 12;
        public int iFldSITUS_UNIT_NUMB = 13;
        public int iFldSITUS_CITY = 14;
        public int iFldSITUS_STATE = 15;
        public int iFldSITUS_ZIP = 16;
        public int iFldDBA_NAME = 17;
        public int iFldCARE_OF = 18;
        public int iFldMAIL_ST_NUMB = 19;
        public int iFldMAIL_PRE_DIR = 20;
        public int iFldMAIL_STREET = 21;
        public int iFldMAIL_POST_DIR = 22;
        public int iFldMAIL_SUFFIX = 23;
        public int iFldMAIL_UNIT_IND = 24;
        public int iFldMAIL_UNIT_NUMB = 25;
        public int iFldMAIL_CITY = 26;
        public int iFldMAIL_STATE = 27;
        public int iFldMAIL_ZIP = 28;
        public int iFldMAIL_PO_BOX = 29;
        public int iFldFOREIGN_ADD1 = 30;
        public int iFldFOREIGN_ADD2 = 31;
        public int iFldFOREIGN_ADD3 = 32;
        public int iFldCHURCH_EX = 33;
        public int iFldVET_EX = 34;
        public int iFldHO_EX = 35;
        public int iFldLAND_VALUE = 36;
        public int iFldIMP_VALUE = 37;
        public int iFldFIXT_VALUE = 38;
        public int iFldPPROP_VALUE = 39;
        public int iFldBOAT_VALUE = 40;
        public int iFldACFT_VALUE = 41;
        public int iFldBOAT_NUM = 42;
        public int iFldACFT_NUM = 43;
        public int iFldSEC_FLG = 44;
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamReader srInputFile = null;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input file
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true, ';');
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    foreach (DataRow fields in dtRoll.Rows)
                    {
                        if (fields[iFldSEC_FLG].ToString() == "0")
                        {
                            count++;
                            unsecrec thisRec = new unsecrec();

                            thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                            thisRec.APN_S = fields[iFldMAPB].ToString() + fields[iFldPG].ToString() + fields[iFldPCL].ToString() + fields[iFldPSUB].ToString() + fields[iFldPARSU].ToString();
                            thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                            thisRec.CO_NUM = coNum;
                            thisRec.CO_ID = co3;
                            thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                            thisRec.TRA = fields[iFldTRA].ToString().PadLeft(6, '0');
                            thisRec.LEGAL = fields[iFldBOAT_NUM].ToString() + fields[iFldACFT_NUM].ToString();
                            thisRec.TYPE = fields[iFldCLASS_CODE].ToString();
                            thisRec.TYPE_STD = xlatType(fields[iFldCLASS_CODE].ToString());
                            if (fields[iFldCHURCH_EX].ToString() != "0" && fields[iFldCHURCH_EX].ToString() != "") thisRec.EXE_CD = "CH";
                            if (fields[iFldVET_EX].ToString() != "0" && fields[iFldVET_EX].ToString() != "") thisRec.EXE_CD = "VE";
                            if (fields[iFldHO_EX].ToString() != "0" && fields[iFldHO_EX].ToString() != "") thisRec.EXE_CD = "HO";

                            thisRec.OWNER1 = cleanLine(fields[iFldDBA_NAME].ToString());
                            thisRec.ASSESSEE = thisRec.OWNER1;
                            thisRec.DBA = thisRec.OWNER1;
                            thisRec.CARE_OF = cleanLine(fields[iFldCARE_OF].ToString());

                            values myVals = new values();
                            myVals.LAND = fields[iFldLAND_VALUE].ToString();
                            myVals.IMPR = fields[iFldIMP_VALUE].ToString();
                            myVals.FIXTR = fields[iFldFIXT_VALUE].ToString();
                            myVals.PERSPROP = addStrings(fields[iFldPPROP_VALUE].ToString(), fields[iFldBOAT_VALUE].ToString(), fields[iFldACFT_VALUE].ToString());
                            myVals.EXE_AMT = addStrings(fields[iFldCHURCH_EX].ToString(), fields[iFldVET_EX].ToString(), fields[iFldHO_EX].ToString());
                            thisRec.LAND = myVals.LAND;
                            thisRec.IMPR = myVals.IMPR;
                            thisRec.FIXTR = myVals.FIXTR;
                            thisRec.PERSPROP = myVals.PERSPROP;
                            thisRec.GROSS = myVals.GROSS;
                            thisRec.EXE_AMT = myVals.EXE_AMT;

                            string mail1 = "";
                            string mail2 = "";
                            if (fields[iFldMAIL_ZIP].ToString() == "00001")
                            {
                                mail1 = fields[iFldFOREIGN_ADD1].ToString();
                                mail2 = fields[iFldFOREIGN_ADD2].ToString() + " " + fields[iFldFOREIGN_ADD3].ToString();
                            }
                            else if (fields[iFldMAIL_PO_BOX].ToString() != "")
                            {
                                mail1 = "PO BOX " + fields[iFldMAIL_PO_BOX].ToString();
                                mail2 = fields[iFldMAIL_CITY].ToString() + " " + fields[iFldMAIL_STATE].ToString() + " " + fields[iFldMAIL_ZIP].ToString();
                            }
                            else
                            {
                                mail1 = fields[iFldMAIL_ST_NUMB].ToString() + " " + fields[iFldMAIL_PRE_DIR].ToString() + " " + fields[iFldMAIL_STREET].ToString() + " " + fields[iFldMAIL_POST_DIR].ToString() + " " + fields[iFldMAIL_SUFFIX].ToString() + " " + fields[iFldMAIL_UNIT_IND].ToString() + " " + fields[iFldMAIL_UNIT_NUMB].ToString();
                                mail2 = fields[iFldMAIL_CITY].ToString() + " " + fields[iFldMAIL_STATE].ToString() + " " + fields[iFldMAIL_ZIP].ToString();
                            }
                            mailing myMail = new mailing(mail1, mail2, "", "");
                            thisRec.M_STRNUM = myMail.m_strnum;
                            thisRec.M_STR_SUB = myMail.m_str_sub;
                            thisRec.M_DIR = myMail.m_dir;
                            thisRec.M_STREET = myMail.m_street;
                            thisRec.M_SUFF = myMail.m_suff;
                            thisRec.M_UNITNO = myMail.m_unit_no;
                            thisRec.M_CITY = myMail.m_city;
                            thisRec.M_ST = myMail.m_st;
                            thisRec.M_ZIP = myMail.m_zip;
                            thisRec.M_ZIP4 = myMail.m_zip4;
                            thisRec.M_ADDR_D = myMail.m_addr_d;
                            thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                            string mySZip = "";
                            if (fields[iFldSITUS_ZIP].ToString() != "00000") mySZip = fields[iFldSITUS_ZIP].ToString();
                            string situs1 = fields[iFldSITUS_ST_NUMB].ToString() + " " + fields[iFldSITUS_PRE_DIR].ToString() + " " + fields[iFldSITUS_STREET].ToString() + " " + fields[iFldSITUS_POST_DIR].ToString() + " " + fields[iFldSITUS_SUFFIX].ToString() + " " + fields[iFldSITUS_UNIT_IND].ToString() + " " + fields[iFldSITUS_UNIT_NUMB].ToString();
                            string situs2 = fields[iFldSITUS_CITY].ToString() + " " + mySZip;
                            situs mySitus = new situs(situs1, situs2);
                            thisRec.S_HSENO = mySitus.s_hseno;
                            thisRec.S_STRNUM = mySitus.s_strnum;
                            thisRec.S_STR_SUB = mySitus.s_str_sub;
                            thisRec.S_DIR = mySitus.s_dir;
                            thisRec.S_STREET = mySitus.s_street;
                            thisRec.S_SUFF = mySitus.s_suff;
                            thisRec.S_UNITNO = mySitus.s_unit_no;
                            thisRec.S_CITY = mySitus.s_city;
                            thisRec.S_ST = mySitus.s_st;
                            thisRec.S_ZIP = mySitus.s_zip;
                            thisRec.S_ZIP4 = mySitus.s_zip4;
                            thisRec.S_ADDR_D = mySitus.s_addr_d;
                            thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                            swOutFile.WriteLine(thisRec.writeOutput());
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Error from sorted input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (srInputFile != null) srInputFile.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                DataRow dr = table.NewRow();
                for (int i = 0; i < csv.FieldCount; i++)
                {
                    dr[i] = csv[i];
                }
                table.Rows.Add(dr);
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }

        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("12", "502");
            stdTypes.Add("14", "101");
            stdTypes.Add("15", "103");
            stdTypes.Add("16", "501");
            stdTypes.Add("17", "200");
            stdTypes.Add("18", "300");
            stdTypes.Add("19", "200");
            stdTypes.Add("20", "100");
            stdTypes.Add("22", "504");
            stdTypes.Add("25", "403");
            stdTypes.Add("27", "103");
            stdTypes.Add("28", "106");
            stdTypes.Add("40", "102");
            stdTypes.Add("44", "400");
            stdTypes.Add("60", "301");
            stdTypes.Add("61", "301");
            stdTypes.Add("79", "201");
        }
    }
}
