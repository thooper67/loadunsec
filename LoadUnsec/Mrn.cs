﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Mrn : County
    {
        public Mrn()
        {
            co3 = "MRN";
            coNum = "21";
            populateStdTypes();
        }

        #region Properties
        // Field names and positions from unsecured roll header
        //  0 PROPERTY ID
        //  1 MAIL TO NAME 1
        //  2 MAIL TO NAME 2
        //  3 ENROLLMENT ID
        //  4 BILL NBR
        //  5 SITUS ADDRESS
        //  6 SITUS CITY, STATE AND ZIP
        //  7 CARE OF NAME
        //  8 ATTENTION NAME
        //  9 MAIL ADDRESS
        // 10 MAIL ADDRESS
        // 11 MAIL ADDRESS
        // 12 MAIL ADDRESS
        // 13 LOC PROP ID
        // 14 TAX RATE AREA
        // 15 USE CODE
        // 16 DEED REF ID
        // 17 RECORDATION DT
        // 18 LAND VALUE
        // 19 IMPROVEMENT VALUE
        // 20 PERSONAL VALUE
        // 21 BUSINESS VALUE
        // 22 HO EXEMPTION
        // 23 OTH 1
        // 24 OTH 1 EXEMPTION
        // 25 OTH 2
        // 26 OTH 2 EXEMPTION
        // 27 OTH 3
        // 28 OTH 3 EXEMPTION
        // 29 NET VALUE
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [Unsecured Roll$] ORDER BY [PROPERTY ID]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();

                        thisRec.APN_S = fields["PROPERTY ID"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        if (fields["LOC PROP ID"].ToString().Trim().Length > 9)
                        {
                            thisRec.FEE_PARCEL_S = fields["LOC PROP ID"].ToString().Replace("-", "");
                            thisRec.FEE_PARCEL_D = fields["LOC PROP ID"].ToString();
                        }
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = fields["TAX RATE AREA"].ToString().Replace("-", "");
                        thisRec.TYPE = fields["USE CODE"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.BILL_NUM = fields["BILL NBR"].ToString();

                        //try
                        //{
                        //    thisRec.DOC_DATE = DateTime.Parse(fields["RECORDATION DT"].ToString()).ToString("yyyyMMdd");
                        //}
                        //catch (Exception e)
                        //{
                        //    thisRec.DOC_DATE = "";
                        //    result = "APN '" + thisRec.APN_D + "' - bad Doc Date: '" + fields["RECORDATION DT"].ToString() + "'" + e.Message;
                        //    log(result);
                        //}
                        //thisRec.DOC_NUM = fields["DEED REF ID"].ToString();
                        
                        thisRec.OWNER1 = cleanLine(fields["MAIL TO NAME 1"].ToString());
                        Regex reDBA = new Regex(@"^(" + County.RE_dba + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mDBA = reDBA.Match(fields["MAIL TO NAME 2"].ToString());
                        if (reDBA.IsMatch(fields["MAIL TO NAME 2"].ToString()))
                        {
                            Group g2 = mDBA.Groups[2];
                            thisRec.DBA = cleanLine(g2.ToString());
                        }
                        else
                        {
                            thisRec.OWNER2 = cleanLine(fields["MAIL TO NAME 2"].ToString());
                        }
                        string careOf = cleanLine(fields["CARE OF NAME"].ToString() + " " + fields["ATTENTION NAME"].ToString());
                        Regex reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mCO = reCO.Match(careOf);
                        if (reCO.IsMatch(careOf))
                        {
                            Group g2 = mCO.Groups[2];
                            thisRec.CARE_OF = g2.ToString();
                        }
                        else
                        {
                            thisRec.CARE_OF = careOf;
                        }

                        string myExeAmt = "";
                        if (fields["HO EXEMPTION "].ToString() != "0")
                        {
                            thisRec.EXE_CD = "HO";
                            myExeAmt = fields["HO EXEMPTION "].ToString();
                        }
                        else
                        {
                            thisRec.EXE_CD = fields["OTH 1"].ToString();
                            myExeAmt = fields["OTH 1 EXEMPTION"].ToString();
                        }
                        values myVals = new values();
                        myVals.LAND = fields["LAND VALUE"].ToString();
                        myVals.IMPR = fields["IMPROVEMENT VALUE "].ToString();
                        myVals.FIXTR = fields["BUSINESS VALUE"].ToString();
                        myVals.PERSPROP = fields["PERSONAL VALUE   "].ToString();
                        myVals.EXE_AMT = myExeAmt;
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string myMailAddr1 = "";
                        string myMailAddr2 = "";
                        Regex reSte = new Regex(@"^(BLDG\s*|FLOOR\s*|ROOM\s*|STE\s+|SUITE\s*)", RegexOptions.IgnoreCase);
                        if (reSte.IsMatch(fields[10].ToString()))
                        {
                            myMailAddr1 = fields[9].ToString() + " " + fields[10].ToString();
                            myMailAddr2 = "";
                        }
                        else
                        {
                            myMailAddr1 = fields[9].ToString();
                            myMailAddr2 = fields[10].ToString();
                        }
                        mailing myMail = new mailing(myMailAddr1, myMailAddr2, fields[11].ToString(), fields[12].ToString());
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        situs mySitus = new situs(fields["SITUS ADDRESS"].ToString(), fields["SITUS CITY, STATE AND ZIP"].ToString());
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("39", "502");
            stdTypes.Add("53", "402");
            stdTypes.Add("55", "400");
            stdTypes.Add("56", "501");
            stdTypes.Add("57", "200");
            stdTypes.Add("59", "100");
            stdTypes.Add("88", "201");
            stdTypes.Add("89", "200");
            stdTypes.Add("99", "300");
        }
    }
}
