﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Ccx : County
    {
        public Ccx()
        {
            co3 = "CCX";
            coNum = "7";
            populateStdTypes();
        }

        #region Propertes
        public int iFldFEE_PCL = 0;
        public int iFldACCT_NO = 1;
        public int iFldACCT_TYPE = 2;
        public int iFldROLL_YR = 3;
        public int iFldM_ADDR = 4;
        public int iFldM_CITY = 5;
        public int iFldM_ZIP = 6;
        public int iFldS_ADDR = 7;
        public int iFldS_CITY = 8;
        public int iFldS_ZIP = 9;
        public int iFldASSE_NAM = 10;
        public int iFldLAND = 11;
        public int iFldLAND_PEN = 12;
        public int iFldIMPR = 13;
        public int iFldIMPR_PEN = 14;
        public int iFldPERSPROP = 15;
        public int iFldPERSPROP_PEN = 16;
        public int iFldEXE_AMT = 17;
        public int iFldEXE_CD = 18;
        public int iFldTRA = 20;
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamReader srInputFile = null;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input file
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true, ';');
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    //while ((line = srInputFile.ReadLine()) != null)
                    foreach (DataRow fields in dtRoll.Rows)
                    {
                        //line = cleanLine(line);
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = fields[iFldACCT_NO].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.FEE_PARCEL_S = fields[iFldFEE_PCL].ToString().Replace("-", "");
                        thisRec.FEE_PARCEL_D = fields[iFldFEE_PCL].ToString();
                        //thisRec.FEE_PARCEL_D = County.formattedNumber(fields[iFldFEE_PCL].ToString(), thisRec.FEE_PARCEL_D_format); Pre-2017 format
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = fields[iFldROLL_YR].ToString();
                        thisRec.TRA = fields[iFldTRA].ToString().PadLeft(6, '0');
                        thisRec.TYPE = fields[iFldACCT_TYPE].ToString();
                        thisRec.TYPE_STD = xlatType(fields[iFldACCT_TYPE].ToString());
                        thisRec.EXE_CD = fields[iFldEXE_CD].ToString();

                        thisRec.OWNER1 = fields[iFldASSE_NAM].ToString();
                        thisRec.ASSESSEE = thisRec.OWNER1;

                        values myVals = new values();
                        long.TryParse(fields[iFldLAND_PEN].ToString(), out long lLandPen);
                        long.TryParse(fields[iFldIMPR_PEN].ToString(), out long lImprPen);
                        long.TryParse(fields[iFldPERSPROP_PEN].ToString(), out long lPersPropPen);
                        myVals.LAND = fields[iFldLAND].ToString();
                        myVals.IMPR = fields[iFldIMPR].ToString();
                        myVals.PERSPROP = fields[iFldPERSPROP].ToString();
                        myVals.PENALTY = (lLandPen + lImprPen + lPersPropPen).ToString();
                        myVals.EXE_AMT = fields[iFldEXE_AMT].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.PENALTY = myVals.PENALTY;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string myMZip = "";
                        if (fields[iFldM_ZIP].ToString() != "00000") myMZip = fields[iFldM_ZIP].ToString();
                        mailing myMail = new mailing(fields[iFldM_ADDR].ToString(), fields[iFldM_CITY].ToString() + " " + myMZip, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string myCity = fields[iFldS_CITY].ToString();
                        Regex reCity = new Regex(@"(.+)\s+(CA)$", RegexOptions.IgnoreCase);
                        Match mCity = reCity.Match(myCity);
                        if (mCity.Success)
                        {
                            Group gCity = mCity.Groups[1];
                            myCity = gCity.ToString();
                        }
                        string mySZip = "";
                        if (fields[iFldS_ZIP].ToString() != "00000") mySZip = fields[iFldS_ZIP].ToString();
                        situs mySitus = new situs(fields[iFldS_ADDR].ToString(), myCity + " " + mySZip);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = myCity + " " + "CA" + " " + mySZip;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from sorted input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (srInputFile != null) srInputFile.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                DataRow dr = table.NewRow();
                for (int i = 0; i < csv.FieldCount; i++)
                {
                    dr[i] = csv[i];
                }
                table.Rows.Add(dr);

            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[1];
            DataView view = table.DefaultView;

            return view.ToTable();
        }

        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("A", "300");
            stdTypes.Add("B", "200");
            stdTypes.Add("C", "200");
            stdTypes.Add("D", "200");
            stdTypes.Add("E", "500");
            stdTypes.Add("F", "500");
            stdTypes.Add("G", "500");
            stdTypes.Add("I", "400");
            stdTypes.Add("J", "400");
            stdTypes.Add("L", "107");
            stdTypes.Add("M", "500");
            stdTypes.Add("N", "504");
            stdTypes.Add("R", "107");
            stdTypes.Add("S", "500");
            stdTypes.Add("T", "500");
            stdTypes.Add("U", "100");
            stdTypes.Add("X", "501");
            stdTypes.Add("9", "100");
        }
    }
}
