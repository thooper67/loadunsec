﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Scl : County
    {
        public Scl()
        {
            co3 = "SCL";
            coNum = "43";
            populateStdTypes();
            populateStdCity();
        }

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Get file in a DataTable
                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = drRoll["ACCT-NO"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.FEE_PARCEL_S = drRoll["APN-NO"].ToString().PadLeft(8,'0');
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = drRoll["CODE"].ToString().PadLeft(6,'0');
                        thisRec.OWNER1 = cleanLine(drRoll["ASSESSEE-NAME"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;
                        thisRec.DBA = cleanLine(drRoll["DBA"].ToString());
                        thisRec.CARE_OF = cleanLine(drRoll["EXTRA-NAME"].ToString());
                        thisRec.TYPE = drRoll["BUS-CODE"].ToString().Substring(2, 3);
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                        values myVals = new values();
                        myVals.LAND = drRoll["LAND-41"].ToString();
                        myVals.IMPR = drRoll["STRUCTURE-41"].ToString();
                        myVals.FIXTR = drRoll["FIXTURES-41"].ToString();
                        myVals.PERSPROP = drRoll["PERS-PROP-41"].ToString();
                        myVals.EXE_AMT = drRoll["OTHER-EXEMPT-41"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;
                        if (thisRec.EXE_AMT == "7000")
                        {
                            thisRec.EXE_CD = "HO";
                        }
                        else if (thisRec.EXE_AMT != "")
                        {
                            thisRec.EXE_CD = "OE";
                        }

                        mailing myMail = new mailing(drRoll["MAILING-ADDRESS"].ToString(), drRoll["CITY-STATE"].ToString() + " " + drRoll["ZIP-CODE"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = drRoll["HOUSE-NO-SITUS"].ToString() + " " + drRoll["ST-DIRECTION-SIT"].ToString() + " " + drRoll["ADDRESS-SITUS"].ToString() + " " + drRoll["ST-SUF-SITUS"].ToString();
                        string situs2 = xlatCommunity(drRoll["CITY-CODE-SITUS"].ToString()) + " CA";
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = drRoll["APARTMENT-NO-SIT"].ToString();
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d + " " + thisRec.S_UNITNO;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("001", "500");
            stdTypes.Add("010", "200");
            stdTypes.Add("011", "200");
            stdTypes.Add("012", "200");
            stdTypes.Add("013", "202");
            stdTypes.Add("014", "202");
            stdTypes.Add("015", "200");
            stdTypes.Add("021", "300");
            stdTypes.Add("031", "102");
            stdTypes.Add("032", "102");
            stdTypes.Add("033", "102");
            stdTypes.Add("034", "501");
            stdTypes.Add("035", "403");
            stdTypes.Add("036", "100");
            stdTypes.Add("037", "101");
            stdTypes.Add("041", "501");
            stdTypes.Add("051", "104");
            stdTypes.Add("052", "100");
            stdTypes.Add("053", "100");
            stdTypes.Add("054", "104");
            stdTypes.Add("055", "100");
            stdTypes.Add("056", "103");
            stdTypes.Add("057", "100");
            stdTypes.Add("058", "100");
            stdTypes.Add("061", "100");
            stdTypes.Add("062", "100");
            stdTypes.Add("063", "100");
            stdTypes.Add("071", "502");
            stdTypes.Add("072", "105");
            stdTypes.Add("073", "502");
            stdTypes.Add("074", "502");
            stdTypes.Add("075", "502");
            stdTypes.Add("076", "502");
            stdTypes.Add("077", "502");
            stdTypes.Add("078", "502");
            stdTypes.Add("079", "502");
            stdTypes.Add("081", "502");
            stdTypes.Add("082", "501");
            stdTypes.Add("083", "102");
            stdTypes.Add("084", "400");
            stdTypes.Add("085", "401");
            stdTypes.Add("086", "103");
            stdTypes.Add("087", "106");
            stdTypes.Add("091", "100");
            stdTypes.Add("092", "100");
            stdTypes.Add("093", "108");
            stdTypes.Add("094", "504");
            stdTypes.Add("095", "504");
            stdTypes.Add("101", "100");
            stdTypes.Add("102", "100");
            stdTypes.Add("103", "100");
            stdTypes.Add("104", "100");
            stdTypes.Add("105", "100");
            stdTypes.Add("106", "100");
            stdTypes.Add("108", "100");
            stdTypes.Add("111", "100");
            stdTypes.Add("113", "100");
            stdTypes.Add("114", "100");
            stdTypes.Add("115", "100");
            stdTypes.Add("117", "100");
            stdTypes.Add("121", "100");
            stdTypes.Add("122", "100");
            stdTypes.Add("123", "100");
            stdTypes.Add("124", "100");
            stdTypes.Add("125", "100");
            stdTypes.Add("131", "100");
            stdTypes.Add("132", "100");
            stdTypes.Add("141", "100");
            stdTypes.Add("142", "100");
            stdTypes.Add("151", "100");
            stdTypes.Add("152", "100");
            stdTypes.Add("153", "100");
            stdTypes.Add("154", "100");
            stdTypes.Add("155", "100");
            stdTypes.Add("156", "100");
            stdTypes.Add("157", "100");
            stdTypes.Add("158", "100");
            stdTypes.Add("161", "100");
            stdTypes.Add("162", "100");
            stdTypes.Add("163", "100");
            stdTypes.Add("164", "100");
            stdTypes.Add("165", "100");
            stdTypes.Add("171", "100");
            stdTypes.Add("172", "100");
            stdTypes.Add("181", "100");
            stdTypes.Add("183", "100");
            stdTypes.Add("184", "100");
            stdTypes.Add("185", "100");
            stdTypes.Add("186", "100");
            stdTypes.Add("187", "100");
            stdTypes.Add("188", "100");
            stdTypes.Add("191", "100");
            stdTypes.Add("192", "100");
            stdTypes.Add("193", "100");
            stdTypes.Add("194", "100");
            stdTypes.Add("195", "100");
            stdTypes.Add("196", "100");
            stdTypes.Add("201", "100");
            stdTypes.Add("202", "100");
            stdTypes.Add("203", "100");
            stdTypes.Add("204", "100");
            stdTypes.Add("205", "100");
            stdTypes.Add("206", "100");
            stdTypes.Add("207", "100");
            stdTypes.Add("211", "100");
            stdTypes.Add("212", "100");
            stdTypes.Add("213", "100");
            stdTypes.Add("221", "100");
            stdTypes.Add("222", "100");
            stdTypes.Add("223", "100");
            stdTypes.Add("224", "100");
            stdTypes.Add("225", "100");
            stdTypes.Add("231", "100");
            stdTypes.Add("241", "100");
            stdTypes.Add("242", "100");
            stdTypes.Add("244", "100");
            stdTypes.Add("245", "100");
            stdTypes.Add("251", "100");
            stdTypes.Add("252", "100");
            stdTypes.Add("253", "100");
            stdTypes.Add("261", "100");
            stdTypes.Add("262", "100");
            stdTypes.Add("263", "100");
            stdTypes.Add("271", "100");
            stdTypes.Add("272", "100");
            stdTypes.Add("273", "100");
            stdTypes.Add("274", "100");
            stdTypes.Add("275", "100");
            stdTypes.Add("276", "100");
            stdTypes.Add("277", "100");
            stdTypes.Add("278", "100");
            stdTypes.Add("281", "100");
            stdTypes.Add("282", "100");
            stdTypes.Add("283", "100");
            stdTypes.Add("291", "100");
            stdTypes.Add("292", "100");
            stdTypes.Add("293", "100");
            stdTypes.Add("294", "100");
            stdTypes.Add("295", "100");
            stdTypes.Add("296", "100");
            stdTypes.Add("297", "100");
            stdTypes.Add("298", "100");
            stdTypes.Add("301", "100");
            stdTypes.Add("302", "100");
            stdTypes.Add("303", "100");
            stdTypes.Add("311", "100");
            stdTypes.Add("312", "100");
            stdTypes.Add("321", "100");
            stdTypes.Add("322", "100");
            stdTypes.Add("323", "100");
            stdTypes.Add("324", "100");
            stdTypes.Add("325", "100");
            stdTypes.Add("326", "100");
            stdTypes.Add("327", "100");
            stdTypes.Add("328", "100");
            stdTypes.Add("329", "100");
            stdTypes.Add("331", "100");
            stdTypes.Add("332", "100");
            stdTypes.Add("333", "100");
            stdTypes.Add("334", "100");
            stdTypes.Add("341", "100");
            stdTypes.Add("342", "100");
            stdTypes.Add("343", "100");
            stdTypes.Add("344", "100");
            stdTypes.Add("345", "100");
            stdTypes.Add("346", "100");
            stdTypes.Add("347", "100");
            stdTypes.Add("348", "100");
            stdTypes.Add("349", "100");
            stdTypes.Add("351", "100");
            stdTypes.Add("352", "100");
            stdTypes.Add("353", "100");
            stdTypes.Add("354", "100");
            stdTypes.Add("355", "100");
            stdTypes.Add("356", "100");
            stdTypes.Add("361", "100");
            stdTypes.Add("362", "100");
            stdTypes.Add("363", "100");
            stdTypes.Add("364", "100");
            stdTypes.Add("365", "100");
            stdTypes.Add("371", "100");
            stdTypes.Add("381", "100");
            stdTypes.Add("382", "100");
            stdTypes.Add("384", "100");
            stdTypes.Add("391", "100");
            stdTypes.Add("392", "100");
            stdTypes.Add("393", "100");
            stdTypes.Add("394", "100");
            stdTypes.Add("401", "100");
            stdTypes.Add("402", "100");
            stdTypes.Add("411", "100");
            stdTypes.Add("412", "100");
            stdTypes.Add("413", "100");
            stdTypes.Add("414", "100");
            stdTypes.Add("421", "100");
            stdTypes.Add("422", "100");
            stdTypes.Add("431", "100");
            stdTypes.Add("432", "100");
            stdTypes.Add("433", "100");
            stdTypes.Add("434", "100");
            stdTypes.Add("435", "100");
            stdTypes.Add("441", "100");
            stdTypes.Add("442", "100");
            stdTypes.Add("443", "100");
            stdTypes.Add("444", "100");
            stdTypes.Add("445", "100");
            stdTypes.Add("446", "100");
            stdTypes.Add("451", "100");
            stdTypes.Add("452", "100");
            stdTypes.Add("453", "100");
            stdTypes.Add("461", "100");
            stdTypes.Add("471", "100");
            stdTypes.Add("472", "100");
            stdTypes.Add("473", "100");
            stdTypes.Add("474", "100");
            stdTypes.Add("475", "100");
            stdTypes.Add("476", "100");
            stdTypes.Add("481", "100");
            stdTypes.Add("491", "100");
            stdTypes.Add("492", "100");
            stdTypes.Add("501", "100");
            stdTypes.Add("502", "100");
            stdTypes.Add("503", "100");
            stdTypes.Add("504", "100");
            stdTypes.Add("511", "100");
            stdTypes.Add("512", "100");
            stdTypes.Add("513", "100");
            stdTypes.Add("601", "100");
            stdTypes.Add("602", "100");
            stdTypes.Add("603", "100");
            stdTypes.Add("604", "100");
            stdTypes.Add("605", "100");
            stdTypes.Add("610", "100");
            stdTypes.Add("611", "100");
            stdTypes.Add("612", "100");
            stdTypes.Add("613", "100");
            stdTypes.Add("614", "100");
            stdTypes.Add("615", "100");
            stdTypes.Add("616", "100");
            stdTypes.Add("617", "100");
            stdTypes.Add("618", "100");
            stdTypes.Add("619", "100");
            stdTypes.Add("621", "100");
            stdTypes.Add("622", "100");
            stdTypes.Add("623", "100");
            stdTypes.Add("624", "100");
            stdTypes.Add("625", "100");
            stdTypes.Add("626", "100");
            stdTypes.Add("627", "100");
            stdTypes.Add("628", "100");
            stdTypes.Add("629", "100");
            stdTypes.Add("631", "100");
            stdTypes.Add("632", "100");
            stdTypes.Add("633", "100");
            stdTypes.Add("634", "100");
            stdTypes.Add("635", "100");
            stdTypes.Add("636", "100");
            stdTypes.Add("637", "100");
            stdTypes.Add("638", "100");
            stdTypes.Add("639", "100");
            stdTypes.Add("641", "100");
            stdTypes.Add("642", "100");
            stdTypes.Add("643", "100");
            stdTypes.Add("651", "100");
            stdTypes.Add("652", "100");
            stdTypes.Add("661", "100");
            stdTypes.Add("662", "100");
            stdTypes.Add("671", "100");
            stdTypes.Add("672", "100");
            stdTypes.Add("673", "100");
            stdTypes.Add("674", "100");
            stdTypes.Add("675", "100");
            stdTypes.Add("676", "100");
            stdTypes.Add("677", "100");
            stdTypes.Add("678", "100");
            stdTypes.Add("681", "100");
            stdTypes.Add("682", "100");
            stdTypes.Add("683", "100");
            stdTypes.Add("684", "100");
            stdTypes.Add("685", "100");
            stdTypes.Add("686", "100");
            stdTypes.Add("687", "100");
            stdTypes.Add("688", "100");
            stdTypes.Add("689", "100");
            stdTypes.Add("691", "100");
            stdTypes.Add("692", "100");
            stdTypes.Add("693", "100");
            stdTypes.Add("694", "100");
            stdTypes.Add("695", "100");
            stdTypes.Add("696", "100");
            stdTypes.Add("697", "100");
            stdTypes.Add("701", "100");
            stdTypes.Add("702", "100");
            stdTypes.Add("703", "100");
            stdTypes.Add("704", "100");
            stdTypes.Add("705", "100");
            stdTypes.Add("711", "100");
            stdTypes.Add("712", "100");
            stdTypes.Add("713", "108");
            stdTypes.Add("714", "108");
            stdTypes.Add("715", "108");
            stdTypes.Add("716", "108");
            stdTypes.Add("717", "108");
            stdTypes.Add("718", "108");
            stdTypes.Add("719", "108");
            stdTypes.Add("721", "108");
            stdTypes.Add("722", "108");
            stdTypes.Add("723", "100");
            stdTypes.Add("724", "100");
            stdTypes.Add("725", "108");
            stdTypes.Add("726", "108");
            stdTypes.Add("727", "108");
            stdTypes.Add("728", "108");
            stdTypes.Add("729", "108");
            stdTypes.Add("731", "100");
            stdTypes.Add("732", "100");
            stdTypes.Add("733", "100");
            stdTypes.Add("734", "100");
            stdTypes.Add("735", "100");
            stdTypes.Add("736", "100");
            stdTypes.Add("737", "100");
            stdTypes.Add("741", "100");
            stdTypes.Add("742", "100");
            stdTypes.Add("743", "100");
            stdTypes.Add("744", "100");
            stdTypes.Add("745", "502");
            stdTypes.Add("750", "503");
            stdTypes.Add("751", "100");
            stdTypes.Add("752", "100");
            stdTypes.Add("753", "100");
            stdTypes.Add("754", "100");
            stdTypes.Add("755", "100");
            stdTypes.Add("756", "100");
            stdTypes.Add("757", "100");
            stdTypes.Add("758", "100");
            stdTypes.Add("759", "100");
            stdTypes.Add("761", "100");
            stdTypes.Add("762", "100");
            stdTypes.Add("763", "100");
            stdTypes.Add("764", "100");
            stdTypes.Add("765", "100");
            stdTypes.Add("766", "100");
            stdTypes.Add("767", "100");
            stdTypes.Add("768", "108");
            stdTypes.Add("769", "108");
            stdTypes.Add("771", "100");
            stdTypes.Add("772", "100");
            stdTypes.Add("773", "100");
            stdTypes.Add("774", "100");
            stdTypes.Add("775", "100");
            stdTypes.Add("781", "100");
            stdTypes.Add("782", "100");
            stdTypes.Add("783", "100");
            stdTypes.Add("784", "100");
            stdTypes.Add("785", "100");
            stdTypes.Add("786", "100");
            stdTypes.Add("791", "100");
            stdTypes.Add("792", "100");
            stdTypes.Add("793", "108");
            stdTypes.Add("794", "108");
            stdTypes.Add("795", "108");
            stdTypes.Add("796", "108");
            stdTypes.Add("797", "108");
            stdTypes.Add("798", "108");
            stdTypes.Add("799", "108");
            stdTypes.Add("801", "100");
            stdTypes.Add("802", "100");
            stdTypes.Add("803", "100");
            stdTypes.Add("804", "100");
            stdTypes.Add("805", "100");
            stdTypes.Add("806", "100");
            stdTypes.Add("807", "100");
            stdTypes.Add("808", "100");
            stdTypes.Add("809", "100");
            stdTypes.Add("811", "100");
            stdTypes.Add("812", "100");
            stdTypes.Add("821", "100");
            stdTypes.Add("822", "100");
            stdTypes.Add("823", "100");
            stdTypes.Add("824", "100");
            stdTypes.Add("825", "100");
            stdTypes.Add("826", "100");
            stdTypes.Add("827", "100");
            stdTypes.Add("828", "100");
            stdTypes.Add("829", "100");
            stdTypes.Add("831", "100");
            stdTypes.Add("832", "100");
            stdTypes.Add("833", "100");
            stdTypes.Add("834", "100");
            stdTypes.Add("835", "100");
            stdTypes.Add("836", "100");
            stdTypes.Add("837", "100");
            stdTypes.Add("838", "100");
            stdTypes.Add("839", "100");
            stdTypes.Add("841", "100");
            stdTypes.Add("842", "100");
            stdTypes.Add("843", "100");
            stdTypes.Add("844", "100");
            stdTypes.Add("845", "100");
            stdTypes.Add("846", "100");
            stdTypes.Add("851", "100");
            stdTypes.Add("852", "100");
            stdTypes.Add("853", "100");
            stdTypes.Add("854", "100");
            stdTypes.Add("855", "100");
            stdTypes.Add("856", "100");
            stdTypes.Add("857", "100");
            stdTypes.Add("858", "100");
            stdTypes.Add("859", "100");
            stdTypes.Add("861", "100");
            stdTypes.Add("862", "100");
            stdTypes.Add("863", "100");
            stdTypes.Add("864", "100");
            stdTypes.Add("865", "100");
            stdTypes.Add("866", "100");
            stdTypes.Add("871", "100");
            stdTypes.Add("872", "100");
            stdTypes.Add("873", "100");
            stdTypes.Add("874", "100");
            stdTypes.Add("875", "100");
            stdTypes.Add("881", "100");
            stdTypes.Add("882", "100");
            stdTypes.Add("883", "100");
            stdTypes.Add("884", "100");
            stdTypes.Add("885", "100");
            stdTypes.Add("886", "100");
            stdTypes.Add("891", "100");
            stdTypes.Add("892", "100");
            stdTypes.Add("893", "100");
            stdTypes.Add("894", "100");
            stdTypes.Add("895", "100");
            stdTypes.Add("896", "100");
            stdTypes.Add("897", "100");
            stdTypes.Add("898", "100");
            stdTypes.Add("899", "100");
            stdTypes.Add("901", "100");
            stdTypes.Add("902", "100");
            stdTypes.Add("903", "100");
            stdTypes.Add("904", "100");
            stdTypes.Add("905", "100");
            stdTypes.Add("906", "100");
            stdTypes.Add("907", "100");
            stdTypes.Add("908", "100");
            stdTypes.Add("909", "100");
            stdTypes.Add("910", "100");
            stdTypes.Add("911", "100");
            stdTypes.Add("912", "100");
            stdTypes.Add("913", "100");
            stdTypes.Add("914", "100");
            stdTypes.Add("915", "100");
            stdTypes.Add("916", "100");
            stdTypes.Add("917", "100");
            stdTypes.Add("918", "100");
            stdTypes.Add("919", "100");
            stdTypes.Add("921", "100");
            stdTypes.Add("922", "100");
            stdTypes.Add("923", "100");
            stdTypes.Add("931", "104");
            stdTypes.Add("932", "100");
            stdTypes.Add("933", "100");
            stdTypes.Add("934", "100");
            stdTypes.Add("935", "104");
            stdTypes.Add("936", "104");
            stdTypes.Add("941", "100");
            stdTypes.Add("942", "100");
            stdTypes.Add("943", "100");
            stdTypes.Add("944", "100");
            stdTypes.Add("999", "500");
        }
        private void populateStdCity()
        {
            // Community Translation Table
            stdCity.Add("AG", "SANTA CLARA");
            stdCity.Add("AL", "ALUM ROCK");
            stdCity.Add("CA", "CAMPBELL");
            stdCity.Add("CO", "COYOTE");
            stdCity.Add("CU", "CUPERTINO");
            stdCity.Add("GI", "GILROY");
            stdCity.Add("HC", "HOLY CITY");
            stdCity.Add("HO", "HOLY CITY");
            stdCity.Add("LI", "LIVERMORE");
            stdCity.Add("LA", "LOS ALTOS");
            stdCity.Add("LH", "LOS ALTOS HILLS");
            stdCity.Add("LG", "LOS GATOS");
            stdCity.Add("MF", "SANTA CLARA");
            stdCity.Add("MI", "MILPITAS");
            stdCity.Add("MS", "MONTE SERENO");
            stdCity.Add("MH", "MORGAN HILL");
            stdCity.Add("MO", "SANTA CLARA");
            stdCity.Add("MT", "MOUNTAIN VIEW");
            stdCity.Add("MV", "MOUNTAIN VIEW");
            stdCity.Add("PA", "PALO ALTO");
            stdCity.Add("PV", "PARADISE VALLEY");
            stdCity.Add("RE", "REDWOOD ESTATES");
            stdCity.Add("SJ", "SAN JOSE");
            stdCity.Add("SM", "SAN MARTIN");
            stdCity.Add("SC", "SANTA CLARA");
            stdCity.Add("SA", "SARATOGA");
            stdCity.Add("ST", "STANFORD");
            stdCity.Add("SU", "SUNNYVALE");
            stdCity.Add("WA", "WATSONVILLE");
        }

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                DataRow dr = table.NewRow();
                for (int i = 0; i < csv.FieldCount; i++)
                {
                    dr[i] = csv[i];
                }
                table.Rows.Add(dr);
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
    }
}
