﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Las : Megabyte
    {
        public Las()
        {
            co3 = "LAS";
            coNum = "18";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("810", "101");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
        }
    }
}
