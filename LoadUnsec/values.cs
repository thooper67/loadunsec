﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class values
    {
        public values()
        {
        }
        #region properties
        private long _land = -1;
        private long _impr = -1;
        private long _fixtr = -1;
        private long _growing = -1;
        private long _persprop = -1;
        private long _fixtr_rp = -1;
        private long _mhpp = -1;
        private long _gross = -1;
        private long _penalty = -1;
        private long _ho_exe = -1;
        private long _other_exe = -1;
        private long _exe_amt = -1;

        public string LAND
        {
            get
            {
                return checkZero(_land);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _land);
            }
        }
        public string IMPR
        {
            get
            {
                return checkZero(_impr);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _impr);
            }
        }
        public string FIXTR
        {
            get
            {
                return checkZero(_fixtr);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _fixtr);
            }
        }
        public string GROWING
        {
            get
            {
                return checkZero(_growing);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _growing);
            }
        }
        public string PERSPROP
        {
            get
            {
                return checkZero(_persprop);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _persprop);
            }
        }
        public string FIXTR_RP
        {
            get
            {
                return checkZero(_fixtr_rp);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _fixtr_rp);
            }
        }
        public string MHPP
        {
            get
            {
                return checkZero(_mhpp);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _mhpp);
            }
        }
        public string GROSS
        {
            get
            {
                if (_gross <= 0) // If we haven't manually assigned a value to _gross:
                {
                    _gross = (_land <= 0 ? 0 : _land) + (_impr <= 0 ? 0 : _impr) + (_fixtr <= 0 ? 0 :_fixtr) + (_growing <= 0 ? 0 :_growing) + (_persprop <= 0 ? 0 : _persprop) + (_fixtr_rp <= 0 ? 0 : _fixtr_rp) + (_mhpp <= 0 ? 0 : _mhpp);
                }
                return checkZero(_gross);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _gross);
            }
        }
        public string PENALTY
        {
            get
            {
                return checkZero(_penalty);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _penalty);
            }
        }
        public string HO_EXE
        {
            get
            {
                return checkZero(_ho_exe);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _ho_exe);
            }
        }
        public string OTHER_EXE
        {
            get
            {
                return checkZero(_other_exe);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _other_exe);
            }
        }
        public string EXE_AMT
        {
            get
            {
                if (_exe_amt <= 0) // If we haven't manually assigned a value to _exe_amt:
                {
                    _exe_amt = (_ho_exe <= 0 ? 0 :_ho_exe) + (_other_exe <= 0 ? 0 :_other_exe);
                }
                return checkZero(_exe_amt);
            }
            set
            {
                long.TryParse(removeDecimal(value), out _exe_amt);
            }
        }
        #endregion

        #region methods
        public static string checkZero(long lValue)
        {
            string result = "";
            if (lValue > 0)
            {
                result = lValue.ToString();
            }
            return result;
        }
        public static string removeDecimal(string input)
        {
            string result = input;
            Regex re = new Regex(@"(\.\d+)$");
            Match m1 = re.Match(input.Trim());
            if (re.IsMatch(input))
            {
                Group g = m1.Groups[1];
                result = input.Replace(g.ToString(), "").Trim();
            }
            return result;
        }
        #endregion
    }
}
