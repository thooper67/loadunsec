﻿
namespace LoadUnsec
{
    class Sjx : Megabyte
    {
        public Sjx()
        {
            co3 = "SJX";
            coNum = "39";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "104");
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
        }
    }

    // Routine to process SJX on old, mainframe system
    //class Sjx : County
    //{
    //    public Sjx()
    //    {
    //        co3 = "SJX";
    //        coNum = "39";
    //        populateStdTypes();
    //        populateStdCity();
    //    }

    //    #region Properties
    //    List<fixfield> fields1 = new List<fixfield>();
    //    List<fixfield> fields2 = new List<fixfield>();
    //    List<fixfield> fields3 = new List<fixfield>();
    //    #endregion

    //    #region Methods
    //    public override string process()
    //    {
    //        string result = "";
    //        int count = 0;
    //        initialize();
    //        StreamWriter swOutFile = null;

    //        // input files
    //        string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
    //        string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
    //        string srcFile3 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file3"]);
    //        if ((srcFile1 == null) || (srcFile2 == null) || (srcFile3 == null)) result = "Problem with source file.";

    //        // open output file
    //        try
    //        {
    //            swOutFile = new StreamWriter(outFile, false);
    //            log("Opened output file " + outFile);
    //        }
    //        catch (Exception e)
    //        {
    //            result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
    //            log(result);
    //        }

    //        if (result == "") // Only work on the output file if there weren't any previous issues.
    //        {
    //            // Write out header
    //            unsecrec hdr = new unsecrec();
    //            swOutFile.WriteLine(hdr.writeHeader());

    //            // Loop through input
    //            try
    //            {
    //                // Get input data
    //                DataTable table = new DataTable();
    //                foreach (fixfield fieldName in fields1)
    //                {
    //                    table.Columns.Add(fieldName.Name, typeof(string));
    //                }

    //                String connectionString = @"Provider=Microsoft.JET.OlEDB.4.0;Data Source=" + srcFile1;
    //                table = fillDataTable(connectionString, "Select * from unsecroll_aan", table, fields1);
    //                connectionString = @"Provider=Microsoft.JET.OlEDB.4.0;Data Source=" + srcFile2;
    //                table = fillDataTable(connectionString, "Select * from unsecroll_aircraft", table, fields2);
    //                connectionString = @"Provider=Microsoft.JET.OlEDB.4.0;Data Source=" + srcFile3;
    //                table = fillDataTable(connectionString, "Select * from unsecroll_boat", table, fields3);

    //                // close input file
    //                table.DefaultView.Sort = "ACCOUNT";
    //                DataView view = table.DefaultView;
    //                DataTable dtRoll = view.ToTable();

    //                count = 0;
    //                foreach (DataRow drRoll in dtRoll.Rows)
    //                {
    //                    count++;
    //                    unsecrec thisRec = new unsecrec();
    //                    thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];
    //                    thisRec.APN_S = drRoll["ACCOUNT"].ToString();
    //                    thisRec.APN_D = thisRec.APN_S;
    //                    thisRec.FEE_PARCEL_S = drRoll["FEE_PCL"].ToString();
    //                    thisRec.FEE_PARCEL_D = formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
    //                    thisRec.CO_NUM = coNum;
    //                    thisRec.CO_ID = co3;
    //                    thisRec.YRASSD = drRoll["ROLL_YR"].ToString();
    //                    thisRec.TRA = drRoll["TRA"].ToString();
    //                    string myType = drRoll["TYPE"].ToString();
    //                    if (myType != " ") thisRec.TYPE = drRoll["TYPE"].ToString().Substring(0, 3);
    //                    thisRec.TYPE_STD = xlatType(thisRec.TYPE);
    //                    thisRec.EXE_CD = drRoll["EXE_CD"].ToString();

    //                    thisRec.OWNER1 = cleanLine(drRoll["OWNER"].ToString());
    //                    thisRec.DBA = cleanLine(drRoll["DBA"].ToString());

    //                    Regex reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
    //                    Match mCO = reCO.Match(drRoll["CARE_OF"].ToString());
    //                    if (mCO.Success)
    //                    {
    //                        Group gCO = mCO.Groups[2];
    //                        thisRec.CARE_OF = cleanLine(gCO.ToString());
    //                    }
    //                    else
    //                    {
    //                        thisRec.CARE_OF = cleanLine(drRoll["CARE_OF"].ToString());
    //                    }

    //                    values myVals = new values();
    //                    myVals.LAND = drRoll["LAND"].ToString();
    //                    myVals.IMPR = drRoll["IMPR"].ToString();
    //                    myVals.PERSPROP = drRoll["PERSPROP"].ToString();
    //                    myVals.EXE_AMT = addStrings(drRoll["HO_EX"].ToString(), drRoll["OTHER_EX"].ToString());
    //                    thisRec.LAND = myVals.LAND;
    //                    thisRec.IMPR = myVals.IMPR;
    //                    thisRec.PERSPROP = myVals.PERSPROP;
    //                    thisRec.GROSS = myVals.GROSS;
    //                    thisRec.EXE_AMT = myVals.EXE_AMT;

    //                    string myZip = "";
    //                    if (drRoll["M_ZIP4"].ToString() != "")
    //                    {
    //                        if (drRoll["M_ZIP4"].ToString() != "0")
    //                        myZip = drRoll["M_ZIP"].ToString() + "-" + drRoll["M_ZIP4"].ToString();
    //                    }
    //                    else
    //                    {
    //                        myZip = drRoll["M_ZIP"].ToString();
    //                    }
    //                    string mail1 = drRoll["M_ADDR"].ToString();
    //                    string mail2 = drRoll["M_CITY"].ToString() + " " + drRoll["M_ST"].ToString() + " " + myZip;
    //                    mailing myMail = new mailing(mail1, mail2, "", "");
    //                    thisRec.M_STRNUM = myMail.m_strnum;
    //                    thisRec.M_STR_SUB = myMail.m_str_sub;
    //                    thisRec.M_DIR = myMail.m_dir;
    //                    thisRec.M_STREET = myMail.m_street;
    //                    thisRec.M_SUFF = myMail.m_suff;
    //                    thisRec.M_UNITNO = myMail.m_unit_no;
    //                    thisRec.M_CITY = myMail.m_city;
    //                    thisRec.M_ST = myMail.m_st;
    //                    thisRec.M_ZIP = myMail.m_zip;
    //                    thisRec.M_ZIP4 = myMail.m_zip4;
    //                    thisRec.M_ADDR_D = myMail.m_addr_d;
    //                    thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

    //                    string street = drRoll["S_STR"].ToString();
    //                    string unit = "";
    //                    if (street.IndexOf('#') > 0)
    //                    {
    //                        unit = " " + street.Substring(street.IndexOf('#'));
    //                        street = street.Substring(0, street.IndexOf('#') - 1);
    //                    }
    //                    string situs1 = drRoll["S_NUM"].ToString() + " " + drRoll["S_DIR"].ToString() + " " + street + " " + drRoll["S_SFX"].ToString() + unit;
    //                    string myCity = cleanLine(drRoll["S_CITY"].ToString());
    //                    if (myCity != "") myCity = myCity.Substring(0, 2);
    //                    string situs2 = xlatCommunity(myCity) + " CA";
    //                    situs mySitus = new situs(situs1, situs2);
    //                    thisRec.S_HSENO = mySitus.s_hseno;
    //                    thisRec.S_STRNUM = mySitus.s_strnum;
    //                    thisRec.S_STR_SUB = mySitus.s_str_sub;
    //                    thisRec.S_DIR = mySitus.s_dir;
    //                    thisRec.S_STREET = mySitus.s_street;
    //                    thisRec.S_SUFF = mySitus.s_suff;
    //                    thisRec.S_UNITNO = mySitus.s_unit_no;
    //                    thisRec.S_CITY = mySitus.s_city;
    //                    thisRec.S_ST = mySitus.s_st;
    //                    thisRec.S_ZIP = mySitus.s_zip;
    //                    thisRec.S_ZIP4 = mySitus.s_zip4;
    //                    thisRec.S_ADDR_D = mySitus.s_addr_d;
    //                    thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

    //                    swOutFile.WriteLine(thisRec.writeOutput());
    //                }
    //            }
    //            catch (Exception e)
    //            {
    //                result = "Error from input line " + count.ToString() + " : " + e.Message;
    //                log(result);
    //            }
    //        }
    //        // close output file
    //        if (swOutFile != null)
    //        {
    //            swOutFile.Flush();
    //            swOutFile.Close();
    //        }
    //        return result;
    //    }
    //    private void initialize()
    //    {
    //        fields1.Add(new fixfield(0, 0, "ROLL_YR", "ROLLYR"));
    //        fields1.Add(new fixfield(0, 0, "ACCOUNT", "BUSACCT"));
    //        fields1.Add(new fixfield(0, 0, "TRA", "TRA"));
    //        fields1.Add(new fixfield(0, 0, "FEE_PCL", "REFAPN"));
    //        fields1.Add(new fixfield(0, 0, "TYPE", "PPCODE"));
    //        fields1.Add(new fixfield(0, 0, "OWNER", "OWNER"));
    //        fields1.Add(new fixfield(0, 0, "CARE_OF", "CARE_OF_NAME"));
    //        fields1.Add(new fixfield(0, 0, "DBA", "DBA_NAME"));
    //        fields1.Add(new fixfield(0, 0, "M_ADDR", "MAIL_STREET"));
    //        fields1.Add(new fixfield(0, 0, "M_CITY", "MAIL_CITY"));
    //        fields1.Add(new fixfield(0, 0, "M_ST", "MAIL_STATE"));
    //        fields1.Add(new fixfield(0, 0, "M_ZIP", "MAIL_ZIPPREFIX"));
    //        fields1.Add(new fixfield(0, 0, "M_ZIP4", "MAIL_ZIPSUFFIX"));
    //        fields1.Add(new fixfield(0, 0, "S_NUM", "SITUS_NUMBER"));
    //        fields1.Add(new fixfield(0, 0, "S_DIR", "SITUS_DIR"));
    //        fields1.Add(new fixfield(0, 0, "S_STR", "SITUS_STREET"));
    //        fields1.Add(new fixfield(0, 0, "S_SFX", "SITUS_TYPE"));
    //        fields1.Add(new fixfield(0, 0, "S_CITY", "SITUS_CITY"));
    //        fields1.Add(new fixfield(0, 0, "LAND", "LAND_AMT"));
    //        fields1.Add(new fixfield(0, 0, "IMPR", "IMPROV_AMT"));
    //        fields1.Add(new fixfield(0, 0, "PERSPROP", "PERSPROP_AMT"));
    //        fields1.Add(new fixfield(0, 0, "HO_EX", "EXEMP_HOX_AMT"));
    //        fields1.Add(new fixfield(0, 0, "OTHER_EX", "EXEMP_OTHER_AMT"));
    //        fields1.Add(new fixfield(0, 0, "EXE_CD", "EXEMP_CODE"));

    //        fields2.Add(new fixfield(0, 0, "ROLL_YR", "ROLLYR"));
    //        fields2.Add(new fixfield(0, 0, "ACCOUNT", "AIRCNUM"));
    //        fields2.Add(new fixfield(0, 0, "TRA", "TRA"));
    //        fields2.Add(new fixfield(0, 0, "FEE_PCL", "REFAPN"));
    //        fields2.Add(new fixfield(0, 0, "TYPE", "PPCODE"));
    //        fields2.Add(new fixfield(0, 0, "OWNER", "OWNER"));
    //        fields2.Add(new fixfield(0, 0, "CARE_OF", "CARE_OF_NAME"));
    //        fields2.Add(new fixfield(0, 0, "DBA", "DBA_NAME"));
    //        fields2.Add(new fixfield(0, 0, "M_ADDR", "MAIL_STREET"));
    //        fields2.Add(new fixfield(0, 0, "M_CITY", "MAIL_CITY"));
    //        fields2.Add(new fixfield(0, 0, "M_ST", "MAIL_STATE"));
    //        fields2.Add(new fixfield(0, 0, "M_ZIP", "MAIL_ZIPPREFIX"));
    //        fields2.Add(new fixfield(0, 0, "M_ZIP4", "MAIL_ZIPSUFFIX"));
    //        fields2.Add(new fixfield(0, 0, "S_NUM", "SITUS_NUMBER"));
    //        fields2.Add(new fixfield(0, 0, "S_DIR", "SITUS_DIR"));
    //        fields2.Add(new fixfield(0, 0, "S_STR", "SITUS_STREET"));
    //        fields2.Add(new fixfield(0, 0, "S_SFX", "SITUS_TYPE"));
    //        fields2.Add(new fixfield(0, 0, "S_CITY", "SITUS_CITY"));
    //        fields2.Add(new fixfield(0, 0, "LAND", "LAND_AMT"));
    //        fields2.Add(new fixfield(0, 0, "IMPR", "IMPROV_AMT"));
    //        fields2.Add(new fixfield(0, 0, "PERSPROP", "PERSPROP_AMT"));
    //        fields2.Add(new fixfield(0, 0, "HO_EX", "EXEMP_HOX_AMT"));
    //        fields2.Add(new fixfield(0, 0, "OTHER_EX", "EXEMP_OTHER_AMT"));
    //        fields2.Add(new fixfield(0, 0, "EXE_CD", "EXEMP_CODE"));

    //        fields3.Add(new fixfield(0, 0, "ROLL_YR", "ROLLYR"));
    //        fields3.Add(new fixfield(0, 0, "ACCOUNT", "BOATNUM"));
    //        fields3.Add(new fixfield(0, 0, "TRA", "TRA"));
    //        fields3.Add(new fixfield(0, 0, "FEE_PCL", "REFAPN"));
    //        fields3.Add(new fixfield(0, 0, "TYPE", "PPCODE"));
    //        fields3.Add(new fixfield(0, 0, "OWNER", "OWNER"));
    //        fields3.Add(new fixfield(0, 0, "CARE_OF", "CARE_OF_NAME"));
    //        fields3.Add(new fixfield(0, 0, "DBA", "DBA_NAME"));
    //        fields3.Add(new fixfield(0, 0, "M_ADDR", "MAIL_STREET"));
    //        fields3.Add(new fixfield(0, 0, "M_CITY", "MAIL_CITY"));
    //        fields3.Add(new fixfield(0, 0, "M_ST", "MAIL_STATE"));
    //        fields3.Add(new fixfield(0, 0, "M_ZIP", "MAIL_ZIPPREFIX"));
    //        fields3.Add(new fixfield(0, 0, "M_ZIP4", "MAIL_ZIPSUFFIX"));
    //        fields3.Add(new fixfield(0, 0, "S_NUM", "SITUS_NUMBER"));
    //        fields3.Add(new fixfield(0, 0, "S_DIR", "SITUS_DIR"));
    //        fields3.Add(new fixfield(0, 0, "S_STR", "SITUS_STREET"));
    //        fields3.Add(new fixfield(0, 0, "S_SFX", "SITUS_TYPE"));
    //        fields3.Add(new fixfield(0, 0, "S_CITY", "SITUS_CITY"));
    //        fields3.Add(new fixfield(0, 0, "LAND", "LAND_AMT"));
    //        fields3.Add(new fixfield(0, 0, "IMPR", "IMPROV_AMT"));
    //        fields3.Add(new fixfield(0, 0, "PERSPROP", "PERSPROP_AMT"));
    //        fields3.Add(new fixfield(0, 0, "HO_EX", "EXEMP_HOX_AMT"));
    //        fields3.Add(new fixfield(0, 0, "OTHER_EX", "EXEMP_OTHER_AMT"));
    //        fields3.Add(new fixfield(0, 0, "EXE_CD", "EXEMP_CODE"));
    //    }
    //    #endregion

    //    private void populateStdTypes()
    //    {
    //        stdTypes.Add("00", "300");
    //        stdTypes.Add("001", "504");
    //        stdTypes.Add("002", "504");
    //        stdTypes.Add("003", "504");
    //        stdTypes.Add("004", "504");
    //        stdTypes.Add("005", "504");
    //        stdTypes.Add("006", "504");
    //        stdTypes.Add("007", "504");
    //        stdTypes.Add("008", "504");
    //        stdTypes.Add("009", "504");
    //        stdTypes.Add("011", "100");
    //        stdTypes.Add("012", "402");
    //        stdTypes.Add("013", "100");
    //        stdTypes.Add("014", "100");
    //        stdTypes.Add("015", "100");
    //        stdTypes.Add("016", "100");
    //        stdTypes.Add("017", "100");
    //        stdTypes.Add("018", "503");
    //        stdTypes.Add("019", "503");
    //        stdTypes.Add("020", "100");
    //        stdTypes.Add("021", "100");
    //        stdTypes.Add("022", "100");
    //        stdTypes.Add("023", "100");
    //        stdTypes.Add("024", "100");
    //        stdTypes.Add("025", "100");
    //        stdTypes.Add("026", "100");
    //        stdTypes.Add("031", "100");
    //        stdTypes.Add("032", "100");
    //        stdTypes.Add("033", "100");
    //        stdTypes.Add("034", "100");
    //        stdTypes.Add("035", "100");
    //        stdTypes.Add("036", "100");
    //        stdTypes.Add("037", "203");
    //        stdTypes.Add("038", "100");
    //        stdTypes.Add("039", "100");
    //        stdTypes.Add("040", "100");
    //        stdTypes.Add("041", "100");
    //        stdTypes.Add("042", "100");
    //        stdTypes.Add("043", "100");
    //        stdTypes.Add("044", "100");
    //        stdTypes.Add("051", "100");
    //        stdTypes.Add("052", "100");
    //        stdTypes.Add("053", "100");
    //        stdTypes.Add("054", "100");
    //        stdTypes.Add("055", "100");
    //        stdTypes.Add("056", "502");
    //        stdTypes.Add("061", "100");
    //        stdTypes.Add("062", "100");
    //        stdTypes.Add("063", "100");
    //        stdTypes.Add("064", "100");
    //        stdTypes.Add("065", "100");
    //        stdTypes.Add("066", "100");
    //        stdTypes.Add("067", "100");
    //        stdTypes.Add("068", "100");
    //        stdTypes.Add("069", "100");
    //        stdTypes.Add("070", "100");
    //        stdTypes.Add("071", "100");
    //        stdTypes.Add("072", "108");
    //        stdTypes.Add("081", "100");
    //        stdTypes.Add("082", "100");
    //        stdTypes.Add("083", "100");
    //        stdTypes.Add("084", "100");
    //        stdTypes.Add("085", "100");
    //        stdTypes.Add("086", "100");
    //        stdTypes.Add("087", "100");
    //        stdTypes.Add("088", "100");
    //        stdTypes.Add("089", "100");
    //        stdTypes.Add("101", "100");
    //        stdTypes.Add("102", "100");
    //        stdTypes.Add("103", "100");
    //        stdTypes.Add("104", "100");
    //        stdTypes.Add("105", "100");
    //        stdTypes.Add("121", "100");
    //        stdTypes.Add("122", "100");
    //        stdTypes.Add("123", "100");
    //        stdTypes.Add("124", "100");
    //        stdTypes.Add("125", "100");
    //        stdTypes.Add("126", "100");
    //        stdTypes.Add("127", "100");
    //        stdTypes.Add("128", "100");
    //        stdTypes.Add("129", "100");
    //        stdTypes.Add("131", "100");
    //        stdTypes.Add("141", "100");
    //        stdTypes.Add("142", "100");
    //        stdTypes.Add("143", "100");
    //        stdTypes.Add("144", "100");
    //        stdTypes.Add("145", "100");
    //        stdTypes.Add("146", "100");
    //        stdTypes.Add("147", "100");
    //        stdTypes.Add("148", "100");
    //        stdTypes.Add("149", "100");
    //        stdTypes.Add("150", "100");
    //        stdTypes.Add("151", "100");
    //        stdTypes.Add("152", "100");
    //        stdTypes.Add("153", "100");
    //        stdTypes.Add("154", "100");
    //        stdTypes.Add("155", "100");
    //        stdTypes.Add("156", "100");
    //        stdTypes.Add("157", "100");
    //        stdTypes.Add("158", "100");
    //        stdTypes.Add("159", "100");
    //        stdTypes.Add("160", "100");
    //        stdTypes.Add("161", "100");
    //        stdTypes.Add("162", "100");
    //        stdTypes.Add("163", "100");
    //        stdTypes.Add("164", "100");
    //        stdTypes.Add("165", "100");
    //        stdTypes.Add("166", "100");
    //        stdTypes.Add("167", "100");
    //        stdTypes.Add("201", "108");
    //        stdTypes.Add("202", "100");
    //        stdTypes.Add("203", "100");
    //        stdTypes.Add("204", "108");
    //        stdTypes.Add("205", "100");
    //        stdTypes.Add("206", "100");
    //        stdTypes.Add("207", "108");
    //        stdTypes.Add("208", "100");
    //        stdTypes.Add("221", "100");
    //        stdTypes.Add("222", "100");
    //        stdTypes.Add("223", "100");
    //        stdTypes.Add("224", "100");
    //        stdTypes.Add("225", "100");
    //        stdTypes.Add("226", "100");
    //        stdTypes.Add("227", "100");
    //        stdTypes.Add("241", "100");
    //        stdTypes.Add("242", "100");
    //        stdTypes.Add("251", "100");
    //        stdTypes.Add("252", "100");
    //        stdTypes.Add("253", "100");
    //        stdTypes.Add("254", "100");
    //        stdTypes.Add("255", "100");
    //        stdTypes.Add("256", "100");
    //        stdTypes.Add("257", "100");
    //        stdTypes.Add("258", "100");
    //        stdTypes.Add("261", "100");
    //        stdTypes.Add("262", "100");
    //        stdTypes.Add("263", "100");
    //        stdTypes.Add("264", "100");
    //        stdTypes.Add("265", "100");
    //        stdTypes.Add("266", "100");
    //        stdTypes.Add("267", "100");
    //        stdTypes.Add("268", "100");
    //        stdTypes.Add("269", "100");
    //        stdTypes.Add("271", "100");
    //        stdTypes.Add("272", "100");
    //        stdTypes.Add("273", "100");
    //        stdTypes.Add("274", "100");
    //        stdTypes.Add("275", "100");
    //        stdTypes.Add("276", "100");
    //        stdTypes.Add("277", "100");
    //        stdTypes.Add("278", "100");
    //        stdTypes.Add("279", "100");
    //        stdTypes.Add("280", "100");
    //        stdTypes.Add("281", "100");
    //        stdTypes.Add("282", "100");
    //        stdTypes.Add("283", "100");
    //        stdTypes.Add("284", "100");
    //        stdTypes.Add("285", "100");
    //        stdTypes.Add("286", "100");
    //        stdTypes.Add("287", "100");
    //        stdTypes.Add("288", "100");
    //        stdTypes.Add("289", "100");
    //        stdTypes.Add("291", "100");
    //        stdTypes.Add("292", "100");
    //        stdTypes.Add("293", "100");
    //        stdTypes.Add("294", "100");
    //        stdTypes.Add("295", "100");
    //        stdTypes.Add("296", "100");
    //        stdTypes.Add("297", "503");
    //        stdTypes.Add("298", "504");
    //        stdTypes.Add("301", "100");
    //        stdTypes.Add("302", "100");
    //        stdTypes.Add("303", "100");
    //        stdTypes.Add("304", "100");
    //        stdTypes.Add("305", "100");
    //        stdTypes.Add("306", "100");
    //        stdTypes.Add("307", "100");
    //        stdTypes.Add("321", "100");
    //        stdTypes.Add("322", "100");
    //        stdTypes.Add("323", "100");
    //        stdTypes.Add("324", "100");
    //        stdTypes.Add("325", "100");
    //        stdTypes.Add("326", "100");
    //        stdTypes.Add("327", "100");
    //        stdTypes.Add("328", "100");
    //        stdTypes.Add("329", "100");
    //        stdTypes.Add("330", "100");
    //        stdTypes.Add("331", "100");
    //        stdTypes.Add("332", "100");
    //        stdTypes.Add("333", "100");
    //        stdTypes.Add("334", "100");
    //        stdTypes.Add("335", "100");
    //        stdTypes.Add("336", "100");
    //        stdTypes.Add("337", "100");
    //        stdTypes.Add("341", "100");
    //        stdTypes.Add("342", "100");
    //        stdTypes.Add("343", "100");
    //        stdTypes.Add("344", "100");
    //        stdTypes.Add("345", "100");
    //        stdTypes.Add("351", "100");
    //        stdTypes.Add("352", "100");
    //        stdTypes.Add("353", "100");
    //        stdTypes.Add("354", "100");
    //        stdTypes.Add("355", "100");
    //        stdTypes.Add("356", "100");
    //        stdTypes.Add("357", "100");
    //        stdTypes.Add("358", "100");
    //        stdTypes.Add("359", "100");
    //        stdTypes.Add("360", "100");
    //        stdTypes.Add("361", "100");
    //        stdTypes.Add("362", "100");
    //        stdTypes.Add("363", "100");
    //        stdTypes.Add("364", "100");
    //        stdTypes.Add("365", "100");
    //        stdTypes.Add("366", "100");
    //        stdTypes.Add("367", "100");
    //        stdTypes.Add("368", "100");
    //        stdTypes.Add("369", "100");
    //        stdTypes.Add("371", "100");
    //        stdTypes.Add("372", "100");
    //        stdTypes.Add("373", "100");
    //        stdTypes.Add("374", "100");
    //        stdTypes.Add("375", "100");
    //        stdTypes.Add("376", "100");
    //        stdTypes.Add("377", "100");
    //        stdTypes.Add("378", "100");
    //        stdTypes.Add("379", "100");
    //        stdTypes.Add("380", "100");
    //        stdTypes.Add("381", "100");
    //        stdTypes.Add("382", "100");
    //        stdTypes.Add("383", "100");
    //        stdTypes.Add("384", "100");
    //        stdTypes.Add("385", "100");
    //        stdTypes.Add("386", "100");
    //        stdTypes.Add("387", "100");
    //        stdTypes.Add("388", "100");
    //        stdTypes.Add("389", "100");
    //        stdTypes.Add("390", "100");
    //        stdTypes.Add("391", "100");
    //        stdTypes.Add("392", "100");
    //        stdTypes.Add("393", "100");
    //        stdTypes.Add("394", "100");
    //        stdTypes.Add("395", "100");
    //        stdTypes.Add("396", "100");
    //        stdTypes.Add("397", "100");
    //        stdTypes.Add("398", "100");
    //        stdTypes.Add("399", "100");
    //        stdTypes.Add("401", "100");
    //        stdTypes.Add("402", "100");
    //        stdTypes.Add("403", "100");
    //        stdTypes.Add("404", "100");
    //        stdTypes.Add("405", "100");
    //        stdTypes.Add("406", "100");
    //        stdTypes.Add("407", "100");
    //        stdTypes.Add("408", "100");
    //        stdTypes.Add("409", "100");
    //        stdTypes.Add("410", "100");
    //        stdTypes.Add("411", "100");
    //        stdTypes.Add("412", "100");
    //        stdTypes.Add("413", "100");
    //        stdTypes.Add("414", "100");
    //        stdTypes.Add("421", "100");
    //        stdTypes.Add("422", "100");
    //        stdTypes.Add("423", "100");
    //        stdTypes.Add("424", "100");
    //        stdTypes.Add("425", "100");
    //        stdTypes.Add("426", "100");
    //        stdTypes.Add("427", "100");
    //        stdTypes.Add("431", "100");
    //        stdTypes.Add("432", "100");
    //        stdTypes.Add("441", "100");
    //        stdTypes.Add("442", "100");
    //        stdTypes.Add("443", "100");
    //        stdTypes.Add("444", "100");
    //        stdTypes.Add("445", "100");
    //        stdTypes.Add("446", "100");
    //        stdTypes.Add("447", "100");
    //        stdTypes.Add("448", "100");
    //        stdTypes.Add("449", "100");
    //        stdTypes.Add("450", "100");
    //        stdTypes.Add("451", "100");
    //        stdTypes.Add("452", "100");
    //        stdTypes.Add("453", "100");
    //        stdTypes.Add("454", "100");
    //        stdTypes.Add("455", "100");
    //        stdTypes.Add("461", "100");
    //        stdTypes.Add("462", "100");
    //        stdTypes.Add("463", "100");
    //        stdTypes.Add("464", "100");
    //        stdTypes.Add("465", "100");
    //        stdTypes.Add("466", "100");
    //        stdTypes.Add("467", "100");
    //        stdTypes.Add("471", "100");
    //        stdTypes.Add("472", "100");
    //        stdTypes.Add("473", "100");
    //        stdTypes.Add("474", "100");
    //        stdTypes.Add("475", "100");
    //        stdTypes.Add("481", "100");
    //        stdTypes.Add("482", "100");
    //        stdTypes.Add("491", "100");
    //        stdTypes.Add("492", "100");
    //        stdTypes.Add("493", "100");
    //        stdTypes.Add("494", "100");
    //        stdTypes.Add("495", "100");
    //        stdTypes.Add("496", "100");
    //        stdTypes.Add("497", "100");
    //        stdTypes.Add("498", "100");
    //        stdTypes.Add("501", "100");
    //        stdTypes.Add("502", "100");
    //        stdTypes.Add("503", "100");
    //        stdTypes.Add("504", "100");
    //        stdTypes.Add("505", "100");
    //        stdTypes.Add("506", "100");
    //        stdTypes.Add("507", "100");
    //        stdTypes.Add("508", "100");
    //        stdTypes.Add("509", "100");
    //        stdTypes.Add("510", "100");
    //        stdTypes.Add("511", "100");
    //        stdTypes.Add("521", "100");
    //        stdTypes.Add("522", "100");
    //        stdTypes.Add("523", "100");
    //        stdTypes.Add("524", "100");
    //        stdTypes.Add("525", "100");
    //        stdTypes.Add("526", "100");
    //        stdTypes.Add("527", "100");
    //        stdTypes.Add("528", "100");
    //        stdTypes.Add("529", "100");
    //        stdTypes.Add("530", "100");
    //        stdTypes.Add("531", "100");
    //        stdTypes.Add("532", "100");
    //        stdTypes.Add("533", "100");
    //        stdTypes.Add("534", "100");
    //        stdTypes.Add("535", "100");
    //        stdTypes.Add("536", "100");
    //        stdTypes.Add("541", "100");
    //        stdTypes.Add("542", "100");
    //        stdTypes.Add("543", "100");
    //        stdTypes.Add("544", "100");
    //        stdTypes.Add("545", "100");
    //        stdTypes.Add("546", "100");
    //        stdTypes.Add("547", "100");
    //        stdTypes.Add("548", "100");
    //        stdTypes.Add("549", "100");
    //        stdTypes.Add("550", "100");
    //        stdTypes.Add("551", "100");
    //        stdTypes.Add("552", "100");
    //        stdTypes.Add("553", "100");
    //        stdTypes.Add("554", "100");
    //        stdTypes.Add("555", "100");
    //        stdTypes.Add("601", "100");
    //        stdTypes.Add("602", "100");
    //        stdTypes.Add("603", "100");
    //        stdTypes.Add("604", "100");
    //        stdTypes.Add("605", "100");
    //        stdTypes.Add("606", "100");
    //        stdTypes.Add("607", "100");
    //        stdTypes.Add("608", "100");
    //        stdTypes.Add("609", "100");
    //        stdTypes.Add("610", "100");
    //        stdTypes.Add("611", "100");
    //        stdTypes.Add("612", "100");
    //        stdTypes.Add("613", "100");
    //        stdTypes.Add("614", "100");
    //        stdTypes.Add("615", "403");
    //        stdTypes.Add("616", "100");
    //        stdTypes.Add("617", "100");
    //        stdTypes.Add("618", "100");
    //        stdTypes.Add("619", "100");
    //        stdTypes.Add("620", "100");
    //        stdTypes.Add("621", "100");
    //        stdTypes.Add("622", "100");
    //        stdTypes.Add("623", "100");
    //        stdTypes.Add("624", "100");
    //        stdTypes.Add("625", "100");
    //        stdTypes.Add("626", "100");
    //        stdTypes.Add("627", "100");
    //        stdTypes.Add("628", "100");
    //        stdTypes.Add("629", "100");
    //        stdTypes.Add("630", "100");
    //        stdTypes.Add("631", "100");
    //        stdTypes.Add("632", "100");
    //        stdTypes.Add("633", "100");
    //        stdTypes.Add("634", "100");
    //        stdTypes.Add("635", "100");
    //        stdTypes.Add("636", "100");
    //        stdTypes.Add("637", "100");
    //        stdTypes.Add("638", "100");
    //        stdTypes.Add("639", "100");
    //        stdTypes.Add("640", "100");
    //        stdTypes.Add("641", "100");
    //        stdTypes.Add("642", "100");
    //        stdTypes.Add("643", "100");
    //        stdTypes.Add("701", "100");
    //        stdTypes.Add("702", "100");
    //        stdTypes.Add("703", "100");
    //        stdTypes.Add("704", "100");
    //        stdTypes.Add("705", "100");
    //        stdTypes.Add("706", "100");
    //        stdTypes.Add("707", "100");
    //        stdTypes.Add("708", "100");
    //        stdTypes.Add("709", "100");
    //        stdTypes.Add("710", "100");
    //        stdTypes.Add("711", "100");
    //        stdTypes.Add("712", "100");
    //        stdTypes.Add("713", "100");
    //        stdTypes.Add("714", "100");
    //        stdTypes.Add("715", "100");
    //        stdTypes.Add("716", "100");
    //        stdTypes.Add("717", "100");
    //        stdTypes.Add("718", "100");
    //        stdTypes.Add("719", "100");
    //        stdTypes.Add("720", "100");
    //        stdTypes.Add("721", "100");
    //        stdTypes.Add("722", "100");
    //        stdTypes.Add("723", "100");
    //        stdTypes.Add("724", "100");
    //        stdTypes.Add("725", "100");
    //        stdTypes.Add("726", "100");
    //        stdTypes.Add("727", "100");
    //        stdTypes.Add("728", "100");
    //        stdTypes.Add("729", "100");
    //        stdTypes.Add("730", "100");
    //        stdTypes.Add("731", "100");
    //        stdTypes.Add("732", "403");
    //        stdTypes.Add("733", "100");
    //        stdTypes.Add("734", "100");
    //        stdTypes.Add("735", "100");
    //        stdTypes.Add("736", "100");
    //        stdTypes.Add("737", "100");
    //        stdTypes.Add("738", "100");
    //        stdTypes.Add("739", "100");
    //        stdTypes.Add("740", "100");
    //        stdTypes.Add("741", "403");
    //        stdTypes.Add("742", "100");
    //        stdTypes.Add("743", "100");
    //        stdTypes.Add("744", "100");
    //        stdTypes.Add("745", "100");
    //        stdTypes.Add("801", "502");
    //        stdTypes.Add("802", "502");
    //        stdTypes.Add("803", "502");
    //        stdTypes.Add("804", "502");
    //        stdTypes.Add("805", "502");
    //        stdTypes.Add("806", "502");
    //        stdTypes.Add("807", "502");
    //        stdTypes.Add("808", "502");
    //        stdTypes.Add("809", "502");
    //        stdTypes.Add("810", "502");
    //        stdTypes.Add("811", "502");
    //        stdTypes.Add("812", "502");
    //        stdTypes.Add("813", "502");
    //        stdTypes.Add("814", "502");
    //        stdTypes.Add("815", "502");
    //        stdTypes.Add("816", "502");
    //        stdTypes.Add("817", "502");
    //        stdTypes.Add("818", "502");
    //        stdTypes.Add("819", "502");
    //        stdTypes.Add("820", "502");
    //        stdTypes.Add("901", "100");
    //        stdTypes.Add("902", "100");
    //        stdTypes.Add("903", "100");
    //        stdTypes.Add("904", "100");
    //        stdTypes.Add("905", "100");
    //        stdTypes.Add("921", "103");
    //        stdTypes.Add("922", "103");
    //        stdTypes.Add("923", "103");
    //        stdTypes.Add("924", "103");
    //        stdTypes.Add("925", "103");
    //        stdTypes.Add("926", "103");
    //        stdTypes.Add("927", "103");
    //        stdTypes.Add("928", "103");
    //        stdTypes.Add("929", "103");
    //        stdTypes.Add("930", "103");
    //        stdTypes.Add("931", "103");
    //        stdTypes.Add("932", "103");
    //        stdTypes.Add("933", "103");
    //        stdTypes.Add("934", "100");
    //        stdTypes.Add("935", "100");
    //        stdTypes.Add("936", "100");
    //        stdTypes.Add("937", "100");
    //        stdTypes.Add("938", "100");
    //        stdTypes.Add("939", "100");
    //        stdTypes.Add("940", "100");
    //        stdTypes.Add("941", "100");
    //        stdTypes.Add("942", "100");
    //        stdTypes.Add("943", "100");
    //        stdTypes.Add("951", "200");
    //        stdTypes.Add("952", "200");
    //        stdTypes.Add("961", "300");
    //        stdTypes.Add("962", "504");
    //        stdTypes.Add("991", "400");
    //        stdTypes.Add("992", "501");
    //        stdTypes.Add("993", "500");
    //        stdTypes.Add("999", "500");
    //    }

    //    private void populateStdCity()
    //    {
    //        stdCity.Add("AC", "ACAMPO");
    //        stdCity.Add("BN", "BANTA");
    //        stdCity.Add("CA", "ACAMPO");
    //        stdCity.Add("CL", "CLEMENTS");
    //        stdCity.Add("ES", "ESCALON");
    //        stdCity.Add("FA", "FARMINGTON");
    //        stdCity.Add("FC", "FRENCH CAMP");
    //        stdCity.Add("FM", "FARMINGTON");
    //        stdCity.Add("GA", "GALT");
    //        stdCity.Add("HO", "HOLT");
    //        stdCity.Add("LA", "LATHROP");
    //        stdCity.Add("LF", "LOCKEFORD");
    //        stdCity.Add("LI", "LINDEN");
    //        stdCity.Add("LK", "LOCKEFORD");
    //        stdCity.Add("LN", "LINDEN");
    //        stdCity.Add("LO", "LODI");
    //        stdCity.Add("MH", "MOUNTAIN HOUSE");
    //        stdCity.Add("MT", "MANTECA");
    //        stdCity.Add("OD", "OAKDALE");
    //        stdCity.Add("OK", "OAKDALE");
    //        stdCity.Add("RB", "RIVERBANK");
    //        stdCity.Add("RI", "RIPON");
    //        stdCity.Add("SJ", "SAN JOAQUIN COUNTY");
    //        stdCity.Add("ST", "STOCKTON");
    //        stdCity.Add("TH", "THORNTON");
    //        stdCity.Add("TR", "TRACY");
    //        stdCity.Add("UN", "UNINCORPORATED");
    //        stdCity.Add("VC", "VICTOR");
    //        stdCity.Add("WB", "WOODBRIDGE");
    //        stdCity.Add("WL", "WALLACE");
    //    }
    //}
}
