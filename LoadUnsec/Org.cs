﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Org : County
    {
        public Org()
        {
            co3 = "ORG";
            coNum = "30";
            populateStdTypes();
        }

        #region Properties
        List<string> fields = new List<string>();
        List<fixfield> fields1 = new List<fixfield>();
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            initialize();

            StreamReader srInputFile1 = null;
            StreamWriter swOutFile = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            srInputFile1 = openStreamReader(srcFile1);
            if (srcFile1 == null) result = "Problem with source file.";

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    // Get input data
                    DataTable table = new DataTable();
                    foreach (string fieldName in fields)
                    {
                        table.Columns.Add(fieldName, typeof(string));
                    }

                    table = fillDataTable(srInputFile1, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file1Length"]), fields1, false); // Add "true" for doChop arg for 2012 data

                    // close input file
                    if (srInputFile1 != null) srInputFile1.Close();
                    table.DefaultView.Sort = "ACCT_NBR";
                    DataView view = table.DefaultView;
                    DataTable dtRoll = view.ToTable();

                    count = 0;
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        if (!string.IsNullOrEmpty(drRoll["ACCT_NBR"].ToString()))
                        {
                            count++;
                            unsecrec thisRec = new unsecrec();
                            thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];
                            thisRec.APN_S = drRoll["ACCT_NBR"].ToString() + drRoll["STM_NBR"].ToString();
                            thisRec.APN_D = thisRec.APN_S;
                            string myFeePcl = "";
                            if(drRoll["APN"].ToString() != "99999999") myFeePcl = drRoll["APN"].ToString();
                            thisRec.FEE_PARCEL_S = myFeePcl;
                            thisRec.FEE_PARCEL_D = formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                            thisRec.ALT_APN = drRoll["ASMNT_NBR"].ToString();
                            thisRec.CO_NUM = coNum;
                            thisRec.CO_ID = co3;
                            thisRec.YRASSD = drRoll["ASMNT_YR"].ToString();
                            thisRec.TRA = drRoll["CURR_TRA"].ToString().PadLeft(6, '0');
                            thisRec.TYPE = drRoll["UNS_ASMNT_TYP"].ToString();
                            thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                            thisRec.EXE_CD = drRoll["XMP_TYP_CDE"].ToString();
                            thisRec.OWNER1 = cleanLine(drRoll["ASE_NME"].ToString());
                            thisRec.ASSESSEE = thisRec.OWNER1;

                            values myVals = new values();
                            myVals.LAND = drRoll["LND_VAL"].ToString();
                            myVals.IMPR = drRoll["IMP_VAL"].ToString();
                            myVals.PERSPROP = drRoll["PP_VAL"].ToString();
                            myVals.PENALTY = drRoll["PENALTY"].ToString();
                            myVals.EXE_AMT = drRoll["XMP_AMT"].ToString();
                            thisRec.LAND = myVals.LAND;
                            thisRec.IMPR = myVals.IMPR;
                            thisRec.PERSPROP = myVals.PERSPROP;
                            thisRec.GROSS = myVals.GROSS;
                            thisRec.PENALTY = myVals.PENALTY;
                            thisRec.EXE_AMT = myVals.EXE_AMT;


                            //- Combine ADDR3 + ' ' + ADDR4; trim and clean.
                            string ADDR3new = cleanLine(drRoll["ASE_ADDR_LINE3"].ToString() + ' ' + drRoll["ASE_ADDR_LINE4"].ToString());
                            string line1 = "";
                            string line2 = "";
                            //- If ADDR3new matches: blank, digits only, 2 char then digits: 
                            //    Combine ADDR2 + ' ' + ADDR3new; trim and clean. feed to addr class (done)
                            Regex re1 = new Regex(@"^\s*[\d-]+\s*$|^\s*\w{2}\s+[\d-]+\s*$|^\s*$", RegexOptions.IgnoreCase);
                            if (re1.IsMatch(ADDR3new))
                            {
                                line2 = cleanLine(drRoll["ASE_ADDR_LINE2"].ToString() + ' ' + ADDR3new);
                                line1 = cleanLine(drRoll["ASE_ADDR_LINE1"].ToString());
                            }
                            else
                            {
                                line2 = ADDR3new;
                                line1 = cleanLine(drRoll["ASE_ADDR_LINE1"].ToString());
                                //- If ADDR1 starts w/ C/O: ADDR1 = C/O, ADDR2 = line1, ADDR3new = line2
                                re1 = new Regex("^(" + County.RE_care_of + ")", RegexOptions.IgnoreCase);
                                Match mCO = re1.Match(line1); // Make sure line1 doesn't have a C/O
                                if (mCO.Success)
                                {
                                    Group g = mCO.Groups[1];
                                    thisRec.CARE_OF = line1.Replace(g.ToString(), "").Trim();

                                    line1 = cleanLine(drRoll["ASE_ADDR_LINE2"].ToString());
                                }
                                else
                                {
                                    //- If ADDR1 contains 'TAX' or 'TAXES': ADDR1 = C/O, ADDR2 => line1, ADDR3new => line2
                                    re1 = new Regex("tax", RegexOptions.IgnoreCase);
                                    if (re1.IsMatch(cleanLine(drRoll["ASE_ADDR_LINE1"].ToString())))
                                    {
                                        thisRec.CARE_OF = drRoll["ASE_ADDR_LINE1"].ToString();
                                        line1 = cleanLine(drRoll["ASE_ADDR_LINE2"].ToString());
                                    }
                                    else
                                    {
                                        re1 = new Regex(@"^(P\.?\s?O\.?\s*B.?X|B.?X|P\.?\s?O\.?\s+DRAWER)\s*\w");
                                        if (re1.IsMatch(cleanLine(drRoll["ASE_ADDR_LINE2"].ToString())))
                                        {
                                            line1 = cleanLine(drRoll["ASE_ADDR_LINE2"].ToString());
                                        }
                                        else
                                        {
                                            re1 = new Regex(@"\d+", RegexOptions.IgnoreCase);
                                            if (!re1.IsMatch(drRoll["ASE_ADDR_LINE1"].ToString()))
                                            {
                                                thisRec.CARE_OF = drRoll["ASE_ADDR_LINE1"].ToString();
                                                line1 = cleanLine(drRoll["ASE_ADDR_LINE2"].ToString());
                                            }
                                            else
                                            {
                                                line1 = cleanLine(drRoll["ASE_ADDR_LINE1"].ToString() + ' ' + drRoll["ASE_ADDR_LINE2"].ToString());
                                            }
                                        }
                                    }
                                }
                            }
                            
                            //- If there are 3 lines: ADDR1 = line1, ADDR2 = see below, ADDR3new = line2
                            //    If ADDR2 is PO Box: Make it line1
                            //    If ADDR2 passes Unit test: Send to unit_no, else C/O

                            mailing myMail = new mailing(line1, line2,"","");
                            thisRec.M_STRNUM = myMail.m_strnum;
                            thisRec.M_STR_SUB = myMail.m_str_sub;
                            thisRec.M_DIR = myMail.m_dir;
                            thisRec.M_STREET = myMail.m_street;
                            thisRec.M_SUFF = myMail.m_suff;
                            thisRec.M_UNITNO = myMail.m_unit_no;
                            thisRec.M_CITY = myMail.m_city;
                            thisRec.M_ST = myMail.m_st;
                            thisRec.M_ZIP = myMail.m_zip;
                            thisRec.M_ZIP4 = myMail.m_zip4;
                            thisRec.M_ADDR_D = myMail.m_addr_d;
                            thisRec.M_CTY_ST_D = myMail.m_cty_st_d;
                            if (!(myMail.msgs == "")) log(myMail.msgs);

                            swOutFile.WriteLine(thisRec.writeOutput());
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }
            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }
            return result;
        }

        private void initialize()
        {
            fields.Add("ASE_NME");
            fields.Add("ASE_ADDR_LINE1");
            fields.Add("ASE_ADDR_LINE2");
            fields.Add("ASE_ADDR_LINE3");
            fields.Add("ASE_ADDR_LINE4");
            fields.Add("ASMNT_YR");
            fields.Add("ASMNT_NBR");
            fields.Add("CURR_TRA");
            fields.Add("APN");
            fields.Add("UNS_ASMNT_TYP");
            fields.Add("ACCT_NBR");
            fields.Add("STM_NBR");
            fields.Add("LND_VAL");
            fields.Add("IMP_VAL");
            fields.Add("PENALTY");
            fields.Add("PP_VAL");
            fields.Add("XMP_AMT");
            fields.Add("XMP_TYP_CDE");

            //                     Length, Position, Name
            fields1.Add(new fixfield(25, 8, "ASE_NME"));
            fields1.Add(new fixfield(25, 34, "ASE_ADDR_LINE1"));
            fields1.Add(new fixfield(25, 60, "ASE_ADDR_LINE2"));
            fields1.Add(new fixfield(25, 86, "ASE_ADDR_LINE3"));
            fields1.Add(new fixfield(25, 112, "ASE_ADDR_LINE4"));
            fields1.Add(new fixfield(4, 138, "ASMNT_YR"));
            fields1.Add(new fixfield(6, 143, "ASMNT_NBR"));
            fields1.Add(new fixfield(5, 150, "CURR_TRA"));
            fields1.Add(new fixfield(8, 156, "APN"));
            fields1.Add(new fixfield(3, 165, "UNS_ASMNT_TYP"));
            fields1.Add(new fixfield(8, 169, "ACCT_NBR"));
            fields1.Add(new fixfield(3, 178, "STM_NBR"));
            fields1.Add(new fixfield(10, 183, "LND_VAL"));
            fields1.Add(new fixfield(10, 195, "IMP_VAL"));
            fields1.Add(new fixfield(10, 207, "PENALTY"));
            fields1.Add(new fixfield(10, 219, "PP_VAL"));
            fields1.Add(new fixfield(10, 231, "XMP_AMT"));
            fields1.Add(new fixfield(1, 242, "XMP_TYP_CDE"));
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("AP1", "300");
            stdTypes.Add("AP2", "301");
            stdTypes.Add("BB1", "100");
            stdTypes.Add("BB2", "101");
            stdTypes.Add("BJ1", "403");
            stdTypes.Add("BT1", "500");
            stdTypes.Add("BL1", "103");
            stdTypes.Add("BL2", "100");
            stdTypes.Add("EA1", "500");
            stdTypes.Add("EA2", "500");
            stdTypes.Add("EA3", "500");
            stdTypes.Add("EA4", "500");
            stdTypes.Add("EA5", "500");
            stdTypes.Add("EA6", "500");
            stdTypes.Add("EA7", "500");
            stdTypes.Add("EB1", "100");
            stdTypes.Add("EB2", "100");
            stdTypes.Add("EB3", "100");
            stdTypes.Add("EB4", "100");
            stdTypes.Add("EB5", "100");
            stdTypes.Add("EB6", "100");
            stdTypes.Add("EB7", "100");
            stdTypes.Add("EF", "501");
            stdTypes.Add("EH", "504");
            stdTypes.Add("EJ", "500");
            stdTypes.Add("EJ1", "500");
            stdTypes.Add("EJ4", "500");
            stdTypes.Add("EL1", "103");
            stdTypes.Add("EL2", "103");
            stdTypes.Add("EL3", "103");
            stdTypes.Add("EL4", "103");
            stdTypes.Add("EL5", "103");
            stdTypes.Add("EL7", "103");
            stdTypes.Add("EM1", "200");
            stdTypes.Add("EM2", "200");
            stdTypes.Add("EM3", "200");
            stdTypes.Add("EM4", "200");
            stdTypes.Add("EM5", "200");
            stdTypes.Add("EM7", "200");
            stdTypes.Add("EP1", "300");
            stdTypes.Add("EP2", "300");
            stdTypes.Add("EP3", "300");
            stdTypes.Add("EP4", "300");
            stdTypes.Add("EP7", "300");
            stdTypes.Add("ER", "501");
            stdTypes.Add("ER1", "501");
            stdTypes.Add("ES", "400");
            stdTypes.Add("ET", "100");
            stdTypes.Add("ET1", "100");
            stdTypes.Add("ET3", "100");
            stdTypes.Add("ET4", "100");
            stdTypes.Add("EX", "504");
            stdTypes.Add("GB1", "400");
            stdTypes.Add("GR1", "400");
            stdTypes.Add("LR1", "501");
            stdTypes.Add("LR2", "501");
            stdTypes.Add("LR3", "501");
            stdTypes.Add("MM1", "200");
            stdTypes.Add("MM2", "201");
            stdTypes.Add("RP1", "503");
        }
    }
}
