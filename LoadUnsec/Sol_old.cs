﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Sol_old : County
    {
        public Sol_old()
        {
            co3 = "SOL";
            coNum = "48";
            populateStdTypes();
            populateStdCity();
        }

        #region Properties
        List<fixfield> fields1 = new List<fixfield>();
        DataTable dtRoll = new DataTable();
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            int srcFile1RecLen = 352;
            long srcFileLength = 0;
            initialize();

            StreamWriter swOutFile = null;
            int count = 0;

            #region srcFile1
            if (File.Exists(srcFile1))
            {
                try
                {
                    FileInfo f = new FileInfo(srcFile1);
                    srcFileLength = f.Length;
                    using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(srcFile1, FileMode.Open))
                    {
                        using (MemoryMappedViewAccessor va = mmf.CreateViewAccessor(0, srcFileLength))
                        {
                            long i = 0;
                            while (i * srcFile1RecLen < srcFileLength)
                            {
                                long offset = i * srcFile1RecLen;
                                byte[] buffer = new byte[srcFile1RecLen];
                                va.ReadArray(offset, buffer, 0, srcFile1RecLen);
                                string myRec = System.Text.Encoding.ASCII.GetString(buffer);
                                DataRow dr = dtRoll.NewRow();
                                foreach (fixfield thisField in fields1)
                                {
                                    dr[thisField.Name] = getStr(myRec, thisField);
                                }
                                dtRoll.Rows.Add(dr);
                                i++;
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    result = "Error opening source file1: '" + srcFile1 + "'. " + e.Message;
                    return result;
                }

            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }
            #endregion

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            // Write out header
            unsecrec hdr = new unsecrec();
            swOutFile.WriteLine(hdr.writeHeader());

            // Loop through input
            try
            {
                dtRoll.DefaultView.Sort = "APN";
                DataView view = dtRoll.DefaultView;
                DataTable dtRollSort = view.ToTable();
                count = 0;
                foreach (DataRow drRoll in dtRollSort.Rows)
                {
                    count++;
                    unsecrec thisRec = new unsecrec();
                    thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                    thisRec.APN_S = drRoll["APN"].ToString();
                    thisRec.APN_D = thisRec.APN_S;
                    thisRec.FEE_PARCEL_S = drRoll["FEE_PCL"].ToString();
                    thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                    thisRec.CO_NUM = coNum;
                    thisRec.CO_ID = co3;
                    thisRec.YRASSD = drRoll["ROLL_YR"].ToString();
                    thisRec.TRA = drRoll["TRA"].ToString().PadLeft(6, '0');
                    thisRec.TYPE = drRoll["TYPE"].ToString();
                    thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                    string myExeCd = "";
                    if(drRoll["EXE_CD"].ToString() != "NOXM") myExeCd = drRoll["EXE_CD"].ToString();
                    thisRec.EXE_CD = myExeCd;

                    thisRec.OWNER1 = cleanLine(drRoll["OWNER"].ToString());
                    thisRec.ASSESSEE = thisRec.OWNER1;
                    thisRec.DBA = cleanLine(drRoll["DBA"].ToString());

                    // Assigns C/O & Addr1
                    string addrLine1 = "";
                    int ptrTop = 1;
                    int ptrBot = 0;
                    string addr1 = drRoll["M_ADDR1"].ToString();
                    string addr2 = drRoll["M_ADDR2"].ToString();
                    string[] ins = new string[2] { addr1, addr2 };

                    while (ptrBot <= ptrTop)
                    {
                        Regex re1 = new Regex(@"^(CO\s+|" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Regex re2 = new Regex(@"^(" + County.RE_dba + ")(.+)$", RegexOptions.IgnoreCase);
                        Regex re3 = new Regex(@"^(P\.?\s?O\.?\s*B.?X|P\.?\s?O\.?\s+DRAWER)(.+)$", RegexOptions.IgnoreCase);
                        Regex re4 = new Regex(@"^(\d+.+)$", RegexOptions.IgnoreCase);
                        Regex re5 = new Regex(@"^([^\d]+)$", RegexOptions.IgnoreCase);
                        Match m1 = re1.Match(ins[ptrBot]);
                        Match m2 = re2.Match(ins[ptrBot]);
                        Match m5 = re5.Match(ins[ptrBot]);

                        if (re1.IsMatch(ins[ptrBot]))
                        {
                            Group g2 = m1.Groups[2];
                            thisRec.CARE_OF = cleanLine(g2.ToString());
                            ptrBot++;
                        }
                        else if (re2.IsMatch(ins[ptrBot]))
                        {
                            Group g2 = m2.Groups[2];
                            thisRec.DBA = cleanLine(g2.ToString());
                            ptrBot++;
                        }
                        else if (re3.IsMatch(ins[ptrBot]))
                        {
                            addrLine1 = ins[ptrBot] + ' ' + addrLine1;
                            ptrBot++;
                        }
                        else if (re4.IsMatch(ins[ptrBot]))
                        {
                            addrLine1 += ' ' + ins[ptrBot];
                            ptrBot++;
                        }
                        else if (re5.IsMatch(ins[ptrBot]))
                        {
                            Group g1 = m5.Groups[1];
                            thisRec.OWNER2 = cleanLine(g1.ToString());
                            ptrBot++;
                        }
                        else
                        {
                            ptrBot++;
                        }
                    }

                    values myVals = new values();
                    long.TryParse(drRoll["PERSPROP"].ToString(), out long lPersprop);
                    long.TryParse(drRoll["VESSEL_PP"].ToString(), out long lVesselPP);
                    myVals.LAND = drRoll["LAND"].ToString();
                    myVals.IMPR = drRoll["IMPR"].ToString();
                    myVals.FIXTR = drRoll["FIXTR"].ToString();
                    myVals.PERSPROP = (lPersprop + lVesselPP).ToString();
                    myVals.PENALTY = drRoll["PENALTY"].ToString();
                    myVals.EXE_AMT = drRoll["EXE_AMT"].ToString();
                    thisRec.LAND = myVals.LAND;
                    thisRec.IMPR = myVals.IMPR;
                    thisRec.FIXTR = myVals.FIXTR;
                    thisRec.PERSPROP = myVals.PERSPROP;
                    thisRec.GROSS = myVals.GROSS;
                    thisRec.PENALTY = myVals.PENALTY;
                    thisRec.EXE_AMT = myVals.EXE_AMT;

                    mailing myMail = new mailing(addrLine1, drRoll["M_CITY_ST"].ToString() + " " + drRoll["M_ZIP"].ToString(), "", "");
                    thisRec.M_STRNUM = myMail.m_strnum;
                    thisRec.M_STR_SUB = myMail.m_str_sub;
                    thisRec.M_DIR = myMail.m_dir;
                    thisRec.M_STREET = myMail.m_street;
                    thisRec.M_SUFF = myMail.m_suff;
                    thisRec.M_UNITNO = myMail.m_unit_no;
                    thisRec.M_CITY = myMail.m_city;
                    thisRec.M_ST = myMail.m_st;
                    thisRec.M_ZIP = myMail.m_zip;
                    thisRec.M_ZIP4 = myMail.m_zip4;
                    thisRec.M_ADDR_D = myMail.m_addr_d;
                    thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                    string myUnit = "";
                    Regex reUnit = new Regex(@"^0+(.+)", RegexOptions.IgnoreCase);
                    Match mUnit = reUnit.Match(drRoll["S_UNITNO"].ToString());
                    if (mUnit.Success)
                    {
                        Group g1 = mUnit.Groups[1];
                        myUnit = g1.ToString();
                    }
                    if (!string.IsNullOrEmpty(myUnit)) myUnit = "#" + myUnit;
                    string situs1 = drRoll["S_HSENO"].ToString() + " " + drRoll["S_STR"].ToString() + " " + myUnit;
                    string situs2 = xlatCommunity(drRoll["S_CITY_CD"].ToString()) + " CA";
                    situs mySitus = new situs(situs1, situs2);
                    thisRec.S_HSENO = mySitus.s_hseno;
                    thisRec.S_STRNUM = mySitus.s_strnum;
                    thisRec.S_STR_SUB = mySitus.s_str_sub;
                    thisRec.S_DIR = mySitus.s_dir;
                    thisRec.S_STREET = mySitus.s_street;
                    thisRec.S_SUFF = mySitus.s_suff;
                    thisRec.S_UNITNO = mySitus.s_unit_no;
                    thisRec.S_CITY = mySitus.s_city;
                    thisRec.S_ST = mySitus.s_st;
                    thisRec.S_ZIP = mySitus.s_zip;
                    thisRec.S_ZIP4 = mySitus.s_zip4;
                    thisRec.S_ADDR_D = mySitus.s_addr_d;
                    thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                    swOutFile.WriteLine(thisRec.writeOutput());
                }
            }
            catch (Exception e)
            {
                log("Error in record: " + count + ". " + e.Message);
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }
            return result;
        }
        private void initialize()
        {
            //                     Length, Position, Name
            fields1.Add(new fixfield(10, 0, "APN"));
            fields1.Add(new fixfield(4, 10, "ROLL_YR"));
            fields1.Add(new fixfield(10, 28, "FEE_PCL"));
            fields1.Add(new fixfield(2, 38, "TYPE"));
            fields1.Add(new fixfield(30, 40, "OWNER"));
            fields1.Add(new fixfield(30, 70, "M_ADDR1"));
            fields1.Add(new fixfield(30, 100, "M_ADDR2"));
            fields1.Add(new fixfield(20, 130, "M_CITY_ST"));
            fields1.Add(new fixfield(10, 150, "M_ZIP"));
            fields1.Add(new fixfield(30, 160, "DBA"));
            fields1.Add(new fixfield(2, 190, "S_CITY_CD"));
            fields1.Add(new fixfield(30, 192, "S_STR"));
            fields1.Add(new fixfield(6, 222, "S_HSENO"));
            fields1.Add(new fixfield(4, 230, "S_UNITNO"));
            fields1.Add(new fixfield(6, 236, "TRA"));
            fields1.Add(new fixfield(12, 242, "LAND"));
            fields1.Add(new fixfield(12, 254, "IMPR"));
            fields1.Add(new fixfield(12, 266, "PERSPROP"));
            fields1.Add(new fixfield(12, 278, "FIXTR"));
            fields1.Add(new fixfield(12, 290, "VESSEL_PP"));
            fields1.Add(new fixfield(12, 302, "PENALTY"));
            fields1.Add(new fixfield(4, 314, "EXE_CD"));
            fields1.Add(new fixfield(12, 318, "EXE_AMT"));
            foreach (fixfield field in fields1)
            {
                dtRoll.Columns.Add(field.Name, typeof(string));
            }
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("AC", "300");
            stdTypes.Add("BT", "200");
            stdTypes.Add("IL", "501");
            stdTypes.Add("PI", "400");
            stdTypes.Add("UP", "101");
        }

        private void populateStdCity()
        {
            stdCity.Add("BN", "BENICIA");
            stdCity.Add("DX", "DIXON");
            stdCity.Add("FF", "FAIRFIELD");
            stdCity.Add("RV", "RIO VISTA");
            stdCity.Add("SU", "SUISUN CITY");
            stdCity.Add("UN", "UNINCORPORATED");
            stdCity.Add("VV", "VACAVILLE");
            stdCity.Add("VJ", "VALLEJO");
        }
    }
}
