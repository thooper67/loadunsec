﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Mno : Megabyte
    {
        public Mno()
        {
            co3 = "MNO";
            coNum = "26";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100"); // BUSINESS ASSESSMENTS
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("862", "401");
            stdTypes.Add("863", "401");
        }
    }
}
