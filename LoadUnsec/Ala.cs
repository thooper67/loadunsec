﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Ala : County
    {
        public Ala()
        {
            co3 = "ALA";
            coNum = "1";
            populateStdTypes();
        }

        #region Properties
        public int iFldROLL_YR = 0;
        public int iFldASSESSEE_ID = 1;
        public int iFldBPP_LOC = 2;
        public int iFldBPP_CLASS = 3;
        public int iFldTRA = 4;
        public int iFldSITUS_ADDRESS = 5;
        public int iFldDBA_NAME = 6;
        public int iFldOWNER_NAME = 7;
        public int iFldMAILING_ADDRESS = 8;
        public int iFldROLL_LAND = 9;
        public int iFldROLL_IMPS = 10;
        public int iFldROLL_PERS = 11;
        public int iFldGROSS_TOTAL = 12;
        public int iFldROLL_HO_EXEMPT = 13;
        public int iFldROLL_OTHER_EXEMPT = 14;
        public int iFldNET_TOTAL = 15;
        public int iFldBPP_PEN_FL = 16;
        public int iFldSORT_PARCEL = 17;
        public int iFldUNSECURED_FL = 18;
        public int iFldILL_FL = 19;
        public int iFldEXEMPT_CD = 20;
        public int iFldCUPS_ACCT = 21;
        public int iFldSITUS_STREET_NUM = 22;
        public int iFldSITUS_PRE_DIRECTIONAL = 23;
        public int iFldSITUS_STREET_NAME = 24;
        public int iFldSITUS_STREET_SUFFIX = 25;
        public int iFldSITUS_POST_DIRECTIONAL = 26;
        public int iFldSITUS_UNIT_DESIGINATOR = 27;
        public int iFldSITUS_UNIT_NUM = 28;
        public int iFldSITUS_CITY_NAME = 29;
        public int iFldSITUS_ZIP_CD = 30;
        public int iFldSITUS_ZIP_PLUS_4 = 31;
        public int iFldMAIL_STREET_NUM = 32;
        public int iFldMAIL_PRE_DIRECTIONAL = 33;
        public int iFldMAIL_STREET_NAME = 34;
        public int iFldMAIL_STREET_SUFFIX = 35;
        public int iFldMAIL_POST_DIRECTIONAL = 36;
        public int iFldMAIL_UNIT_DESIGNATOR = 37;
        public int iFldMAIL_UNIT_NUM = 38;
        public int iFldMAIL_CITY_NAME = 39;
        public int iFldMAIL_STATE_CD = 40;
        public int iFldMAIL_ZIP_CD = 41;
        public int iFldMAIL_ZIP_PLUS_4 = 42;
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamReader srInputFile = null;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input file
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true, '\t');
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    foreach (DataRow fields in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_D = fields[iFldCUPS_ACCT].ToString() + "-" + fields[iFldBPP_CLASS].ToString().PadLeft(2,'0') + "-" + fields[iFldROLL_YR].ToString().Substring(2,2);
                        thisRec.APN_S = thisRec.APN_D.Replace("-","");
                        thisRec.FEE_PARCEL_S = fields[iFldSORT_PARCEL].ToString();
                        Regex reFP = new Regex(@"^(.{4})(.{4})(.{3})(.{2})$");
                        Match m1 = reFP.Match(thisRec.FEE_PARCEL_S);
                        if (m1.Success)
                        {
                            thisRec.FEE_PARCEL_D = m1.Groups[1].ToString() + "-" + m1.Groups[2].ToString() + "-" + m1.Groups[3].ToString() + "-" + m1.Groups[4].ToString();
                        }
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = fields[iFldROLL_YR].ToString();
                        thisRec.TRA = fields[iFldTRA].ToString().PadLeft(6, '0');
                        thisRec.TYPE = fields[iFldBPP_CLASS].ToString().PadLeft(2, '0');
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.EXE_CD = fields[iFldEXEMPT_CD].ToString();

                        thisRec.OWNER1 = fields[iFldOWNER_NAME].ToString();
                        thisRec.DBA = fields[iFldDBA_NAME].ToString();

                        values myVals = new values();
                        myVals.LAND = fields[iFldROLL_LAND].ToString();
                        myVals.IMPR = fields[iFldROLL_IMPS].ToString();
                        myVals.PERSPROP = fields[iFldROLL_PERS].ToString();
                        myVals.HO_EXE = fields[iFldROLL_HO_EXEMPT].ToString();
                        myVals.OTHER_EXE = fields[iFldROLL_OTHER_EXEMPT].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string mail1 = fields["MAIL_STREET_NUM"].ToString() + " " + fields["MAIL_PRE_DIRECTIONAL"].ToString() + " " + fields["MAIL_STREET_NAME"].ToString() + " " + fields["MAIL_STREET_SUFFIX"].ToString() + " " +
                                          fields["MAIL_POST_DIRECTIONAL"].ToString() + " " + fields["MAIL_UNIT_DESIGNATOR"].ToString() + " " + fields["MAIL_UNIT_NUM"].ToString();
                        string mail2 = fields["MAIL_CITY_NAME"].ToString() + " " + fields["MAIL_STATE_CD"].ToString() + " " + fields["MAIL_ZIP_CD"].ToString();
                        mailing myMail = new mailing(mail1, mail2, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = fields["SITUS_STREET_NUM"].ToString() + " " + fields["SITUS_PRE_DIRECTIONAL"].ToString() + " " + fields["SITUS_STREET_NAME"].ToString() + " " + fields["SITUS_STREET_SUFFIX"].ToString() + " " +
                                          fields["SITUS_POST_DIRECTIONAL"].ToString() + " " + fields["SITUS_UNIT_DESIGINATOR"].ToString() + " " + fields["SITUS_UNIT_NUM"].ToString();
                        string situs2 = fields["SITUS_CITY_NAME"].ToString() + " " + "CA" + " " + fields["SITUS_ZIP_CD"].ToString();
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from sorted input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (srInputFile != null) srInputFile.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                string unsFlg = csv[18];
                if (unsFlg == "U")
                {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
                }
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[21];
            DataView view = table.DefaultView;

            return view.ToTable();
        }

        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("00", "102");
            stdTypes.Add("01", "103");
            stdTypes.Add("02", "106");
            stdTypes.Add("03", "100");
            stdTypes.Add("04", "100");
            stdTypes.Add("05", "100");
            stdTypes.Add("06", "105");
            stdTypes.Add("07", "503");
            stdTypes.Add("08", "102");
            stdTypes.Add("09", "102");
            stdTypes.Add("13", "301");
            stdTypes.Add("14", "101");
            stdTypes.Add("19", "300");
            stdTypes.Add("20", "300");
            stdTypes.Add("21", "200");
            stdTypes.Add("22", "201");
            stdTypes.Add("23", "200");
            stdTypes.Add("30", "400");
            stdTypes.Add("31", "501");
        }
    }
}
