﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;

namespace LoadUnsec
{
    class Mod : Megabyte
    {
        public Mod()
        {
            co3 = "MOD";
            coNum = "25";

            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
        }

        // Old Crest processing

        #region Properties
        //public int iFldAPN = 0;
        //public int iFldTRA = 1;
        //public int iFldNAME1 = 2;
        //public int iFldNAME2 = 3;
        //public int iFldADDRESS = 4;
        //public int iFldCITY_STATE = 5;
        //public int iFldZIP = 6;
        //public int iFldDBA = 7;
        //public int iFldLAND = 8;
        //public int iFldIMPR = 9;
        //public int iFldPERSPROP = 10;
        //public int iFldFIXTR = 11;
        //public int iFldTOTAL_EXE = 12;
        //public int iFldDESC = 14;
        //public int iFldTYPE = 15;
        //public int iFldYEAR = 23;
        #endregion

        #region Methods
        //public override string process()
        //{
        //    string result = "";
        //    string line = "";
        //    int count = 0;
        //    StreamReader srInputFile = null;
        //    StreamWriter swOutFile = null;

        //    // open input file
        //    string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
        //    if (File.Exists(srcFile1))
        //    {
        //        try
        //        {
        //            srInputFile = new StreamReader(srcFile1);
        //        }
        //        catch (Exception e)
        //        {
        //            result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
        //            log(result);
        //        }
        //    }
        //    else
        //    {
        //        result = co3 + " Source file '" + srcFile1 + "' does not exist.";
        //        log(result);
        //        return result;
        //    }

        //    // open output file
        //    try
        //    {
        //        swOutFile = new StreamWriter(outFile, false);
        //        log("Opened output file " + outFile);
        //    }
        //    catch (Exception e)
        //    {
        //        result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
        //        log(result);
        //    }

        //    if (result == "") // Only work on the output file if there weren't any previous issues.
        //    {
        //        // Write out header
        //        unsecrec hdr = new unsecrec();
        //        swOutFile.WriteLine(hdr.writeHeader());

        //        // Loop through input
        //        try
        //        {
        //            while ((line = srInputFile.ReadLine()) != null)
        //            {
        //                //Console.WriteLine(line);
        //                line = cleanLine(line);
        //                count++;
        //                unsecrec thisRec = new unsecrec();

        //                string[] fields = line.Split('~');
        //                thisRec.APN_S = fields[iFldAPN];
        //                thisRec.APN_D = thisRec.APN_S;
        //                thisRec.CO_NUM = coNum;
        //                thisRec.CO_ID = co3;
        //                thisRec.YRASSD = fields[iFldYEAR];
        //                thisRec.TRA = fields[iFldTRA];
        //                thisRec.LEGAL = fields[iFldDESC].Trim();
        //                thisRec.TYPE = fields[iFldTYPE];
        //                thisRec.TYPE_STD = xlatType(fields[iFldTYPE]);

        //                string name1 = fields[iFldNAME1];
        //                string name2 = fields[iFldNAME2];
        //                owner myOwner = new owner(name1, name2);
        //                thisRec.OWNER1 = myOwner.owner1;
        //                thisRec.OWNER2 = myOwner.owner2;
        //                thisRec.DBA = fields[iFldDBA];
        //                thisRec.CARE_OF = myOwner.care_of;
        //                // If (DBA already filled from DBA field) AND (Name2 isn't a C/O) Then fill Owner2 with name2:dba result
        //                if (!string.IsNullOrEmpty(thisRec.DBA) && string.IsNullOrEmpty(thisRec.CARE_OF) && (thisRec.DBA.CompareTo(myOwner.dba) != 0)) thisRec.OWNER2 = myOwner.dba;
        //                if (string.IsNullOrEmpty(thisRec.DBA)) thisRec.DBA = myOwner.dba;

        //                values myVals = new values();
        //                myVals.LAND = fields[iFldLAND];
        //                myVals.IMPR = fields[iFldIMPR];
        //                myVals.FIXTR = fields[iFldFIXTR];
        //                myVals.PERSPROP = fields[iFldPERSPROP];
        //                myVals.EXE_AMT = fields[iFldTOTAL_EXE];
        //                thisRec.LAND = myVals.LAND;
        //                thisRec.IMPR = myVals.IMPR;
        //                thisRec.FIXTR = myVals.FIXTR;
        //                thisRec.PERSPROP = myVals.PERSPROP;
        //                thisRec.GROSS = myVals.GROSS;
        //                thisRec.EXE_AMT = myVals.EXE_AMT;

        //                mailing myMail = new mailing(fields[iFldADDRESS], fields[iFldCITY_STATE] + " " + fields[iFldZIP], "", "");
        //                thisRec.M_STRNUM = myMail.m_strnum;
        //                thisRec.M_STR_SUB = myMail.m_str_sub;
        //                thisRec.M_DIR = myMail.m_dir;
        //                thisRec.M_STREET = myMail.m_street;
        //                thisRec.M_SUFF = myMail.m_suff;
        //                thisRec.M_UNITNO = myMail.m_unit_no;
        //                thisRec.M_CITY = myMail.m_city;
        //                thisRec.M_ST = myMail.m_st;
        //                thisRec.M_ZIP = myMail.m_zip;
        //                thisRec.M_ZIP4 = myMail.m_zip4;
        //                thisRec.M_ADDR_D = myMail.m_addr_d;
        //                thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

        //                swOutFile.WriteLine(thisRec.writeOutput());
        //                //if (count > 30) break;
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            result = "Error from input line " + count.ToString() + " : " + e.Message;
        //            log(result);
        //        }
        //    }

        //    // close input file
        //    if (srInputFile != null) srInputFile.Close();

        //    // close output file
        //    if (swOutFile != null)
        //    {
        //        swOutFile.Flush();
        //        swOutFile.Close();
        //    }

        //    return result;
        //}
        #endregion

        //private void populateStdTypes()
        //{
        //    stdTypes.Add("11", "104"); // BUSINESS ASSESSMENTS
        //    stdTypes.Add("12", "300");
        //    stdTypes.Add("13", "200");
        //    stdTypes.Add("14", "501");
        //    stdTypes.Add("15", "400");
        //    stdTypes.Add("16", "400");
        //    stdTypes.Add("17", "401");
        //    stdTypes.Add("18", "400");
        //    stdTypes.Add("19", "402");
        //    stdTypes.Add("41", "500");
        //    stdTypes.Add("49", "500");
        //    stdTypes.Add("51", "101");
        //    stdTypes.Add("55", "500");
        //    stdTypes.Add("61", "500");
        //}
    }
}
