﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Sbd : County
    {
        public Sbd()
        {
            co3 = "SBD";
            coNum = "36";
            populateStdTypes();
        }

        #region Properties
        public int iFldPCL_BK = 0;
        public int iFldPCL_PG = 1;
        public int iFldPCL_LN = 2;
        public int iFldPCL_TYPE = 3;
        public int iFldPCL_SEQ = 4;
        public int iFldROLL_YR = 5;
        public int iFldTYPE = 11;
        public int iFldTRA = 16;
        public int iFldLAND = 20;
        public int iFldIMPR = 21;
        public int iFldIMPR_PEN = 22;
        public int iFldPERSPROP = 23;
        public int iFldPERSPROP_PEN = 24;
        public int iFldEXE_TYPE = 25;
        public int iFldEXE_AMT = 26;
        public int iFldS_HSENO = 32;
        public int iFldS_STR_DIR = 33;
        public int iFldS_STR_NAM = 34;
        public int iFldS_STR_SFX = 35;
        public int iFldS_UNITNO = 36;
        public int iFldS_CITY = 37;
        public int iFldS_ZIP = 38;
        public int iFldS_ZIP4 = 39;
        public int iFldOWNER = 40;
        public int iFldOWNER2 = 43;
        public int iFldDBA = 49;
        public int iFldCARE_OF = 51;
        public int iFldM_ADDR = 52;
        public int iFldM_CITY_ST_ZIP = 53;
        public int iFldDESC = 59;
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            string line = "";
            int count = 0;
            StreamReader srInputFile = null;
            StreamWriter swOutFile = null;

            // open input file
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    srInputFile = new StreamReader(srcFile1);
                    srInputFile.ReadLine(); // Read a line to bypass the header.
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while ((line = srInputFile.ReadLine()) != null)
                    {
                        line = cleanLine(line);
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];

                        string[] fields = line.Split('~');
                        thisRec.APN_S = fields[iFldPCL_BK] + fields[iFldPCL_PG] + fields[iFldPCL_LN] + fields[iFldPCL_TYPE] + fields[iFldPCL_SEQ];
                        //thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                        Regex reAD = new Regex(@"^(.{4})(.{3})(.{2})(.{1})(.{3})$");
                        Match m1 = reAD.Match(thisRec.APN_S);
                        if (m1.Success)
                        {
                            thisRec.APN_D = m1.Groups[1].ToString() + "-" + m1.Groups[2].ToString() + "-" + m1.Groups[3].ToString() + "-" + m1.Groups[4].ToString() + "-" + m1.Groups[5].ToString();
                        }
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = fields[iFldROLL_YR];
                        thisRec.TRA = fields[iFldTRA].Substring(1, 6);
                        thisRec.LEGAL = fields[iFldDESC].Trim();
                        thisRec.TYPE = fields[iFldTYPE];
                        thisRec.TYPE_STD = xlatType(fields[iFldTYPE]);
                        thisRec.EXE_CD = fields[iFldEXE_TYPE];

                        thisRec.OWNER1 = fields[iFldOWNER];
                        thisRec.OWNER2 = fields[iFldOWNER2];
                        thisRec.DBA = fields[iFldDBA];

                        // Pull out C/O names from mailing address
                        string myMAddr = fields[iFldM_ADDR];
                        Regex reCO1 = new Regex(@"(.+)\s+(" + County.RE_care_of + @")\s+(.+)$", RegexOptions.IgnoreCase);
                        Match mCO1 = reCO1.Match(myMAddr);
                        Regex reCO2 = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mCO2 = reCO2.Match(fields[iFldCARE_OF]);
                        if (mCO1.Success)
                        {
                            Group g1 = mCO1.Groups[1];
                            Group g2 = mCO1.Groups[3];
                            myMAddr = g1.ToString();
                            thisRec.CARE_OF = g2.ToString();
                        }
                        else if (mCO2.Success)
                        {
                            Group g2 = mCO2.Groups[2];
                            thisRec.CARE_OF = g2.ToString();
                        }
                        else
                        {
                            thisRec.CARE_OF = fields[iFldCARE_OF];
                        }

                        values myVals = new values();
                        long.TryParse(fields[iFldIMPR_PEN].ToString(), out long lImprPen);
                        long.TryParse(fields[iFldPERSPROP_PEN].ToString(), out long lPersPropPen);
                        myVals.LAND = fields[iFldLAND];
                        myVals.IMPR = fields[iFldIMPR];
                        myVals.PERSPROP = fields[iFldPERSPROP];
                        myVals.PENALTY = (lImprPen + lPersPropPen).ToString();
                        myVals.EXE_AMT = fields[iFldEXE_AMT];
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.PENALTY = myVals.PENALTY;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        mailing myMail = new mailing(myMAddr, fields[iFldM_CITY_ST_ZIP], "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string myZip = (fields[iFldS_ZIP4].Trim()=="")?fields[iFldS_ZIP]:fields[iFldS_ZIP] + "-" + fields[iFldS_ZIP4];
                        string myUnit = fields[iFldS_UNITNO];
                        Regex reUnit = new Regex(@"^(s[-\s+])(.*)", RegexOptions.IgnoreCase);
                        Match mUnit = reUnit.Match(myUnit);
                        if (mUnit.Success)
                        {
                            Group g1 = mUnit.Groups[1];
                            Group g2 = mUnit.Groups[2];
                            myUnit = "STE " + g2.ToString();
                        }
                        reUnit = new Regex(@"^(u\s+)(.*)", RegexOptions.IgnoreCase);
                        mUnit = reUnit.Match(myUnit);
                        if (mUnit.Success)
                        {
                            Group g1 = mUnit.Groups[1];
                            Group g2 = mUnit.Groups[2];
                            myUnit = "UNIT " + g2.ToString();
                        }
                        reUnit = new Regex(@"^(b\s+)(.*)", RegexOptions.IgnoreCase);
                        mUnit = reUnit.Match(myUnit);
                        if (mUnit.Success)
                        {
                            Group g1 = mUnit.Groups[1];
                            Group g2 = mUnit.Groups[2];
                            myUnit = "BLDG " + g2.ToString();
                        }
                        string situs1 = fields[iFldS_HSENO] + " " + fields[iFldS_STR_DIR] + " " + fields[iFldS_STR_NAM] + " " + fields[iFldS_STR_SFX] + " " + myUnit;
                        string situs2 = fields[iFldS_CITY] + " CA " + myZip;
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = fields[iFldS_CITY];
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = fields[iFldS_ZIP];
                        thisRec.S_ZIP4 = fields[iFldS_ZIP4];
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (srInputFile != null) srInputFile.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("AGR", "502");
            stdTypes.Add("AIR", "300");
            stdTypes.Add("APT", "101");
            stdTypes.Add("BOA", "200");
            stdTypes.Add("BPL", "100");
            stdTypes.Add("CEL", "503");
            stdTypes.Add("CEM", "100");
            stdTypes.Add("COM", "100");
            stdTypes.Add("DRW", "100");
            stdTypes.Add("ELE", "503");
            stdTypes.Add("ENT", "100");
            stdTypes.Add("EXM", "504");
            stdTypes.Add("FNC", "106");
            stdTypes.Add("IMP", "501");
            stdTypes.Add("LSE", "103");
            stdTypes.Add("MFG", "100");
            stdTypes.Add("MIN", "401");
            stdTypes.Add("OIL", "403");
            stdTypes.Add("ORE", "100");
            stdTypes.Add("PIN", "400");
            stdTypes.Add("PRF", "100");
            stdTypes.Add("ROW", "500");
            stdTypes.Add("SPC", "500");
            stdTypes.Add("SVC", "108");
            stdTypes.Add("TRN", "301");
            stdTypes.Add("WTR", "503");
            stdTypes.Add("XAC", "500");
        }
    }
}
