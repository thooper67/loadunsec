﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Imp : County
    {
        public Imp()
        {
            co3 = "IMP";
            coNum = "13";
            populateStdTypes();
        }

        #region Properties
        // Field names and positions from AsmtMaster header
        //  0 FeeParcel
        //  1 Asmt
        //  2 TRA
        //  6 Assessee
        //  7 InCareOf
        //  8 DBA
        //  9 A_Street
        // 10 A_City
        // 11 A_State
        // 12 A_Zip
        // 17 F_Situs1
        // 18 F_Situs2
        // 23 Land
        // 24 Imp
        // 25 PersProp
        // 26 Growing

        // Field names and positions from IMP_PPAssetSummary header
        //  0 AsmtRollYear
        //  1 Asmt
        //  2 FormNum
        //  3 PPValue
        //  4 Status
        //  5 XfrPPValue
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            int iValueCount = 0;
            CsvReader csv1 = null;
            CsvReader csv2 = null;
            StreamWriter swOutFile = null;

            // open input file
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true, ';');
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }
            string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
            if (File.Exists(srcFile2))
            {
                try
                {
                    csv2 = new CsvReader(new StreamReader(srcFile2), true, '\t');
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile2 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile2 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                DataTable dtRoll = getTable(csv1);
                DataTable dtValues = getTable2(csv2);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = drRoll["Asmt"].ToString();
                        thisRec.APN_D = County.formattedNumber(drRoll["Asmt"].ToString(), thisRec.APN_D_format);
                        thisRec.FEE_PARCEL_S = drRoll["FeeParcel"].ToString();
                        thisRec.FEE_PARCEL_D = County.formattedNumber(drRoll["FeeParcel"].ToString(), thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = drRoll["TRA"].ToString();
                        thisRec.TYPE = drRoll["Asmt"].ToString().Substring(0, 3);
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                        thisRec.OWNER1 = cleanLine(drRoll["Assessee"].ToString());
                        thisRec.ASSESSEE = thisRec.OWNER1;

                        Regex reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mCO = reCO.Match(drRoll["InCareOf"].ToString());
                        if (reCO.IsMatch(drRoll["InCareOf"].ToString()))
                        {
                            Group gCO = mCO.Groups[2];
                            thisRec.CARE_OF = cleanLine(gCO.ToString());
                        }
                        else
                        {
                            thisRec.CARE_OF = cleanLine(drRoll["InCareOf"].ToString());
                        }

                        Regex reDBA = new Regex(@"^(" + County.RE_dba + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mDBA = reDBA.Match(drRoll["DBA"].ToString());
                        if (reDBA.IsMatch(drRoll["DBA"].ToString()))
                        {
                            Group gDBA = mDBA.Groups[2];
                            thisRec.DBA = cleanLine(gDBA.ToString());
                        }
                        else
                        {
                            thisRec.DBA = cleanLine(drRoll["DBA"].ToString());
                        }

                        // Keep reading value records until we find a match, or realize there isn't a match.
                        bool bValMatch = false;
                        string sRollAsmt = drRoll["Asmt"].ToString();
                        string sValueAsmt = dtValues.Rows[iValueCount]["Asmt"].ToString();
                        while ((iValueCount < dtValues.Rows.Count - 1) && (sRollAsmt.CompareTo(sValueAsmt) > 0))
                        {
                            iValueCount++;
                            sValueAsmt = dtValues.Rows[iValueCount]["Asmt"].ToString();
                        }
                        if (sRollAsmt.CompareTo(sValueAsmt) == 0) bValMatch = true;

                        values myVals = new values();
                        if (bValMatch)
                        {
                            myVals.PERSPROP = dtValues.Rows[iValueCount]["PPValue"].ToString();
                        }
                        myVals.LAND = drRoll["Land"].ToString();
                        myVals.IMPR = drRoll["Imp"].ToString();
                        myVals.GROWING = drRoll["Growing"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROWING = myVals.GROWING;
                        thisRec.GROSS = myVals.GROSS;

                        string myZip = drRoll["A_Zip"].ToString();
                        Regex reZip = new Regex(@"^\d{9}$");
                        if (reZip.IsMatch(myZip))
                        {
                            myZip = myZip.Substring(0, 5) + "-" + myZip.Substring(5, 4);
                        }
                        else
                        {
                            myZip = drRoll["A_Zip"].ToString();
                        }
                        mailing myMail = new mailing(drRoll["A_Street"].ToString(), drRoll["A_City"].ToString() + " " + drRoll["A_State"].ToString() + " " + myZip, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string mySitus1 = "";
                        string mySitus2 = "";
                        if (drRoll["F_Situs1"].ToString() != "NULL") mySitus1 = drRoll["F_Situs1"].ToString();
                        if (drRoll["F_Situs2"].ToString() != "NULL") mySitus2 = drRoll["F_Situs2"].ToString();
                        situs mySitus = new situs(mySitus1, mySitus2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("805", "105");
            stdTypes.Add("807", "401");
            stdTypes.Add("810", "101");
            stdTypes.Add("811", "106");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
        }

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                DataRow dr = table.NewRow();
                for (int i = 0; i < csv.FieldCount; i++)
                {
                    dr[i] = csv[i];
                }
                table.Rows.Add(dr);
                
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[1];
            DataView view = table.DefaultView;

            return view.ToTable();
        }

        private DataTable getTable2(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                if (csv[0] == ConfigurationManager.AppSettings["year"])
                {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
                }

            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[1];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
    }
}
