﻿using System;
using System.Linq;

namespace LoadUnsec
{
    class Mad : Megabyte
    {
        public Mad()
        {
            co3 = "MAD";
            coNum = "20";
            populateStdTypes();
        }

        private void populateStdTypes()
        {
            stdTypes.Add("800", "100");
            stdTypes.Add("810", "103");
            stdTypes.Add("820", "300");
            stdTypes.Add("830", "200");
            stdTypes.Add("850", "501");
            stdTypes.Add("853", "403");
            stdTypes.Add("860", "400");
            stdTypes.Add("870", "503");
        }
    }
}
