﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Men_old : County
    {
        public Men_old()
        {
            co3 = "MEN";
            coNum = "23";
            populateStdTypes();
        }

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            CsvReader csv1 = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    csv1 = new CsvReader(new StreamReader(srcFile1), true);
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Get file in a DataTable
                DataTable dtRoll = getTable(csv1);

                // Loop through input
                try
                {
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();

                        thisRec.APN_S = drRoll["PIN"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = ConfigurationManager.AppSettings["year"];
                        thisRec.TRA = drRoll["TRA"].ToString();
                        thisRec.OWNER1 = cleanLine(drRoll["LegalParty1"].ToString());
                        thisRec.OWNER2 = cleanLine(drRoll["LegalParty2"].ToString() + " " + drRoll["LegalParty3"].ToString() + " " + drRoll["LegalParty4"].ToString());
                        thisRec.DBA = cleanLine(drRoll["DoingBusinessAs"].ToString());
                        thisRec.CARE_OF = cleanLine(drRoll["MailName"].ToString());
                        thisRec.LEGAL = cleanLine(drRoll["BusinessUseCodeDescription"].ToString());
                        thisRec.TYPE = drRoll["ClassCode"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);

                        values myVals = new values();
                        long.TryParse(drRoll["HOX"].ToString(), out long lHOExe);
                        long.TryParse(drRoll["VetExempt"].ToString(), out long lVetExe);
                        long.TryParse(drRoll["DisabledVetExempt"].ToString(), out long lDVExe);
                        long.TryParse(drRoll["ChurchExempt"].ToString(), out long lChurchExe);
                        long.TryParse(drRoll["RELX"].ToString(), out long lReligiousExe);
                        long.TryParse(drRoll["WelfarePPSchoolExempt"].ToString(), out long lSchoolExe);
                        long.TryParse(drRoll["WelfareHospExempt"].ToString(), out long lHospitalExe);
                        long.TryParse(drRoll["WelfareCharityRel"].ToString(), out long lWelfareExe);
                        myVals.LAND = drRoll["Land"].ToString();
                        myVals.IMPR = drRoll["Improvements"].ToString();
                        myVals.FIXTR = drRoll["TradeFixturesAmount"].ToString();
                        myVals.GROWING = drRoll["LivingImprovements"].ToString();
                        myVals.PERSPROP = drRoll["PersonalValue"].ToString();
                        myVals.PENALTY = drRoll["TenPercentAssdPenalty"].ToString();
                        myVals.HO_EXE = (lHOExe + lVetExe + lDVExe + lChurchExe + lReligiousExe + lSchoolExe + lHospitalExe + lWelfareExe).ToString();
                        if (myVals.HO_EXE != "")
                        {
                            myVals.OTHER_EXE = "0";
                        }
                        else
                        {
                            myVals.OTHER_EXE = drRoll["OtherExmpt"].ToString();
                        }
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.GROWING = myVals.GROWING;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.PENALTY = myVals.PENALTY;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        mailing myMail = new mailing(drRoll["MailAddress"].ToString(), cleanLine(drRoll["MailCity"].ToString()) + " " + drRoll["MailState"].ToString() + " " + drRoll["MailZipCode"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = drRoll["SitusStreetNumber"].ToString() + " " + drRoll["SitusStreetNumberSuffix"].ToString() + " " + drRoll["SitusStreetPredirectional"].ToString() + " " + drRoll["SitusStreetName"].ToString() + " " + drRoll["SitusStreetType"].ToString() + " " + drRoll["SitusUnitNumber"].ToString();
                        string situs2 = drRoll["SitusCityName"].ToString() + " CA " + drRoll["SitusZipCode"].ToString();
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = cleanLine(thisRec.S_STRNUM + " " + thisRec.S_DIR + " " + thisRec.S_STREET + " " + thisRec.S_SUFF + " " + thisRec.S_UNITNO);
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("PPA-AG", "502");
            stdTypes.Add("PPA-AIR", "300");
            stdTypes.Add("PPA-BUS", "100");
            stdTypes.Add("PPA-COND", "102");
            stdTypes.Add("PPA-FIN", "106");
            stdTypes.Add("PPA-PS", "400");
            stdTypes.Add("PPA-LEAS", "107");
            stdTypes.Add("PPA-VESSEL", "200");
        }

        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                string classType = csv[5];
                if (classType == "UnsecuredPP")
                {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
                }
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
    }
}
