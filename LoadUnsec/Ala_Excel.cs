﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Data.Odbc;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Ala_Excel : County
    {
        public Ala_Excel()
        {
            co3 = "ALA";
            coNum = "1";
            populateStdTypes();
            populateStdCity();
        }

        #region Propertes
        // Field names and positions from unsecured roll header
        //  0 Account Number
        //  1 Assessee Number
        //  2 Assessee Year
        //  3 Assessee Class
        //  4 Assessee Division
        //  5 Assessee Location
        //  6 Assessee Type
        //  7 TRA
        //  8 Situs Number
        //  9 Situs Street
        // 10 Situs City
        // 11 Situs Room
        // 12 DBA
        // 13 Owner
        // 14 Mailing Address
        // 15 Mailing Room
        // 16 Mailing City
        // 17 Mailing Zip
        // 18 Land
        // 19 Imps
        // 20 Personal Property
        // 21 HOEX
        // 22 OTEX
        // 23 Net Total
        // 24 Penalty
        // 25 Parcel
        // 26 Secure Key
        // 27 ILL
        // 28 Exemption Code
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            StreamWriter swOutFile = null;
            FileStream stream = null;
            string connStr;
            OdbcConnection oConn;
            OdbcCommand oCmd;
            OdbcDataReader fields=null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            if (File.Exists(srcFile1))
            {
                try
                {
                    connStr = String.Format(ConfigurationManager.AppSettings["excelConnStr"], srcFile1, srcFolder);
                    oConn = new OdbcConnection(connStr);
                    string sSelect = "SELECT * FROM [" + ConfigurationManager.AppSettings[co3 + "file1"].Replace(".xlsx", "") + "$] WHERE [Secure Key]='U' ORDER BY [Account Number]";
                    oCmd = new OdbcCommand(sSelect, oConn);
                    oConn.Open();
                    fields = oCmd.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                    log(result);
                }
            }
            else
            {
                result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                log(result);
                return result;
            }

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }
            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    while (fields.Read())
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];

                        thisRec.APN_S = fields["Account Number"].ToString().Replace("-","");
                        thisRec.APN_D = fields["Account Number"].ToString();
                        thisRec.FEE_PARCEL_S = fields["Parcel"].ToString();
                        Regex reFP = new Regex(@"^(.{4})(.{4})(.{3})(.{2})$");
                        Match m1 = reFP.Match(thisRec.FEE_PARCEL_S);
                        if (m1.Success)
                        {
                            thisRec.FEE_PARCEL_D = m1.Groups[1].ToString() + "-" + m1.Groups[2].ToString() + "-" + m1.Groups[3].ToString() + "-" + m1.Groups[4].ToString();
                        }
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = fields["Assessee Year"].ToString();
                        thisRec.TRA = fields["TRA"].ToString().PadLeft(6,'0');
                        thisRec.OWNER1 = cleanLine(fields["Owner"].ToString());
                        thisRec.DBA = cleanLine(fields["DBA"].ToString());
                        thisRec.EXE_CD = fields["Exemption Code"].ToString();
                        thisRec.TYPE = fields["Assessee Class"].ToString();
                        thisRec.TYPE_STD = xlatType(fields["Assessee Class"].ToString());

                        values myVals = new values();
                        myVals.LAND = fields["Land"].ToString().Replace("$","");
                        myVals.IMPR = fields["Imps"].ToString().Replace("$", "");
                        myVals.PERSPROP = fields["Personal Property"].ToString().Replace("$", "");
                        decimal otex;
                        decimal hoex;
                        decimal.TryParse(fields["OTEX"].ToString().Replace("$", ""), out otex);
                        decimal.TryParse(fields["HOEX"].ToString().Replace("$", ""), out hoex);
                        myVals.EXE_AMT = (otex + hoex).ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string mail1 = fields["MAIL_STREET_NUM"].ToString() + " " + fields["MAIL_PRE_DIRECTIONAL"].ToString() + " " + fields["MAIL_STREET_NAME"].ToString() + " " + fields["MAIL_STREET_SUFFIX"].ToString() + " " + 
                                          fields["MAIL_POST_DIRECTIONAL"].ToString() + " " + fields["MAIL_UNIT_DESIGNATOR"].ToString() + " " + fields["MAIL_UNIT_NUM"].ToString();
                        string mail2 = fields["MAIL_CITY_NAME"].ToString() + " " + fields["MAIL_STATE_CD"].ToString() + " " + fields["MAIL_ZIP_CD"].ToString();
                        mailing myMail = new mailing(mail1, mail2, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = fields["SITUS_STREET_NUM"].ToString() + " " + fields["SITUS_PRE_DIRECTIONAL"].ToString() + " " + fields["SITUS_STREET_NAME"].ToString() + " " + fields["SITUS_STREET_SUFFIX"].ToString() + " " +
                                          fields["SITUS_POST_DIRECTIONAL"].ToString() + " " + fields["SITUS_UNIT_DESIGINATOR"].ToString() + " " + fields["SITUS_UNIT_NUM"].ToString();
                        string situs2 = fields["SITUS_CITY_NAME"].ToString() + " " + "CA" + " " + fields["SITUS_ZIP_CD"].ToString();
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        // Address processing for pre-2017 layout
                        //string myZip = fields["Mailing Zip"].ToString();
                        //Regex reZip = new Regex(@"^\d{9}$");
                        //if (reZip.IsMatch(myZip))
                        //{
                        //    myZip = myZip.Substring(0, 5) + "-" + myZip.Substring(5, 4);
                        //}
                        //mailing myMail = new mailing(fields["Mailing Address"].ToString(), fields["Mailing City"].ToString() + " " + myZip, "", "");
                        //thisRec.M_STRNUM = myMail.m_strnum;
                        //thisRec.M_STR_SUB = myMail.m_str_sub;
                        //thisRec.M_DIR = myMail.m_dir;
                        //thisRec.M_STREET = myMail.m_street;
                        //thisRec.M_SUFF = myMail.m_suff;
                        //thisRec.M_UNITNO = fields["Mailing Room"].ToString().Trim()==""?cleanLine(myMail.m_unit_no):cleanLine(fields["Mailing Room"].ToString());
                        //thisRec.M_CITY = myMail.m_city;
                        //thisRec.M_ST = myMail.m_st;
                        //thisRec.M_ZIP = myMail.m_zip;
                        //thisRec.M_ZIP4 = myMail.m_zip4;
                        //thisRec.M_ADDR_D = cleanLine(fields["Mailing Address"].ToString() + " " + fields["Mailing Room"].ToString());
                        //thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        //situs mySitus = new situs(fields["Situs Number"].ToString() + " " + fields["Situs Street"].ToString(), xlatCommunity(fields["Situs City"].ToString()) + " CA");
                        //thisRec.S_HSENO = mySitus.s_hseno;
                        //thisRec.S_STRNUM = mySitus.s_strnum;
                        //thisRec.S_STR_SUB = mySitus.s_str_sub;
                        //thisRec.S_DIR = mySitus.s_dir;
                        //thisRec.S_STREET = mySitus.s_street;
                        //thisRec.S_SUFF = mySitus.s_suff;
                        //thisRec.S_UNITNO = fields["Situs Room"].ToString();
                        //thisRec.S_CITY = mySitus.s_city;
                        //thisRec.S_ST = mySitus.s_st;
                        //thisRec.S_ZIP = mySitus.s_zip;
                        //thisRec.S_ZIP4 = mySitus.s_zip4;
                        //thisRec.S_ADDR_D = cleanLine(fields["Situs Number"].ToString() + " " + fields["Situs Street"].ToString() + " " + fields["Situs Room"].ToString());
                        //thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }

            // close input file
            if (stream != null) stream.Close();

            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }

            return result;
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("00", "102");
            stdTypes.Add("01", "103");
            stdTypes.Add("02", "106");
            stdTypes.Add("03", "100");
            stdTypes.Add("04", "100");
            stdTypes.Add("05", "100");
            stdTypes.Add("06", "105");
            stdTypes.Add("07", "503");
            stdTypes.Add("08", "102");
            stdTypes.Add("09", "102");
            stdTypes.Add("13", "301");
            stdTypes.Add("14", "101");
            stdTypes.Add("19", "300");
            stdTypes.Add("20", "300");
            stdTypes.Add("21", "200");
            stdTypes.Add("22", "201");
            stdTypes.Add("23", "200");
            stdTypes.Add("30", "400");
            stdTypes.Add("31", "501");
        }
        private void populateStdCity()
        {
            // Community Translation Table
            stdCity.Add("ALA", "ALAMEDA");
            stdCity.Add("ALB", "ALBANY");
            stdCity.Add("ALT", "ALTAMONT");
            stdCity.Add("BRK", "BERKELEY");
            stdCity.Add("CTV", "CASTRO VALLEY");
            stdCity.Add("DUB", "DUBLIN");
            stdCity.Add("EMY", "EMERYVILLE");
            stdCity.Add("FRE", "FREMONT");
            stdCity.Add("HAY", "HAYWARD");
            stdCity.Add("LIV", "LIVERMORE");
            stdCity.Add("MTH", "MOUNTAIN HOUSE");
            stdCity.Add("MTN", "MOUNTAIN HOUSE");
            stdCity.Add("MUR", "MURRY");
            stdCity.Add("NHV", "NEW HAVEN");
            stdCity.Add("NEW", "NEWARK");
            stdCity.Add("OAK", "OAKLAND");
            stdCity.Add("PMT", "PIEDMONT");
            stdCity.Add("PLE", "PLEASANTON");
            stdCity.Add("SLE", "SAN LEANDRO");
            stdCity.Add("SLZ", "SAN LORENZO");
            stdCity.Add("SUN", "SUNOL");
            stdCity.Add("UNC", "UNION CITY");
        }

    }
}
