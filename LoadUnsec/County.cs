﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.OleDb;
using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    public class County
    {
        #region Variables
        private string _co3;
        private readonly string _prodFolder = ConfigurationManager.AppSettings["prodFolder"];
        private readonly string _logFolder = ConfigurationManager.AppSettings["logFolder"];
        private string _logFile;
        private string _srcFolder = "";
        private string _outFile = "";
        #endregion

        // Constructor
        public County()
        {
        }

        #region Properties
        public string co3 
        { 
            get { return _co3; }
            set 
            { 
                _co3 = value;
                _logFile = Path.Combine(_logFolder, "LoadUns_" + co3 + "_" + DateTime.Now.ToString("yyyyMMdd") + ".log");
                log("Processing " + _co3);
            }
        }
        public string coNum = "0";
        public string prodFolder { get { return _prodFolder; } }
        public string logFolder { get { return _logFolder; } }
        public string logFile { get { return _logFile; } }
        public string srcFolder
        {
            get 
            {
                if (_srcFolder == "")
                {
                    //_srcFolder = ConfigurationManager.AppSettings[co3 + "srcFolder"];
                    _srcFolder = String.Format(ConfigurationManager.AppSettings["srcFolder"], co3, ConfigurationManager.AppSettings["year"]);
                }
                return _srcFolder; 
            }
        }
        public string outFile
        {
            get
            {
                if (_outFile == "")
                {
                    _outFile = ConfigurationManager.AppSettings["outFile"];
                    _outFile = String.Format(_outFile, co3);
                }
                return _outFile;
            }
        }
        public Dictionary<string, string> stdTypes = new Dictionary<string, string>();
        public Dictionary<string, string> stdCity = new Dictionary<string, string>();
        public Dictionary<string, string> stdUnit = new Dictionary<string, string>();
        public Dictionary<string, string> stdExe = new Dictionary<string, string>();
        #endregion

        #region Methods
        public virtual string process()
        {
            return "ERROR";
        }
        public void log(string message)
        {
            StreamWriter sw = new StreamWriter(_logFile, true);
            sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ' ' + message);
            sw.Flush();
            sw.Close();
        }
        public StreamReader openStreamReader(string srcFile)
        {
            StreamReader result = null;
            string msg = "";
            if (File.Exists(srcFile))
            {
                try
                {
                    result = new StreamReader(srcFile);
                }
                catch (Exception e)
                {
                    msg = "Error opening input file '" + srcFile + "'. :" + e.Message;
                    log(msg);
                }
            }
            else
            {
                msg = co3 + " Source file '" + srcFile + "' does not exist.";
                log(msg);
            }
            return result;
        }
        
        public DataTable fillDataTable(string connectionString, string select, DataTable table, List<fixfield> myFields)
        {
            OleDbConnection conn = new OleDbConnection();
            try
            {
                DataSet ds = new DataSet();
                conn = new OleDbConnection(connectionString);
                conn.Open();

                OleDbDataAdapter da = new OleDbDataAdapter(select, conn);
                da.Fill(ds);
                DataTable dt = ds.Tables[0];
                foreach (DataRow rows in dt.Rows)
                {
                    DataRow dr = table.NewRow();
                    foreach (fixfield thisField in myFields)
                    {
                        dr[thisField.Name] = rows[thisField.Format].ToString();
                    }
                    table.Rows.Add(dr);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                conn.Close();
            }
            return table;
        }
        public DataTable fillDataTable(StreamReader srInputFile, DataTable table, int lineLength, List<fixfield> myFields, bool doTrim = true, bool doChop = false)
        {
            // Read the first line
            string line = srInputFile.ReadLine();
            int count = 0;

            while (!string.IsNullOrEmpty(line))
            {
                count++;
                //if (count > 16000) break;
                if (doTrim) line = line.Trim(); // This will cause problems with source files without a character in the last byte of the line.
                if (doChop && (line.Length > lineLength)) line = line.Substring(0, lineLength);
                if (line.Length == lineLength)
                {
                    DataRow dr = table.NewRow();
                    foreach (fixfield thisField in myFields)
                    {
                        dr[thisField.Name] = getStr(line, thisField);
                    }
                    table.Rows.Add(dr);
                }
                else
                {
                    log("Bad record(line: " + count + "): " + line);
                }
                // Read the next line
                line = srInputFile.ReadLine();
            }
            return table;
        }
        public DataTable fillDataTable(CsvReader csv, DataTable table, List<fixfield> myFields)
        {
            DataTable dt = new DataTable();
            while (csv.ReadNextRecord())
            {
                DataRow dr = table.NewRow();
                for (int i = 0; i < csv.FieldCount; i++)
                {
                    dr[i] = csv[i];
                }
                table.Rows.Add(dr);

            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[1];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
        public static string formattedNumber(string inputField, string outputFormat)
        {
            string result = "";
            long lInput = 0;
            long.TryParse(inputField, out lInput);
            if (lInput != 0) result = lInput.ToString(outputFormat);
            return result;
        }
        public string xlatType(string type)
        {
            string result = "";
            type = type.Trim();
            if (string.IsNullOrEmpty(type))
            {
                // Ignore type if it is null or empty.
            } 
            else if(stdTypes.ContainsKey(type))
            {
                result = stdTypes[type];
            }
            else
            {
                log("Unknown standard type: '" + type + "'");
            }
            return result;
        }
        public string xlatCommunity(string community)
        {
            string result = "";
            community = community.Trim();
            if (string.IsNullOrEmpty(community))
            {
                // Ignore type if it is null or empty.
            }
            else if (stdCity.ContainsKey(community))
            {
                result = stdCity[community];
            }
            else
            {
                log("Unknown standard city: '" + community + "'");
            }
            return result;
        }
        public string xlatUnit(string unit)
        {
            string result = "";
            unit = unit.Trim();
            if (string.IsNullOrEmpty(unit))
            {
                // Ignore exemption if it is null or empty.
            }
            else if (stdUnit.ContainsKey(unit))
            {
                result = stdUnit[unit];
            }
            else
            {
                log("Unknown unit type: '" + unit + "'");
            }
            return result;
        }
        public string xlatExe(string exe)
        {
            string result = "";
            exe = exe.Trim();
            if (string.IsNullOrEmpty(exe))
            {
                // Ignore exemption if it is null or empty.
            }
            else if (stdExe.ContainsKey(exe))
            {
                result = stdExe[exe];
            }
            else
            {
                log("Unknown exemption type: '" + exe + "'");
            }
            return result;
        }
        public static string cleanLine(string line)
        {
            string result = line.Replace("\0", "");
            result = result.Replace("\"", "");
            result = result.Replace("*", "");
            result = result.Replace("|", "");
            Regex rgx = new Regex(@" +");
            result = rgx.Replace(result, " ");
            result = result.ToUpper();
            return result.Trim();
        }
        public static DateTime? fixDate(string inDate)
        {
            DateTime? result = null;
            int iD = 0;
            int iM = 0;
            int iY = 0;
            int iMyDate;
            string sMyDate = "";
            inDate = inDate.Trim();
                
            /*
            8 or 7 digits
            A YYYYMMDD
            B MMDDYYYY

            5 or 6 digits
            C MMDDYY

            Figure out what flavor the date is
            1. If <= 6 digits then it is C (pad to 6 digits)
            2. Pad to 8 digits
            3. Look at first 2 digits. If > 12 then it is A otherwise it is B 
             */
            try
            {
                if ((inDate.Length >= 5) && (int.TryParse(inDate, out iMyDate)) && (iMyDate > 0))
                {
                    if (inDate.Length <= 6)
                    { // MMDDYY
                        sMyDate = iMyDate.ToString("000000");
                        int.TryParse(sMyDate.Substring(0, 2), out iM);
                        int.TryParse(sMyDate.Substring(2, 2), out iD);
                        int.TryParse(sMyDate.Substring(4, 2), out iY);
                        int iThisYr;
                        int.TryParse(DateTime.Now.ToString("yy"), out iThisYr); // Get last 2 digits of current year
                        iY = (iY > iThisYr) ? iY + 1900 : iY + 2000;
                    }
                    else
                    {
                        sMyDate = iMyDate.ToString("00000000");
                        int iFirst2;
                        int.TryParse(sMyDate.Substring(0, 2), out iFirst2);
                        if (iFirst2 > 12)
                        { // YYYYMMDD
                            int.TryParse(sMyDate.Substring(4, 2), out iM);
                            int.TryParse(sMyDate.Substring(6, 2), out iD);
                            int.TryParse(sMyDate.Substring(0, 4), out iY);
                        }
                        else
                        { // MMDDYYYY
                            int.TryParse(sMyDate.Substring(0, 2), out iM);
                            int.TryParse(sMyDate.Substring(2, 2), out iD);
                            int.TryParse(sMyDate.Substring(4, 4), out iY);
                        }
                    }
                    iD = (iD == 0) ? 1 : iD;
                    result = DateTime.Parse(iM.ToString() + "/" + iD.ToString() + "/" + iY.ToString());
                }
            }
            catch
            {
            }

            return result;
        }
        public static string getStr(string line, fixfield field)
        {
            return line.Substring(field.Offset, field.Length).Trim();
        }
        public static byte[] getBufferSlice(byte[] line, fixfield field)
        {
            byte[] result = new byte[field.Length];

            for (int i = 0; i < field.Length; i++)
            {
                result[i] = line[field.Offset + i];
            }
            return result;
        }
        public static string addStrings(string in1, string in2, string in3, string in4)
        {
            return addStrings(addStrings(in1, in2, in3), in4);
        }
        public static string addStrings(string in1, string in2, string in3)
        {
            return addStrings(addStrings(in1, in2), in3);
        }
        public static string addStrings(string in1, string in2)
        {
            long result = 0;
            long lIn1, lIn2;

            long.TryParse(values.removeDecimal(in1), out lIn1);
            long.TryParse(values.removeDecimal(in2), out lIn2);
            result = lIn1 + lIn2;

            return result.ToString();
        }
        //public static string RE_care_of = @"C[/\\\.][0O]\.*\s*[:;]*\s*|ATTENTION[:;\-]*\s*|ATTE*N*\s*\.*[:;\-]*\s*|ATTE*[N\s:;\-\.]\s*|ATN[\s:;\-\.]+|R\.?E[:;\.]?\s+|REF[:;\.]\s+|@\s*|%\s*";
        public static string RE_care_of = @"C[/\\\.][0O]\.*\s*[:;]*\s*|ATTENTION[:;\-]*\s*|ATTN[:;\-\.]\s*|ATTE*[N\s:;\-\.]\s*|ATN[\s:;\-\.]+|R\.?E[:;\.]?\s+|REF[:;\.]\s+|@\s*|%\s*";
        public static string RE_dba = @"[\s\\/]*DBA\s*[:;,]*\s*";
        #endregion
    }
}
