		MailAddress1(38)	MailAddress2(39)	MailAddress3(40)	MailAddress4(41)
Done	ADDR				City State Zip
Done	DBA XXXXX			ADDR				City State Zip
Done	C/O XXXXX			ADDR				City State Zip
Done	C/O XXXXX			DBA XXXXX			ADDR				City State Zip
Done	ATTN: XXXXX			ADDR				City State Zip
Done	ATTN : XXXXX		ADDR				City State Zip
Done	DEPT XXXXX			DBA AAAAA			ADDR				City State Zip
Done	XXXXX				DBA AAAAA			ADDR				City State Zip		(XXXXX should be C/O)
Done	R.E. XXXXX			DBA AAAAA			ADDR				City State Zip
Done	XXXXX				ADDR				City State Zip							(XXXXX should be C/O)


Flavors:
DBA
DBA:
ATTN:
ATTN :
C/O
DEPT
XXXXX w/DBA (i.e. CV3105, TREVASKIS DEAN, BRIAN KING, DERVA LEWIS, SUTTER CREEK OFFICE)
XXXXX w/o DBA (i.e. TAX DEPARTMENT MS - 10FS-1, PROPERTY TAX DEPARTMENT, GEORGE MCELROY & ASSOCIATES)
R.E.

Funky Addresses:
100 N SEPULVEDA BLVD 12TH FLR, 2453 GRAND CANAL 2ND FLOOR (Floor examples)
165 LENON LANE, SUITE 200 (Suite example)
815 COURT ST STE 6 (Suite example)
20025 HWY 88 UNIT 4 (Unit example)
1701 S MILLS AVE APT 130 (Apartment example)
PO BOX 971 PMB 327, 11310 PROSPECT DR STE 10 PMB 199 (Post Mail Box examples)
2250 E. IMPERIAL HIGHWAY, CA/LA2/MS A130 (Mailstop example)
2000 CAMANCHE RD  SP 211, 2621 PRESCOTT RD SPC 139, 1400 W MARLETTE ST SPACE 87 (Space examples)
