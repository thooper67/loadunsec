﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text.RegularExpressions;

using LumenWorks.Framework.IO.Csv;

namespace LoadUnsec
{
    class Scr : County
    {
        public Scr()
        {
            base.co3 = "SCR";
            base.coNum = "44";
            populateStdTypes1();
            populateStdTypes2();
            populateCities();
        }

        #region Properties
        public int iFldAPN = 0;
        public int iFldROLL_YR = 2;
        public int iFldTYPE = 4;
        public int iFldLAND = 7;
        public int iFldIMPR = 8;
        public int iFldPERSPROP = 9;
        public int iFldOWNER = 15;
        public int iFldM_ADDR1 = 16;
        public int iFldM_ADDR2 = 17;
        public int iFldM_CITY = 19;
        public int iFldM_ST = 20;
        public int iFldM_ZIP = 21;
        public int iFldM_STR_NUM = 22;
        public int iFldM_DIR = 23;
        public int iFldM_STR_NAM = 24;
        public int iFldM_SFX = 25;
        public int iFldM_UNIT = 27;
        public int iFldM_UNIT2 = 28;
        public int iFldS_ADDR = 29;

        public int iFld2APN = 0;
        public int iFld2TRA = 2;
        List<fixfield> fields = new List<fixfield>();
        DataTable dtRoll = new DataTable();
        DataTable dtTRA = new DataTable();
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            string version = ConfigurationManager.AppSettings[co3 + "version"];
            if (version == "2014")
            {
                // Custom 2013 or later processing  **BE SURE TO REMOVE QUOTES FROM RAW FILES**
                log("Processing as delimited file.");
                int count = 0;
                int iTRACount = 0;
                StreamReader srInputFile = null;
                StreamWriter swOutFile = null;
                CsvReader csv1 = null;
                CsvReader csv2 = null;

                // open input file
                string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
                if (File.Exists(srcFile1))
                {
                    try
                    {
                        csv1 = new CsvReader(new StreamReader(srcFile1), true, '|');
                    }
                    catch (Exception e)
                    {
                        result = "Error opening input file '" + srcFile1 + "'. :" + e.Message;
                        log(result);
                    }
                }
                else
                {
                    result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                    log(result);
                    return result;
                }
                string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
                if (File.Exists(srcFile2))
                {
                    try
                    {
                        csv2 = new CsvReader(new StreamReader(srcFile2), true, '|');
                    }
                    catch (Exception e)
                    {
                        result = "Error opening input file '" + srcFile2 + "'. :" + e.Message;
                        log(result);
                    }
                }
                else
                {
                    result = co3 + " Source file '" + srcFile2 + "' does not exist.";
                    log(result);
                    return result;
                }

                // open output file
                try
                {
                    swOutFile = new StreamWriter(outFile, false);
                    log("Opened output file " + outFile);
                }
                catch (Exception e)
                {
                    result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                    log(result);
                }

                if (result == "") // Only work on the output file if there weren't any previous issues.
                {
                    // Write out header
                    unsecrec hdr = new unsecrec();
                    swOutFile.WriteLine(hdr.writeHeader());

                    DataTable dtRoll = getTable(csv1);
                    DataTable dtTRA = getTable(csv2);

                    // Loop through input
                    try
                    {
                        //while ((line = srInputFile.ReadLine()) != null)
                        foreach (DataRow fields in dtRoll.Rows)
                        {
                            count++;
                            unsecrec thisRec = new unsecrec();
                            thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];

                            // Keep reading value records until we find a match, or realize there isn't a match.
                            bool bTRAMatch = false;
                            string sRollAsmt = fields[iFldAPN].ToString();
                            string sTRAAsmt = dtTRA.Rows[iTRACount][iFld2APN].ToString();
                            while ((iTRACount < dtTRA.Rows.Count - 1) && (sRollAsmt.CompareTo(sTRAAsmt) > 0))
                            {
                                iTRACount++;
                                sTRAAsmt = dtTRA.Rows[iTRACount]["Pin"].ToString();
                            }
                            if (sRollAsmt.CompareTo(sTRAAsmt) == 0) bTRAMatch = true;

                            thisRec.APN_S = fields[iFldAPN].ToString();
                            thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                            thisRec.CO_NUM = coNum;
                            thisRec.CO_ID = co3;
                            thisRec.YRASSD = fields[iFldROLL_YR].ToString();
                            thisRec.TYPE = fields[iFldTYPE].ToString().ToUpper();
                            thisRec.TYPE_STD = xlatType(fields[iFldTYPE].ToString());
                            if (bTRAMatch)
                            {
                                string myTRA = dtTRA.Rows[iTRACount][iFld2TRA].ToString().Substring(0, 5);
                                thisRec.TRA = myTRA.PadLeft(6, '0');
                            }

                            thisRec.OWNER1 = cleanLine(fields[iFldOWNER].ToString());
                            string RE_care_of = @"C[/\\\.][0O]\.*\s*[:;]*\s*|ATT:\s+|ATTN[\s:]\s*";
                            Regex reCO = new Regex(@"(.+)(" + RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                            Match mCO = reCO.Match(fields[iFldOWNER].ToString());
                            if (reCO.IsMatch(fields[iFldOWNER].ToString()))
                            {
                                Group g1 = mCO.Groups[1];
                                Group g3 = mCO.Groups[3];
                                thisRec.OWNER1 = cleanLine(g1.ToString());
                                thisRec.CARE_OF = cleanLine(g3.ToString());
                            }

                            values myVals = new values();
                            myVals.LAND = fields[iFldLAND].ToString();
                            myVals.IMPR = fields[iFldIMPR].ToString();
                            myVals.PERSPROP = fields[iFldPERSPROP].ToString();
                            thisRec.LAND = myVals.LAND;
                            thisRec.IMPR = myVals.IMPR;
                            thisRec.PERSPROP = myVals.PERSPROP;
                            thisRec.GROSS = myVals.GROSS;

                            string myUnit = fields[iFldM_UNIT].ToString() + " " + fields[iFldM_UNIT2].ToString();
                            if (!string.IsNullOrEmpty(myUnit)) myUnit = "#" + myUnit;
                            string mail1 = fields[iFldM_STR_NUM].ToString() + " " + fields[iFldM_DIR].ToString() + " " + fields[iFldM_STR_NAM].ToString() + " " + fields[iFldM_SFX].ToString() + " " + myUnit;
                            string mail2 = fields[iFldM_CITY].ToString() + " " + fields[iFldM_ST].ToString() + " " + fields[iFldM_ZIP].ToString();
                            mailing myMail = new mailing(mail1, mail2, "", "");
                            thisRec.M_STRNUM = myMail.m_strnum;
                            thisRec.M_STR_SUB = myMail.m_str_sub;
                            thisRec.M_DIR = myMail.m_dir;
                            thisRec.M_STREET = myMail.m_street;
                            thisRec.M_SUFF = myMail.m_suff;
                            thisRec.M_UNITNO = myMail.m_unit_no;
                            thisRec.M_CITY = myMail.m_city;
                            thisRec.M_ST = myMail.m_st;
                            thisRec.M_ZIP = myMail.m_zip;
                            thisRec.M_ZIP4 = myMail.m_zip4;
                            thisRec.M_ADDR_D = cleanLine(fields[iFldM_ADDR1].ToString());
                            thisRec.M_CTY_ST_D = cleanLine(fields[iFldM_ADDR2].ToString());

                            if (!string.IsNullOrWhiteSpace(fields[iFldS_ADDR].ToString()))
                            {
                                situs mySitus = new situs("", fields[iFldS_ADDR].ToString());
                                string myLine2 = mySitus.s_st + " " + mySitus.s_zip + (string.IsNullOrEmpty(mySitus.s_zip4) ? "" : "-" + mySitus.s_zip4);
                                string myLine1 = mySitus.s_city;
                                bool foundCity = false;

                                foreach (string key in stdCity.Keys)
                                {
                                    Regex re = new Regex("(.*)" + key + "$", RegexOptions.IgnoreCase);
                                    if (re.IsMatch(myLine1))
                                    {
                                        Match m = re.Match(myLine1);
                                        myLine2 = stdCity[key] + " " + myLine2;
                                        myLine1 = m.Groups[1].ToString();
                                        foundCity = true;
                                        break;
                                    }
                                }
                                if (!foundCity)
                                {
                                    log("City not found for (line " + count + "): " + fields[iFldS_ADDR].ToString());
                                }
                                mySitus = new situs(myLine1, myLine2);
                                thisRec.S_HSENO = mySitus.s_hseno;
                                thisRec.S_STRNUM = mySitus.s_strnum;
                                thisRec.S_STR_SUB = mySitus.s_str_sub;
                                thisRec.S_DIR = mySitus.s_dir;
                                thisRec.S_STREET = mySitus.s_street;
                                thisRec.S_SUFF = mySitus.s_suff;
                                thisRec.S_UNITNO = mySitus.s_unit_no;
                                thisRec.S_CITY = mySitus.s_city;
                                thisRec.S_ST = mySitus.s_st;
                                thisRec.S_ZIP = mySitus.s_zip;
                                thisRec.S_ZIP4 = mySitus.s_zip4;
                                thisRec.S_ADDR_D = mySitus.s_addr_d;
                                thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;
                            }

                            swOutFile.WriteLine(thisRec.writeOutput());
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Error from sorted input line " + count.ToString() + " : " + e.Message;
                        log(result);
                    }
                }

                // close input file
                if (srInputFile != null) srInputFile.Close();

                // close output file
                if (swOutFile != null)
                {
                    swOutFile.Flush();
                    swOutFile.Close();
                }
            }
            else
            {
                string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
                int srcFile1RecLen = 700;
                long srcFileLength = 0;
                initialize();

                StreamWriter swOutFile = null;
                int count = 0;

                #region srcFile1
                if (File.Exists(srcFile1))
                {
                    try
                    {
                        FileInfo f = new FileInfo(srcFile1);
                        srcFileLength = f.Length;
                        using (MemoryMappedFile mmf = MemoryMappedFile.CreateFromFile(srcFile1, FileMode.Open))
                        {
                            using (MemoryMappedViewAccessor va = mmf.CreateViewAccessor(0, srcFileLength))
                            {
                                long i = 0;
                                while (i * srcFile1RecLen < srcFileLength)
                                {
                                    long offset = i * srcFile1RecLen;
                                    byte[] buffer = new byte[srcFile1RecLen];
                                    va.ReadArray(offset, buffer, 0, srcFile1RecLen);
                                    string myRec = System.Text.Encoding.ASCII.GetString(buffer);

                                    DataRow dr = dtRoll.NewRow();
                                    foreach (fixfield thisField in fields)
                                    {
                                        switch (thisField.Format)
                                        {
                                            case "PD":
                                                dr[thisField.Name] = ebcdic.ConvertPd2Str(getBufferSlice(buffer, thisField));
                                                break;
                                            default:
                                                dr[thisField.Name] = getStr(myRec, thisField);
                                                break;
                                        }
                                    }
                                    dtRoll.Rows.Add(dr);
                                    i++;
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        result = "Error opening source file1: '" + srcFile1 + "'. " + e.Message;
                        return result;
                    }

                }
                else
                {
                    result = co3 + " Source file '" + srcFile1 + "' does not exist.";
                    log(result);
                    return result;
                }
                #endregion

                // open output file
                try
                {
                    swOutFile = new StreamWriter(outFile, false);
                    log("Opened output file " + outFile);
                }
                catch (Exception e)
                {
                    result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                    log(result);
                }

                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    dtRoll.DefaultView.Sort = "APN";
                    DataView view = dtRoll.DefaultView;
                    DataTable dtRollSort = view.ToTable();
                    count = 0;
                    foreach (DataRow drRoll in dtRollSort.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.APN_D_format = ConfigurationManager.AppSettings[co3 + "apnFormat"];
                        if (drRoll["FEE_PCL"].ToString().Length == 8)
                        {
                            thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat1"];
                        }
                        else if (drRoll["FEE_PCL"].ToString().Length == 9)
                        {
                            thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat2"];
                        }
                        else if (drRoll["FEE_PCL"].ToString().Length == 10)
                        {
                            thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat3"];
                        }
                        else if (drRoll["FEE_PCL"].ToString().Length == 11)
                        {
                            thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat4"];
                        }

                        thisRec.APN_S = drRoll["APN"].ToString();
                        thisRec.APN_D = County.formattedNumber(thisRec.APN_S, thisRec.APN_D_format);
                        thisRec.FEE_PARCEL_S = drRoll["FEE_PCL"].ToString();
                        thisRec.FEE_PARCEL_D = County.formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = coNum;
                        thisRec.CO_ID = co3;
                        thisRec.YRASSD = "20" + drRoll["ROLL_YR"].ToString();
                        thisRec.TRA = drRoll["TRA"].ToString().PadLeft(6, '0');
                        thisRec.TYPE = drRoll["TYPE"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.EXE_CD = drRoll["EXE_CD"].ToString();

                        string myOwner = cleanLine(drRoll["ASSESSEE"].ToString() + " " + drRoll["DBA2"].ToString());
                        string RE_care_of = @"C[/\\\.][0O]\.*\s*[:;]*\s*|ATT:\s+|ATTN[\s:]\s*";
                        Regex reCO = new Regex(@"(.+)(" + RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mCO = reCO.Match(myOwner);
                        if (reCO.IsMatch(myOwner))
                        {
                            Group g1 = mCO.Groups[1];
                            Group g3 = mCO.Groups[3];
                            thisRec.OWNER1 = cleanLine(g1.ToString());
                            thisRec.ASSESSEE = thisRec.OWNER1;
                            thisRec.DBA = thisRec.OWNER1;
                            thisRec.CARE_OF = cleanLine(g3.ToString());
                        }
                        else
                        {
                            thisRec.OWNER1 = myOwner;
                            thisRec.ASSESSEE = thisRec.OWNER1;
                            thisRec.DBA = thisRec.OWNER1;
                        }

                        reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        mCO = reCO.Match(drRoll["OWNER2"].ToString());
                        if (reCO.IsMatch(drRoll["OWNER2"].ToString()))
                        {
                            Group gCO = mCO.Groups[2];
                            thisRec.CARE_OF = cleanLine(gCO.ToString());
                        }
                        else
                        {
                            thisRec.OWNER2 = cleanLine(drRoll["OWNER2"].ToString());
                        }

                        values myVals = new values();
                        myVals.LAND = drRoll["LAND"].ToString();
                        myVals.IMPR = drRoll["IMPR"].ToString();
                        myVals.FIXTR = drRoll["FIXTR"].ToString();
                        myVals.PERSPROP = drRoll["PERSPROP"].ToString();
                        myVals.EXE_AMT = drRoll["EXE_AMT"].ToString();
                        thisRec.LAND = myVals.LAND;
                        thisRec.IMPR = myVals.IMPR;
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        mailing myMail = new mailing(drRoll["M_STR"].ToString(), drRoll["M_CITY"].ToString() + " " + drRoll["M_ST"].ToString() + " " + drRoll["M_ZIP"].ToString(), "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string myStreet = drRoll["S_STR"].ToString();
                        string myCity = drRoll["S_CITY"].ToString();
                        if (myCity.Length < 4)
                        {
                            if (myCity == "SC")
                            {
                                myCity = "SANTA CRUZ";
                            }
                            else if (myCity == "SV")
                            {
                                myCity = "SCOTTS VALLEY";
                            }
                            else
                            {
                                myStreet += myCity;
                                myCity = "";
                            }
                        }
                        situs mySitus = new situs(myStreet, myCity + " CA");
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    log("Error in record: " + count + ". " + e.Message);
                }

                // close output file
                if (swOutFile != null)
                {
                    swOutFile.Flush();
                    swOutFile.Close();
                }
            }
            return result;
        }
        private DataTable getTable(CsvReader csv)
        {
            DataTable table = new DataTable();
            foreach (string col in csv.GetFieldHeaders())
            {
                table.Columns.Add(col, typeof(string));
            }
            while (csv.ReadNextRecord())
            {
                    DataRow dr = table.NewRow();
                    for (int i = 0; i < csv.FieldCount; i++)
                    {
                        dr[i] = csv[i];
                    }
                    table.Rows.Add(dr);
            }
            table.DefaultView.Sort = csv.GetFieldHeaders()[0];
            DataView view = table.DefaultView;

            return view.ToTable();
        }
        private void initialize()
        {
            //                     Length, Position, Name
            fields.Add(new fixfield(5, 0, "TRA"));
            fields.Add(new fixfield(8, 5, "APN"));
            fields.Add(new fixfield(2, 16, "ROLL_YR"));
            fields.Add(new fixfield(6, 28, "LAND", "PD"));
            fields.Add(new fixfield(6, 34, "IMPR", "PD"));
            fields.Add(new fixfield(6, 40, "PERSPROP", "PD"));
            fields.Add(new fixfield(6, 46, "FIXTR", "PD"));
            fields.Add(new fixfield(6, 52, "EXE_AMT", "PD"));
            fields.Add(new fixfield(4, 58, "EXE_CD"));
            fields.Add(new fixfield(11, 118, "FEE_PCL"));
            fields.Add(new fixfield(1, 314, "TYPE"));
            fields.Add(new fixfield(40, 396, "ASSESSEE"));
            fields.Add(new fixfield(40, 436, "OWNER2"));
            fields.Add(new fixfield(28, 476, "M_STR"));
            fields.Add(new fixfield(25, 504, "M_CITY"));
            fields.Add(new fixfield(2, 529, "M_ST"));
            fields.Add(new fixfield(5, 531, "M_ZIP"));
            fields.Add(new fixfield(23, 578, "DBA2"));
            fields.Add(new fixfield(27, 607, "S_STR"));
            fields.Add(new fixfield(13, 634, "S_CITY"));
            foreach (fixfield field in fields)
            {
                dtRoll.Columns.Add(field.Name, typeof(string));
            }
        }
        #endregion

        private void populateStdTypes1()
        {
            stdTypes.Add("Aircraft", "300");
            stdTypes.Add("BusinessPP", "101");
            stdTypes.Add("LocalReal", "400");
            stdTypes.Add("Vessel", "200");
        }
        private void populateStdTypes2()
        {
            stdTypes.Add("A", "502");
            stdTypes.Add("B", "100");
            stdTypes.Add("F", "106");
            stdTypes.Add("G", "400");
            stdTypes.Add("L", "103");
            stdTypes.Add("M", "501");
            stdTypes.Add("P", "300");
            stdTypes.Add("Q", "100");
            stdTypes.Add("R", "501");
            stdTypes.Add("S", "100");
            stdTypes.Add("U", "501");
            stdTypes.Add("V", "200");
        }
        private void populateCities()
        {
            stdCity.Add("APTOS", "APTOS");
            stdCity.Add("BEN LOMOND", "BEN LOMOND");
            stdCity.Add("BONNY DOON", "BONNY DOON");
            stdCity.Add("BOULDER CREEK", "BOULDER CREEK");
            stdCity.Add("BOULDER CRIK", "BOULDER CREEK");
            stdCity.Add("BROOKDALE", "BROOKDALE");
            stdCity.Add("CAPITOLA", "CAPITOLA");
            stdCity.Add("CORRALITOS", "CORRALITOS");
            stdCity.Add("DAVENPORT", "DAVENPORT");
            stdCity.Add("FELTON", "FELTON");
            stdCity.Add("FREEDOM", "FREEDOM");
            stdCity.Add("FREEOM", "FREEDOM");
            stdCity.Add("LA SELVA BCH", "LA SELVA BEACH");
            stdCity.Add("LA SELVA BEACH", "LA SELVA BEACH");
            stdCity.Add("LOS GATOS", "LOS GATOS");
            stdCity.Add("MOUNT HERMON", "MOUNT HERMON");
            stdCity.Add("MT HERMON", "MOUNT HERMON");
            stdCity.Add("PAJARO DUNES", "PAJARO DUNES");
            stdCity.Add("PARADISE PARK", "PARADISE PARK");
            stdCity.Add("SANTA CRUZ", "SANTA CRUZ");
            stdCity.Add("SANTA CURZ", "SANTA CRUZ");
            stdCity.Add("SNATA CRUZ", "SANTA CRUZ");
            stdCity.Add("SC", "SANTA CRUZ");
            stdCity.Add("SCOTTS VALLEY", "SCOTTS VALLEY");
            stdCity.Add(" SV", "SCOTTS VALLEY");
            stdCity.Add("SOQUEL", "SOQUEL");
            stdCity.Add("WATSONVILLE", "WATSONVILLE");
            stdCity.Add("WATSOVNILLE", "WATSONVILLE");

        }
    }
}
