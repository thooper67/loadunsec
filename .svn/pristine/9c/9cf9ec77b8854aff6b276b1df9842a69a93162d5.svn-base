﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LoadUnsec
{
    class Sdx : County
    {
        public Sdx()
        {
            base.co3 = "SDX";
            base.coNum = "37";
            populateStdTypes();
        }

        #region Properties
        List<string> fields = new List<string>();
        List<fixfield> fields1 = new List<fixfield>();
        List<fixfield> fields2 = new List<fixfield>();
        List<fixfield> fields3 = new List<fixfield>();
        #endregion

        #region Methods
        public override string process()
        {
            string result = "";
            int count = 0;
            initialize();

            StreamReader srInputFile1 = null;
            StreamReader srInputFile2 = null;
            StreamReader srInputFile3 = null;
            StreamWriter swOutFile = null;

            // open input files
            string srcFile1 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file1"]);
            string srcFile2 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file2"]);
            string srcFile3 = Path.Combine(srcFolder, ConfigurationManager.AppSettings[co3 + "file3"]);
            srInputFile1 = openStreamReader(srcFile1);
            srInputFile2 = openStreamReader(srcFile2);
            srInputFile3 = openStreamReader(srcFile3);
            if ((srcFile1 == null) || (srcFile2 == null) || (srcFile3 == null)) result = "Problem with source file.";

            // open output file
            try
            {
                swOutFile = new StreamWriter(outFile, false);
                log("Opened output file " + outFile);
            }
            catch (Exception e)
            {
                result = "There was a problem opening the output file '" + outFile + "'. " + e.Message;
                log(result);
            }

            if (result == "") // Only work on the output file if there weren't any previous issues.
            {
                // Write out header
                unsecrec hdr = new unsecrec();
                swOutFile.WriteLine(hdr.writeHeader());

                // Loop through input
                try
                {
                    // Get input data
                    DataTable table = new DataTable();
                    foreach (string fieldName in fields)
                    {
                        table.Columns.Add(fieldName, typeof(string));
                    }

                    table = fillDataTable(srInputFile1, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file1Length"]), fields1);
                    table = fillDataTable(srInputFile2, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file2Length"]), fields2);
                    table = fillDataTable(srInputFile3, table, int.Parse(ConfigurationManager.AppSettings[co3 + "file3Length"]), fields3);

                    // close input file
                    if (srInputFile1 != null) srInputFile1.Close();
                    if (srInputFile2 != null) srInputFile2.Close();
                    if (srInputFile3 != null) srInputFile3.Close();
                    table.DefaultView.Sort = "ACCOUNT";
                    DataView view = table.DefaultView;
                    DataTable dtRoll = view.ToTable();

                    count = 0;
                    foreach (DataRow drRoll in dtRoll.Rows)
                    {
                        count++;
                        unsecrec thisRec = new unsecrec();
                        thisRec.FEE_PARCEL_D_format = ConfigurationManager.AppSettings[co3 + "feePclFormat"];
                        thisRec.APN_S = drRoll["ACCOUNT"].ToString();
                        thisRec.APN_D = thisRec.APN_S;
                        thisRec.FEE_PARCEL_S = drRoll["MPBKPG"].ToString();
                        thisRec.FEE_PARCEL_D = formattedNumber(thisRec.FEE_PARCEL_S, thisRec.FEE_PARCEL_D_format);
                        thisRec.CO_NUM = base.coNum;
                        thisRec.CO_ID = base.co3;
                        thisRec.YRASSD = drRoll["RCYEAR"].ToString();
                        thisRec.TRA = drRoll["TRA"].ToString().PadLeft(6, '0');
                        thisRec.LEGAL = cleanLine(drRoll["MANUFACTR"].ToString() + " " + drRoll["MANMOD"].ToString() + " " + drRoll["MANMAK"].ToString() + " " + drRoll["MANYR"].ToString());
                        thisRec.TYPE = drRoll["RECTYP"].ToString();
                        thisRec.TYPE_STD = xlatType(thisRec.TYPE);
                        thisRec.EXE_CD = drRoll["TABEXM"].ToString();
                        thisRec.BILL_NUM = drRoll["BILLNUM"].ToString();

                        thisRec.OWNER1 = cleanLine(drRoll["OWNNAM1"].ToString());
                        // Concat O2 w/ O3; pull out DBA & C/O
                        string myOwner2 = drRoll["OWNNAM2"].ToString() + " " + drRoll["OWNNAM3"].ToString();

                        while (true)
                        {
                            // Find a dba NOT at the end
                            // If concat contains dba split on dba, 1 -> o2, 2-> dba
                            Regex re = new Regex(@"(.+)" + County.RE_dba + "(.+)$", RegexOptions.IgnoreCase);
                            Match m1 = re.Match(myOwner2);
                            if (m1.Success)
                            {
                                Group g1 = m1.Groups[1];
                                Group g2 = m1.Groups[2];
                                thisRec.OWNER2 = cleanLine(g1.ToString());
                                thisRec.DBA = cleanLine(Regex.Replace(g2.ToString(), County.RE_dba + "$", "", RegexOptions.IgnoreCase));
                                break;
                            }

                            // If concat starts w/ dba: Remove dba and result -> dba
                            re = new Regex(@"^" + County.RE_dba + "(.+)", RegexOptions.IgnoreCase);
                            m1 = re.Match(myOwner2.Trim());
                            if (m1.Success)
                            {
                                Group g1 = m1.Groups[1];
                                thisRec.DBA = cleanLine(g1.ToString());
                                break;
                            }

                            thisRec.OWNER2 = cleanLine(myOwner2);
                            break;
                        }
                        Regex reCO = new Regex(@"^(" + County.RE_care_of + ")(.+)$", RegexOptions.IgnoreCase);
                        Match mCO = reCO.Match(drRoll["CAREOF"].ToString());
                        if (mCO.Success)
                        {
                            Group gCO = mCO.Groups[2];
                            thisRec.CARE_OF = cleanLine(gCO.ToString());
                        }
                        else
                        {
                            thisRec.CARE_OF = cleanLine(drRoll["CAREOF"].ToString());
                        }
                        
                        values myVals = new values();
                        myVals.FIXTR = drRoll["FIX1"].ToString();
                        myVals.PERSPROP = drRoll["PERSPROP"].ToString();
                        myVals.EXE_AMT = drRoll["EXMAMT"].ToString();
                        thisRec.FIXTR = myVals.FIXTR;
                        thisRec.PERSPROP = myVals.PERSPROP;
                        thisRec.GROSS = myVals.GROSS;
                        thisRec.EXE_AMT = myVals.EXE_AMT;

                        string mail1 = "";
                        Regex reMailNum = new Regex(@"^\s*0\s*$", RegexOptions.IgnoreCase);
                        if (!reMailNum.IsMatch(drRoll["STRNUM"].ToString()))
                        {
                            mail1 = drRoll["STRNUM"].ToString() + " " + drRoll["STRFRA"].ToString() + " " + drRoll["STRDIR"].ToString() + " ";
                        }
                        mail1 = mail1 + drRoll["STRNAM"].ToString() + " " + drRoll["STRTYP"].ToString() + " " + drRoll["STRSRM"].ToString();
                        string mail2 = drRoll["CITY"].ToString() + " " + drRoll["STATE"].ToString() + " " + drRoll["ZIPCD1"].ToString();
                        mailing myMail = new mailing(mail1, mail2, "", "");
                        thisRec.M_STRNUM = myMail.m_strnum;
                        thisRec.M_STR_SUB = myMail.m_str_sub;
                        thisRec.M_DIR = myMail.m_dir;
                        thisRec.M_STREET = myMail.m_street;
                        thisRec.M_SUFF = myMail.m_suff;
                        thisRec.M_UNITNO = myMail.m_unit_no;
                        thisRec.M_CITY = myMail.m_city;
                        thisRec.M_ST = myMail.m_st;
                        thisRec.M_ZIP = myMail.m_zip;
                        thisRec.M_ZIP4 = myMail.m_zip4;
                        thisRec.M_ADDR_D = myMail.m_addr_d;
                        thisRec.M_CTY_ST_D = myMail.m_cty_st_d;

                        string situs1 = "";
                        Regex reSitNum = new Regex(@"^\s*0\s*$", RegexOptions.IgnoreCase);
                        if (!reSitNum.IsMatch(drRoll["LOCNUM"].ToString()))
                        {
                            situs1 = drRoll["LOCNUM"].ToString() + " " + drRoll["LOCFRA"].ToString() + " " + drRoll["LOCDIR"].ToString() + " ";
                        }
                        situs1 = situs1 + drRoll["LOCNAM"].ToString() + " " + drRoll["LOCTYP"].ToString() + " " + drRoll["LOCSRM"].ToString();
                        string situs2 = drRoll["CITY"].ToString();
                        situs mySitus = new situs(situs1, situs2);
                        thisRec.S_HSENO = mySitus.s_hseno;
                        thisRec.S_STRNUM = mySitus.s_strnum;
                        thisRec.S_STR_SUB = mySitus.s_str_sub;
                        thisRec.S_DIR = mySitus.s_dir;
                        thisRec.S_STREET = mySitus.s_street;
                        thisRec.S_SUFF = mySitus.s_suff;
                        thisRec.S_UNITNO = mySitus.s_unit_no;
                        thisRec.S_CITY = mySitus.s_city;
                        thisRec.S_ST = mySitus.s_st;
                        thisRec.S_ZIP = mySitus.s_zip;
                        thisRec.S_ZIP4 = mySitus.s_zip4;
                        thisRec.S_ADDR_D = mySitus.s_addr_d;
                        thisRec.S_CTY_ST_D = mySitus.s_cty_st_d;

                        swOutFile.WriteLine(thisRec.writeOutput());
                    }
                }
                catch (Exception e)
                {
                    result = "Error from input line " + count.ToString() + " : " + e.Message;
                    log(result);
                }
            }
            // close output file
            if (swOutFile != null)
            {
                swOutFile.Flush();
                swOutFile.Close();
            }
            return result;
        }
        private void initialize()
        {
            fields.Add("MPBKPG");
            fields.Add("ACCOUNT");
            fields.Add("BILLNUM");
            fields.Add("RCYEAR");
            fields.Add("RECTYP");
            fields.Add("OWNNAM1");
            fields.Add("OWNNAM2");
            fields.Add("OWNNAM3");
            fields.Add("CAREOF");
            fields.Add("STRNUM");
            fields.Add("STRFRA");
            fields.Add("STRDIR");
            fields.Add("STRNAM");
            fields.Add("STRTYP");
            fields.Add("STRSRM");
            fields.Add("CITY");
            fields.Add("STATE");
            fields.Add("ZIPCD1");
            fields.Add("LOCFRA");
            fields.Add("LOCDIR");
            fields.Add("LOCTYP");
            fields.Add("LOCSRM");
            fields.Add("LOCCIT");
            fields.Add("LOCNAM");
            fields.Add("LOCNUM");
            fields.Add("TRA");
            fields.Add("TABEXM");
            fields.Add("EXMAMT");
            fields.Add("PERSPROP");
            fields.Add("FIX1");
            fields.Add("MANUFACTR");
            fields.Add("MANMOD");
            fields.Add("MANMAK");
            fields.Add("MANYR");

            //                     Length, Position, Name
            fields1.Add(new fixfield(10, 0, "MPBKPG"));
            fields1.Add(new fixfield(20, 10, "ACCOUNT"));
            fields1.Add(new fixfield(10, 31, "BILLNUM"));
            fields1.Add(new fixfield(4, 42, "RCYEAR"));
            fields1.Add(new fixfield(3, 46, "RECTYP"));
            fields1.Add(new fixfield(40, 49, "OWNNAM1"));
            fields1.Add(new fixfield(40, 89, "OWNNAM2"));
            fields1.Add(new fixfield(13, 129, "STRNUM"));
            fields1.Add(new fixfield(3, 142, "STRFRA"));
            fields1.Add(new fixfield(3, 145, "STRDIR"));
            fields1.Add(new fixfield(32, 148, "STRNAM"));
            fields1.Add(new fixfield(4, 180, "STRTYP"));
            fields1.Add(new fixfield(8, 184, "STRSRM"));
            fields1.Add(new fixfield(30, 192, "CITY"));
            fields1.Add(new fixfield(2, 222, "STATE"));
            fields1.Add(new fixfield(5, 239, "ZIPCD1"));
            fields1.Add(new fixfield(3, 244, "LOCFRA"));
            fields1.Add(new fixfield(3, 247, "LOCDIR"));
            fields1.Add(new fixfield(4, 250, "LOCTYP"));
            fields1.Add(new fixfield(8, 254, "LOCSRM"));
            fields1.Add(new fixfield(30, 262, "LOCCIT"));
            fields1.Add(new fixfield(32, 292, "LOCNAM"));
            fields1.Add(new fixfield(13, 324, "LOCNUM"));
            fields1.Add(new fixfield(5, 337, "TRA"));
            fields1.Add(new fixfield(13, 360, "EXMAMT"));
            fields1.Add(new fixfield(13, 373, "PERSPROP"));
            fields1.Add(new fixfield(30, 426, "MANMOD"));
            fields1.Add(new fixfield(30, 456, "MANMAK"));
            fields1.Add(new fixfield(4, 487, "MANYR"));

            fields2.Add(new fixfield(10, 0, "MPBKPG"));
            fields2.Add(new fixfield(20, 10, "ACCOUNT"));
            fields2.Add(new fixfield(10, 31, "BILLNUM"));
            fields2.Add(new fixfield(4, 42, "RCYEAR"));
            fields2.Add(new fixfield(3, 46, "RECTYP"));
            fields2.Add(new fixfield(40, 49, "OWNNAM1"));
            fields2.Add(new fixfield(40, 89, "OWNNAM2"));
            fields2.Add(new fixfield(40, 129, "OWNNAM3"));
            fields2.Add(new fixfield(40, 169, "CAREOF"));
            fields2.Add(new fixfield(13, 209, "STRNUM"));
            fields2.Add(new fixfield(3, 222, "STRFRA"));
            fields2.Add(new fixfield(3, 225, "STRDIR"));
            fields2.Add(new fixfield(32, 228, "STRNAM"));
            fields2.Add(new fixfield(4, 260, "STRTYP"));
            fields2.Add(new fixfield(8, 264, "STRSRM"));
            fields2.Add(new fixfield(30, 272, "CITY"));
            fields2.Add(new fixfield(2, 302, "STATE"));
            fields2.Add(new fixfield(5, 319, "ZIPCD1"));
            fields2.Add(new fixfield(3, 324, "LOCFRA"));
            fields2.Add(new fixfield(3, 327, "LOCDIR"));
            fields2.Add(new fixfield(4, 330, "LOCTYP"));
            fields2.Add(new fixfield(8, 334, "LOCSRM"));
            fields2.Add(new fixfield(30, 342, "LOCCIT"));
            fields2.Add(new fixfield(32, 372, "LOCNAM"));
            fields2.Add(new fixfield(13, 404, "LOCNUM"));
            fields2.Add(new fixfield(5, 417, "TRA"));
            fields2.Add(new fixfield(13, 440, "EXMAMT"));
            fields2.Add(new fixfield(13, 453, "PERSPROP"));
            fields2.Add(new fixfield(30, 574, "MANUFACTR"));
            fields2.Add(new fixfield(30, 604, "MANMOD"));
            fields2.Add(new fixfield(30, 634, "MANMAK"));
            fields2.Add(new fixfield(4, 665, "MANYR"));

            fields3.Add(new fixfield(10, 0, "MPBKPG"));
            fields3.Add(new fixfield(20, 10, "ACCOUNT"));
            fields3.Add(new fixfield(10, 31, "BILLNUM"));
            fields3.Add(new fixfield(4, 42, "RCYEAR"));
            fields3.Add(new fixfield(3, 46, "RECTYP"));
            fields3.Add(new fixfield(40, 49, "OWNNAM1"));
            fields3.Add(new fixfield(40, 89, "OWNNAM2"));
            fields3.Add(new fixfield(40, 129, "OWNNAM3"));
            fields3.Add(new fixfield(40, 169, "CAREOF"));
            fields3.Add(new fixfield(13, 209, "STRNUM"));
            fields3.Add(new fixfield(3, 222, "STRFRA"));
            fields3.Add(new fixfield(3, 225, "STRDIR"));
            fields3.Add(new fixfield(32, 228, "STRNAM"));
            fields3.Add(new fixfield(4, 260, "STRTYP"));
            fields3.Add(new fixfield(8, 264, "STRSRM"));
            fields3.Add(new fixfield(30, 272, "CITY"));
            fields3.Add(new fixfield(2, 302, "STATE"));
            fields3.Add(new fixfield(5, 319, "ZIPCD1"));
            fields3.Add(new fixfield(3, 324, "LOCFRA"));
            fields3.Add(new fixfield(3, 327, "LOCDIR"));
            fields3.Add(new fixfield(4, 330, "LOCTYP"));
            fields3.Add(new fixfield(8, 334, "LOCSRM"));
            fields3.Add(new fixfield(30, 342, "LOCCIT"));
            fields3.Add(new fixfield(32, 372, "LOCNAM"));
            fields3.Add(new fixfield(13, 404, "LOCNUM"));
            fields3.Add(new fixfield(5, 417, "TRA"));
            fields3.Add(new fixfield(3, 427, "TABEXM"));
            fields3.Add(new fixfield(13, 430, "EXMAMT"));
            fields3.Add(new fixfield(10, 443, "PERSPROP"));
            fields3.Add(new fixfield(10, 453, "FIX1"));
        }
        #endregion

        private void populateStdTypes()
        {
            stdTypes.Add("AAU", "300");
            stdTypes.Add("ACA", "300");
            stdTypes.Add("ADB", "200");
            stdTypes.Add("AG", "502");
            stdTypes.Add("AGA", "502");
            stdTypes.Add("AGL", "502");
            stdTypes.Add("AGM", "502");
            stdTypes.Add("AHQ", "100");
            stdTypes.Add("APA", "100");
            stdTypes.Add("APM", "100");
            stdTypes.Add("APT", "100");
            stdTypes.Add("AUD", "100");
            stdTypes.Add("BBS", "100");
            stdTypes.Add("BBV", "100");
            stdTypes.Add("BBY", "103");
            stdTypes.Add("BL", "100");
            stdTypes.Add("BU", "100");
            stdTypes.Add("DBA", "200");
            stdTypes.Add("DBI", "200");
            stdTypes.Add("DBM", "200");
            stdTypes.Add("DDB", "200");
            stdTypes.Add("HS", "502");
            stdTypes.Add("HSA", "502");
            stdTypes.Add("K", "500");
            stdTypes.Add("LM", "103");
            stdTypes.Add("LMA", "103");
            stdTypes.Add("OBA", "200");
            stdTypes.Add("ODB", "200");
            stdTypes.Add("RLL", "107");
            stdTypes.Add("S", "100");
            stdTypes.Add("SBA", "200");
            stdTypes.Add("SBI", "200");
            stdTypes.Add("SBM", "200");
            stdTypes.Add("SBZ", "200");
            stdTypes.Add("SDB", "200");
            stdTypes.Add("SOS", "200");
            stdTypes.Add("V", "100");
            stdTypes.Add("VA", "100");
            stdTypes.Add("XC", "504");
            stdTypes.Add("XCA", "504");
            stdTypes.Add("XS", "504");
            stdTypes.Add("XSA", "504");
            stdTypes.Add("XW", "504");
            stdTypes.Add("XWA", "504");
            stdTypes.Add("XWU", "504");
        }
    }
}
